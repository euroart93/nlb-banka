<?php
Site::getHeader('header-login'); ?>

<div class="login-main -prelogin-autoheight">

	<!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
	<div class="page-cover inner-video video-cover img-overlay-blur">
		<img class="desktop-img promo-bg-0 slide-active" src="img/bg/login-bg-blur.png" alt="cover" data-object-fit="cover">
	</div>

	<div class="prelogin-full -prelogin-autoheight">
		<div class="inner">
			<a href="#" type="button" class="close-box">Zatvori</a>
			<div class="col-left">
				<h2>Kontakti</h2>
				<p>Ako želite da posetite neku od ekspozitura ili pronađete najbliži bankomat NLB Banke.</p>
				<div class="button-actions">
					<button type="submit" class="btn-1 color-1">Pronađi NLB</button>
					<button type="submit" class="btn-1 color-1">Zakažite Sastanak</button>
				</div>
			</div>
			<div class="col-right -no-col-head">	
				<div class="bank-title-sample-wrapper">
					<h2 class="bank-sample-title">Želite da kontaktirate NLB Banku?</h2>
				</div>
				<div class="contact-data-wrapper">
					<div class="half">
						<div class="data">
							<i class="globe-icon"></i>
							<p><span>Web stranica:</span> <a href="#" target="_blank">www.nlb.rs</a></p>
						</div>
						<div class="data">
							<i class="mail-icon"></i>
							<p><span>E-mail adresa:</span> <a href="mailto:info@nlb.rs">info@nlb.rs</a></p>
						</div>
						<div class="data">
							<i class="phone-icon"></i>
							<p><span>Kontakt telefon:</span>011 7 15 15 22</p>
						</div>
						<div class="data">
							<i class="pin-icon"></i>
							<p><span>Adresa:</span>Bulevar Mihajla Pupina 165v, 11000 Beograd</p>
						</div>
						<div class="data">
							<i class="facebook-icon"></i>
							<p><span>Facebook:</span><a href="#" target="_blank">facebook.com/NLB.Banka</a></p>
						</div>
						<div class="data">
							<i class="linkedin-icon"></i>
							<p><span>Linkedin:</span><a href="#" target="_blank">linkedin.com/company/nlb-banka</a></p>
						</div>
						<div class="data">
							<i class="youtube-icon"></i>
							<p><span>Youtube:</span><a href="#" target="_blank">youtube.com/user/NLBBankaBeograd</a></p>
						</div>
					</div>

					<div class="half">
						<div class="map-wrapper">
							<div class="infobox-wrapper">
								<div id="infobox1" class="infobox bg-atm">
							        <p class="loc-title">Bank</p>
							        <p class="loc-info">Bulevar Mihajla Pupina 165v<br>
							        	Beograd
							        </p>
							    </div>
							</div>
							<div id="map-canvas-small" data-method="googleMapContact"></div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="login-footer -prelogin-autoheight">
		<div class="inner">
			<!--<ul class="lang-nav">
				<li class="active"><a href="#">ENG</a></li>
				<li><a href="#">SRB</a></li>
			</ul>-->
			<ul class="social">
				<li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
				<li class="youtube"><a href="#" class="icon-youtube">Youtube</a></li>
				<li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
				<li class="history"><a href="#" class="icon-history">History</a></li>
			</ul>
		</div>
	</div>


</div>


</div><!-- /Wrapper -->
