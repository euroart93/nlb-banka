<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="main-content">
		<div class="widget">
			<ul class="widget-actions widget-corner">
				<li>
					<div class="widget-options" data-method="openSideModal" data-filter-node="#transactions-filter-2">
            			<ul class="trigger color-1">
            				<li></li>
            				<li></li>
            				<li></li>
            			</ul>
            		</div>
				</li>
			</ul>

			<div class="container top-padding">
				<h2><i class="icon-money-3"></i>Bill presentment</h2>
				<ul class="widget-tabs style-2">
					<li><a href="<?php echo Site::url() ?>/bill-presentment-subscribed-bills"">Subscribed Bills</a></li>
					<li class="active"><a href="<?php echo Site::url() ?>/bill-presentment-recived-bills">Received Bills</a></li>
				</ul>

				<a href="#" class="table-corner-button btn-1 xs-btn color-1" data-method="openSideModal" data-filter-node="#new-subscription">New subscription</a>

				<div class="custom-table border-top m-bottom">
					<div class="table-row row-1 table-head grid">
						<div class="col col-ls-6 col-ms-5 col-ss-6">
							<p class="col-text head-txt-1 center">Beneficiary name</p>
						</div>
						<div class="col col-ls-6 col-ms-hidden">
							<p class="col-text head-txt-1 center">Beneficiary address</p>
						</div>
						<div class="col col-ls-4 col-ms-2 col-ss-hidden">
							<p class="col-text head-txt-1 center">Status</p>
						</div>
						<div class="col col-ls-8 col-ms-5 col-ss-6">
							<p class="col-text head-txt-1 center">Description</p>
						</div>
					</div>

					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-6 col-ms-5 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Telecom Sacramento</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Fulton Street 465, San Francisco</p>
							</div>
							<div class="col col-ls-4 col-ms-2 col-ss-hidden">
								<p class="col-text text-5 text-cl-5 center">Active</p>
							</div>
							<div class="col col-ls-8 col-ms-5 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Pay</a>
								<a href="#" class="btn-1 xs-btn color-3">Skip</a>
								<a href="#" class="btn-1 xs-btn color-1">Schedule</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-6 col-ms-5 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Electrodistribution San Francisco</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Maple Street 54a, Sacramento</p>
							</div>
							<div class="col col-ls-4 col-ms-2 col-ss-hidden">
								<p class="col-text text-5 text-cl-3 center">Pending</p>
							</div>
							<div class="col col-ls-8 col-ms-5 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Pay</a>
								<a href="#" class="btn-1 xs-btn color-3">Skip</a>
								<a href="#" class="btn-1 xs-btn color-1">Schedule</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-6 col-ms-5 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Tax department</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Fulton street 465, San Francisco</p>
							</div>
							<div class="col col-ls-4 col-ms-2 col-ss-hidden">
								<p class="col-text text-5 text-cl-4 center">Cancelled</p>
							</div>
							<div class="col col-ls-8 col-ms-5 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Pay</a>
								<a href="#" class="btn-1 xs-btn color-3">Skip</a>
								<a href="#" class="btn-1 xs-btn color-1">Schedule</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-6 col-ms-5 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Telecom Sacramento</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Fulton Street 465, San Francisco</p>
							</div>
							<div class="col col-ls-4 col-ms-2 col-ss-hidden">
								<p class="col-text text-5 text-cl-5 center">Active</p>
							</div>
							<div class="col col-ls-8 col-ms-5 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Pay</a>
								<a href="#" class="btn-1 xs-btn color-3">Skip</a>
								<a href="#" class="btn-1 xs-btn color-1">Schedule</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-6 col-ms-5 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Electrodistribution San Francisco</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Maple Street 54a, Sacramento</p>
							</div>
							<div class="col col-ls-4 col-ms-2 col-ss-hidden">
								<p class="col-text text-5 text-cl-3 center">Pending</p>
							</div>
							<div class="col col-ls-8 col-ms-5 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Pay</a>
								<a href="#" class="btn-1 xs-btn color-3">Skip</a>
								<a href="#" class="btn-1 xs-btn color-1">Schedule</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-6 col-ms-5 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Tax department</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Fulton street 465, San Francisco</p>
							</div>
							<div class="col col-ls-4 col-ms-2 col-ss-hidden">
								<p class="col-text text-5 text-cl-4 center">Cancelled</p>
							</div>
							<div class="col col-ls-8 col-ms-5 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Pay</a>
								<a href="#" class="btn-1 xs-btn color-3">Skip</a>
								<a href="#" class="btn-1 xs-btn color-1">Schedule</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-6 col-ms-5 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Telecom Sacramento</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Fulton Street 465, San Francisco</p>
							</div>
							<div class="col col-ls-4 col-ms-2 col-ss-hidden">
								<p class="col-text text-5 text-cl-5 center">Active</p>
							</div>
							<div class="col col-ls-8 col-ms-5 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Pay</a>
								<a href="#" class="btn-1 xs-btn color-3">Skip</a>
								<a href="#" class="btn-1 xs-btn color-1">Schedule</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-6 col-ms-5 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Electrodistribution San Francisco</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Maple Street 54a, Sacramento</p>
							</div>
							<div class="col col-ls-4 col-ms-2 col-ss-hidden">
								<p class="col-text text-5 text-cl-3 center">Pending</p>
							</div>
							<div class="col col-ls-8 col-ms-5 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Pay</a>
								<a href="#" class="btn-1 xs-btn color-3">Skip</a>
								<a href="#" class="btn-1 xs-btn color-1">Schedule</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-6 col-ms-5 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Tax department</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Fulton street 465, San Francisco</p>
							</div>
							<div class="col col-ls-4 col-ms-2 col-ss-hidden">
								<p class="col-text text-5 text-cl-4 center">Cancelled</p>
							</div>
							<div class="col col-ls-8 col-ms-5 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Pay</a>
								<a href="#" class="btn-1 xs-btn color-3">Skip</a>
								<a href="#" class="btn-1 xs-btn color-1">Schedule</a>
							</div>
						</a>
					</div>
					
					<div class="hidden-content">
						<div class="table-row row-1 grid">
							<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
								<div class="col col-ls-6 col-ms-5 col-ss-6">
									<p class="col-text text-5 text-cl-2 left">Telecom Sacramento</p>
								</div>
								<div class="col col-ls-6 col-ms-hidden">
									<p class="col-text text-5 text-cl-2 left">Fulton Street 465, San Francisco</p>
								</div>
								<div class="col col-ls-4 col-ms-2 col-ss-hidden">
									<p class="col-text text-5 text-cl-5 center">Active</p>
								</div>
								<div class="col col-ls-8 col-ms-5 col-ss-6 action-wrap-1">
									<a href="#" class="btn-1 xs-btn color-3">Pay</a>
									<a href="#" class="btn-1 xs-btn color-3">Skip</a>
									<a href="#" class="btn-1 xs-btn color-1">Schedule</a>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
								<div class="col col-ls-6 col-ms-5 col-ss-6">
									<p class="col-text text-5 text-cl-2 left">Electrodistribution San Francisco</p>
								</div>
								<div class="col col-ls-6 col-ms-hidden">
									<p class="col-text text-5 text-cl-2 left">Maple Street 54a, Sacramento</p>
								</div>
								<div class="col col-ls-4 col-ms-2 col-ss-hidden">
									<p class="col-text text-5 text-cl-3 center">Pending</p>
								</div>
								<div class="col col-ls-8 col-ms-5 col-ss-6 action-wrap-1">
									<a href="#" class="btn-1 xs-btn color-3">Pay</a>
									<a href="#" class="btn-1 xs-btn color-3">Skip</a>
									<a href="#" class="btn-1 xs-btn color-1">Schedule</a>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
								<div class="col col-ls-6 col-ms-5 col-ss-6">
									<p class="col-text text-5 text-cl-2 left">Tax department</p>
								</div>
								<div class="col col-ls-6 col-ms-hidden">
									<p class="col-text text-5 text-cl-2 left">Fulton street 465, San Francisco</p>
								</div>
								<div class="col col-ls-4 col-ms-2 col-ss-hidden">
									<p class="col-text text-5 text-cl-4 center">Cancelled</p>
								</div>
								<div class="col col-ls-8 col-ms-5 col-ss-6 action-wrap-1">
									<a href="#" class="btn-1 xs-btn color-3">Pay</a>
									<a href="#" class="btn-1 xs-btn color-3">Skip</a>
									<a href="#" class="btn-1 xs-btn color-1">Schedule</a>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
								<div class="col col-ls-6 col-ms-5 col-ss-6">
									<p class="col-text text-5 text-cl-2 left">Telecom Sacramento</p>
								</div>
								<div class="col col-ls-6 col-ms-hidden">
									<p class="col-text text-5 text-cl-2 left">Fulton Street 465, San Francisco</p>
								</div>
								<div class="col col-ls-4 col-ms-2 col-ss-hidden">
									<p class="col-text text-5 text-cl-5 center">Active</p>
								</div>
								<div class="col col-ls-8 col-ms-5 col-ss-6 action-wrap-1">
									<a href="#" class="btn-1 xs-btn color-3">Pay</a>
									<a href="#" class="btn-1 xs-btn color-3">Skip</a>
									<a href="#" class="btn-1 xs-btn color-1">Schedule</a>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
								<div class="col col-ls-6 col-ms-5 col-ss-6">
									<p class="col-text text-5 text-cl-2 left">Electrodistribution San Francisco</p>
								</div>
								<div class="col col-ls-6 col-ms-hidden">
									<p class="col-text text-5 text-cl-2 left">Maple Street 54a, Sacramento</p>
								</div>
								<div class="col col-ls-4 col-ms-2 col-ss-hidden">
									<p class="col-text text-5 text-cl-3 center">Pending</p>
								</div>
								<div class="col col-ls-8 col-ms-5 col-ss-6 action-wrap-1">
									<a href="#" class="btn-1 xs-btn color-3">Pay</a>
									<a href="#" class="btn-1 xs-btn color-3">Skip</a>
									<a href="#" class="btn-1 xs-btn color-1">Schedule</a>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
								<div class="col col-ls-6 col-ms-5 col-ss-6">
									<p class="col-text text-5 text-cl-2 left">Tax department</p>
								</div>
								<div class="col col-ls-6 col-ms-hidden">
									<p class="col-text text-5 text-cl-2 left">Fulton street 465, San Francisco</p>
								</div>
								<div class="col col-ls-4 col-ms-2 col-ss-hidden">
									<p class="col-text text-5 text-cl-4 center">Cancelled</p>
								</div>
								<div class="col col-ls-8 col-ms-5 col-ss-6 action-wrap-1">
									<a href="#" class="btn-1 xs-btn color-3">Pay</a>
									<a href="#" class="btn-1 xs-btn color-3">Skip</a>
									<a href="#" class="btn-1 xs-btn color-1">Schedule</a>
								</div>
							</a>
						</div>
					</div>
				</div>
				<button type="button" class="btn-1 color-2 big-btn" data-method="showMore">Show more</button>

			</div>

		</div>
	</div>

</div>

<?php Site::getFooter(); ?>