<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="main-content content-full">
		<div class="widget widget-transaction-list">
			<ul class="widget-actions widget-corner">
				<li>
					<div class="widget-options">
            			<ul class="trigger color-2" data-method="optionsTrigger">
            				<li></li>
            				<li></li>
            				<li></li>
            			</ul>
            			<ul class="options-list options-list-2">
            				<li><a href="#">Filter transactions</a></li>
            				<li><a href="#">Settings</a></li>
            				<li class="border"><a href="#">Export...</a></li>
            				<li><a href="#" class="active">List view</a></li>
            				<li><a href="#">Bubble chart view</a></li>
            				<li><a href="#">Bar chart view</a></li>
            				<li><a href="#">Flow chart view</a></li>
            				<li><a href="#">Calendar view</a></li>
            				<li><a href="#">Map view</a></li>
            			</ul>
            		</div>
				</li>
			</ul>
			<div class="container top-padding">
				<h2>Remittance</h2>
				<div class="custom-table table-2 border-top m-bottom">
					<div class="table-row row-1 table-head grid">
						<div class="col no-border col-ls-3 col-ms-3 col-ss-6">
							<p class="col-text head-txt-1 center">Date</p>
						</div>
						<div class="col no-border col-ls-13 col-ms-6 col-ss-hidden">
							<p class="col-text head-txt-1 left">Sender information</p>
						</div>
						<div class="col no-border col-ls-4 col-ms-hidden">
							<p class="col-text head-txt-1 center">Status</p>
						</div>
						<div class="col no-border col-ls-4 col-ms-3 col-ss-6">
							<p class="col-text head-txt-1 center">Amount</p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
							<div class="col col-ls-3 col-ms-3 col-ss-6">
								<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
							</div>
							<div class="col col-ls-13 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-6 text-cl-4 center">In process</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
							<div class="col col-ls-3 col-ms-3 col-ss-6">
								<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
							</div>
							<div class="col col-ls-13 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-6 text-cl-4 center">In process</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
							<div class="col col-ls-3 col-ms-3 col-ss-6">
								<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
							</div>
							<div class="col col-ls-13 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-6 text-cl-4 center">In process</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
							<div class="col col-ls-3 col-ms-3 col-ss-6">
								<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
							</div>
							<div class="col col-ls-13 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-6 text-cl-4 center">In process</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
							<div class="col col-ls-3 col-ms-3 col-ss-6">
								<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
							</div>
							<div class="col col-ls-13 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-6 text-cl-4 center">In process</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
							<div class="col col-ls-3 col-ms-3 col-ss-6">
								<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
							</div>
							<div class="col col-ls-13 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-6 text-cl-4 center">In process</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
							<div class="col col-ls-3 col-ms-3 col-ss-6">
								<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
							</div>
							<div class="col col-ls-13 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-6 text-cl-4 center">In process</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
							<div class="col col-ls-3 col-ms-3 col-ss-6">
								<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
							</div>
							<div class="col col-ls-13 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-6 text-cl-4 center">In process</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
							<div class="col col-ls-3 col-ms-3 col-ss-6">
								<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
							</div>
							<div class="col col-ls-13 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-6 text-cl-4 center">In process</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
							<div class="col col-ls-3 col-ms-3 col-ss-6">
								<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
							</div>
							<div class="col col-ls-13 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-6 text-cl-3 center">Completed</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
							<div class="col col-ls-3 col-ms-3 col-ss-6">
								<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
							</div>
							<div class="col col-ls-13 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-6 text-cl-3 center">Completed</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
							<div class="col col-ls-3 col-ms-3 col-ss-6">
								<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
							</div>
							<div class="col col-ls-13 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-6 text-cl-3 center">Completed</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
							<div class="col col-ls-3 col-ms-3 col-ss-6">
								<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
							</div>
							<div class="col col-ls-13 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-6 text-cl-3 center">Completed</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					
					<div class="hidden-content">
						<div class="table-row row-1 grid">
							<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
								<div class="col col-ls-3 col-ms-3 col-ss-6">
									<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
								</div>
								<div class="col col-ls-13 col-ms-6 col-ss-hidden">
									<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
								</div>
								<div class="col col-ls-4 col-ms-hidden">
									<p class="col-text text-6 text-cl-4 center">In process</p>
								</div>
								<div class="col col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
								<div class="col col-ls-3 col-ms-3 col-ss-6">
									<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
								</div>
								<div class="col col-ls-13 col-ms-6 col-ss-hidden">
									<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
								</div>
								<div class="col col-ls-4 col-ms-hidden">
									<p class="col-text text-6 text-cl-3 center">Completed</p>
								</div>
								<div class="col col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
								<div class="col col-ls-3 col-ms-3 col-ss-6">
									<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
								</div>
								<div class="col col-ls-13 col-ms-6 col-ss-hidden">
									<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
								</div>
								<div class="col col-ls-4 col-ms-hidden">
									<p class="col-text text-6 text-cl-4 center">In process</p>
								</div>
								<div class="col col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
								<div class="col col-ls-3 col-ms-3 col-ss-6">
									<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
								</div>
								<div class="col col-ls-13 col-ms-6 col-ss-hidden">
									<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
								</div>
								<div class="col col-ls-4 col-ms-hidden">
									<p class="col-text text-6 text-cl-3 center">Completed</p>
								</div>
								<div class="col col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#" data-method="openSideModal" data-filter-node="#remittance-detail">
								<div class="col col-ls-3 col-ms-3 col-ss-6">
									<p class="col-text text-6 text-cl-2 center">7 FEB 2016</p>
								</div>
								<div class="col col-ls-13 col-ms-6 col-ss-hidden">
									<p class="col-text text-6 text-cl-2 left">Jonathan Doe,Neue Strasse 236,Munich,Germany</p>
								</div>
								<div class="col col-ls-4 col-ms-hidden">
									<p class="col-text text-6 text-cl-4 center">In process</p>
								</div>
								<div class="col col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
								</div>
							</a>
						</div>
					</div>
				</div>
				<button type="button" class="btn-1 color-2 big-btn" data-method="showMore">Show more</button>
			</div>
		</div>


	</div>

</div>

<?php Site::getFooter(); ?>