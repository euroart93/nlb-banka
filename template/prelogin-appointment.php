<?php
Site::getHeader('header-login'); ?>



<div class="login-main">
	<!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
	<div class="page-cover inner-video video-cover img-overlay-blur">
		<img class="desktop-img promo-bg-0 slide-active" src="img/bg/login-bg-blur.png" alt="cover" data-object-fit="cover">
	</div>

	<div class="prelogin-full full-break">
		<div class="inner">
			<div class="col-left">
				<h2>Zakaži sastanak</h2>
				<p>Da biste zakazali sastanak u nekoj od naših ekspozitura, molimo popunite polja u formularu.
					Odaberite najbližu ekspozituru, željeni termin sastanka i ostavite svoje kontakt podatke (ime i prezime, broj telefona i e-mail adresu).
					U najkraćem mogućem roku javićemo Vam se sa zakazanim terminom sastanka.</p>

			</div>
			<div class="col-right">
				<div class="custom-padding-right">
			<div class="product-single widget">
				<div class="info-inner">
					<div class="group">
						<div class="group-inner">
							<label class="label-1">Ekspozitura u kojoj želite zakazati sastanak:</label>
							<div class="select-3">
								<select data-method="customSelect3">
		        					<option value="0">Bulevar Mihajla Pupina 165v, 11000 Beograd</option>
		        					<option value="1">Ekspoziture i bankomati</option>
		        					<option value="2">Ekspoziture i bankomati</option>
		        				</select>
		        			</div>
						</div>
						<div class="half custom-padding-right">
							<div class="group">
								<div class="group-inner">
									<label class="label-1">Ime i prezime:</label>
									<input type="text" class="input-1">
								</div>
								<div class="group-inner">
									<label class="label-1">Vaš broj telefona:</label>
									<input type="tel" class="input-1">
								</div>
							</div>
					</div>
					<div class="half custom-padding-left">
						<div class="group">
							<div class="group-inner">
								<label class="label-1">Vaša e-mail adresa::</label>
								<input type="email" class="input-1">
							</div>
							<div class="group-inner">
							<div class="custom-form">
								<div class="half">
									<div class="group-inner">
										<label class="label-1">Željeni datum:</label>
										<div class="date-wrap" style="max-width: 100%">
											<input type="text" class="input-1 dark date-input" value="30/01/2017" data-method="dateInput">
										</div>
									</div>
								</div>
								<div class="half-right custom-padding-left">
									<div class="group-inner no-padding">
										<label class="label-1">Termin:</label>
										<div class="select-3">
											<select data-method="customSelect3">
					        					<option value="0">13:00 h</option>
					        					<option value="1">14:00 h</option>
					        					<option value="2">15:00 h</option>
					        				</select>
					        			</div>
											</div>
										</div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="group">
				<div class="product-actions group-inner">
					<a href="#" class="btn-1 color-1" data-method="popupTrigger">Pošalji zahtjev</a>
					<a href="#" class="btn-1 color-2 no-margin alignright">Odustani</a>
				</div>
			</div>
		</div>
</div>
			</div>
		</div>
	</div>

	<div class="login-footer">
		<div class="inner">
			<!--<ul class="lang-nav">
				<li class="active"><a href="#">ENG</a></li>
				<li><a href="#">SRB</a></li>
			</ul>-->
			<ul class="social">
				<li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
				<li class="youtube"><a href="#" class="icon-youtube">Youtube</a></li>
				<li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
				<li class="history"><a href="#" class="icon-history">History</a></li>
			</ul>
		</div>
	</div>


</div>


</div><!-- /Wrapper -->

<div class="appointment-popup appointment-hidden"> <!--used in prelogin-appointment-->
	<div class="popup-window appointment-hidden">
		<h1 class="appointment-hidden">Hvala</h1>
		<p class="appointment-hidden">Vaš zahtev za zakazivanje sastanka je primljen. <br>
			Kontaktiraćemo Vas u najkraćem mogućem roku.
		</p>
		<a href="#" class="btn-1 color-1 appointment-hidden" data-method="popupTrigger">U redu</a>
	</div>
</div>
