<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="account-row account-bg-3">
		<div class="bg-overlay overlay-2"></div>
		<div class="account-small-slider" data-method="accountSlider2">
	        <!-- Slides -->
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Current Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-1" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Second Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-2" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Third Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-3" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Fourth Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-4" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
	        </div>
	    </div>
	    <div class="slider-button-prev"></div>
		<div class="slider-button-next"></div>
	</div>
	
	<div class="main-content">
		<div class="widget calendar-view">
			<ul class="widget-actions widget-corner">
				<li>
					<div class="widget-options">
            			<ul class="trigger color-1" data-method="optionsTrigger"> 
            				<li></li>
            				<li></li>
            				<li></li>
            			</ul>
            			<ul class="options-list options-list-2">
            				<li><a href="#">Filter transactions</a></li>
            				<li><a href="#">Settings</a></li>
            				<li class="border"><a href="#">Export...</a></li>
            				<li><a href="#" class="active">List view</a></li>
            				<li><a href="#">Bar chart view</a></li>
            				<li><a href="#">Flow chart view</a></li>
            				<li><a href="#">Calendar view</a></li>
            				<li><a href="#">Map view</a></li>
            			</ul>
            		</div>
				</li>
			</ul>
			<div class="container top-padding">
				<h2>Transactions - Calendar View</h2>

				<form action="#" method="#">

					<div class="time-lapse-trigger">
						<div class="select-3">
							<select data-method="customSelect3">
		    					<option value="0">January 2016</option>
		    					<option value="1">February 2016</option>
		    					<option value="2">March 2016</option>
		    					<option value="3">April 2016</option>
		    					<option value="4">May 2016</option>
		    					<option value="5">June 2016</option>
		    					<option value="6">July 2016</option>
		    					<option value="7">August 2016</option>
		    					<option value="8">September 2016</option>
		    					<option value="9">October 2016</option>
		    					<option value="10">November 2016</option>
		    					<option value="11">December 2016</option>
		    				</select>
		    			</div>
		    		</div>

					<div class="calendar">
						<div class="table-head">
							<div class="th"><p>Monday</p></div>
							<div class="th"><p>Tuesday</p></div>
							<div class="th"><p>Wednesday</p></div>
							<div class="th"><p>Thursday</p></div>
							<div class="th"><p>Friday</p></div>
							<div class="th"><p>Saturday</p></div>
							<div class="th"><p>Sunday</p></div>
						</div>

						<div class="table-body">
							<div class="tr">
								<div class="td last-month">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">26. <span>Monday</span></p>
									</a>
								</div>
								<div class="td last-month">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">27. <span>Tuesday</span></p>
									</a>
								</div>
								<div class="td last-month">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">28. <span>Wednesday</span></p>
									</a>
								</div>
								<div class="td last-month">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">29. <span>Thursday</span></p>
									</a>
								</div>
								<div class="td last-month">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">30. <span>Friday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">1. <span>Saturday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">2. <span>Sunday</span></p>
									</a>
								</div>
							</div>

							<div class="tr">
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">3. <span>Monday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">4. <span>Tuesday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">5. <span>Wednesday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">6. <span>Thursday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">7. <span>Friday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">8. <span>Saturday</span></p>
										<div class="transaction-tracker-wrapper">
											<div class="transaction-tracker down">
												<i class="tranaction-down"></i>
												<p>2</p>
											</div>
											<div class="transaction-tracker up">
												<i class="tranaction-up"></i>
												<p>6</p>
											</div>
										</div>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">9. <span>Sunday</span></p>
									</a>
								</div>
							</div>

							<div class="tr">
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">10. <span>Monday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">11. <span>Tuesday</span></p>
										<div class="transaction-tracker-wrapper">
											<div class="transaction-tracker up">
												<i class="tranaction-up"></i>
												<p>3</p>
											</div>
										</div>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">12. <span>Wednesday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">13. <span>Thursday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">14. <span>Friday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">15. <span>Saturday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">16. <span>Sunday</span></p>
									</a>
								</div>
							</div>

							<div class="tr">
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">17. <span>Monday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">18. <span>Tuesday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">19. <span>Wednesday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">20. <span>Thursday</span></p>
										<div class="transaction-tracker-wrapper">
											<div class="transaction-tracker down">
												<i class="tranaction-down"></i>
												<p>2</p>
											</div>
										</div>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">21. <span>Friday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">22. <span>Saturday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">23. <span>Sunday</span></p>
									</a>
								</div>
							</div>

							<div class="tr">
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">24. <span>Monday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">25. <span>Tuesday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">26. <span>Wednesday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">27. <span>Thursday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">28. <span>Friday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">29. <span>Saturday</span></p>
									</a>
								</div>
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">30. <span>Sunday</span></p>
									</a>
								</div>
							</div>

							<div class="tr">
								<div class="td">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">31. <span>Monday</span></p>
									</a>
								</div>
								<div class="td next-month">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">1. <span>Tuesday</span></p>
									</a>
								</div>
								<div class="td next-month">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">2. <span>Wednesday</span></p>
									</a>
								</div>
								<div class="td next-month">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">3. <span>Thursday</span></p>
									</a>
								</div>
								<div class="td next-month">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">4. <span>Friday</span></p>
									</a>
								</div>
								<div class="td next-month">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">5. <span>Saturday</span></p>
									</a>
								</div>
								<div class="td next-month">
									<a href="#" data-method="openSideModal" data-filter-node="#side-modal-transaction-list">
										<p class="date">6. <span>Sunday</span></p>
									</a>
								</div>
							</div>
						</div>

					</div>

				</form>

			</div>
		</div>
	</div>

</div>

<?php Site::getFooter(); ?>