<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li class="selected">Standarno plaćanje</li></a>
				<a href="#"><li>Interni transferi</li></a>
				<a href="#"><li>Devizno plaćanje</li></a>
				<a href="#"><li>Moji šabloni</li></a>
				<a href="#"><li>Pregled plaćanja</li></a>
			</ul>
		</div>
	</div>

	<div class="widget widget-select-account widget-content-white row" data-method="accountSelect">
		<div class="account-selected">
			<div class="col-ss-12 col-ls-2 acc-img">
				<img src="img/demo/acc-img-medium.jpg">
			</div>
			<div class="col-ss-12 col-ls-6 col-border">
				<div class="content">
					<p class="small">Ime računa:</p>
					<p class="big acc-name">Moj tekući račun</p>
				</div>
			</div>
			<div class="col-ss-12 col-ls-7 col-border">
				<div class="content">
					<p class="small">Broj računa:</p>
					<p class="big-s acc-number">115-0000000000567898-6</p>
				</div>
			</div>
			<div class="col-ss-12 col-ls-7 col-border">
				<div class="content">
					<p class="small float-right">Raspoloživo stanje:</p>
					<p class="big acc-amount float-right">5.123.543,89 RSD</p>
				</div>
			</div>
			<div class="col-ss-1">
				<a href="#" class="acc-list-btn acc-drop-btn" type="button" data-method="accountSelectTrigger">
					<div class="triangle"></div>
				</a>
			</div>
		</div>
		<div class="account-select">
  			<ul class="options">
    			 <li class="item">
    			 	<div class="widget row">
						<div class="col-ss-2 col-ms-2 col-ls-2">
							<div class="triangle-right-white"></div>
							<img class="desaturate" src="img/demo/acc-img-medium.jpg">
						</div>
						<div class="col-ss-8 col-ms-9 col-ls-6">
							<div class="content">
								<p class="acc-select-small">Ime računa:</p>
								<p class="big acc-name">Moj tekući račun</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small">Broj računa:</p>
								<p class="big-s acc-number">111-0000000000567898-6</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small float-right">Raspoloživo stanje:</p>
								<p class="big acc-amount float-right">5.123.543,89 RSD</p>
							</div>
						</div>
						<div class="col-ss-1 col-ms-1 col-ls-1  ">
							<div class="triangle-right"></div>
					</div>
    			 </li>
    			 <li class="item">
    			 	<div class="widget row">
						<div class="col-ss-2 col-ms-2 col-ls-2">
							<div class="triangle-right-white"></div>
							<img class="desaturate" src="img/demo/acc-img-medium-2.jpg">
						</div>
						<div class="col-ss-8 col-ms-9 col-ls-6">
							<div class="content">
								<p class="acc-select-small">Ime računa:</p>
								<p class="big acc-name">Drugi tekući račun</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small">Broj računa:</p>
								<p class="big-s acc-number">112-0000000000567898-6</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small float-right">Raspoloživo stanje:</p>
								<p class="big acc-amount float-right">1.850,89 RSD</p>
							</div>
						</div>
						<div class="col-ss-1 col-ms-1 col-ls-1  ">
							<div class="triangle-right"></div>
					</div>
    			 </li>
    			 <li class="item">
    			 	<div class="widget row">
						<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium-3.jpg">
						</div>
						<div class="col-ss-8 col-ms-9 col-ls-6">
							<div class="content">
								<p class="acc-select-small">Ime računa:</p>
								<p class="big acc-name">Treći tekući račun</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small">Broj računa:</p>
								<p class="big-s acc-number">113-0000000000567898-6</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small float-right">Raspoloživo stanje:</p>
								<p class="big acc-amount float-right">2.850,89 RSD</p>
							</div>
						</div>
						<div class="col-ss-1 col-ms-1 col-ls-1  ">
							<div class="triangle-right"></div>
					</div>
    			 </li>
    			 <li class="item">
    			 	<div class="widget row">
						<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium-4.jpg">
						</div>
						<div class="col-ss-8 col-ms-9 col-ls-6">
							<div class="content">
								<p class="acc-select-small">Ime računa:</p>
								<p class="big acc-name">Četvrti tekući račun</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small">Broj računa:</p>
								<p class="big-s acc-number">114-0000000000567898-6</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small float-right">Raspoloživo stanje:</p>
								<p class="big acc-amount float-right">3.850,89 RSD</p>
							</div>
						</div>
						<div class="col-ss-1 col-ms-1 col-ls-1  ">
							<div class="triangle-right"></div>
					</div>
    			 </li>
   				 
			</ul>
		</div>
	</div>
	
	<div class="main-content">

	
		<form action="#" method="get">

			<div class="widget content-white left-padding payment-widget payment-steps">
				<h3 class="title-4 color-2"><span>Standarno plaćanje</span></h3>
				<ul class="steps left">
					<li >1 <span>Payment order</span></li>
					<li class="active">2 <span>Payment review</span></li>
					<li>3 <span>Payment confirmation</span></li>
				</ul>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Podaci o platiocu</span></h3>
					</div>
						<div class="col-right">
							<div class="plain-list">
							<div>
								<p class="key">Naziv,adresa i mesto platioca:</p>
								<p class="value">Petar Petrović,Jurija Gagarina 98,Beograd</p>
							</div>
						</div>
					</div>
				</div>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Podaci o primaocu</span></h3>
					</div>
					<div class="col-right">
							<div class="plain-list">
							<div>
								<p class="key">Naziv,adresa i mesto primaoca:</p>
								<p class="value">Telekom Srbija,Bulevar Franše D Eperea 2,Beograd</p>
							</div>
							<div>
								<p class="key">Račun:</p>
								<p class="value">160-0000000000602-16</p>
							</div>
							<div>
								<p class="key">Model i poziv na broj odobrenja:</p>
								<p class="value">00 3212546-16022017-747</p>
							</div>
					</div>
				</div>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Detalji plaćanja</span></h3>
					</div>
					<div class="col-right">
							<div class="plain-list">
							<div>
								<p class="key">Iznos:</p>
								<p class="value">5.000,00 RSD</p>
							</div>
							<div>
								<p class="key">Provizija:</p>
								<p class="value">150,00 RSD</p>
							</div>
							<div>
								<p class="key">Šifra plaćanja:</p>
								<p class="value">289-Plaćanje telekom usluga</p>
							</div>
							<div>
								<p class="key">Svrha plaćanja:</p>
								<p class="value">Račun za telekom usluge za 01/2017</p>
							</div>
					</div>
				</div>
		
				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Terminski plan:</span></h3>
					</div>
					<div class="col-right">
							<div class="plain-list">
								<div>
									<p class="key">Datum:</p>
									<p class="value">16.02.2017</p>
								</div>
								<div>
									<p class="key">Hitno plaćanje:</p>
									<p class="value">NE</p>
								</div>
							</div>
						<div class="custom-form uk-grid">
							<div class="divider-30"></div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<button type="button" class="btn-1 color-3">Ispravi podatke</button>
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<a href="#" data-filter-node="#side-modal-payment-center" data-method="openSideModal" class="btn-1 color-1">Plati</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</form>
	</div>

</div>

<?php Site::getFooter(); ?>