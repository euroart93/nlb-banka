<?php Site::getHeader(); ?>

	<div id="main">

		<div class="submenu">
			<div class="container">
				<ul>
					<a href="#"><li>Standarno plaćanje</li></a>
					<a href="#"><li class="selected">Administracija</li></a>
					<a href="#"><li>Lista zahteva</li></a>
					<a href="#"><li>Notifikacije</li></a>
					<a href="#"><li>Mobilno bankarstvo</li></a>
				</ul>
			</div>
		</div>
		
		<div class="main-content">
			<div class="widget content-white left-padding payment-widget payment-steps settings">
				<h3 class="title-4 color-2"><span>Administracija</span></h3>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Podaci o prijavama</span></h3>
						<p>
							Na ovom mestu možete proveriti vreme i trajanje poslednje prijave.
						</p>
					</div>
					<div class="col-right">
						<div class="plain-list">
							<div>
								<p class="key">Poslednja prijava na e-bankarstvo:</p>
								<p class="value">15.02.2017.22:30:15</p>
							</div>
							<div>
								<p class="key">Trajanje poslednje prijave:</p>
								<p class="value">17 minuta</p>
							</div>
							<div>
								<p class="key">Korisničko ime:</p>
								<p class="value">igor.jovanovic</p>
							</div>
						</div>
						<div class="group uk-width-1-2">
							<div class="group-inner input">
								<a href="#" class="btn-1 color-1">Sačuvaj izmene</a>
							</div>
						</div>
					</div>
				</div>




				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Korisničke informacije</span></h3>
						<p>
							Na ovom mestu možete izmeniti Vaše korisničke podatke: korisničko ime, lozinku i sigurnosnu sliku.
						</p>
					</div>
					<div class="col-right">
						<div class="plain-list">
							<div>
								<p class="key">Korisničko ime:</p>
								<p class="value">igor.jovanovic</p>
							</div>
						</div>
						<div class="group uk-width-1-2">
							<div class="group-inner input">
								<a href="#" class="btn-1 color-1" data-filter-node="#side-modal-settings-username-change" data-method="openSideModal">Izmeni korisničko ime</a>
							</div>
						</div>

						<div class="divider"></div>

						<div class="plain-list">
							<div>
								<p class="key">Lozinka:</p>
								<p class="value">**********</p>
							</div>
						</div>
						<div class="group uk-width-1-2">
							<div class="group-inner input">
								<a href="#" class="btn-1 color-1" data-filter-node="#side-modal-settings-password-change" data-method="openSideModal">Izmeni lozinku</a>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>



<?php Site::getFooter(); ?>