<?php
Site::getHeader('header-login'); ?>

<div class="login-main">

	<!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
	<div class="page-cover inner-video video-cover img-overlay-blur">
		<img class="desktop-img promo-bg-0 slide-active" src="img/bg/login-bg-blur.png" alt="cover" data-object-fit="cover">
	</div>

	<div class="prelogin-full">
		<div class="custom-inner">
			<div class="custom-exchange">
				<div class="half margin-bottom">
					<a href="#" type="button" class="login-close">X</a>
					<h1>Za korisnike internet <br>bankarstva NLB Banke</h1>
					<div class="currency-wrapper relative">
						<img src="img/photos/eu.png" alt="">
						<p class="currency-value">EUR</p>
					</div>
					<p>Kurs na dan: 30.01.2017.</p>
					<div class="custom-currency-details">
						<div class="half">
							<p class="currency-detail">120,9915 <span>Kupovni kurs:</span></p>
						</div>
						<div class="half border-left">
							<p class="currency-detail">124,0015 <span>Prodajni kurs:</span></p>
						</div>
					</div>
					<div class="button-center">
						<a href="#" class="btn-1 color-1 full-width">Ulogujte se</a>
					</div>

				</div>

				<div class="half">
					<h1>Želite da postanete korisnik <br>internet bankarstva NLB Banke?</h1>
					<p class="custom-exchange-ad">Povoljniji kurs za EUR je samo jedna od prednosti korišćenja internet
						bankarstva NLB Banke. Zakažite sastanak u nekoj od naših ekspozitura, saznajte sve detalje i priključite se NLB porodici.</p>
						<div class="button-center">
							<a href="#" class="btn-1 color-1 full-width">Zatraži sastanak</a>
						</div>
						<div class="vertical-line">

						</div>
				</div>
			</div>
		</div>
	</div>

	<div class="login-footer">
		<div class="inner">
			<!--<ul class="lang-nav">
				<li class="active"><a href="#">ENG</a></li>
				<li><a href="#">SRB</a></li>
			</ul>-->
			<ul class="social">
				<li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
				<li class="youtube"><a href="#" class="icon-youtube">Youtube</a></li>
				<li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
				<li class="history"><a href="#" class="icon-history">History</a></li>
			</ul>
		</div>
	</div>


</div>


</div><!-- /Wrapper -->
