<?php 
Site::getHeader(); ?>

<div id="main">
	
	<div class="account-row account-bg-3">
		<div class="bg-overlay overlay-2"></div>
		<div class="account-small-slider" data-method="accountSlider2">
	        <!-- Slides -->
            <div class="slide">
            	<div class="account-box">
					<div class="card-logo">
						<img src="<?php echo Site::url() ?>/img/photos/mastercard.png" alt="Card logo">
					</div>
            		<p class="acc-name">My Current Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-1" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<div class="card-logo">
						<img src="<?php echo Site::url() ?>/img/photos/mastercard.png" alt="Card logo">
					</div>
            		<p class="acc-name">My Second Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-2" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<div class="card-logo">
						<img src="<?php echo Site::url() ?>/img/photos/visa.png" alt="Card logo">
					</div>
            		<p class="acc-name">My Third Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-3" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<div class="card-logo">
						<img src="<?php echo Site::url() ?>/img/photos/visa.png" alt="Card logo">
					</div>
            		<p class="acc-name">My Fourth Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-4" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
	        </div>
	    </div>
	    <div class="slider-button-prev"></div>
		<div class="slider-button-next"></div>
	</div>

	<div class="main-content">
		<div class="widget content-white payment-widget">
			<h2><i class="icon-card-4"></i>Card details</h2>

			<div class="widget-slat">
				<div class="col-left">
					<h3 class="title-1 color-2 title-full"><span>Osnovni detalji kartice</span></h3>
					<div class="card-preview">
						<img src="<?php echo Site::url() ?>/img/photos/card-preview.jpg" alt="Card">
					</div>
				</div>
				<div class="col-right">
					<div class="plain-list">
						<div>
							<p class="key">Card Name:</p>
							<p class="value">My MasterCard</p>
						</div>
						<div>
							<p class="key">Card Brand:</p>
							<p class="value">MasterCard</p>
						</div>
						<div>
							<p class="key">Card Number:</p>
							<p class="value">1234-XXXX-XXXX-9876</p>
						</div>
						<div>
							<p class="key">Card Owner:</p>
							<p class="value">Mladen Srečković</p>
						</div>
						<div>
							<p class="key">Card Type:</p>
							<p class="value">Credit Card</p>
						</div>
						<div>
							<p class="key">Card Priority:</p>
							<p class="value">Primary Card</p>
						</div>
						<div>
							<p class="key">Card Status:</p>
							<p class="value">Active</p>
						</div>
						<div>
							<p class="key">Card Expiration date:</p>
							<p class="value">17.05.2017.</p>
						</div>
					</div>
				</div>
			</div>

			<div class="widget-slat">
				<div class="col-left">
					<h3 class="title-1 color-2 title-full -more"><span>More Cards detail</span>
						<a href="#" class="plus" data-method="toggleOptions">
                            <span class="idle">+</span>
                            <span class="focus">-</span>
                        </a>
					</h3>
				</div>
				<div class="col-right expanable">
					<div class="plain-list">
						<div>
							<p class="key">Card Type:</p>
							<p class="value">Credit Card</p>
						</div>
						<div>
							<p class="key">Card Status:</p>
							<p class="value">Active</p>
						</div>
						<div>
							<p class="key">Odobreni iznos kredita:</p>
							<p class="value">590.250,39 RSD</p>
						</div>
					</div>
				</div>
			</div>

			<div class="widget-slat">
				<form action="#" method="get">
					<div class="col-left">
						<h3 class="title-1 color-2 title-full"><span>Consumption Channels</span></h3>
					</div>
					<div class="col-right">
						<div class="plain-list">
							<div>
								<p class="key"><strong>Transakcija gotovine</strong></p>
								<div class="checkbox-wrap-2">
									<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
								</div>
							</div>
							<div>
								<p class="key"><strong>POS</strong></p>
								<div class="checkbox-wrap-2">
									<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
								</div>
							</div>
							<div>
								<p class="key"><strong>Internet</strong></p>
								<div class="checkbox-wrap-2">
									<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>

			<div class="widget-slat">
				<div class="col-left">
					<h3 class="title-1 color-2 title-full"><span>Card options</span></h3>
				</div>
				<div class="col-right">
					<div class="links-list">
						<a href="#" class="pl-10">Transaction list</a>
						<a href="#" class="pl-10">Product list</a>
					</div>
				</div>
			</div>
		</div> <!-- widget-content end -->
	</div> <!-- main-content end -->

</div> <!-- main end -->

<?php Site::getFooter(); ?>