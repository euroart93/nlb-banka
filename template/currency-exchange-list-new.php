<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li>Kursna lista</li></a>
				<a href="#"><li>Kupovina/prodaja deviza</li></a>
				<a href="#"><li class="selected">Lista menjačkih transakcija</li></a>
			</ul>
		</div>
	</div>


	<div class="main-content">

	
		<div class="widget widget-transaction-list payment-list content-white m-top currency-exchange-list-new">
			<div class="widget-header header-dark">
				<div class="cnt-2">
					<div class="col-left">
						<h3 class="title-4 color-2"><span>Lista menjačkih transakcija</span></h3>
					</div>
					<div class="col-right">
						<ul class="widget-actions bg-widget"  data-method="BGoptionsTrigger">
							<a href="#" data-filter-node="#side-modal-currency-transaction-filter" data-method="openSideModal">
								<p class="widget-name">Filter</p>
							</a>
							<li>
								<div class="widget-options">
			            			<ul class="trigger" data-method="optionsTrigger">
			            				<li></li>
			            				<li></li>
			            				<li></li>
			            			</ul>
			            		</div>
							</li>
						</ul>
					</div>
				</div>
				<ul class="widget-tabs">
					<li class="active"><a href="#">Sve</a></li>
					<li><a href="#">Kupovina</a></li>
					<li><a href="#">Prodaja</a></li>
				</ul>
			</div>
			<div class="custom-table">

				<div class="table-header">
					<div class="table-head grid">
						<div class="col col-ls-3 col-ms-2 col-ss-6">
							<p class="left">Datum valute</p>
						</div>
						<div class="col col-ls-8 col-ms-4 col-ss-hidden">
							<p class="left">Sa računa</p>
						</div>
						<div class="col col-ls-8 col-ms-hidden">
							<p class="left">Na račun</p>
						</div>
						<div class="col col-ls-5 col-ms-4 col-ss-6">
							<p class="right">Iznos</p>
						</div>
					</div>
				</div>
				<div class="plain-list transactions-plain-list">
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-25</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-drop col-text text-3 text-cl-3 right">-2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					

				<div class="dropdown hidden-content">
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
					<div class="table-row row-1 grid">
						<div class="col col-ls-3 col-ms-2 col-ss-6">
							<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
						</div>
						<div class="col col-ls-8 col-ms-4 col-ss-hidden">
							<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
						</div>
						<div class="col col-ls-8 col-ms-hidden">
							<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
						</div>
						<div class="col col-ls-5 col-ms-4 col-ss-6">
							<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
						</div>
					</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-currency-transaction-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-6">
								<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
							</div>
							<div class="col col-ls-8 col-ms-4 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="change-rise col-text text-3 text-cl-3 right">2.600,00 EUR</p>
							</div>
						</div>
					</a>
				</div>
			</div>
			<div class="button-wrap">
				<button class="btn-2 color-1 alignright" data-method="showMore">Prikaži još transakcija</button>
			</div>
		</div>
	</div>

</div>

<?php Site::getFooter(); ?>