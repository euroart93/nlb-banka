<?php Site::getHeader(); ?>

<div id="main">

	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li class="selected">Kursna lista</li></a>
				<a href="#"><li>Kupopvina/prodaja deviza</li></a>
				<a href="#"><li>Lista menjačkih transakcija</li>
			</ul>
		</div>
	</div>
	
	<div class="main-content">
		<div class="prelogin-full currency-exchange">
			<div class="inner">
				<div class="col-left">
					<h3 class="title-4 color-2"><span>Kursna lista</span></h3>
					<p>Na dan: 01.02.2017, sreda</p>
					<div class="custom-form uk-grid">
						<div class="group uk-width" style="width: 100%">
							<label class="label-2">Prikaži listu za dan:</label>
							<div class="date-wrap" style="max-width: 100%">
								<input type="text" class="input-1 dark date-input" value="30/01/2017" data-method="dateInput">
							</div>
							<!-- <div class="border-hor"></div> -->
						</div>
						
					</div>
					<div class="button-actions">
						<a href="#" class="btn-1 color-1">Kupi devize</a>
						<a href="#" class="btn-1 color-1">prodaj devize</a>
					</div>
				</div>
				<div class="col-right -no-col-head">
					<div class="scrollable-content" data-method="customScroll">

						<div class="custom-table">
							<div class="table-row row-1 table-head no-padding grid">
								<div class="col no-border col-ls-12 col-ms-6 col-ss-6">
									<p class="col-text head-txt-1 left">Valuta/Zemlja</p>
								</div>
								<div class="col no-border col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text head-txt-1 center">Kupovni</p>
								</div>
								<div class="col no-border col-ls-4 col-ms-hidden">
									<p class="col-text head-txt-1 center">Srednji</p>
								</div>
								<div class="col no-border col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text head-txt-1 center">Prodajni</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/eu-2.png" alt="">
										<p class="currency-value">1 EUR</p>
										<p class="currency">Euro</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/usa-2.png" alt="">
										<p class="currency-value">1 USD</p>
										<p class="currency">Američki dolar</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/swiss-2.png" alt="">
										<p class="currency-value">1 CHF</p>
										<p class="currency">Švajcarski franak</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/great-britain-2.png" alt="">
										<p class="currency-value">1 GBP</p>
										<p class="currency">Britanska funta</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/canada-2.png" alt="">
										<p class="currency-value">1 CAD</p>
										<p class="currency">Kanadski dolar</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/norway.png" alt="">
										<p class="currency-value">1 NOK</p>
										<p class="currency">Norveška kruna</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/eu-2.png" alt="">
										<p class="currency-value">1 EUR</p>
										<p class="currency">Euro</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/usa-2.png" alt="">
										<p class="currency-value">1 USD</p>
										<p class="currency">Američki dolar</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/swiss-2.png" alt="">
										<p class="currency-value">1 CHF</p>
										<p class="currency">Švajcarski franak</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/great-britain-2.png" alt="">
										<p class="currency-value">1 GBP</p>
										<p class="currency">Britanska funta</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/canada-2.png" alt="">
										<p class="currency-value">1 CAD</p>
										<p class="currency">Kanadski dolar</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/norway.png" alt="">
										<p class="currency-value">1 NOK</p>
										<p class="currency">Norveška kruna</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
	</div>

	</div>




<?php Site::getFooter(); ?>