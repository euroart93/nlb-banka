<?php Site::getHeader(); ?>
	<div id="main">
		<div class="submenu">
			<div class="container">
				<ul>
					<a href="#"><li>Detalji kartice</li></a>
					<a href="#"><li>Lista transakcija</li></a>
					<a href="#"><li  class="selected"''>Lista izvoda</li></a>
					<a href="#"><li>Upravljanje karticom</li></a>
				</ul>
			</div>
		</div>
		
		
		<div class="widget card-new-details widget-select-account widget-content-white row" data-method="accountSelect">
			<div class="account-selected">
				<div class="col-ss-12 col-ls-2 acc-img">
					<img src="img/icons/mastercard-color-icon.png">
				</div>
				<div class="col-ss-12 col-ls-6 col-border">
					<div class="content">
						<p class="small">Ime kartice:</p>
						<p class="big acc-name">Moj Mastercard</p>
					</div>
				</div>
				<div class="col-ss-12 col-ls-7 col-border">
					<div class="content">
						<p class="small">Broj kartice:</p>
						<p class="big-s acc-number">1234**** ****5678</p>
					</div>
				</div>
				<div class="col-ss-12 col-ls-7 col-border">
					<div class="content">
						<p class="small float-right">Raspoloživo stanje:</p>
						<p class="big acc-amount float-right">5.123.543,89 RSD</p>
					</div>
				</div>
				<div class="col-ss-1">
					<a href="#" class="acc-list-btn acc-drop-btn" type="button" data-method="accountSelectTrigger">
						<div class="triangle"></div>
					</a>
				</div>
			</div>
			<div class="account-select">
      			<ul class="options">
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
									<div class="card-brand-mastercard">
										<div class="img-mask"></div>
										<img class="desaturate" src="img/icons/mastercard-color-icon.png">
									</div>
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime kartice:</p>
									<p class="big acc-name">Drugi Mastercard</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj kartice:</p>
									<p class="big-s acc-number">4321**** ****9876</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">929.543,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
									<div class="card-brand-visa">
										<div class="img-mask"></div>
										<img class="desaturate" src="img/icons/visa-color-icon2.png">
									</div>
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime kartice:</p>
									<p class="big acc-name">Visa debitna</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj kartice:</p>
									<p class="big-s acc-number">4578**** ****2678</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">1.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
									<div class="card-brand-maestro">
										<div class="img-mask"></div>
										<img class="desaturate" src="img/icons/maestro-color-icon.png">
									</div>
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime kartice:</p>
									<p class="big acc-name">Maestro debitna</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj kartice:</p>
									<p class="big-s acc-number">9643**** ****2345</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">2.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
						<li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
									<div class="card-brand-dina">
										<div class="img-mask"></div>
										<img class="desaturate" src="img/icons/dina-color-icon.png">
									</div>
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime kartice:</p>
									<p class="big acc-name">Dina debitna</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj kartice:</p>
									<p class="big-s acc-number">9643**** ****2345</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">2.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 </li>
    			</ul>
			</div>
		</div>

		<div class="main-content">

			<div class="widget content-white account-list-of-excerpts">
				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-4 color-2"><span>Lista izvoda</span></h3>
						<p>Moguće je pruzeti izvode samo za posljednjih 24mjeseca.za preuzimanje izvoda u PDF formatu,kliknite na link Preuzmi pored odabranog meseca.</p>
					</div>
					<div class="col-right">
						
						<div class="custom-table mt-25">

							<div class="table-row row-1 grid">
								<div class="col col-ls-20 col-ms-9 col-ss-9">
									<div class="document-wrapper">
										<i class="account-pdf-icon">pdf</i>
										<p class="col-text text-5 text-cl-2 left">Januar 2017</p>
									</div>
								</div>
								
								<div class="col col-ls-3 col-ms-3 col-ss-3">
									<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-20 col-ms-9 col-ss-9">
									<div class="document-wrapper">
										<i class="account-pdf-icon">pdf</i>
										<p class="col-text text-5 text-cl-2 left">Decembar 2016</p>
									</div>
								</div>
								
								<div class="col col-ls-3 col-ms-3 col-ss-3">
									<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-20 col-ms-9 col-ss-9">
									<div class="document-wrapper">
										<i class="account-pdf-icon">pdf</i>
										<p class="col-text text-5 text-cl-2 left">Januar 2017</p>
									</div>
								</div>
								
								<div class="col col-ls-3 col-ms-3 col-ss-3">
									<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-20 col-ms-9 col-ss-9">
									<div class="document-wrapper">
										<i class="account-pdf-icon">pdf</i>
										<p class="col-text text-5 text-cl-2 left">Decembar 2016</p>
									</div>
								</div>
								
								<div class="col col-ls-3 col-ms-3 col-ss-3">
									<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-20 col-ms-9 col-ss-9">
									<div class="document-wrapper">
										<i class="account-pdf-icon">pdf</i>
										<p class="col-text text-5 text-cl-2 left">Januar 2017</p>
									</div>
								</div>
								
								<div class="col col-ls-3 col-ms-3 col-ss-3">
									<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-20 col-ms-9 col-ss-9">
									<div class="document-wrapper">
										<i class="account-pdf-icon">pdf</i>
										<p class="col-text text-5 text-cl-2 left">Decembar 2016</p>
									</div>
								</div>
								
								<div class="col col-ls-3 col-ms-3 col-ss-3">
									<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-20 col-ms-9 col-ss-9">
									<div class="document-wrapper">
										<i class="account-pdf-icon">pdf</i>
										<p class="col-text text-5 text-cl-2 left">Januar 2017</p>
									</div>
								</div>
								
								<div class="col col-ls-3 col-ms-3 col-ss-3">
									<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-20 col-ms-9 col-ss-9">
									<div class="document-wrapper">
										<i class="account-pdf-icon">pdf</i>
										<p class="col-text text-5 text-cl-2 left">Decembar 2016</p>
									</div>
								</div>
								
								<div class="col col-ls-3 col-ms-3 col-ss-3">
									<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-20 col-ms-9 col-ss-9">
									<div class="document-wrapper">
										<i class="account-pdf-icon">pdf</i>
										<p class="col-text text-5 text-cl-2 left">Januar 2017</p>
									</div>
								</div>
								
								<div class="col col-ls-3 col-ms-3 col-ss-3">
									<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-20 col-ms-9 col-ss-9">
									<div class="document-wrapper">
										<i class="account-pdf-icon">pdf</i>
										<p class="col-text text-5 text-cl-2 left">Decembar 2016</p>
									</div>
								</div>
								
								<div class="col col-ls-3 col-ms-3 col-ss-3">
									<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
								</div>
							</div>
							<div class="hidden-content">
								<div class="table-row row-1 grid">
									<div class="col col-ls-20 col-ms-9 col-ss-9">
										<div class="document-wrapper">
											<i class="account-pdf-icon">pdf</i>
											<p class="col-text text-5 text-cl-2 left">Januar 2017</p>
										</div>
									</div>
									
									<div class="col col-ls-3 col-ms-3 col-ss-3">
										<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
									</div>
								</div>
								<div class="table-row row-1 grid">
									<div class="col col-ls-20 col-ms-9 col-ss-9">
										<div class="document-wrapper">
											<i class="account-pdf-icon">pdf</i>
											<p class="col-text text-5 text-cl-2 left">Decembar 2016</p>
										</div>
									</div>
									
									<div class="col col-ls-3 col-ms-3 col-ss-3">
										<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
									</div>
								</div>
								<div class="table-row row-1 grid">
									<div class="col col-ls-20 col-ms-9 col-ss-9">
										<div class="document-wrapper">
											<i class="account-pdf-icon">pdf</i>
											<p class="col-text text-5 text-cl-2 left">Januar 2017</p>
										</div>
									</div>
									
									<div class="col col-ls-3 col-ms-3 col-ss-3">
										<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
									</div>
								</div>
								<div class="table-row row-1 grid">
									<div class="col col-ls-20 col-ms-9 col-ss-9">
										<div class="document-wrapper">
											<i class="account-pdf-icon">pdf</i>
											<p class="col-text text-5 text-cl-2 left">Decembar 2016</p>
										</div>
									</div>
									
									<div class="col col-ls-3 col-ms-3 col-ss-3">
										<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
									</div>
								</div>
								<div class="table-row row-1 grid">
									<div class="col col-ls-20 col-ms-9 col-ss-9">
										<div class="document-wrapper">
											<i class="account-pdf-icon">pdf</i>
											<p class="col-text text-5 text-cl-2 left">Decembar 2016</p>
										</div>
									</div>
									
									<div class="col col-ls-3 col-ms-3 col-ss-3">
										<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
									</div>
								</div>
								<div class="table-row row-1 grid">
									<div class="col col-ls-20 col-ms-9 col-ss-9">
										<div class="document-wrapper">
											<i class="account-pdf-icon">pdf</i>
											<p class="col-text text-5 text-cl-2 left">Decembar 2016</p>
										</div>
									</div>
									
									<div class="col col-ls-3 col-ms-3 col-ss-3">
										<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
									</div>
								</div>
								<div class="table-row row-1 grid">
									<div class="col col-ls-20 col-ms-9 col-ss-9">
										<div class="document-wrapper">
											<i class="account-pdf-icon">pdf</i>
											<p class="col-text text-5 text-cl-2 left">Decembar 2016</p>
										</div>
									</div>
									
									<div class="col col-ls-3 col-ms-3 col-ss-3">
										<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
									</div>
								</div>
								<div class="table-row row-1 grid">
									<div class="col col-ls-20 col-ms-9 col-ss-9">
										<div class="document-wrapper">
											<i class="account-pdf-icon">pdf</i>
											<p class="col-text text-5 text-cl-2 left">Decembar 2016</p>
										</div>
									</div>
									
									<div class="col col-ls-3 col-ms-3 col-ss-3">
										<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
									</div>
								</div>
								<div class="table-row row-1 grid">
									<div class="col col-ls-20 col-ms-9 col-ss-9">
										<div class="document-wrapper">
											<i class="account-pdf-icon">pdf</i>
											<p class="col-text text-5 text-cl-2 left">Decembar 2016</p>
										</div>
									</div>
									
									<div class="col col-ls-3 col-ms-3 col-ss-3">
										<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
									</div>
								</div>
								<div class="table-row row-1 grid">
									<div class="col col-ls-20 col-ms-9 col-ss-9">
										<div class="document-wrapper">
											<i class="account-pdf-icon">pdf</i>
											<p class="col-text text-5 text-cl-2 left">Decembar 2016</p>
										</div>
									</div>
									
									<div class="col col-ls-3 col-ms-3 col-ss-3">
										<a href="#" class="btn-1 xs-btn color-transparent">Preuzmi</a>
									</div>
								</div>
							</div>
						</div>
						

						<div class="button-wrap">
							<a href="#" class="btn-2 color-1 alignright" data-method="showMore">Prikaži još izvoda</a>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
<?php Site::getFooter(); ?>
