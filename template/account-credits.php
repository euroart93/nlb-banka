<?php
Site::getHeader(); ?>

<div id="main">

	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li class="selected">Detalji kredita</li></a>
				<a href="#"><li>Lista transakcija</li></a>
			</ul>
		</div>
	</div>

	<div class="widget widget-select-account widget-content-white row" data-method="accountSelect">
			<div class="account-selected">
				<div class="col-ss-12 col-ls-2 acc-img">
					<img src="img/demo/acc-img-9.jpg">
				</div>
				<div class="col-ss-12 col-ls-6 col-border">
					<div class="content">
						<p class="small">Ime kredita:</p>
						<p class="big acc-name">NLB Auto kredit</p>
					</div>
				</div>
				<div class="col-ss-12 col-ls-7 col-border">
					<div class="content">
						<p class="small">Broj ugovora:</p>
						<p class="big-s acc-number">8051200000234</p>
					</div>
				</div>
				<div class="col-ss-12 col-ls-7 col-border">
					<div class="content">
						<p class="small float-right">Preostali dug:</p>
						<p class="big acc-amount float-right">4.653.543,89 RSD</p>
					</div>
				</div>
				<div class="col-ss-1">
					<a href="#" class="acc-list-btn acc-drop-btn" type="button" data-method="accountSelectTrigger">
						<div class="triangle"></div>
					</a>
				</div>
			</div>
			<div class="account-select">
      			<ul class="options">
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime kredita:</p>
									<p class="big acc-name">NLB Auto kredit 2</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj ugovora:</p>
									<p class="big-s acc-number">8051200000235</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="small float-right">Preostali dug:</p>
									<p class="big acc-amount float-right">9.653.543,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/car3.png">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime kredita:</p>
									<p class="big acc-name">NLB Auto kredit 3</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj ugovora:</p>
									<p class="big-s acc-number">8051200000237</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="small float-right">Preostali dug:</p>
									<p class="big acc-amount float-right">2.653.543,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
									<div class="triangle-right-white"></div>
									<img class="desaturate" src="img/demo/car2.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime kredita:</p>
									<p class="big acc-name">NLB Auto kredit 4</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj ugovora:</p>
									<p class="big-s acc-number">8051200000239</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="small float-right">Preostali dug:</p>
									<p class="big acc-amount float-right">6.653.543,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
									<div class="triangle-right-white"></div>
									<img class="desaturate" src="img/demo/car1.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime kredita:</p>
									<p class="big acc-name">NLB Auto kredit 6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj ugovora:</p>
									<p class="big-s acc-number">8051200000235</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="small float-right">Preostali dug:</p>
									<p class="big acc-amount float-right">3.653.543,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
       				 
    			</ul>
			</div>
		</div>

	<div class="main-content">
		
		<div class="widget content-white payment-widget">
			<div class="widget-header header-dark">
				<div class="cnt-2">
					<div class="col-left">
						<h3 class="title-4 color-2"><span>Detalji računa</span></h3>
					</div>
					<div class="col-right">
						<ul class="widget-actions bg-widget"  data-method="BGoptionsTrigger">
							<p class="widget-name">Opcije</p>
							<li>
								<div class="widget-options">
			            			<ul class="trigger" data-method="optionsTrigger">
			            				<li></li>
			            				<li></li>
			            				<li></li>
			            			</ul>
			            			<ul class="options-list">
			            				<li><a href="#">Uplati na štedni račun</a></li>
			            				<li><a href="#">Prebaci sredstva</a></li>
			            				<li><a href="#">Promeni naziv računa</a></li>
			            				<li><a href="#">Promeni sliku računa</a></li>
			            				<li><a href="#">Lista transakcija po računu</a></li>
			            				<li><a href="#">Lista izvoda</a></li>
			            			</ul>
			            		</div>
							</li>
						</ul>
					</div>
				</div>

			</div>

			<div class="widget-slat">
				<div class="col-left">
					<div class="account-thumbnail">
						<div class="account-thumbnail-bg thumbnail-pic-3 grayscale"></div>
						<img class="account-thumbnail-pic" src="img/demo/acc-img-9.jpg" alt="">
						<p class="account-thumbnail-name">NLB Auto kredita</p>
						<p class="account-thumbnail-number">80512000000234</p>
					</div>
				</div>
				<div class="col-right account-savings cf">
					<div class="plain-list">
						<div>
							<p class="key">Korisnik kredita:</p>
							<p class="value bigger">Ana Marković</p>
						</div>
						<div>
							<p class="key">Odobreni iznos kredita:</p>
							<p class="value">6.000.000,00 RSD</p>
						</div>
						<div>
							<p class="key">Preostali dug:</p>
							<p class="value">4.692.390,00 RSD</p>
						</div>
						<div>
							<p class="key">Dospele obaveze:</p>
							<p class="value">36.000,00 RSD<a href="#" class="btn-pay">plati</a></p>
						</div>
						<div>
							<p class="key">Iznos anuiteta:</p>
							<p class="value">36.000,00 RSD</p>
						</div>
						<div class="dropdown">
							<div>
								<p class="key">Nedospela glavnica:</p>
								<p class="value">3.250.000,00 RSD</p>
							</div>
							<div>
								<p class="key">Broj ugovora:</p>
								<p class="value">80512000000234</p>
							</div>
							<div>
								<p class="key">Iznos posljednje promene:</p>
								<p class="value  rise change-drop">-25.250,00</p>
							</div>
							<div>
								<p class="key">Datum posljednje promene:</p>
								<p class="value" >16.02.2017.</p>
							</div>
							<div>
								<p class="key">Iznos sledeće rate:</p>
								<p class="value">25.000,00 RSD</p>
							</div>
							<div>
								<p class="key">Datum sledeće rate:</p>
								<p class="value">16.03.2017</p>
							</div>
							<div>
								<p class="key">Nominalna kamatna stopa:</p>
								<p class="value">Ref.KS NBS + 6,00%</p>
							</div>
							<div>
								<p class="key">Tip kredita:</p>
								<p class="value">NLB Auto kredit</p>
							</div>
							<div>
								<p class="key">Rok otplate:</p>
								<p class="value">72 meseca</p>
							</div>
						</div>

					</div>
					<a href="#" class="btn-2 color-1 alignright" data-method="panelDropTrigger">Prikaži više detalja</a>
				</div>
			</div>

		</div>
	</div>

</div>

<?php Site::getFooter(); ?>
