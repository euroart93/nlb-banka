<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li>Kursna lista</li></a>
				<a href="#"><li class="selected">Kupovina/prodaja deviza</li></a>
				<a href="#"><li>Lista menjačkih transakcija</li></a>
			</ul>
		</div>
	</div>


	<div class="main-content">
		<form action="#" method="get">

			<div class="widget content-white left-padding payment-widget payment-steps m-top">
				<h3 class="title-4 color-2"><span>Kupovina/prodaja deviza</span></h3>
				<ul class="steps left">
					<li>1 <span>Menjački nalog</span></li>
					<li>2 <span>Provera unosa</span></li>
					<li class="active">3 Potvrda naloga</li>
				</ul>

				<div class="widget-slat">
					<div class="col-left">
						<div class="successfull">
							<img class="successfull-icon" src="img/icons/icon-check-7.png">
							<p class="successfull-title"><strong>Nalog uspješno primljen</p>
						</div>
					</div>
						<div class="col-right">
							<p class="steps-p">Vaš nalog je uspješno primljen i zaveden pod jedinstvenim brojem <color>132664-654</color> dalje promene<br> statusa Vašeg naloga možete pratiti kroz <color><a href="#" >listu menjačkih transakcija</a></color></p>

							<ul class="side-modal-account-options pdf-download">
								<h4 class="title-modal-options color-2 ">Opcije</h4>
									
								<a href="#">
									<li>Nova menjačka transakcija
									<i class="side-modal-options-triangle"></i>
									</li>
								</a>

								<a href="#">
									<li>Preuzmi potvrdu u PDF formatu
									<i class="side-modal-options-triangle"></i>
									</li>
								</a>	
							</ul>	
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

</div>

<?php Site::getFooter(); ?>