<?php
Site::getHeader('header-login'); ?>

<div class="login-main">

	<!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
	<div class="page-cover inner-video video-cover img-overlay-blur">
		<img class="desktop-img promo-bg-0 slide-active" src="img/bg/login-bg-blur.png" alt="cover" data-object-fit="cover">
	</div>

	<div class="prelogin-full">
		<div class="inner">
			<div class="col-left">
				<h2>Pronađi NLB</h2>
				<div class="custom-form">
					<div class="group">
						<div class="group-inner input-search">
							<input type="search" class="input-1" placeholder="Traži...">
							<button type="button" class="search-btn"></button>
						</div>
					</div>
					<div class="group">
						<div class="group-inner">
							<div class="select-3">
								<select data-method="customSelect3">
		        					<option value="0">Ekspoziture i bankomati</option>
		        					<option value="1">Ekspoziture i bankomati</option>
		        					<option value="2">Ekspoziture i bankomati</option>
		        				</select>
		        			</div>
						</div>
					</div>
					<div class="group">
						<div class="group-inner">
							<div class="select-3">
								<select data-method="finderSelect customSelect3">
		        					<option value="1">Prikaži na mapi</option>
		        					<option value="2">List view</option>
		        				</select>
		        			</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-right prelogin-map">
				<div class="col-content content-1 active">
					<div class="map-wrapper">
						<div class="infobox-wrapper">
							<div id="infobox1" class="infobox bg-atm">
						        <p class="loc-title">ATM</p>
						        <p class="loc-info">Jurija Gagarina 12<br>
						        	Beograd
						        </p>
						    </div>
						    <div id="infobox2" class="infobox bg-branch">
						        <p class="loc-title">Branch</p>
						        <p class="loc-info">Jurija Gagarina 22<br>
						        	Beograd
						        </p>
						    </div>
						     <div id="infobox3" class="infobox bg-atm">
						        <p class="loc-title">ATM</p>
						        <p class="loc-info">Jurija Gagarina 32<br>
						        	Beograd
						        </p>
						    </div>
						    <div id="infobox4" class="infobox bg-branch">
						        <p class="loc-title">Branch</p>
						        <p class="loc-info">Jurija Gagarina 42<br>
						        	Beograd
						        </p>
						    </div>
						     <div id="infobox5" class="infobox bg-branch">
						        <p class="loc-title">Branch</p>
						        <p class="loc-info">Jurija Gagarina 52<br>
						        	Beograd
						        </p>
						    </div>
						</div>
						<div id="map-canvas" data-method="googleMapATM"></div>
					</div>
				</div>
				<div class="col-content content-2">
					<ul class="map-list" data-method="customScroll">
						<li>
							<div class="item-main has-content" data-method="mapList">
								<i class="icon-branch-2"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">Branch</p>
							</div>
							<div class="hidden-content">
								<ul class="item-info">
									<li>
										<i class="icon-tel-1"></i>
										<p class="info-txt-1">Contact phone</p>
										<p class="info-txt-2">+40 21 1234 567</p>
									</li>
									<li>
										<i class="icon-clock-1"></i>
										<p class="info-txt-1">Working hours</p>
										<p class="info-txt-2">Mon-Fri 09:00 - 20:00, Sat 09:00 - 13:00</p>
									</li>
									<li>
										<i class="icon-mail-1"></i>
										<p class="info-txt-1">E-mail address</p>
										<p class="info-txt-2"><a href="#">branch@bank.com</a></p>
									</li>
								</ul>
							</div>
						</li>
						<li>
							<div class="item-main">
								<i class="icon-atm-1"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">ATM</p>
							</div>
						</li>
						<li>
							<div class="item-main">
								<i class="icon-atm-1"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">ATM</p>
							</div>
						</li>
						<li>
							<div class="item-main has-content" data-method="mapList">
								<i class="icon-branch-2"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">Branch</p>
							</div>
							<div class="hidden-content">
								<ul class="item-info">
									<li>
										<i class="icon-tel-1"></i>
										<p class="info-txt-1">Contact phone</p>
										<p class="info-txt-2">+40 21 1234 567</p>
									</li>
									<li>
										<i class="icon-clock-1"></i>
										<p class="info-txt-1">Working hours</p>
										<p class="info-txt-2">Mon-Fri 09:00 - 20:00, Sat 09:00 - 13:00</p>
									</li>
									<li>
										<i class="icon-mail-1"></i>
										<p class="info-txt-1">E-mail address</p>
										<p class="info-txt-2"><a href="#">branch@bank.com</a></p>
									</li>
								</ul>
							</div>
						</li>
						<li>
							<div class="item-main">
								<i class="icon-atm-1"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">ATM</p>
							</div>
						</li>
						<li>
							<div class="item-main has-content" data-method="mapList">
								<i class="icon-branch-2"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">Branch</p>
							</div>
							<div class="hidden-content">
								<ul class="item-info">
									<li>
										<i class="icon-tel-1"></i>
										<p class="info-txt-1">Contact phone</p>
										<p class="info-txt-2">+40 21 1234 567</p>
									</li>
									<li>
										<i class="icon-clock-1"></i>
										<p class="info-txt-1">Working hours</p>
										<p class="info-txt-2">Mon-Fri 09:00 - 20:00, Sat 09:00 - 13:00</p>
									</li>
									<li>
										<i class="icon-mail-1"></i>
										<p class="info-txt-1">E-mail address</p>
										<p class="info-txt-2"><a href="#">branch@bank.com</a></p>
									</li>
								</ul>
							</div>
						</li>
						<li>
							<div class="item-main">
								<i class="icon-atm-1"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">ATM</p>
							</div>
						</li>
						<li>
							<div class="item-main">
								<i class="icon-atm-1"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">ATM</p>
							</div>
						</li>
						<li>
							<div class="item-main has-content" data-method="mapList">
								<i class="icon-branch-2"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">Branch</p>
							</div>
							<div class="hidden-content">
								<ul class="item-info">
									<li>
										<i class="icon-tel-1"></i>
										<p class="info-txt-1">Contact phone</p>
										<p class="info-txt-2">+40 21 1234 567</p>
									</li>
									<li>
										<i class="icon-clock-1"></i>
										<p class="info-txt-1">Working hours</p>
										<p class="info-txt-2">Mon-Fri 09:00 - 20:00, Sat 09:00 - 13:00</p>
									</li>
									<li>
										<i class="icon-mail-1"></i>
										<p class="info-txt-1">E-mail address</p>
										<p class="info-txt-2"><a href="#">branch@bank.com</a></p>
									</li>
								</ul>
							</div>
						</li>
						<li>
							<div class="item-main">
								<i class="icon-atm-1"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">ATM</p>
							</div>
						</li>
						<li>
							<div class="item-main has-content" data-method="mapList">
								<i class="icon-branch-2"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">Branch</p>
							</div>
							<div class="hidden-content">
								<ul class="item-info">
									<li>
										<i class="icon-tel-1"></i>
										<p class="info-txt-1">Contact phone</p>
										<p class="info-txt-2">+40 21 1234 567</p>
									</li>
									<li>
										<i class="icon-clock-1"></i>
										<p class="info-txt-1">Working hours</p>
										<p class="info-txt-2">Mon-Fri 09:00 - 20:00, Sat 09:00 - 13:00</p>
									</li>
									<li>
										<i class="icon-mail-1"></i>
										<p class="info-txt-1">E-mail address</p>
										<p class="info-txt-2"><a href="#">branch@bank.com</a></p>
									</li>
								</ul>
							</div>
						</li>
						<li>
							<div class="item-main">
								<i class="icon-atm-1"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">ATM</p>
							</div>
						</li>
						<li>
							<div class="item-main">
								<i class="icon-atm-1"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">ATM</p>
							</div>
						</li>
						<li>
							<div class="item-main has-content" data-method="mapList">
								<i class="icon-branch-2"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">Branch</p>
							</div>
							<div class="hidden-content">
								<ul class="item-info">
									<li>
										<i class="icon-tel-1"></i>
										<p class="info-txt-1">Contact phone</p>
										<p class="info-txt-2">+40 21 1234 567</p>
									</li>
									<li>
										<i class="icon-clock-1"></i>
										<p class="info-txt-1">Working hours</p>
										<p class="info-txt-2">Mon-Fri 09:00 - 20:00, Sat 09:00 - 13:00</p>
									</li>
									<li>
										<i class="icon-mail-1"></i>
										<p class="info-txt-1">E-mail address</p>
										<p class="info-txt-2"><a href="#">branch@bank.com</a></p>
									</li>
								</ul>
							</div>
						</li>
						<li>
							<div class="item-main">
								<i class="icon-atm-1"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">ATM</p>
							</div>
						</li>
						<li>
							<div class="item-main has-content" data-method="mapList">
								<i class="icon-branch-2"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">Branch</p>
							</div>
							<div class="hidden-content">
								<ul class="item-info">
									<li>
										<i class="icon-tel-1"></i>
										<p class="info-txt-1">Contact phone</p>
										<p class="info-txt-2">+40 21 1234 567</p>
									</li>
									<li>
										<i class="icon-clock-1"></i>
										<p class="info-txt-1">Working hours</p>
										<p class="info-txt-2">Mon-Fri 09:00 - 20:00, Sat 09:00 - 13:00</p>
									</li>
									<li>
										<i class="icon-mail-1"></i>
										<p class="info-txt-1">E-mail address</p>
										<p class="info-txt-2"><a href="#">branch@bank.com</a></p>
									</li>
								</ul>
							</div>
						</li>
						<li>
							<div class="item-main">
								<i class="icon-atm-1"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">ATM</p>
							</div>
						</li>
						<li>
							<div class="item-main">
								<i class="icon-atm-1"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">ATM</p>
							</div>
						</li>
						<li>
							<div class="item-main has-content" data-method="mapList">
								<i class="icon-branch-2"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">Branch</p>
							</div>
							<div class="hidden-content">
								<ul class="item-info">
									<li>
										<i class="icon-tel-1"></i>
										<p class="info-txt-1">Contact phone</p>
										<p class="info-txt-2">+40 21 1234 567</p>
									</li>
									<li>
										<i class="icon-clock-1"></i>
										<p class="info-txt-1">Working hours</p>
										<p class="info-txt-2">Mon-Fri 09:00 - 20:00, Sat 09:00 - 13:00</p>
									</li>
									<li>
										<i class="icon-mail-1"></i>
										<p class="info-txt-1">E-mail address</p>
										<p class="info-txt-2"><a href="#">branch@bank.com</a></p>
									</li>
								</ul>
							</div>
						</li>
						<li>
							<div class="item-main">
								<i class="icon-atm-1"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">ATM</p>
							</div>
						</li>
						<li>
							<div class="item-main has-content" data-method="mapList">
								<i class="icon-branch-2"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">Branch</p>
							</div>
							<div class="hidden-content">
								<ul class="item-info">
									<li>
										<i class="icon-tel-1"></i>
										<p class="info-txt-1">Contact phone</p>
										<p class="info-txt-2">+40 21 1234 567</p>
									</li>
									<li>
										<i class="icon-clock-1"></i>
										<p class="info-txt-1">Working hours</p>
										<p class="info-txt-2">Mon-Fri 09:00 - 20:00, Sat 09:00 - 13:00</p>
									</li>
									<li>
										<i class="icon-mail-1"></i>
										<p class="info-txt-1">E-mail address</p>
										<p class="info-txt-2"><a href="#">branch@bank.com</a></p>
									</li>
								</ul>
							</div>
						</li>
						<li>
							<div class="item-main">
								<i class="icon-atm-1"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">ATM</p>
							</div>
						</li>
						<li>
							<div class="item-main">
								<i class="icon-atm-1"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">ATM</p>
							</div>
						</li>
						<li>
							<div class="item-main has-content" data-method="mapList">
								<i class="icon-branch-2"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">Branch</p>
							</div>
							<div class="hidden-content">
								<ul class="item-info">
									<li>
										<i class="icon-tel-1"></i>
										<p class="info-txt-1">Contact phone</p>
										<p class="info-txt-2">+40 21 1234 567</p>
									</li>
									<li>
										<i class="icon-clock-1"></i>
										<p class="info-txt-1">Working hours</p>
										<p class="info-txt-2">Mon-Fri 09:00 - 20:00, Sat 09:00 - 13:00</p>
									</li>
									<li>
										<i class="icon-mail-1"></i>
										<p class="info-txt-1">E-mail address</p>
										<p class="info-txt-2"><a href="#">branch@bank.com</a></p>
									</li>
								</ul>
							</div>
						</li>
						<li>
							<div class="item-main">
								<i class="icon-atm-1"></i>
								<p class="name">Bulevardul Eroilor 232, Bucharest</p>
								<p class="cat-name">ATM</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="login-footer">
		<div class="inner">
			<!--<ul class="lang-nav">
				<li class="active"><a href="#">ENG</a></li>
				<li><a href="#">SRB</a></li>
			</ul>-->
			<ul class="social">
				<li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
				<li class="youtube"><a href="#" class="icon-youtube">Youtube</a></li>
				<li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
				<li class="history"><a href="#" class="icon-history">History</a></li>
			</ul>
		</div>
	</div>


</div>


</div><!-- /Wrapper -->
