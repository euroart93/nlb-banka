<!DOCTYPE html>
<!--[if IE 8 ]><html lang="hr" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="hr" class="no-js ie9"> <![endif]-->
<html lang="hr">

<head>
	<meta charset="utf-8">
	<title>NLB</title>

	<link id="style" href="<?php echo Site::url('/css/style.css'); ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?php echo Site::url('/css/1200.css') ?>" type="text/css"  media="only screen and (max-width: 1200px)">
	<link rel="stylesheet" href="<?php echo Site::url('/css/980.css') ?>" type="text/css"  media="only screen and (max-width: 980px)">
	<link rel="stylesheet" href="<?php echo Site::url('/css/720.css') ?>" type="text/css"  media="only screen and (max-width: 720px)">
	<link rel="stylesheet" href="<?php echo Site::url('/css/600.css') ?>" type="text/css"  media="only screen and (max-width: 600px)">
	<link rel="stylesheet" href="<?php echo Site::url('/css/450.css') ?>" type="text/css"  media="only screen and (max-width: 450px)">

	<link rel="stylesheet" href="<?php echo Site::url('/css/jquery-ui.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?php echo Site::url('/css/slick.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?php echo Site::url('/css/FancySelect.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?php echo Site::url('/css/ddslick.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?php echo Site::url('/css/uikit.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?php echo Site::url('/css/jquery.mCustomScrollbar.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?php echo Site::url('/css/introjs.css') ?>" type="text/css">

	<link rel="shortcut icon" href="<?php echo Site::url('/img/icons/favicon-1.png'); ?>">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<meta property="og:site_name" content="">
	<meta property="og:title" content="">
	<meta property="og:image" content="">
	<meta property="og:description" content="">

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700' rel='stylesheet' type='text/css'>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/mootools/1.4.1/mootools-yui-compressed.js"></script>
	<script defer src="<?php echo Site::url('/js/script.js') ?>"></script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBt2prP_feQ7VlmQ2NCR9WqFqgHfoT1us"></script>
	<script src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>

</head>

<body class="<?php echo Site::$bodyClass; ?>">

	<div class="mobile-menu">
	 	<div class="profile-bar">
	    	<!--<div class="profile-img">
		    	<div class="img-box">
		    		<img src="img/demo/profile-1.jpg" alt="profile-img">
		    	</div>
	    		<span>24</span>
		    </div-->
		    <div class="profile-icons">
		    	<a href="#" class="icon-inbox active"><span class="notice">24</span></a>
	    		<a href="#" class="icon-settings-1"></a>
	    		<a href="#" class="icon-out-1"></a>
		    </div>
	    </div>
		<ul class="main-nav">
    		<li><a href="#" class="active"><i class="icon-dashboard-2"></i>Početna</a></li>
	    	<li><a href="#"><i class="icon-safe-3"></i>Plaćanja</a></li>
	    	<li><a href="#"><i class="icon-card-2"></i>Menjačnica</a></li>
	    	<li class="has-menu">
	    		<a href="#" class="subnav-trigger" data-method="subnavMobile"><i class="icon-money-2"></i>Payments</a>

	    		<ul class="submenu">
	    			<li><a href="#">Internal Payment</a></li>
	    			<li class="active"><a href="#">P2P Payment</a></li>
	    			<li><a href="#">External Payment</a></li>
	    			<li><a href="#">Bills on Click</a></li>
	    		</ul>
	    	</li>
	    	<li><a href="#"><i class="icon-exchange-2"></i>Štednja</a></li>
	    	<li><a href="#"><i class="icon-contact-2"></i>Kartice</a></li>
	    	<li><a href="#"><i class="icon-catalogue-3"></i>Kredit</a></li>
	    </ul>

			<div class="hidden-lang login-footer"> <!--language nav header .login-footer just for style-->
				<ul class="lang-nav">
					<li class="active"><a href="#">SRB</a></li>
					<li><a href="#">ENG</a></li>
				</ul>
			</div>

	</div>

	<div id="wrapper">

	<!-- Header -->
	<header id="page-header" role="banner">
	    <div class="container">
			<div class="site-branding">
				<h1 class="site-logo"><a href="<?php echo Site::url(dashboard) ?>"  title="Digital Edge">Digital Edge</a></h1>
			</div>

			<ul class="main-nav">
		    	<li class="active"><a href="#"><i class="icon-dashboard-1"></i>Početna</a></li>
		    	<li><a href="#"><i class="icon-safe-1"></i>Plaćanja</a></li>
		    	<li><a href="#"><i class="icon-card-1"></i>Menjačnica</a></li>
		    	<li class="">
		    		<a href="#" class="subnav-trigger" data-method="subnavTrigger"><i class="icon-money-1"></i>Računi</a>
					<div class="submenu-full">
						<div class="container">
							<ul>
								<li class="active"><a href="#">Detalji računa</a></li>
								<li><a href="#">Lista transakcija</a></li>
								<li><a href="#">Lista izvoda</a></li>
							</ul>
						</div>
					</div>
		    	</li>
		    	<li><a href="#"><i class="icon-exchange-1"></i>Štednja</a></li>
		    	<li><a href="#"><i class="icon-contact-1"></i>Kartice</a></li>
		    	<li><a href="#"><i class="icon-catalogue-3"></i>Kredit</a></li>
		    </ul>

				<div class="header-lang login-footer"> <!--language nav header .login-footer just for style-->
					<ul class="lang-nav">
						<li class="active"><a href="#">SRB</a></li>
						<li><a href="#">ENG</a></li>
					</ul>
				</div>

		    <div class="profile-bar">
		    	<!--<div class="profile-img">
			    	<div class="img-box">
			    		<img src="img/demo/profile-1.jpg" alt="profile-img">
			    	</div>
		    		<span>24</span>
			    </div>-->
			    <a href="#" class="icon-inbox active"><span class="notice">24</span></a>
		    	<a href="#" class="icon-settings-1"></a>
		    	<a href="#" class="icon-out-1"></a>
		    </div>

		    <div class="menu-btn" data-method="menuTrigger">
		    	<ul>
		    		<li class="first"></li>
		    		<li class="second"></li>
		    		<li class="third"></li>
		    	</ul>
		    </div>
	    </div>



	</header>
	<!-- /Header -->
