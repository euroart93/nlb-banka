<!DOCTYPE html>
<!--[if IE 8 ]><html lang="hr" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="hr" class="no-js ie9"> <![endif]-->
<html lang="hr">

<head>
	<meta charset="utf-8">
	<title>NLB Login</title>

	<link id="style" href="<?php echo Site::url('/css/style.css'); ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?php echo Site::url('/css/1200.css') ?>" type="text/css"  media="only screen and (max-width: 1200px)">
	<link rel="stylesheet" href="<?php echo Site::url('/css/980.css') ?>" type="text/css"  media="only screen and (max-width: 980px)">
	<link rel="stylesheet" href="<?php echo Site::url('/css/720.css') ?>" type="text/css"  media="only screen and (max-width: 720px)">
	<link rel="stylesheet" href="<?php echo Site::url('/css/600.css') ?>" type="text/css"  media="only screen and (max-width: 600px)">
	<link rel="stylesheet" href="<?php echo Site::url('/css/450.css') ?>" type="text/css"  media="only screen and (max-width: 450px)">

	<link rel="stylesheet" href="<?php echo Site::url('/css/jquery.mCustomScrollbar.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?php echo Site::url('/css/jquery-ui.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?php echo Site::url('/css/uikit.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?php echo Site::url('/css/slick.css') ?>" type="text/css">

	<link rel="shortcut icon" href="<?php echo Site::url('/img/icons/favicon.png'); ?>">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<meta property="og:site_name" content="">
	<meta property="og:title" content="">
	<meta property="og:image" content="">
	<meta property="og:description" content="">

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700' rel='stylesheet' type='text/css'>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script defer src="<?php echo Site::url('/js/script.js') ?>"></script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBt2prP_feQ7VlmQ2NCR9WqFqgHfoT1us"></script>
	<script src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>

</head>

<body class="<?php echo Site::$bodyClass; ?>">

	

	<div class="mobile-menu">
		<ul class="main-nav">
			<?php if(Site::$page == 'prelogin'): ?>
	    	<li><a href="#" class="active"><i class="icon-login"></i>Prijava</a></li>
	    	<?php else: ?>
	    	<li><a href="#"><i class="icon-login"></i>Prijava</a></li>
	    	<?php endif; ?>

	    	<li><a href="#"><i class="icon-contact"></i>Kontakti</a></li>
	    	<li><a href="#"><i class="icon-exchange"></i>Kursna lista</a></li>

			<?php if(Site::$page == 'prelogin-atm'): ?>
	    	<li><a href="#" class="active"><i class="icon-pin"></i>Pronađi NLB</a></li>
	    	<?php else: ?>
	    	<li><a href="#"><i class="icon-pin"></i>Pronađi NLB</a></li>
	    	<?php endif; ?>

	    	<li><a href="#"><i class="icon-catalogue"></i>Proizvodi</a></li>
	    	<li><a href="#"><i class="icon-eye"></i>Demo</a></li>
	    	<li><a href="#"><i class="icon-question"></i>Pomoć</a></li>
	    </ul>

			<div class="hidden-lang login-footer"> <!--hidden language nav header .login-footer just for style-->
				<ul class="lang-nav">
					<li class="active"><a href="#">SRB</a></li>
					<li><a href="#">ENG</a></li>
				</ul>
			</div>

	</div>

	<div id="wrapper">

		<!-- Header -->
		<header id="login-header">

		    <div class="container">
				<div class="site-branding">
					<h1 class="site-logo"><a href="<?php echo Site::url() ?>"  title="NLB Banka">NLB Banka</a></h1>
				</div>
		    </div>

		    <ul class="main-nav">
		    	<?php if(Site::$page == 'prelogin'): ?>
		    	<li><a href="#" class="active"><i class="icon-login"></i>Prijava</a></li>
		    	<?php else: ?>
		    	<li><a href="#"><i class="icon-login"></i>Prijava</a></li>
		    	<?php endif; ?>

		    	<li><a href="#"><i class="icon-contact"></i>Kontakti</a></li>
		    	<li><a href="#"><i class="icon-exchange"></i>Kursna lista</a></li>

		    	<?php if(Site::$page == 'prelogin-atm'): ?>
		    	<li><a href="#" class="active"><i class="icon-pin"></i>Pronađi NLB</a></li>
		    	<?php else: ?>
		    	<li><a href="#"><i class="icon-pin"></i>Pronađi NLB</a></li>
		    	<?php endif; ?>

		    	<li><a href="#"><i class="icon-catalogue"></i>Proizvodi</a></li>
		    	<li><a href="#"><i class="icon-eye"></i>Demo</a></li>
		    	<li><a href="#"><i class="icon-question"></i>Pomoć</a></li>
		    </ul>


				<div class="header-lang login-footer"> <!--language nav header .login-footer just for style-->
					<ul class="lang-nav">
						<li class="active"><a href="#">SRB</a></li>
						<li><a href="#">ENG</a></li>
					</ul>
				</div>





		    <ul class="menu-btn with-notify" data-method="menuTrigger">
		    	<li class="first"></li>
		    	<li class="second"></li>
		    	<li class="third"></li>
		    </ul>


		</header>
		<!-- /Header -->
