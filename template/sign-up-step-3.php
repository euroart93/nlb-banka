<?php 
Site::getHeader('header-login'); ?>

<div class="login-main">

  <!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
  <div class="page-cover inner-video video-cover img-overlay">
    <img class="desktop-img" src="img/bg/login-bg.jpg" alt="cover" data-object-fit="cover">
    <img class="mobile-img" src="img/bg/login-mobile.jpg" alt="cover" data-object-fit="cover">
  </div>

  <div class="prelogin-full sign-up-steps">
    <div class="inner">
      <div class="col-head">
        <a href="#" class="close">Close</a>
      </div>
      <div class="col-left">
        <h2>Now create your <br> username &amp; <br> password</h2>
      </div>
      <div class="col-right">
        <ul class="login-steps">
          <li>1</li>
          <li>2</li>
          <li class="active">3 Login data</li>
          <li>4</li>
          <li>5</li>
          <li>6</li>
          <li>7</li>
        </ul>
        <form action="#" method="#">
          <div class="scrollable-content" data-method="customScroll">

            <div class="custom-form uk-grid">

              <div class="group uk-width-1-1">
                <div class="group-inner">
                  <label class="label-1">Username:</label>
                  <input type="text" class="input-1" value="JuricaV">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner password-hint">
                  <label class="label-1">Password:</label>
                  <input type="password" class="input-1" value="SomeSasSword">
                  <a href="#" class="hint">?</a>
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Repeat password:</label>
                  <input type="password" class="input-1" value="SomeSasSword">
                </div>
              </div>

             </div>

             <div class="grey-divider"></div>

             <div class="hint-picure-wrapper">
                <p>Choose your hint picture:</p>
                <ul class="hint-pictures" data-method="hintImgSelection">
                  <li><img src="<?php echo Site::url() ?>/img/photos/norway.png" alt="Hint picture"></li>
                  <li><img src="<?php echo Site::url() ?>/img/photos/norway.png" alt="Hint picture"></li>
                  <li><img src="<?php echo Site::url() ?>/img/photos/norway.png" alt="Hint picture"></li>
                  <li class="active"><img src="<?php echo Site::url() ?>/img/photos/norway.png" alt="Hint picture"></li>
                  <li><img src="<?php echo Site::url() ?>/img/photos/norway.png" alt="Hint picture"></li>
                  <li><img src="<?php echo Site::url() ?>/img/photos/norway.png" alt="Hint picture"></li>
                  <li><img src="<?php echo Site::url() ?>/img/photos/norway.png" alt="Hint picture"></li>
                  <li><img src="<?php echo Site::url() ?>/img/photos/norway.png" alt="Hint picture"></li>
                  <li><img src="<?php echo Site::url() ?>/img/photos/norway.png" alt="Hint picture"></li>
                  <li><img src="<?php echo Site::url() ?>/img/photos/norway.png" alt="Hint picture"></li>
                  <li><img src="<?php echo Site::url() ?>/img/photos/norway.png" alt="Hint picture"></li>
                  <li><img src="<?php echo Site::url() ?>/img/photos/norway.png" alt="Hint picture"></li>
                </ul>
                <p>Hint: Remember your chosen picture, it will aid you during login process.</p>
             </div>

          </div>

          <div class="button-actions">
            <a href="#" class="btn-1 color-3">Back</a>
            <a href="#" class="btn-1 color-1">Next step</a>
          </div>

        </form>
      </div>
    </div>
  </div>

  <div class="login-footer">
    <div class="inner">
      <ul class="lang-nav">
        <li class="active"><a href="#">ENG</a></li>
        <li><a href="#">SRB</a></li>
      </ul>
      <ul class="social">
        <li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
        <li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
        <li class="twitter"><a href="#" class="icon-twitter">Twitter</a></li>
      </ul>
    </div>
  </div>


</div>


</div><!-- /Wrapper -->


