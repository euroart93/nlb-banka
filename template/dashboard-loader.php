<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="accounts-widget">
		<div class="bg-overlay overlay-1"></div>
		<div id="account-slider" class="swiper-container account-tab tab-1 active" data-method="accountSlider">
	        <!-- Slides -->
            <div class="slide">
            	<div class="account-box account-bg-1">
            		<div class="widget-options">
            			<ul class="trigger" data-method="optionsTrigger">
            				<li></li>
            				<li></li>
            				<li></li>
            			</ul>
            			<ul class="options-list">
            				<li><a href="#">Option 1</a></li>
            				<li><a href="#">Option 2</a></li>
            				<li><a href="#">Option 3</a></li>
            				<li><a href="#">Option 4</a></li>
            			</ul>
            		</div>
            		<div class="account-main">
            			<div class="acc-img">
            				<img src="img/demo/acc-img.jpg" alt="acc-image">
            			</div>
	            		<p class="acc-nr">115-0000000000567898-65</p>
	            		<p class="acc-name">My Account</p>
	            	</div>
	            	<div class="balance-slat">
	            		<p class="acc-balance-txt">Current balance:</p>
	            		<div class="main-balance">
	            			<div class="select-2">
	            				<select data-method="customSelect">
	            					<option value="0">RSD</option>
	            					<option value="1">EUR</option>
	            					<option value="2">USD</option>
	            					<option value="3">GBP</option>
	            				</select>
	            			</div>
	            			<p>5.125.893,49</p>
	            		</div>
	            		<p class="transaction-txt">Last transaction | 24.10.2015</p>
	            		<p class="small-balance">7.291,00 RSD</p>
	            	</div>
	            	<a href="#" class="account-btn icon-money-2"></a>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box account-bg-1">
            		<div class="widget-options">
            			<ul class="trigger" data-method="optionsTrigger">
            				<li></li>
            				<li></li>
            				<li></li>
            			</ul>
            			<ul class="options-list">
            				<li><a href="#">Option 1</a></li>
            				<li><a href="#">Option 2</a></li>
            				<li><a href="#">Option 3</a></li>
            				<li><a href="#">Option 4</a></li>
            			</ul>
            		</div>
            		<div class="account-main">
            			<div class="acc-img">
            				<img src="img/demo/acc-img.jpg" alt="acc-image">
            			</div>
	            		<p class="acc-nr">115-0000000000567898-65</p>
	            		<p class="acc-name">My Current Account</p>
	            	</div>
	            	<div class="balance-slat">
	            		<p class="acc-balance-txt">Current balance:</p>
	            		<div class="main-balance">
	            			<div class="select-2">
	            				<select data-method="customSelect">
	            					<option value="0">RSD</option>
	            					<option value="1">EUR</option>
	            					<option value="2">USD</option>
	            					<option value="3">GBP</option>
	            				</select>
	            			</div>
	            			<p>2.125.893,49</p>
	            		</div>
	            		<p class="transaction-txt">Last transaction | 24.10.2015</p>
	            		<p class="small-balance">7.291,00 RSD</p>
	            	</div>
	            	<a href="#" class="account-btn icon-money-2"></a>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box account-bg-1">
            		<div class="widget-options">
            			<ul class="trigger" data-method="optionsTrigger">
            				<li></li>
            				<li></li>
            				<li></li>
            			</ul>
            			<ul class="options-list">
            				<li><a href="#">Option 1</a></li>
            				<li><a href="#">Option 2</a></li>
            				<li><a href="#">Option 3</a></li>
            				<li><a href="#">Option 4</a></li>
            			</ul>
            		</div>
            		<div class="account-main">
            			<div class="acc-img">
            				<img src="img/demo/acc-img.jpg" alt="acc-image">
            			</div>
	            		<p class="acc-nr">115-0000000000567898-65</p>
	            		<p class="acc-name">Another Account</p>
	            	</div>
	            	<div class="balance-slat">
	            		<p class="acc-balance-txt">Current balance:</p>
	            		<div class="main-balance">
	            			<div class="select-2">
	            				<select data-method="customSelect">
	            					<option value="0">RSD</option>
	            					<option value="1">EUR</option>
	            					<option value="2">USD</option>
	            					<option value="3">GBP</option>
	            				</select>
	            			</div>
	            			<p>5.893,49</p>
	            		</div>
	            		<p class="transaction-txt">Last transaction | 24.10.2015</p>
	            		<p class="small-balance">7.291,00 RSD</p>
	            	</div>
	            	<a href="#" class="account-btn icon-money-2"></a>
            	</div>
            </div>
             <div class="slide">
            	<div class="account-box account-bg-1">
            		<div class="widget-options">
            			<ul class="trigger" data-method="optionsTrigger">
            				<li></li>
            				<li></li>
            				<li></li>
            			</ul>
            			<ul class="options-list">
            				<li><a href="#">Option 1</a></li>
            				<li><a href="#">Option 2</a></li>
            				<li><a href="#">Option 3</a></li>
            				<li><a href="#">Option 4</a></li>
            			</ul>
            		</div>
            		<div class="account-main">
            			<div class="acc-img">
            				<img src="img/demo/acc-img.jpg" alt="acc-image">
            			</div>
	            		<p class="acc-nr">115-0000000000567898-65</p>
	            		<p class="acc-name">My Current Account 4</p>
	            	</div>
	            	<div class="balance-slat">
	            		<p class="acc-balance-txt">Current balance:</p>
	            		<div class="main-balance">
	            			<div class="select-2">
	            				<select data-method="customSelect">
	            					<option value="0">RSD</option>
	            					<option value="1">EUR</option>
	            					<option value="2">USD</option>
	            					<option value="3">GBP</option>
	            				</select>
	            			</div>
	            			<p>125.893,49</p>
	            		</div>
	            		<p class="transaction-txt">Last transaction | 24.10.2015</p>
	            		<p class="small-balance">7.291,00 RSD</p>
	            	</div>
	            	<a href="#" class="account-btn icon-money-2"></a>
            	</div>
            </div>
             <div class="slide">
            	<div class="account-box account-bg-1">
            		<div class="widget-options">
            			<ul class="trigger" data-method="optionsTrigger">
            				<li></li>
            				<li></li>
            				<li></li>
            			</ul>
            			<ul class="options-list">
            				<li><a href="#">Option 1</a></li>
            				<li><a href="#">Option 2</a></li>
            				<li><a href="#">Option 3</a></li>
            				<li><a href="#">Option 4</a></li>
            			</ul>
            		</div>
            		<div class="account-main">
            			<div class="acc-img">
            				<img src="img/demo/acc-img.jpg" alt="acc-image">
            			</div>
	            		<p class="acc-nr">115-0000000000567898-65</p>
	            		<p class="acc-name">My Current Account 5</p>
	            	</div>
	            	<div class="balance-slat">
	            		<p class="acc-balance-txt">Current balance:</p>
	            		<div class="main-balance">
	            			<div class="select-2">
	            				<select data-method="customSelect">
	            					<option value="0">RSD</option>
	            					<option value="1">EUR</option>
	            					<option value="2">USD</option>
	            					<option value="3">GBP</option>
	            				</select>
	            			</div>
	            			<p>32.893,21</p>
	            		</div>
	            		<p class="transaction-txt">Last transaction | 24.10.2015</p>
	            		<p class="small-balance">3.291,00 RSD</p>
	            	</div>
	            	<a href="#" class="account-btn icon-money-2"></a>
            	</div>
            </div>
		</div>
        <div class="slider-button-prev"></div>
		<div class="slider-button-next"></div>
		<ul class="slider-tabs">
			<li class="active"><a href="#">Accounts</a></li>
			<li><a href="#">Cards</a></li>
			<li><a href="#">Loans</a></li>
			<li><a href="#">Savings</a></li>
		</ul>
	</div>



	<!-- Grid widgets -->
	<div class="widget-grid grid">

		<div class="container">

			<div class="widget-grid-options">
				<p class="grid-title">My dashboard</p>
				<div class="grid-options">
					<div class="grid-option option-dropdown">
						<a href="#" class="widget-select" data-method="dropdownTrigger"><i class="icon-grid-1"></i>Choose widgets</a>
						<div class="widget-select-menu">
							<ul>
								<li>
									ATM/Branch Finder
									<input type="checkbox" class="checkbox-1 checked" checked data-method="customCheckbox1">
								</li>
								<li>
									Latest Transactions
									<input type="checkbox" class="checkbox-1 checked" checked data-method="customCheckbox1">
								</li>
								<li>
									Quick Actions
									<input type="checkbox" class="checkbox-1 checked" checked data-method="customCheckbox1">
								</li>
								<li>
									Exchange Rates
									<input type="checkbox" class="checkbox-1 checked" checked data-method="customCheckbox1">
								</li>
							</ul>
						</div>
					</div>
					<div class="grid-option">
						<a href="#" class="reset"><i class="icon-reset-1"></i>Reset layout</a>
					</div>
				</div>
			</div>

			<ul class="sortable-widgets-container" data-method="sortableWidgets">

				<!-- Widget Map -->
				<li class="widget widget-map col-ls-8 col-ms-12 widget-h-1">
					<div class="widget-inner">
						<div class="cnt">
							<div class="widget-header">
								<div class="col-left">
									<p class="title-1 color-2"><span>ATM/Branch Finder</span></p>
								</div>
								<div class="col-right">
									<ul class="widget-actions">
										<li class="drag-handle"><i class="icon-move-2"></i></li>
										<li>
											<div class="widget-options">
						            			<ul class="trigger color-1" data-method="optionsTrigger">
						            				<li></li>
						            				<li></li>
						            				<li></li>
						            			</ul>
						            			<ul class="options-list">
						            				<li><a href="#">Option 1</a></li>
						            				<li><a href="#">Option 2</a></li>
						            				<li><a href="#">Option 3</a></li>
						            				<li><a href="#">Option 4</a></li>
						            			</ul>
						            		</div>
										</li>
									</ul>
								</div>
							</div>
							<input type="search" class="map-search place-1" placeholder="Search...">
						</div>
						<div class="map-wrapper">
							<div class="infobox-wrapper">
								<div id="infobox1" class="infobox bg-atm">
							        <p class="loc-title">Empire State Building</p>
							        <p class="loc-info">380 Madison Avenue<br>
							        	New York, NY, 12345
							        </p>
							    </div>
							    <div id="infobox2" class="infobox bg-atm">
							        <p class="loc-title"><span>ATM</span></p>
							         <p class="loc-info">380 Madison Avenue<br>
							        	New York, NY, 12345
							        </p>
							    </div>
							     <div id="infobox3" class="infobox bg-branch">
							        <p class="loc-title"><span>Branch</span></p>
							         <p class="loc-info">380 Madison Avenue<br>
							        	New York, NY, 12345
							        </p>
							    </div>
							    <div id="infobox4" class="infobox bg-atm">
							        <p class="loc-title"><span>ATM</span></p>
							        <p class="loc-info">380 Madison Avenue<br>
							        	New York, NY, 12345
							        </p>
							    </div>
							</div>
							<div id="map-canvas" data-method="googleMap"></div>
						</div>
					</div>
				</li>

				<!-- Widget Transactions -->
				<li class="widget widget-transactions col-ls-16 col-ms-12 widget-h-1">
					<div class="widget-inner">
						<div class="loader-wrap">
							<div class="cssload-container">
								<div class="cssload-speeding-wheel"></div>
							</div>
						</div>
						<div class="widget-header header-dark">
							<div class="cnt">
								<div class="col-left">
									<h2 class="title-1 color-1"><span>Latest Transactions</span></h2>
								</div>
								<div class="col-right">
									<ul class="widget-actions">
										<li class="drag-handle"><i class="icon-move-1"></i></li>
										<li>
											<div class="widget-options">
						            			<ul class="trigger" data-method="optionsTrigger">
						            				<li></li>
						            				<li></li>
						            				<li></li>
						            			</ul>
						            			<ul class="options-list">
						            				<li><a href="#">Option 1</a></li>
						            				<li><a href="#">Option 2</a></li>
						            				<li><a href="#">Option 3</a></li>
						            				<li><a href="#">Option 4</a></li>
						            			</ul>
						            		</div>
										</li>
									</ul>
								</div>
							</div>
							<ul class="widget-tabs">
								<li class="active"><a href="#">All</a></li>
								<li><a href="#">Sent</a></li>
								<li><a href="#">Received</a></li>
								<li><a href="#">Pending</a></li>
							</ul>
						</div>
						<div class="custom-table">
							<div class="table-row row-1 grid">
								<div class="col col-ls-5 col-ms-3 col-ss-6 tag-col">
									<div class="tag tagcolor-1 up">II</div>
									<p class="col-text text-1 text-cl-1 left">7 Feb 2016</p>
								</div>
								<div class="col col-ls-7 col-ms-5 col-ss-hidden">
									<p class="col-text text-2 text-cl-2 left"><span>To: Ivana Ivanović<br>115-00000000004265988-23</span></p>
								</div>
								<div class="col col-ls-6 col-ms-hidden">
									<p class="col-text text-2 text-cl-2 left"><span>Lorem ipsum dolor sit amet consectetur</span></p>
								</div>
								<div class="col col-ls-6 col-ms-4 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">323.450,00 RSD</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-5 col-ms-3 col-ss-6 tag-col">
									<div class="tag tagcolor-2 down">DP</div>
									<p class="col-text text-1 text-cl-1 left">6 Feb 2016</p>
								</div>
								<div class="col col-ls-7 col-ms-5 col-ss-hidden">
									<p class="col-text text-2 text-cl-2 left"><span>From: Darko Pejnović<br>115-00000000004265988-23</span></p>
								</div>
								<div class="col col-ls-6 col-ms-hidden">
									<p class="col-text text-2 text-cl-2 left"><span>Lorem ipsum dolor sit amet consectetur</span></p>
								</div>
								<div class="col col-ls-6 col-ms-4 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">1.550,00 RSD</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-5 col-ms-3 col-ss-6 tag-col">
									<div class="tag tagcolor-3 up">AS</div>
									<p class="col-text text-1 text-cl-1 left">4 Feb 2016</p>
								</div>
								<div class="col col-ls-7 col-ms-5 col-ss-hidden">
									<p class="col-text text-2 text-cl-2 left"><span>To: Aleksandra Stanković<br>115-00000000004265988-23</span></p>
								</div>
								<div class="col col-ls-6 col-ms-hidden">
									<p class="col-text text-2 text-cl-2 left"><span>Lorem ipsum dolor sit amet consectetur</span></p>
								</div>
								<div class="col col-ls-6 col-ms-4 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">5.250.129,99 RSD</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-5 col-ms-3 col-ss-6 tag-col">
									<div class="tag tagcolor-1 up">II</div>
									<p class="col-text text-1 text-cl-1 left">7 Feb 2016</p>
								</div>
								<div class="col col-ls-7 col-ms-5 col-ss-hidden">
									<p class="col-text text-2 text-cl-2 left"><span>To: Ivana Ivanović<br>115-00000000004265988-23</span></p>
								</div>
								<div class="col col-ls-6 col-ms-hidden">
									<p class="col-text text-2 text-cl-2 left"><span>Lorem ipsum dolor sit amet consectetur</span></p>
								</div>
								<div class="col col-ls-6 col-ms-4 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">323.450,00 RSD</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-5 col-ms-3 col-ss-6 tag-col">
									<div class="tag tagcolor-2 down">DP</div>
									<p class="col-text text-1 text-cl-1 left">6 Feb 2016</p>
								</div>
								<div class="col col-ls-7 col-ms-5 col-ss-hidden">
									<p class="col-text text-2 text-cl-2 left"><span>From: Darko Pejnović<br>115-00000000004265988-23</span></p>
								</div>
								<div class="col col-ls-6 col-ms-hidden">
									<p class="col-text text-2 text-cl-2 left"><span>Lorem ipsum dolor sit amet consectetur</span></p>
								</div>
								<div class="col col-ls-6 col-ms-4 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">1.550,00 RSD</p>
								</div>
							</div>
						</div>
						<div class="widget-buttons">
							<a href="#" class="btn-1 color-1">View all transaction</a>
						</div>
					</div>
				</li>

				<!-- Widget quick actions -->
				<li class="widget widget-quick-actions col-ls-8 col-ms-6 col-ss-12 widget-h-1">
					<div class="widget-inner">
						<div class="widget-header">
							<div class="cnt">
								<div class="col-left">
									<h2 class="title-1 color-2"><span>Quick actions</span></h2>
								</div>
								<div class="col-right">
									<ul class="widget-actions">
										<li class="drag-handle"><i class="icon-move-2"></i></li>
										<li>
											<div class="widget-options">
						            			<ul class="trigger color-1" data-method="optionsTrigger">
						            				<li></li>
						            				<li></li>
						            				<li></li>
						            			</ul>
						            			<ul class="options-list">
						            				<li><a href="#">Option 1</a></li>
						            				<li><a href="#">Option 2</a></li>
						            				<li><a href="#">Option 3</a></li>
						            				<li><a href="#">Option 4</a></li>
						            			</ul>
						            		</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="actions-wrap">
							<div class="action-box action-1">
								<a href="#"><i class="icon-money-2"></i><span>P2P payment</span></a>
							</div>
							<div class="action-box action-2">
								<a href="#"><i class="icon-exchange-2"></i><span>Exchange</span></a>
							</div>
							<div class="action-box action-3">
								<a href="#"><i class="icon-card-2"></i><span>View my cards</span></a>
							</div>
							<div class="action-box action-4">
								<a href="#"><i class="icon-safe-3"></i><span>View my accounts</span></a>
							</div>
						</div>
					</div>
				</li>

				<!-- Widget Exchange -->
				<li class="widget widget-exchange col-ls-8 col-ms-6 col-ss-12 widget-h-1">
					<div class="widget-inner">
						<div class="widget-header">
							<div class="cnt">
								<div class="col-left">
									<h2 class="title-1 color-2"><span>Exchange rates</span></h2>
								</div>
								<div class="col-right">
									<ul class="widget-actions">
										<li class="drag-handle"><i class="icon-move-2"></i></li>
										<li>
											<div class="widget-options">
						            			<ul class="trigger color-1" data-method="optionsTrigger">
						            				<li></li>
						            				<li></li>
						            				<li></li>
						            			</ul>
						            			<ul class="options-list">
						            				<li><a href="#">Option 1</a></li>
						            				<li><a href="#">Option 2</a></li>
						            				<li><a href="#">Option 3</a></li>
						            				<li><a href="#">Option 4</a></li>
						            			</ul>
						            		</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="table-head row-1 grid">
								<div class="col col-ls-9 col-ms-6 col-ss-6">
									<p class="left"></p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-hidden">
									<p class="center">Buy</p>
								</div>
								<div class="col col-ls-5 col-ms-hidden">
									<p class="center">Mid</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-6">
									<p class="center">Sell</p>
								</div>
							</div>
						</div>
						<div class="custom-table">
							<div class="table-row row-1 grid">
								<div class="col col-ls-9 col-ms-6 col-ss-6 flag-col no-border">
									<div class="flag-box"><img src="img/photos/eu.png" alt="flag"></div>
									<p class="col-text text-4 text-cl-2 left">1 EUR</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-hidden no-border">
									<p class="col-text text-5 text-cl-2 center">0.2345</p>
								</div>
								<div class="col col-ls-5 col-ms-hidden no-border">
									<p class="col-text text-5 text-cl-2 center">0.2435</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-6 no-border">
									<p class="col-text text-5 text-cl-2 center">0.2522</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-9 col-ms-6 col-ss-6 flag-col no-border">
									<div class="flag-box"><img src="img/photos/usa.png" alt="flag"></div>
									<p class="col-text text-4 text-cl-2 left">1 USD</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-hidden no-border">
									<p class="col-text text-5 text-cl-2 center">0.2345</p>
								</div>
								<div class="col col-ls-5 col-ms-hidden no-border">
									<p class="col-text text-5 text-cl-2 center">0.2435</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-6 no-border">
									<p class="col-text text-5 text-cl-2 center">0.2522</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-9 col-ms-6 col-ss-6 flag-col no-border">
									<div class="flag-box"><img src="img/photos/swiss.png" alt="flag"></div>
									<p class="col-text text-4 text-cl-2 left">1 CHF</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-hidden no-border">
									<p class="col-text text-5 text-cl-2 center">0.2345</p>
								</div>
								<div class="col col-ls-5 col-ms-hidden no-border">
									<p class="col-text text-5 text-cl-2 center">0.2435</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-6 no-border">
									<p class="col-text text-5 text-cl-2 center">0.2522</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-9 col-ms-6 col-ss-6 flag-col no-border">
									<div class="flag-box"><img src="img/photos/great-britain.png" alt="flag"></div>
									<p class="col-text text-4 text-cl-2 left">1 GBP</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-hidden no-border">
									<p class="col-text text-5 text-cl-2 center">0.2345</p>
								</div>
								<div class="col col-ls-5 col-ms-hidden no-border">
									<p class="col-text text-5 text-cl-2 center">0.2435</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-6 no-border">
									<p class="col-text text-5 text-cl-2 center">0.2522</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-9 col-ms-6 col-ss-6 flag-col no-border">
									<div class="flag-box"><img src="img/photos/canada.png" alt="flag"></div>
									<p class="col-text text-4 text-cl-2 left">1 CAD</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-hidden no-border">
									<p class="col-text text-5 text-cl-2 center">0.2345</p>
								</div>
								<div class="col col-ls-5 col-ms-hidden no-border">
									<p class="col-text text-5 text-cl-2 center">0.2435</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-6 no-border">
									<p class="col-text text-5 text-cl-2 center">0.2522</p>
								</div>
							</div>
						</div>
						<div class="widget-buttons two-buttons">
						<a href="#" class="btn-1 color-2">Sell currency</a>
							<a href="#" class="btn-1 color-1">Buy currency</a>
						</div>
					</div>
				</li>

				<!-- Widget Advertising -->
				<li class="widget widget-advertising col-ls-8 col-ms-6 col-ss-12 widget-h-1">
					<div class="widget-inner">
						<img class="promo" src="img/demo/promo-1.jpg" alt="promo" data-object-fit="cover">
						<h2>Download our new<br><strong>mobile app</strong></h2>
						<ul class="apps">
							<li><a href="#"><img src="img/photos/appstore.png" alt=""></a></li>
							<li><a href="#"><img src="img/photos/googleplay.png" alt=""></a></li>
						</ul>
					</div>
				</li>

			</ul>


		</div>
	</div>

<div>

</div>

<?php Site::getFooter(); ?>