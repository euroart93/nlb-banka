<?php
Site::getHeader(); ?>

<div id="main">

	<div class="main-content content-full">
		<div class="widget widget-inbox">
			<div class="container">
				<div class="col-left">
					<h2><!--<i class="icon-mail-2"></i>-->Poruke</h2>
					<div class="sub-col-left">
						<button type="button" class="btn-1 color-1 full-width" data-method="openSideModal" data-filter-node="#side-modal-new-msg">Nova Poruka</button>
						<ul class="message-options">

							<li>
								<a href="#" class="active">Primljene poruke <span>429</span></a>
								<ul class="submenu">
									<li><a href="#">Nepročitane poruke <span class="active">24</span></a></li>
									<li><a href="#">Prioritetne poruke <span>96</span></a></li>
								</ul>
							</li>
							<li>
								<a href="#">Poslate poruke <span>25</span></a>
							</li>
							<li>
								<a href="#">Arhiva <span>89</span></a>
							</li>

						</ul>
					</div>
				</div>
				<div class="col-right">
					<div class="search-box search-1">
						<input type="search" class="place-1" placeholder="Pretraži poruke">
					</div>

					<!--<div class="group">
						<div class="group-inner">
							<div class="select-3 icon-select">
								<select data-method="customSelect3">
		        					<option data-imagesrc="<?php echo Site::url('/img/icons/icon-mail-3.png') ?>" value="0">All messages</option>
		        					<option data-imagesrc="<?php echo Site::url('/img/icons/icon-puzzle.png') ?>" value="1">Option 1</option>
		        					<option data-imagesrc="<?php echo Site::url('/img/icons/icon-service-2.png') ?>" value="2">Option 2</option>
		        					<option data-imagesrc="<?php echo Site::url('/img/icons/icon-service-3.png') ?>" value="3">Option 3</option>
		        					<option data-imagesrc="<?php echo Site::url('/img/icons/icon-service-4.png') ?>" value="4">Option 4</option>
		        					<option data-imagesrc="<?php echo Site::url('/img/icons/icon-service-5.png') ?>" value="5">Option 5</option>
		        					<option data-imagesrc="<?php echo Site::url('/img/icons/icon-service-6.png') ?>" value="6">Option 6</option>
		        				</select>
		        			</div>
						</div>
					</div>-->

					<div class="custom-table table-2">
						<div class="table-row row-first row-1 grid">
							<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
								<div class="tag tag-2 tagcolor-6 icon-service-1"></div>
								<p class="col-text text-6 text-cl-2 msg-title left"><strong>Vaša nova kreditna kartica je spremna, možete je podići u ekspozi...</strong></p>
								<p class="col-text text-6 text-cl-1 msg-date right">16:32</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
								<div class="tag tag-2 tagcolor-6 icon-service-1"></div>
								<p class="col-text text-6 text-cl-2 msg-title left"><strong>Obavest o provedenoj transakciji</strong></p>
								<p class="col-text text-6 text-cl-1 msg-date right">12.02.2017</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
								<div class="tag tag-2 tagcolor-7 icon-service-2  notice"></div>
								<p class="col-text text-6 text-cl-2 msg-title left">Obavest o provedenoj transakciji</p>
								<p class="col-text text-6 text-cl-1 msg-date right">12.02.2017</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
								<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
								<p class="col-text text-6 text-cl-2 msg-title left">Obavest o provedenoj transakciji</p>
								<p class="col-text text-6 text-cl-1 msg-date right">12.02.2017</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
								<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
								<p class="col-text text-6 text-cl-2 msg-title left">Obavest o provedenoj transakciji</p>
								<p class="col-text text-6 text-cl-1 msg-date right">12.02.2017</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
								<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
								<p class="col-text text-6 text-cl-2 msg-title left">Odbijena transakcija</p>
								<p class="col-text text-6 text-cl-1 msg-date right">12.02.2017</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
								<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
								<p class="col-text text-6 text-cl-2 msg-title left">Obavest o provedenoj transakciji</p>
								<p class="col-text text-6 text-cl-1 msg-date right">12.02.2017</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
								<div class="tag tag-2 tagcolor-7 icon-service-2  notice"></div>
								<p class="col-text text-6 text-cl-2 msg-title left">Od 10.02. izvodi kreditnih kartica biće dostupni samo preko e-bankinga</p>
								<p class="col-text text-6 text-cl-1 msg-date right">12.02.2017</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
								<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
								<p class="col-text text-6 text-cl-2 msg-title left">Obavest o provedenoj transakciji</p>
								<p class="col-text text-6 text-cl-1 msg-date right">12.02.2017</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
								<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
								<p class="col-text text-6 text-cl-2 msg-title left">Obavest o provedenoj transakciji</p>
								<p class="col-text text-6 text-cl-1 msg-date right">12.02.2017</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
								<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
								<p class="col-text text-6 text-cl-2 msg-title left">Još povoljniji uslovi gotovinskih kredita u 2017. godini</p>
								<p class="col-text text-6 text-cl-1 msg-date right">12.02.2017</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
								<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
								<p class="col-text text-6 text-cl-2 msg-title left">Obavest o provedenoj transakciji</p>
								<p class="col-text text-6 text-cl-1 msg-date right">12.02.2017</p>
							</div>
						</div>


						<!--<div class="table-row row-1 grid">
							<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
								<div class="tag tag-2 tagcolor-7 icon-service-3"></div>
								<p class="col-text text-6 text-cl-2 msg-title left">Maecenas aliquet tellus in velit tempor, id sagittis tellus semper</p>
								<p class="col-text text-6 text-cl-1 msg-date right">5 FEB 2016</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
								<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
								<p class="col-text text-6 text-cl-2 msg-title left"><strong>Maecenas aliquet tellus in velit tempor, id sagittis tellus semper</strong></p>
								<p class="col-text text-6 text-cl-1 msg-date right">5 FEB 2016</p>
							</div>
						</div>-->

						<div class="hidden-content">
							<div class="table-row row-1 grid">
								<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
									<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
									<p class="col-text text-6 text-cl-2 msg-title left"><strong>Maecenas aliquet tellus in velit tempor, id sagittis tellus semper</strong></p>
									<p class="col-text text-6 text-cl-1 msg-date right">5 FEB 2016</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
									<div class="tag tag-2 tagcolor-6 icon-service-1"></div>
									<p class="col-text text-6 text-cl-2 msg-title left">Maecenas aliquet tellus in velit tempor, id sagittis tellus semper</p>
									<p class="col-text text-6 text-cl-1 msg-date right">5 FEB 2016</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
									<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
									<p class="col-text text-6 text-cl-2 msg-title left">Maecenas aliquet tellus in velit tempor, id sagittis tellus semper</p>
									<p class="col-text text-6 text-cl-1 msg-date right">5 FEB 2016</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
									<div class="tag tag-2 tagcolor-7 icon-service-3"></div>
									<p class="col-text text-6 text-cl-2 msg-title left">Maecenas aliquet tellus in velit tempor, id sagittis tellus semper</p>
									<p class="col-text text-6 text-cl-1 msg-date right">5 FEB 2016</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-24 tag-col" data-method="openSideModal" data-filter-node="#side-modal-inbox">
									<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
									<p class="col-text text-6 text-cl-2 msg-title left"><strong>Maecenas aliquet tellus in velit tempor, id sagittis tellus semper</strong></p>
									<p class="col-text text-6 text-cl-1 msg-date right">5 FEB 2016</p>
								</div>
							</div>
						</div>
					</div>
					<button type="button" class="btn-1 color-1" data-method="showMore"><b>+</b> PRIKAŽI VIŠE PORUKA</button>
				</div>
			</div>
		</div>

	</div>

</div>

<?php Site::getFooter(); ?>
