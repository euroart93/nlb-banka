<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="account-row profile-row account-bg-1">
		<div class="bg-overlay overlay-2"></div>
		<div class="">
			<div class="img-box">
				<img src="img/demo/profile-1.jpg" alt="profile-img">
			</div>
			<p>Anna Kendrick</p>
		</div>
	</div>

	<div class="main-content">
		
		<div class="widget content-white">
			<div class="widget-slat">
				<div class="col-left">
					<h2><i class="icon-settings-3"></i>Self Care</h2>
				</div>
				<div class="col-right">
					<div class="group-inner">
						<div class="select-3">
							<select  data-method="customSelect3">
            					<option value="0">My profile</option>
        						<option value="1">Authentication and Authorization</option>
            					<option value="2">Applications</option>
            					<option value="3">My documents</option>
            					<option value="4">Notifications</option>
            				</select>
            			</div>
					</div>
				</div>
			</div>

			<div class="widget-slat">
				<div class="col-left">
					<h3 class="title-1 color-2 title-full"><span>Personal data</span></h3>
					<p>Data is read-only. To change your personal data, please visit the bank.</p>
				</div>
				<div class="col-right">
					<div class="profile-img-group">
						<div class="img-box" data-method="openPopup" data-popup="18">
							<img src="img/demo/profile-1.jpg" alt="">
							<span>Change<br>photo</span>
						</div>
						<a href="#" class="cancel-link"></a>
					</div>
					<div class="plain-list">
						<div>
							<p class="key">Name and surname:</p>
							<p class="value">Anna Kendrick</p>
						</div>
						<div>
							<p class="key">Date of birth:</p>
							<p class="value">15.04.1986.</p>
						</div>
						<div>
							<p class="key">Citizenship:</p>
							<p class="value">Resident</p>
						</div>
						<div>
							<p class="key">Country of birth:</p>
							<p class="value">Serbia</p>
						</div>
						<div>
							<p class="key">Address:</p>
							<p class="value">256 Mulholland Drive, Hollywood</p>
						</div>
						<div>
							<p class="key">City:</p>
							<p class="value">Los Angeles</p>
						</div>
						<div>
							<p class="key">Postal code:</p>
							<p class="value">90210</p>
						</div>
					</div>
				</div>
				<div class="divider-20"></div>
			</div>
	
			<div class="widget-slat">
				<form action="#" method="#">
					<div class="col-left">
						<h3 class="title-1 color-2 title-full"><span>Contact address</span></h3>
						<p>To change your contact address, fill in the data and click the Save Changes button. Fields marked with * are mandatory.</p>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<label class="label-1">City:</label>
								<div class="group-inner important">
									<input type="text" class="input-1" value="San Francisco">
								</div>
							</div>
							<div class="group uk-width-1-1">
								<label class="label-1">Municipality:</label>
								<div class="group-inner">
									<div class="select-3">
										<select  data-method="customSelect3">
			            					<option value="0">LA County</option>
			            					<option value="1">Option 2</option>
			            					<option value="2">Option 3</option>
			            					<option value="3">Option 4</option>
			            				</select>
			            			</div>
								</div>
							</div>
							<div class="group uk-width-1-1">
								<label class="label-1">Street address:</label>
								<div class="group-inner important">
									<input type="text" class="input-1" value="Mulholland Drive 356">
								</div>
							</div>
							<div class="group uk-width-1-1">
								<label class="label-1">Apartment:</label>
								<div class="group-inner important">
									<input type="text" class="input-1" value="3">
								</div>
							</div>
							<div class="group uk-width" style="width: 175px;">
								<div class="group-inner">
									<button type="submit" class="btn-1 color-2" data-method="openPopup" data-popup="19">Save changes</button>
								</div>
							</div>
							<div class="divider-20"></div>
						</div>
					</div>
				</form>
			</div>

			<div class="widget-slat">
				<form action="#" method="#">
					<div class="col-left">
						<h3 class="title-1 color-2 title-full"><span>Phone numbers</span></h3>
						<p>Mobile phone number is mandatory. You can enter one additional phone number.</p>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<label class="label-1">Mobile phone number:</label>
								<div class="group-inner important">
									<input type="tel" class="input-1" value="+381631234567">
								</div>
							</div>
							<div class="group uk-width" style="width: 175px;">
								<div class="group-inner">
									<button type="submit" class="btn-1 color-2" data-method="openPopup" data-popup="21">Save changes</button>
								</div>
							</div>
							<div class="divider-20"></div>
						</div>
					</div>
				</form>
			</div>

			<div class="widget-slat">
				<div class="col-left">
					<h3 class="title-1 color-2 title-full"><span>E-mail</span></h3>
				</div>
				<div class="col-right">
					<div class="custom-form uk-grid">
						<div class="group uk-width-1-1">
							<label class="label-1">E-mail address:</label>
							<div class="group-inner">
								<input type="email" class="input-1" value="anna.kendrick@hollywood.com">
							</div>
						</div>
						<div class="group uk-width" style="width: 175px;">
							<div class="group-inner">
								<button type="submit" class="btn-1 color-2" data-method="openPopup" data-popup="19">Save changes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>

<?php Site::getFooter(); ?>