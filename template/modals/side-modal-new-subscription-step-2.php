<div class="side-modal">
	<div class="side-modal-inner">
		
		<div id="new-subscription-2" class="side-modal-popup">
			<div class="actions">
				<a href="#" class="close-box" data-method="closeSideModal">Close</a>
			</div>

			<div class="content-inner" data-method="customScroll">
				<h2>New subscription</h2>
				<ul class="steps left mb-50">
					<li>1 <span>Select bill issuer</span></li>
					<li class="active">2 <span>Bill issuer details</span></li>
					<li>3 <span>Subscription detils</span></li>
				</ul>

				<div class="plain-list border-top">
					<div>
						<p class="key">Bill issuer:</p>
						<p class="value">Electrodistribution San Francisco</p>
					</div>
					<div>
						<p class="key">Address:</p>
						<p class="value">Maple Street 54a, Sacramento</p>
					</div>
					<div>
						<p class="key">Account number:</p>
						<p class="value">355000320014579115</p>
					</div>
					<div>
						<p class="key">Payment description:</p>
						<p class="value">Electricity bill payment</p>
					</div>
					<div>
						<p class="key">Bill issuing period:</p>
						<p class="value">Monthly</p>
					</div>
					<div>
						<p class="key">Issuing date:</p>
						<p class="value">1. of month</p>
					</div>
					<div>
						<p class="key">Payment due date:</p>
						<p class="value">30. of month</p>
					</div>
					<div>
						<p class="key">Payment fee:</p>
						<p class="value">0.0 USD</p>
					</div>
				</div>
			</div>

			<div class="bottom-actions">
				<a href="#" class="btn-1 color-3" data-method="closeInbox">Cancel</a>
				<a href="#" class="btn-1 color-1" data-method="closeInbox">Continue</a>
			</div>

		</div>

	</div>
</div>