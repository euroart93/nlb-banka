<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-bill-subscription" class="side-modal-popup">
			<div class="actions">
				<div class="col-left">
					<h3 class="title-4 color-2"><span>Nova pretplata</span></h3>
				</div>
				<div class="col-right">
					<a href="#" class="close-box" data-method="closeSideModal">Close</a>
				</div>
			</div>
			<form>
				<div class="content-inner" data-method="customScroll">
					<ul class="steps left mb-50">
						<li class="active">1 <span>izdavač računa</span></li>
						<li>2 <span>Detalji pretplate</span></li>
						<li>3 <span>Detalji izdavača računa</span></li>
					</ul>

					<div class="search-box search-1 mb-50">
						<input type="search" class="place-1" placeholder="Pretraži">
					</div>

					<div class="modal-transaction-details plain-list">
				
						<div class="content-inner">
							<div class="widget-slat">
								<div class="plain-list">
									<div>
										<div class="subscription-option">
											<p class="value">Elektrodistribucija Beograd</p>
											<p class="key">Račun za usluge</p>
										</div>
									</div>
									<div>
										<div class="subscription-option">
											<p class="value">Radiotelevizija Srbije</p>
											<p class="key">Račun za usluge</p>
										</div>
									</div>
									<div>
										<div class="subscription-option">
											<p class="value">Infostan</p>
											<p class="key">Račun za usluge </p>
										</div>
									</div>
									<div class="selected">
										<div class="subscription-option">
											<p class="value">SBB</p>
											<p class="key">Račun za usluge</p>
										</div>
									</div>
									<div>
										<div class="subscription-option">
											<p class="value">Elektrodistribucija Beograd</p>
											<p class="key">Račun za usluge</p>
										</div>
									</div>
									<div>
										<div class="subscription-option">
											<p class="value">Radiotelevizija Srbije</p>
											<p class="key">Račun za usluge</p>
										</div>
									</div>
									<div>
										<div class="subscription-option">
											<p class="value">Infostan</p>
											<p class="key">Račun za usluge </p>
										</div>
									</div>
									<div>
										<div class="subscription-option">
											<p class="value">SBB</p>
											<p class="key">Račun za usluge</p>
										</div>
									</div>
								</div>
							</div>
					</div>
				</div>
			</form>
		</div>
		<div class="bottom-actions">
			<a href="#" class="btn-1 color-1" data-method="openSideModal" data-filter-node="#side-modal-bill-subscription-2">nastavi</a>
			<a href="#" class="btn-1 color-3" data-method="closeInbox">odustani</a>
		</div>
	</div>
</div>