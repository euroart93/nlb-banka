<div class="side-modal">
	<div class="side-modal-inner">
		
		<div id="new-subscription" class="side-modal-popup">
			<div class="actions">
				<a href="#" class="close-box" data-method="closeSideModal">Close</a>
			</div>
			
			<form>
				<div class="content-inner" data-method="customScroll">
					<h2>New subscription</h2>
					<ul class="steps left mb-50">
						<li class="active">1 <span>Select bill issuer</span></li>
						<li>2 <span>Bill issuer details</span></li>
						<li>3 <span>Subscription detils</span></li>
					</ul>

					<div class="search-box search-1 mb-50">
						<input type="search" class="place-1" placeholder="Search messages">
					</div>

					<div class="plain-list">
						<div>
							<p class="value-description">Telecom Sacramento <span>Service bill</span></p>
						</div>
						<div>
							<p class="value-description">Electrodistribution San Francisco <span>Service bill</span></p>
						</div>
						<div>
							<p class="value-description">SBB <span>Service bill</span></p>
						</div>
					</div>
				</div>
				<div class="bottom-actions">
					<a href="#" class="btn-1 color-3" data-method="closeInbox">Cancel</a>
					<a href="#" class="btn-1 color-1" data-method="closeInbox">Continue</a>
				</div>
			</form>


		</div>

	</div>
</div>