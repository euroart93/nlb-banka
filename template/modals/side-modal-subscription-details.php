<div class="side-modal">
	<div class="side-modal-inner">
		
		<div id="subscription-details" class="side-modal-popup">
			<div class="actions">
				<a href="#" class="close-box" data-method="closeSideModal">Close</a>
			</div>

			<div class="content-inner" data-method="customScroll">
				<h2>Subscription details</h2>

				<div class="plain-list border-top">
					<div>
						<p class="key">Description</p>
						<p class="value">Electricity bill</p>
					</div>
				</div>

				<div class="plain-list">
					<div>
						<p class="value-description">Telecom Sacramento JP <span class="data">Fulton Street 465, San Francisco</span></p>
					</div>
					<div>
						<p class="key">Account number</p>
						<p class="value">340-0000011010911-62</p>
					</div>
					<div>
						<p class="key">Bill issuing preiods</p>
						<p class="value">Monthly</p>
					</div>
					<div>
						<p class="key">Due date</p>
						<p class="value">30. of month</p>
					</div>
					<div>
						<p class="key">Name of the payer</p>
						<p class="value">Demo Korisnik</p>
					</div>
					<div>
						<p class="key">Payer address</p>
						<p class="value">Ulica broj 1., Beograd</p>
					</div>
					<div>
						<a href="#" class="link-row">
							<p class="value-description">Current acount <span class="data">123-4567890123456-78</span></p>
						</a>
					</div>
					<div>
						<p class="key">Purpose of payment</p>
						<p class="value">Service bill</p>
					</div>
					<div>
						<p class="key">Status</p>
						<p class="value color-2">Active</p>
					</div>
				</div>
			</div>

			<div class="bottom-actions">
				<a href="#" class="btn-1 color-1 full-width" data-method="closeInbox">Cancel Bill subscription</a>
			</div>


		</div>

	</div>
</div>