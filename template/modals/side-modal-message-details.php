<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-message-details" class="side-modal-popup ">
			<div class="actions">
				<div class="col-left">
					<h3 class="title-4 color-2"><span>Još povoljniji uslovi gotovinskih<br>kredita u 2017 godini.</span></h3>
					<p>08.02.2017. 15:24</p>
				</div>
				<div class="col-right">
					<a href="#" class="close-box" data-method="closeSideModal">Close</a>
				</div>
			</div>
			<div class="modal-transaction-details  news">
				
				<div class="content-inner" data-method="customScroll">
					<div class="widget-slat">
						<img src="img/demo/Layer-10.jpg">
						<p>NLB Dinarski gotovinski i krediti za refinansiranje uz dodatni keš.</p>
							
						<ul>
							<li>Fiksna rata tokom celokupnog perioda otplate kredita</li>
							<li>Iznos kredita: 50.000-1.500.000 RSD</li>
							<li>Period otplate kredita:6-100 meseci</li>
							<li>Fiksna nominalna kamatna stopa:</li>
							<li>9,95%-gotovinski krediti za refinansiranje uz dodatni
							keš-uz prijem zarade preko NLB banke-period otplate 6-24 meseca</li>
							<li>12,95%-gotovinski krediti za refinansiranje uz dodatni
							dodatni keš-uz prijem zarade preko NLB banke-period otplate
							25-100 meseci</li>
						</ul>

						<p>pored vrlo povoljne ponude keš kredita za klijente sa prijemom
						zarade u NLB banci,pripremili smo adekvantnu ponudu za sve ostale klijente</p>
						 	
					</div>
				</div>
				<div class="bottom-actions uk-grid">
					<div class="group uk-width-1-2">
						<div class="group-inner input">
							<a href="#" class="btn-1 color-2" data-method="closeInbox">arhiviraj poruku</a>
						</div>
					</div>
					<div class="group uk-width-1-2">
						<div class="group-inner input">
							<a href="#" class="btn-1 color-1" data-method="closeInbox">Pošalji upit</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>