<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-settings-phone" class="center-modal-3 side-modal-popup">
			<div class="popup-authorize">
				<div class="horiz-blue-center"></div>
				<h3 class="card-modal-title">Promena telefonskog broja</h3>
				<img src="img/bg/sms-authorize.png">
			</div>
			<p>
				Poslali ste zahtev za promenu telefonskog broja. Jednokratna lozinka poslata je SMS porukom na Vaš mobilni telefon. Molimo, unesite je u polje ispod. Ako Vam lozinka nije stigla, molimo, kliknite na dugme ‘Pošalji ponovo’.
			</p>
			<div class="custom-form uk-grid">
				<div class="group-inner group-center">
						<a href="#" data-filter-node="#side-modal-payment-center" data-method="openSideModal" class="center-btn ">Pošalji ponovo</a>
				</div>

				<div class="divider-30"></div>
				<div class="group uk-width-1-1">
					<div class="group-inner">
						<label class="label-1">Unesite jednokratnu lozinku:</label>
						<input type="text" class="input-1" value="">
					</div>
				</div>	
				<div class="group uk-width-1-2">
					<div class="group-inner">
						<a href="#" class="btn-1 color-1">Potvrdi</a>
					</div>
				</div>
				
				<div class="group uk-width-1-2">
					<div class="group-inner">
						<a href="#" data-filter-node="#side-modal-payment-center" data-method="closeSideModal" class="btn-1 color-3 ">Odustani</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>