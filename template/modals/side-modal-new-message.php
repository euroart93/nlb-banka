<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-new-message" class="side-modal-popup">
			<div class="actions">
				<div class="col-left">
					<h3 class="title-4 color-2"><span>Nova poruka</span></h3>
				</div>
				<div class="col-right">
					<a href="#" class="close-box" data-method="closeSideModal">Close</a>
				</div>
			</div>
			<form>
				<div class="content-inner" data-method="customScroll">
					<div class="custom-form uk-grid">
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Vrsta poruke:</label>
								<div class="select-3">
									<select data-method="customSelect3">
		            					<option value="0">Opšti upit/pitanje</option>
		            					<option value="1">Option 1</option>
		            					<option value="2">Option 2</option>
		            					<option value="3">Option 3</option>
		            				</select>
		            			</div>
							</div>
						</div>
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Naslov poruke:</label>
								<input type="text" class="input-1" value="Lorem ipsum dolor">
							</div>
						</div>
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Vaša poruka:</label>
								<textarea class="textarea-1" value="Lorem ipsum dolor"></textarea>
							</div>
						</div>
					</div>
					<div class="attachments">
						<p class="attachments-title">Prilozi</p>
						<ul>
							<li>
								<i class="icon-attachment-2"></i>
								<p class="name">Pellentesque sagittis vehicula maximus</p>
								<p class="doc">PDF dokument</p>
								<a href="#" class="att-delete-2"></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="bottom-actions uk-grid">
					<div class="group uk-width-1-2">
						<div class="group-inner input">
							<a href="#" class="btn-1 color-2" data-method="closeInbox">Dodaj prilog</a>
						</div>
					</div>
					<div class="group uk-width-1-2">
						<div class="group-inner input">
							<a href="#" class="btn-1 color-1" data-method="closeInbox">Pošalji poruku</a>
						</div>
					</div>
				</div>
			</form>
		</div>

	</div>
</div>