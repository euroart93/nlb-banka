<div class="side-modal">
	<div class="side-modal-inner">
		
		<div id="transactions-filter-2" class="side-modal-popup">
			<div class="actions">
				<a href="#" class="close-box" data-method="closeSideModal">Close</a>
			</div>
			<form>
				<div class="content-inner" data-method="customScroll">
					<h2>Filter transactions</h2>
					<div class="custom-form uk-grid">
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Account:</label>
								<div class="select-3">
									<select data-method="customSelect3">
		            					<option value="0">My current account</option>
		            					<option value="1">Account 1</option>
		            					<option value="2">Account 2</option>
		            					<option value="3">Account 3</option>
		            				</select>
		            			</div>
							</div>
						</div>
						<div class="group uk-width-1-1">
							<label class="label-1">Date from:</label>
							<div class="group-inner date-wrap full-width">
								<input type="text" class="input-1 dark date-input" value="15/02/2016" data-method="dateInput">
							</div>
						</div>
						<div class="group uk-width-1-1">
							<label class="label-1">Date to:</label>
							<div class="group-inner date-wrap full-width">
								<input type="text" class="input-1 dark date-input" value="15/02/2016" data-method="dateInput">
							</div>
						</div>
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Amount from:</label>
								<input type="text" class="input-1">
							</div>
						</div>
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Amount to:</label>
								<input type="text" class="input-1">
							</div>
						</div>
					</div>
				</div>
				<div class="bottom-actions">
					<a href="#" class="btn-1 color-3" data-method="closeInbox">Reset</a>
					<a href="#" class="btn-1 color-1" data-method="closeInbox">Apply</a>
				</div>
			</form>


		</div>

	</div>
</div>