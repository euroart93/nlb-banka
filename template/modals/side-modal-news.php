<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-news" class="side-modal-popup news">
			<div class="actions">
				<div class="col-left">
					<h3 class="title-4 color-2">
					<span>Vlasti SAD već 12 godina znaju za crni scenario na brani</span></h3>
					<p>B92 • 08.02.2017. 15:24</p>
				</div>
				<div class="col-right">
					<a href="#" class="close-box" data-method="closeSideModal">Close</a>
				</div>
			</div>
			<div class="modal-transaction-details plain-list news">
				
				<div class="content-inner" data-method="customScroll">
					<div class="widget-slat">
						<img src="img/demo/vesti-img.jpg">
						<p>
							Kalifornijski ured za vode u nedjelju popodne po lokalnom vremenu na Twitteru je objavio da bi ispust uz branu mogao popustiti "u sljedećih sat vremena". Nekoliko sati kasnije situacija izgleda manje dramatična, a oštećeni ispust i dalje je na mjestu.<br>

						 	Kalifornijske su vlasti priopćile da puštaju oko 2830 kubnih metara vode po sekundi kako bi pokušale smanjiti razinu vode u jezeru. "Jednom kad imate oštećenje takve strukture to je katastrofa", kazao je direktor ureda za vode Bill Croyle. Dodao je međutim da cjelovitost brane nije ugrožena oštećenim ispustom. U poslijepodnevnim satima u nedjelju šerif okruga Butte County Kory Honea kazao je na društvenim mrežama da je na snazi evakuacija stanovnika, ponovivši tri puta "ovo NIJE vježba". Stanovnicima mjesta Oroville, sjeverno od Sacramenta, u kojem živi oko 16.000 ljudi rečeno je da se upute prema sjeveru.<br>

							Kalifornijski ured za vode u nedjelju popodne po lokalnom vremenu na Twitteru je objavio da bi ispust uz branu mogao popustiti "u sljedećih sat vremena". Nekoliko sati kasnije situacija izgleda manje dramatična, a oštećeni ispust i dalje je na mjestu. Kalifornijske su vlasti priopćile da puštaju oko 2830 kubnih metara vode po sekundi kako bi pokušale smanjiti razinu vode u jezeru. "Jednom kad imate oštećenje takve strukture to je katastrofa", kazao je direktor ureda za vode Bill Croyle. Dodao je međutim da cjelovitost brane nije ugrožena oštećenim ispustom. U poslijepodnevnim satima u nedjelju šerif okruga Butte County Kory Honea kazao je na društvenim mrežama da je na snazi evakuacija stanovnika, ponovivši tri puta "ovo NIJE vježba". Stanovnicima mjesta Oroville, sjeverno od Sacramenta, u kojem živi oko 16.000 ljudi rečeno je da se upute prema sjeveru.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>