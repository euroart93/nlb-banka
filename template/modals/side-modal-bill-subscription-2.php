<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-bill-subscription-2" class="side-modal-popup">
			<div class="actions">
				<div class="col-left">
					<h3 class="title-4 color-2"><span>Nova pretplata</span></h3>
				</div>
				<div class="col-right">
					<a href="#" class="close-box" data-method="closeSideModal">Close</a>
				</div>
			</div>
			<form>
				<div class="content-inner" data-method="customScroll">
					<ul class="steps left mb-50">
						<li>1 <span>izdavač računa</span></li>
						<li class="active">2 <span>Detalji pretplate</span></li>
						<li>3 <span>Detalji izdavača računa</span></li>
					</ul>

					<p class="text-top">
						Molimo, ispunite potrebne podatke.Indefikacioni broj korisnika
						pronaći ćete na bilo kom mesečnom računu.
					</p>

					<div class="custom-form uk-grid">
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Ime pretplate:</label>
								<input type="text" class="input-1">
							</div>
						</div>
					</div>

					<h4 class="title-6 color-2"><span>podaci o platiocu</span></h4>
					<div class="modal-transaction-details plain-list">
						<div class="content-inner">
							<div class="widget-slat">
								<div class="custom-form uk-grid">
									<div class="group uk-width-1-1">
										<div class="group-inner">
											<label class="label-1">Ime platioca:</label>
											<input type="text" class="input-1">
										</div>
									</div>
									<div class="group uk-width-1-1">
										<div class="group-inner">
											<label class="label-1">Adresa:</label>
											<input type="text" class="input-1">
										</div>
									</div>
									<div class="group uk-width-1-1">
										<div class="group-inner">
											<label class="label-1">Mesto:</label>
											<input type="text" class="input-1">
										</div>
									</div>
									<div class="group uk-width-1-1">
										<div class="group-inner">
											<label class="label-1">Indetifikacion broj korisnika:</label>
											<input type="text" class="input-1">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="bottom-actions">
					<a href="#" class="btn-1 color-1" data-method="openSideModal" data-filter-node="#side-modal-bill-subscription-3">nastavi</a>
					<a href="#" class="btn-1 color-3" data-method="closeInbox">odustani</a>
				</div>
			</form>
		</div>
	</div>
</div>