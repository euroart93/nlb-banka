<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-settings-password-change" class="center-modal-3 side-modal-popup">
			<div class="popup-authorize">
				<div class="horiz-blue-center"></div>
				<h3 class="card-modal-title">Promena lozinke</h3>
			</div>
			<p class="left-p">
				Kako bi ste promenili svoju lozinku,molim potrebne<br>
				podatke u dole navedena polja.Nakon potvrde jednokratnom<br>
				lozinkom Vaša nova lozinka biće spremna za upotrebu.  
			</p>
			<div class="custom-form uk-grid">
				<div class="divider-30"></div>
				<div class="group uk-width-1-1">
					<div class="group-inner">
						<label class="label-1">Unesite trenutnu(staru) lozinku:</label>
						<input type="text" class="input-1" value="">
					</div>
				</div>	
				<div class="group uk-width-1-1">
					<div class="group-inner">
						<label class="label-1">Unesite novu lozinku:</label>
						<input type="text" class="input-1" value="">
					</div>
				</div> 
				<div class="group uk-width-1-1">
					<div class="group-inner">
						<label class="label-1">Ponovite novu lozinku:</label>
						<input type="text" class="input-1" value="">
					</div>
				</div> 
				
				<div class="divider-30"></div>

				<div class="group uk-width-1-2">
					<div class="group-inner">
						<a href="#" class="btn-1 color-1">Potvrdi</a>
					</div>
				</div>
				
				<div class="group uk-width-1-2">
					<div class="group-inner">
						<a href="#" data-filter-node="#side-modal-payment-center" data-method="closeSideModal" class="btn-1 color-3 ">Odustani</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>