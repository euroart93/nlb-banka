<div class="side-modal">
	<div class="side-modal-inner">
		
		<div id="new-subscription-3" class="side-modal-popup">
			<div class="actions">
				<a href="#" class="close-box" data-method="closeSideModal">Close</a>
			</div>
			<form>
				<div class="content-inner" data-method="customScroll">
					<h2>New subscription</h2>
					<ul class="steps left mb-50">
						<li>1 <span>Select bill issuer</span></li>
						<li>2 <span>Bill issuer details</span></li>
						<li class="active">3 <span>Subscription detils</span></li>
					</ul>

					<h3 class="title-1 color-2"><span>Subscriptios info</span></h3>
					<div class="custom-form uk-grid">
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Service nickname</label>
								<input type="text" class="input-1">
							</div>
						</div>
					</div>

					<h3 class="title-1 color-2"><span>Payer Details</span></h3>
					<div class="custom-form uk-grid">
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Name</label>
								<input type="text" class="input-1">
							</div>
						</div>

						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Address</label>
								<input type="text" class="input-1">
							</div>
						</div>

						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">City</label>
								<input type="text" class="input-1">
							</div>
						</div>

						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Bill ID</label>
								<input type="text" class="input-1">
							</div>
						</div>
					</div>

					<h3 class="title-1 color-2"><span>Recive a notification</span></h3>
					<div class="notification-wraper no-border">
						<div class="description">
							<p>Receive notification when new bill from 
							the issuer is received?</p>
						</div>
						<div class="notification-trigger">
							<div class="checkbox-wrap-2">
								<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
							</div>
						</div>
					</div>

					<div class="notification-wraper no-border">
						<div class="description">
							<p>Receive notification about unpaid bill on 
							due date?</p>
						</div>
						<div class="notification-trigger">
							<div class="checkbox-wrap-2">
								<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
							</div>
						</div>
					</div>

				
				</div>
				<div class="bottom-actions">
					<a href="#" class="btn-1 color-3" data-method="closeInbox">Cancel</a>
					<button type="submit" class="btn-1 color-1" data-method="closeInbox">Continue</button>
				</div>
			</form>


		</div>

	</div>
</div>