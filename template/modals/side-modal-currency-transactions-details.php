<div class="side-modal">
	<div class="side-modal-inner">
		
		<div id="side-modal-currency-transaction-details" class="side-modal-popup">
			<div class="actions">
				<div class="col-left">
					<h3 class="title-4 color-2"><span>Filter transakcija</span></h3>
				</div>
				<div class="col-right">
					<a href="#" class="close-box" data-method="closeSideModal">Close</a>
				</div>
			</div>
				<div class="content-inner" data-method="customScroll">
					<div class="plain-list">
						<div>
							<p class="key">Sa računa:</p>
							<p class="value">Moj tekući račun</p>
						</div>
						<div>
							<p class="key">Na račun:</p>
							<p class="value">Moj devizni račun</p>
						</div>
						<div>
							<p class="key">Vrsta transakcije:</p>
							<p class="value">Kupovina deviza</p>
						</div>
						<div>
							<p class="key">Iznos:</p>
							<p class="value">250,00 EUR</p>
						</div>
						<div>
							<p class="key">Valuta:</p>
							<p class="value">EUR</p>
						</div>
						<div>
							<p class="key">Iznos u RSD:</p>
							<p class="value">30.954,00 RSD</p>
						</div>
						<div>
							<p class="key">Kurs:</p>
							<p class="value">123,8160</p>
						</div>
						<div>
							<p class="key">Datum valute:</p>
							<p class="value">08.022017.</p>
						</div>
					</div>
				</div>
				<ul class="side-modal-account-options pdf-download">
					<h4 class="title-modal-options color-2 ">Opcije</h4>
						
					<a href="#">
						<li>Ponovi transakciju
						<i class="side-modal-options-triangle"></i>
						</li>
					</a>
					<a href="#">
						<li>Preuzmi potvrdu u PDF formatu
						<i class="side-modal-options-triangle"></i>
						</li>
					</a>	
				</ul>
		</div>
	</div>
</div>