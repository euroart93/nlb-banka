<div class="side-modal">
	<div class="side-modal-inner">
		
		<div id="side-modal-dashboard-shortcut-selector" class="side-modal-popup">
			<div class="actions">
				<div class="col-left">
					<h3 class="title-4 color-2"><span>Odaberite svoje brze akcije</span></h3>
				</div>
				<div class="col-right">
					<a href="#" class="close-box" data-method="closeSideModal">Close</a>
				</div>
                <p class="col-left">Odaberite brze akcije koje želite prikazati na svojem početnom zaslonu.
                Napomena: potrebno je odabrati <strong>četiri</strong> akcije.</p>
			</div>
			<form>
				<div class="content-inner" data-method="customScroll">
					<div class="dashboard-shortcut-list">		
						<div class="description">
                            <p class="shortcut-name">Računi detalji</p>
                            <div class="float-right notification-trigger trigger-mobile ">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <p class="shortcut-name">Štednja detalji</p>
                            <div class="float-right notification-trigger trigger-mobile ">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <p class="shortcut-name">Krediti detalji</p>
                            <div class="float-right notification-trigger trigger-mobile ">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <p class="shortcut-name">Kartice detalji</p>
                            <div class="float-right notification-trigger trigger-mobile ">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <p class="shortcut-name">Plaćanje</p>
                            <div class="float-right notification-trigger trigger-mobile ">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <p class="shortcut-name">Interni prenos</p>
                            <div class="float-right notification-trigger trigger-mobile ">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <p class="shortcut-name">Kupoprodaja deviza</p>
                            <div class="float-right notification-trigger trigger-mobile ">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <p class="shortcut-name">Devizni nalog</p>
                            <div class="float-right notification-trigger trigger-mobile ">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <p class="shortcut-name">Bank offers</p>
                            <div class="float-right notification-trigger trigger-mobile ">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <p class="shortcut-name">Šabloni</p>
                            <div class="float-right notification-trigger trigger-mobile ">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <p class="shortcut-name">Kursna lista</p>
                            <div class="float-right notification-trigger trigger-mobile ">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <p class="shortcut-name">Katalog proizvoda</p>
                            <div class="float-right notification-trigger trigger-mobile ">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <p class="shortcut-name">Odjava</p>
                            <div class="float-right notification-trigger trigger-mobile ">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <p class="shortcut-name">Podešavanja</p>
                            <div class="float-right notification-trigger trigger-mobile ">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
					</div>
				</div>
					
				<div class="bottom-actions">
					<a href="#" class="btn-1 btn-width color-3" data-method="closeInbox">Potvrdi izbor</a>
					<a href="#" class="btn-1 btn-width color-1" data-method="closeInbox">Odustani</a>
				</div>
			</form>
		</div>
	</div>
</div>