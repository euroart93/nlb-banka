<div class="side-modal">
	<div class="side-modal-inner">
		
		<div id="standing-order-details" class="side-modal-popup">
			<div class="actions">
				<div class="col-left">
					<h3 class="title-4 color-2"><span>Detalji trajnog naloga</span></h3>
				</div>
				<div class="col-right">
					<a href="#" class="close-box" data-method="closeSideModal">Close</a>
				</div>
			</div>
				<div class="content-inner" data-method="customScroll">
					<div class="plain-list">
						<div>
							<p class="key">Naziv:</p>
							<p class="value">Mesečni prenos sredstava</p>
						</div>
						<div>
							<p class="key">Tip trajnog naloga:</p>
							<p class="value">Trajni nalog eksterni prenos</p>
						</div>
						<div>
							<p class="key">Vrsta transakcije:</p>
							<p class="value">Kupovina deviza</p>
						</div>
						<div>
							<p class="key">Početni datum:</p>
							<p class="value">01.04.2017.</p>
						</div>
						<div>
							<p class="key">Datum završetka:</p>
							<p class="value">30.09.2017.</p>
						</div>
						<div>
							<p class="key">Period:</p>
							<p class="value">Mesečni</p>
						</div>
						<div>
							<p class="key">Datum sledeće naplate:</p>
							<p class="value">01.08.2017.</p>
						</div>
						<div>
							<p class="key">Ime računa platioca:</p>
							<p class="value">Moj tekući račun</p>
						</div>
						<div>
							<p class="key">Račun platioca:</p>
							<p class="value">310-1500000065432-15</p>
						</div>
						<div>
							<p class="key">Naziv primaoca:</p>
							<p class="value">Petar Petrović</p>
						</div>
						<div>
							<p class="key">Račun primaoca:</p>
							<p class="value">310-1500000023456-15</p>
						</div>
						<div>
							<p class="key">Adresa primaoca:</p>
							<p class="value">Jurija Gagarina 98</p>
						</div>
						<div>
							<p class="key">Mesto primaoca:</p>
							<p class="value">Beograd</p>
						</div>
						<div>
							<p class="key">Model i poziv na broj:</p>
							<p class="value">0512345-654</p>
						</div>
					</div>
				</div>
				<ul class="side-modal-account-options pdf-download">
					<h4 class="title-modal-options color-2 ">Opcije</h4>
						
					<a href="#" data-method="openPopup" data-popup="24">
						<li>Promeni naziv radnog naloga
						<i class="side-modal-options-triangle"></i>
						</li>
					</a>
				</ul>

				<div class="bottom-actions">
			</div>
		<a class="btn-1 color-1 full-width" data-method="openPopup" data-popup="23">
			otkaži trajni nalog
		</a>
		</div>
	</div>
</div>