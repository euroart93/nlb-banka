<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-my-templates-details" class="side-modal-popup">
			<div class="actions">
				<div class="col-left">
					<h3 class="title-4 color-2"><span>Detalji šablona</span></h3>
				</div>
				<div class="col-right">
					<a href="#" class="close-box" data-method="closeSideModal">Close</a>
				</div>
			</div>
			<div class="modal-transaction-details plain-list">
				
				<div class="content-inner" data-method="customScroll">
						<div class="widget-slat">
								<div>
									<p class="key">Naziv šablona:</p>
									<p class="value">Elektrodistribucija Beograd</p>
								</div>

								<h4 class="title-6 color-2"><span>platioc</span></h4>

								<div class="plain-list">
									<div>
										<p class="key">Naziv:</p>
										<p class="value">Igor Jovanović</p>
									</div>
									<div>
										<p class="key">Adresa:</p>
										<p class="value">Generala Štefanika 15</p>
									</div>
									<div>
										<p class="key">Mesto:</p>
										<p class="value">Beograd</p>
									</div>
									<div>
										<p class="key">Broj Računa:</p>
										<p class="value">125-0000000000106-5</p>
									</div>
								</div>

								<h4 class="title-6 color-2"><span>Primalac</span></h4>

								<div class="plain-list">
									<div>
										<p class="key">Naziv:</p>
										<p class="value">Igor Jovanović</p>
									</div>
									<div>
										<p class="key">Adresa:</p>
										<p class="value">Generala Štefanika 15</p>
									</div>
									<div>
										<p class="key">Mesto:</p>
										<p class="value">Beograd</p>
									</div>
									<div>
										<p class="key">Model i poziv na broj:</p>
										<p class="value">00 23-174-06326578</p>
									</div>
									<div>
										<p class="key">Broj Računa:</p>
										<p class="value">125-0000000000106-5</p>
									</div>
								</div>

								<h4 class="title-6 color-2"><span>Detalji platnog naloga</span></h4>

								<div class="plain-list">
									<div>
										<p class="key">Svrha plaćanja:</p>
										<p class="value">Račun za el.energiju 02/2017</p>
									</div>
									<div>
										<p class="key">Šifra plaćanja:</p>
										<p class="value">289</p>
									</div>
									<div>
										<p class="key">Iznos:</p>
										<p class="value">10.000,00 RSD</p>
									</div>
								</div>
						</div>
				</div>
				<div class="bottom-actions">
					<a href="#" class="btn-1 btn-width color-3" data-method="closeInbox">novi nalog</a>
					<a href="#" class="btn-1 btn-width color-1" data-method="closeInbox">izbriši šablon</a>
				</div>
			</div>
			
		</div>
	</div>
</div>