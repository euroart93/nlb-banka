<div class="side-modal">
	<div class="side-modal-inner">
		
		<div id="transaction-detail" class="side-modal-popup">
			<div class="actions">
				<a href="#" class="close-box" data-method="closeSideModal">Close</a>
			</div>

			<div class="content-inner no-action-btns" data-method="customScroll">
				<h2>Transaction details</h2>
				<div class="plain-list">
					<div class="action-pointer">
						<p class="key">From account:</p>
						<p class="value">My Current Account</p>
					</div>
					<div class="action-pointer">
						<p class="key">To account:</p>
						<p class="value">My Foreign Currency Account</p>
					</div>
					<div class="tag up">
						<p class="key">Type of Exchange:</p>
						<p class="value">Buy currency</p>
					</div>
					<div>
						<p class="key">Conversion amount:</p>
						<p class="value">250,00 EUR</p>
					</div>
					<div>
						<p class="key">Currency:</p>
						<p class="value">EUR</p>
					</div>
					<div>
						<p class="key">Value date:</p>
						<p class="value">07/02/2016</p>
					</div>
					<div>
						<p class="key">Transfer status:</p>
						<p class="value">Completed</p>
					</div>
					<div class="action-pointer">
						<p class="key">Category:</p>
						<p class="value icon-value">Holidays &amp; <i class="icon-traveling"></i> Traveling</p>
					</div>
				</div>
				<h3 class="title-1 color-2"><span>Geolocation</span></h3>
				<div class="map-wrapper-side-modal">
					<div class="infobox-wrapper">
						<div id="infobox1" class="infobox bg-atm">
					        <p class="loc-title">Bank</p>
					        <p class="loc-info">Jurija Gagarina 12<br>
					        	Beograd
					        </p>
					    </div>
					</div>
					<div id="map-canvas-side-modal" data-method="googleMapSideModal"></div>
				</div>

				<h3 class="title-1 color-2"><span>Options</span></h3>
				<div class="plain-list no-mb">
					<div class="plain-list-link action-pointer">
						<a href="#">Create standing order</a>
					</div>
					<div class="plain-list-link action-pointer">
						<a href="#">Repeat transaction</a>
					</div>
					<div class="plain-list-link action-pointer">
						<a href="#">Download PDF confirmation</a>
					</div>
				</div>

			</div>

		</div>

	</div>
</div>