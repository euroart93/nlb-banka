<div class="side-modal">
	<div class="side-modal-inner">
		
		<div id="side-modal-currency-transaction-filter" class="side-modal-popup">
			<div class="actions">
				<div class="col-left">
					<h3 class="title-4 color-2"><span>Filter transakcija</span></h3>
				</div>
				<div class="col-right">
					<a href="#" class="close-box" data-method="closeSideModal">Close</a>
				</div>
			</div>
			<form>
				<div class="content-inner" data-method="customScroll">

					<div class="custom-form uk-grid">
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Sa računa:</label>
								<div class="select-3">
									<select data-method="customSelect3">
		            					<option value="0">Moj tekući račun</option>
		            					<option value="1">moj tekući račun 2</option>
		            					<option value="2">moj tekući račun 3</option>
		            				</select>
		            			</div>
							</div>
						</div>
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Na račun:</label>
								<div class="select-3">
									<select data-method="customSelect3">
		            					<option value="0">Moj devizni</option>
		            					<option value="1">Moj devizni 2</option>
		            					<option value="2">Moj devizni 3</option>
		            				</select>
		            			</div>
							</div>
						</div>
						<div class="group uk-width-1-1">
							<label class="label-1">Datum od:</label>
							<div class="group-inner date-wrap full-width">
								<input type="text" class="input-1 dark date-input" value="01/05/2016" data-method="dateInput">
							</div>
						</div>
						<div class="group uk-width-1-1">
							<label class="label-1">Datum do:</label>
							<div class="group-inner date-wrap full-width">
								<input type="text" class="input-1 dark date-input" value="01/05/2016" data-method="dateInput">
							</div>
						</div>
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Iznos od:</label>
								<input type="text" class="input-1" value="0,00">
							</div>
						</div>
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Iznos do:</label>
								<input type="text" class="input-1" value="0,00">
							</div>
						</div>
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Valuta:</label>
								<div class="select-3">
									<select data-method="customSelect3">
		            					<option value="0">EUR</option>
		            					<option value="1">USD</option>
		            					<option value="2">GBP</option>
		            					<option value="3">AUD</option>
		            				</select>
		            			</div>
							</div>
						</div>
						<div class="group uk-width-1-1" style="padding-bottom: 100px;">
							<div class="group-inner">
								<label class="label-1">Tip transakcije:</label>
								<div class="select-3">
									<select data-method="customSelect3">
		            					<option value="0">Kupovina</option>
		            					<option value="1">Prodaja</option>
		            				</select>
		            			</div>
							</div>
						</div>
					</div>
				</div>
				<div class="bottom-actions">
					<a href="#" class="btn-1 btn-width color-3" data-method="closeInbox">Filtriraj</a>
					<a href="#" class="btn-1 btn-width color-1" data-method="closeInbox">Poništi filter</a>
				</div>
			</form>


		</div>

	</div>
</div>