<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-transaction-list" class="side-modal-popup">
			<div class="actions">
				<a href="#" class="close-box" data-method="closeSideModal">Close</a>
			</div>
				<div class="content-inner no-action-btns" data-method="customScroll">
					<h2>Transactions: Nov 8, 2016</h2>

					<h3 class="title-1 color-2" style="margin-bottom: 9px"><span>Outgoing</span></h3>
					
					<div class="custom-table table-2 border-top m-bottom">
						<div class="table-row row-1 grid">
							<div class="col col-ls-15 col-ms-8 col-ss-8 tag-col">
								<div class="tag tag-2 tagcolor-1 up">II</div>
								<p class="col-text text-7 text-cl-2 left"><span>To: Ivana Ivanović<br>115-00000000004265988-23</span></p>
							</div>
							<div class="col col-ls-9 col-ms-4 col-ss-4 no-border">
								<p class="col-text text-3 text-cl-3 right">99.323.450,<span>00 RSD</span></p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-15 col-ms-8 col-ss-8 tag-col">
								<div class="tag tag-2 tagcolor-2 up">DP</div>
								<p class="col-text text-7 text-cl-2 left"><span>To: Darko Pejnović<br>115-00000000004265988-23</span></p>
							</div>
							<div class="col col-ls-9 col-ms-4 col-ss-4 no-border">
								<p class="col-text text-3 text-cl-3 right">5.900,<span>00 RSD</span></p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-15 col-ms-8 col-ss-8 tag-col">
								<div class="tag tag-2 tagcolor-3 up">AS</div>
								<p class="col-text text-7 text-cl-2 left"><span>To: Aleksandra Stanković<br>115-00000000004265988-23</span></p>
							</div>
							<div class="col col-ls-9 col-ms-4 col-ss-4 no-border">
								<p class="col-text text-3 text-cl-3 right">323.450,<span>00 RSD</span></p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-15 col-ms-8 col-ss-8 tag-col">
								<div class="tag tag-2 tagcolor-2 up">DP</div>
								<p class="col-text text-7 text-cl-2 left"><span>To: Darko Pejnović<br>115-00000000004265988-23</span></p>
							</div>
							<div class="col col-ls-9 col-ms-4 col-ss-4 no-border">
								<p class="col-text text-3 text-cl-3 right">323.450,<span>00 RSD</span></p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-15 col-ms-8 col-ss-8 tag-col">
								<div class="tag tag-2 tagcolor-3 up">AS</div>
								<p class="col-text text-7 text-cl-2 left"><span>To: Aleksandra Stanković<br>115-00000000004265988-23</span></p>
							</div>
							<div class="col col-ls-9 col-ms-4 col-ss-4 no-border">
								<p class="col-text text-3 text-cl-3 right">323.450,<span>00 RSD</span></p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-15 col-ms-8 col-ss-8 tag-col">
								<div class="tag tag-2 tagcolor-1 up">II</div>
								<p class="col-text text-7 text-cl-2 left"><span>To: Ivana Ivanović<br>115-00000000004265988-23</span></p>
							</div>
							<div class="col col-ls-9 col-ms-4 col-ss-4 no-border">
								<p class="col-text text-3 text-cl-3 right">323.450,<span>00 RSD</span></p>
							</div>
						</div>
					</div>

					<h3 class="title-1 color-2" style="margin-bottom: 9px"><span>Incoming</span></h3>

					<div class="custom-table table-2 border-top m-bottom">
						<div class="table-row row-1 grid">
							<div class="col col-ls-15 col-ms-8 col-ss-8 tag-col">
								<div class="tag tag-2 tagcolor-1 up">II</div>
								<p class="col-text text-7 text-cl-2 left"><span>To: Ivana Ivanović<br>115-00000000004265988-23</span></p>
							</div>
							<div class="col col-ls-9 col-ms-4 col-ss-4 no-border">
								<p class="col-text text-3 text-cl-3 right">99.323.450,<span>00 RSD</span></p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-15 col-ms-8 col-ss-8 tag-col">
								<div class="tag tag-2 tagcolor-2 up">DP</div>
								<p class="col-text text-7 text-cl-2 left"><span>To: Darko Pejnović<br>115-00000000004265988-23</span></p>
							</div>
							<div class="col col-ls-9 col-ms-4 col-ss-4 no-border">
								<p class="col-text text-3 text-cl-3 right">5.900,<span>00 RSD</span></p>
							</div>
						</div>
					</div>

				</div>
		</div>

	</div>
</div>