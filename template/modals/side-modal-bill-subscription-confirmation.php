<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-bill-subscription-confirmation" class="center-modal-3 side-modal-popup">
			<div class="popup-authorize">
				<div class="horiz-blue-center"></div>
				<h4>Nova pretplata</h4>
				<p>
					Vaš zahtev za novu pretplatu na račun uspešno je poslat. Ako su svi uneseni podaci tačni, prvi račun možete da očekujete tokom sledećeg meseca.
				</p>	
			</div>

			<div class="custom-form uk-grid">
				<div class="group uk-width-1-1">
					<div class="group-inner">
						<a href="#" data-filter-node="#side-modal-bill-subscription-confirmation" data-method="closeSideModal" class="btn-1 color-1 ">u redu</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>