<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-bill-details" class="side-modal-popup">
			<div class="actions">
				<div class="col-left">
					<h3 class="title-4 color-2"><span>Detalji pretplate</span></h3>
				</div>
				<div class="col-right">
					<a href="#" class="close-box" data-method="closeSideModal">Close</a>
				</div>
			</div>
			<div class="modal-transaction-details plain-list">
				
				<div class="content-inner" data-method="customScroll">
						<div class="widget-slat">
								<div class="plain-list">
									<div>
										<p class="key">Ime računa:</p>
										<p class="value">Račun za struju</p>
									</div>
									<div>
										<p class="key">Naziv primaoca:</p>
										<p class="value">Generala Štefanika 15</p>
									</div>
									<div>
										<p class="key">Adresa:</p>
										<p class="value">Generala Štefanika 15</p>
									</div>
									<div>
										<p class="key">Mesto:</p>
										<p class="value">Beograd</p>
									</div>
									<div>
										<p class="key">Broj računa:</p>
										<p class="value">125-000000000106-5</p>
									</div>
									<div>
										<p class="key">Period izdavanja računa:</p>
										<p class="value">Mesečni</p>
									</div>
									<div>
										<p class="key">Svrha plaćanja:</p>
										<p class="value">Račun za uslugu</p>
									</div>
									<div>
										<p class="key">Status:</p>
										<p class="value">Aktivan</p>
									</div>
								</div>

								<h4 class="title-6 color-2"><span>podaci o platiocu</span></h4>

								<div class="plain-list">
									<div>
										<p class="key">Ime platioca:</p>
										<p class="value">Igor Jovanović</p>
									</div>
									<div>
										<p class="key">Adresa:</p>
										<p class="value">Generala Štefanika 15</p>
									</div>
									<div>
										<p class="key">Mesto:</p>
										<p class="value">Beograd</p>
									</div>
									<div>
										<p class="key">Model i poziv na broj:</p>
										<p class="value">00 23-174-06326578</p>
									</div>
									<div>
										<p class="key">Račun platioca:</p>
										<p class="value">125-0000000000106-5</p>
									</div>
								</div>
						</div>
				</div>
				<div class="bottom-actions">
					<a href="#" class="btn-1 btn-width color-1" data-method="closeInbox">otkaži pretplatu</a>
				</div>
			</div>
			
		</div>
	</div>
</div>