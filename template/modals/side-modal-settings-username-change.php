<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-settings-username-change" class="center-modal-3 side-modal-popup">
			<div class="popup-authorize">
				<div class="horiz-blue-center"></div>
				<h3 class="card-modal-title">Promena korisničkog imena</h3>
			</div>
			<p class="left-p">
				Vaše trenutno korisničko ime je :<span>igor.jovanovic.</span>Kako biste<br>
				promenili svoje korisničko ime, molimo unesite potrebne<br>
				podatke u dole navedena polja. 
			</p>
			<div class="custom-form uk-grid">
				<div class="divider-30"></div>
				<div class="group uk-width-1-1">
					<div class="group-inner">
						<label class="label-1">Unesite novo korisničko ime:</label>
						<input type="text" class="input-1" value="">
					</div>
				</div>	
				<div class="group uk-width-1-1">
					<div class="group-inner">
						<label class="label-1">Ponovite novo korisničko ime:</label>
						<input type="text" class="input-1" value="">
					</div>
				</div> 
				
				<div class="divider-30"></div>

				<div class="group uk-width-1-2">
					<div class="group-inner">
						<a href="#"  class="btn-1 color-1">Potvrdi</a>
					</div>
				</div>
				
				<div class="group uk-width-1-2">
					<div class="group-inner">
						<a href="#" data-filter-node="#side-modal-payment-center" data-method="closeSideModal" class="btn-1 color-3 ">Odustani</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>