<div class="side-modal">
	<div class="side-modal-inner">
		
		<div id="side-modal-inbox" class="side-modal-popup inbox-bg-1">
			<div class="actions">
				<a href="#" class="close-box" data-method="closeSideModal">Close</a>
				<a href="#" class="next-box">Next</a>
				<a href="#" class="prev-box">Prev</a>
			</div>
			<div class="content-inner" data-method="customScroll">
				<div class="inbox-content">
					<h2>Class aptent taciti sociosqu ad litora torquent per conubia</h2>
					<h3>Bank offers <span></span> 4 Feb 2016 15:24</h3>
					<p>Suspendisse pulvinar elit non pulvinar consequat. Vivamus aliquam mollis metus, eu dapibus neque tincidunt eu. Praesent felis metus, suscipit sit amet dolor in, commodo ornare enim. Aenean enim magna, efficitur ut nisl sed, pretium fringilla ligula. Phasellus id lorem leo.</p>
					<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis eleifend neque sollicitudin <a href="#">pretium cursus</a>. Nunc faucibus elementum urna. Vivamus sollicitudin ligula at magna elementum, sed volutpat sapien elementum. Nulla ullamcorper ac leo nec cursus. </p>
					<p>Nam vehicula tincidunt tempor. Cras ac elementum est. Integer ut diam quis ligula <strong>dapibus iaculis ac a nunc</strong>. Sed sagittis sem sem, ut hendrerit libero pulvinar eu. Morbi vitae blandit neque. Mauri</p>
					<p>Maecenas laoreet, erat vel elementum congue, elit nisi interdum justo, at fringilla odio lorem nec neque. Pellentesque sagittis vehicula maximus. Vestibulum vitae lacus sollicitudin, consequat sapien vel, mollis lacus. Aliquam eu nulla vel nulla lacinia volutpat.</p>
					<img src="img/demo/msg-img.jpg" alt="img">
				</div>
				<div class="attachments">
					<p class="attachments-title">Attachments</p>
					<ul>
						<li>
							<a href="#">
								<i class="icon-attachment-1"></i>
								<p class="name">Pellentesque sagittis vehicula maximus</p>
								<p class="doc">PDF document</p>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="icon-attachment-1"></i>
								<p class="name">Class aptent taciti sociosqu ad litora torquent</p>
								<p class="doc">XLS document</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="bottom-actions">
				<a href="#" class="btn-1 color-2" daat-method="closeInbox">Archive message</a>
				<a href="#" class="btn-1 color-1" data-method="openInbox" data-inbox="2">Reply</a>
			</div>
		</div>

	</div>
</div>