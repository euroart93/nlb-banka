<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-bill-subscription-sms" class="center-modal-2 side-modal-popup">
			<div class="popup-authorize">
				<div class="horiz-blue-center"></div>
				<h3 class="card-modal-title">Autorizacija plaćanja</h3>
				<img src="img/bg/sms-authorize.png">
			</div>
			<p>Autoricacijski kod za plaćanje poslat je na Vaš broj mobilnog 
			telefona.Ako ga niste primili,kliknite na dugme 'Pošalji ponovno.
			</p>	

			<div class="custom-form uk-grid">
				<div class="group-inner group-center">
						<a href="#" data-filter-node="#side-modal-payment-center" data-method="openSideModal" class="center-btn ">Pošalji ponovo</a>
				</div>

				<div class="divider-30"></div>
				<div class="group uk-width-1-1">
					<div class="group-inner">
						<label class="label-1">Unesite jednokratnu lozinku:</label>
						<input type="text" class="input-1" value="">
					</div>
				</div>	
				<div class="group uk-width-1-2">
					<div class="group-inner">
						<a href="#" data-method="openSideModal" data-filter-node="#side-modal-bill-subscription-confirmation" class="btn-1 color-1">Potvrdi</a>
					</div>
				</div>
				
				<div class="group uk-width-1-2">
					<div class="group-inner">
						<a href="#" data-filter-node="#side-modal-payment-center" data-method="closeSideModal" class="btn-1 color-3 ">Odustani</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>