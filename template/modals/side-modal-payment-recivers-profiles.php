<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-payment-recivers-profiles" class="side-modal-popup ">
			<div class="actions">
				<div class="col-left">
					<h3 class="title-4 color-2"><span>Odaberi primaoca/šablon</span></h3>
				</div>
				<div class="col-right">
					<a href="#" class="close-box" data-method="closeSideModal">Close</a>
				</div>
			</div>
			
			<div class="recivers-list">
				<ul class="widget-tabs" style="margin-bottom: 25px;">
					<li class="active"><a href="#">Moji<br>Šabloni</a></li>
					<li><a href="#">Prethodni<br>primaoci</a></li>
					<li><a href="#">Predefinisani<br>primaoci</a></li>
				</ul>
				<div class="group news" style="margin-bottom: 25px;">
					<div class="group-inner input-search">
						<input type="search" class="input-1" placeholder="Traži...">
						<button type="button" class="search-btn"></button>
					</div>
				</div>
				
				<div class="content-inner" data-method="customScroll">
					<div class="reciver-info cf">
						<div class="id-tag">E</div>
						<p class="id-p">Elektroprivreda Srbije</p>
						<a href="#" class="btn-1 btn-width-card color-1">Odaberi</a>
						<div class="hidden">
							<div class="plain-list cf">
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">RS3511503816599631</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Model i poziv na broj:</p>
									<p class="value">00  3212546-160</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Šifra plaćanja:</p>
									<p class="value">290-Elektroprivreda Srbije</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Iznos:</p>
									<p class="value">1.200,00</p>
								</div>
							</div>
						</div>
					</div>
					<div class="reciver-info cf">
						<div class="id-img">
							<img src="img/icons/reciver-1.png">
						</div>
						<p class="id-p">Elektroprivreda Srbije</p>
						<a href="#" class="btn-1 btn-width-card color-1">Odaberi</a>
						<div class="hidden">
							<div class="plain-list cf">
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">RS3511503816599631</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Model i poziv na broj:</p>
									<p class="value">00  3212546-160</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Šifra plaćanja:</p>
									<p class="value">290-Elektroprivreda Srbije</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Iznos:</p>
									<p class="value">1.200,00</p>
								</div>
							</div>
						</div>
					</div>
					<div class="reciver-info cf">
						<div class="id-tag">O</div>
						<p class="id-p">Osiguranje za auto</p>
						<a href="#" class="btn-1 btn-width-card color-1">Odaberi</a>
						<div class="hidden">
							<div class="plain-list cf">
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">RS3511503816599631</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Model i poziv na broj:</p>
									<p class="value">00  3212546-160</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Šifra plaćanja:</p>
									<p class="value">298-Osiguranje za auti</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Iznos:</p>
									<p class="value">7.200,00</p>
								</div>
							</div>
						</div>
					</div>
					<div class="reciver-info cf">
						<div class="id-img">
							<img src="img/icons/reciver-2.png">
						</div>
						<p class="id-p">Milica Todorović</p>
						<a href="#" class="btn-1 btn-width-card color-1">Odaberi</a>
						<div class="hidden">
							<div class="plain-list cf">
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">RS3511503816599631</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Model i poziv na broj:</p>
									<p class="value">00  3212546-160</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Šifra plaćanja:</p>
									<p class="value">292-igraonica</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Iznos:</p>
									<p class="value">8.200,00</p>
								</div>
							</div>
						</div>
					</div>
					<div class="reciver-info cf">
						<div class="id-tag">E</div>
						<p class="id-p">Elektroprivreda Srbije</p>
						<a href="#" class="btn-1 btn-width-card color-1">Odaberi</a>
						<div class="hidden">
							<div class="plain-list cf">
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">RS3511503816599631</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Model i poziv na broj:</p>
									<p class="value">00  3212546-160</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Šifra plaćanja:</p>
									<p class="value">290-Elektroprivreda Srbije</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Iznos:</p>
									<p class="value">1.200,00</p>
								</div>
							</div>
						</div>
					</div>
					<div class="reciver-info cf">
						<div class="id-tag">V</div>
						<p class="id-p">Vodoprivred Srbije</p>
						<a href="#" class="btn-1 btn-width-card color-1">Odaberi</a>
						<div class="hidden">
							<div class="plain-list cf">
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">RS3511503816599631</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Model i poziv na broj:</p>
									<p class="value">00  3212546-160</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Šifra plaćanja:</p>
									<p class="value">280-Vodoprivreda Srbije</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Iznos:</p>
									<p class="value">1.200,00</p>
								</div>
							</div>
						</div>
					</div>
					<div class="reciver-info cf">
						<div class="id-tag">N</div>
						<p class="id-p">Nešto.d.o.o</p>
						<a href="#" class="btn-1 btn-width-card color-1">Odaberi</a>
						<div class="hidden">
							<div class="plain-list cf">
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">RS3511503816599631</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Model i poziv na broj:</p>
									<p class="value">00  3212546-160</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Šifra plaćanja:</p>
									<p class="value">230-Nešto d.o.o</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Iznos:</p>
									<p class="value">13.200,00</p>
								</div>
							</div>
						</div>
					</div>
					<div class="reciver-info cf">
						<div class="id-tag">M</div>
						<p class="id-p">Med i Mleko</p>
						<a href="#" class="btn-1 btn-width-card color-1">Odaberi</a>
						<div class="hidden">
							<div class="plain-list cf">
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">RS3511503816599631</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Model i poziv na broj:</p>
									<p class="value">00  3212546-160</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Šifra plaćanja:</p>
									<p class="value">230-Medi i mleko</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Iznos:</p>
									<p class="value">200,00</p>
								</div>
							</div>
						</div>
					</div>
					<div class="reciver-info cf">
						<div class="id-tag">E</div>
						<p class="id-p">Elektroprivreda Srbije</p>
						<a href="#" class="btn-1 btn-width-card color-1">Odaberi</a>
						<div class="hidden">
							<div class="plain-list cf">
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">RS3511503816599631</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Model i poziv na broj:</p>
									<p class="value">00  3212546-160</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Šifra plaćanja:</p>
									<p class="value">290-Elektroprivreda Srbije</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Iznos:</p>
									<p class="value">1.200,00</p>
								</div>
							</div>
						</div>
					</div>
					<div class="reciver-info cf">
						<div class="id-img">
							<img src="img/icons/reciver-1.png">
						</div>
						<p class="id-p">Elektroprivreda Srbije</p>
						<a href="#" class="btn-1 btn-width-card color-1">Odaberi</a>
						<div class="hidden">
							<div class="plain-list cf">
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">RS3511503816599631</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Model i poziv na broj:</p>
									<p class="value">00  3212546-160</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Šifra plaćanja:</p>
									<p class="value">290-Elektroprivreda Srbije</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Iznos:</p>
									<p class="value">1.200,00</p>
								</div>
							</div>
						</div>
					</div>
					<div class="reciver-info cf">
						<div class="id-tag">O</div>
						<p class="id-p">Osiguranje za auto</p>
						<a href="#" class="btn-1 btn-width-card color-1">Odaberi</a>
						<div class="hidden">
							<div class="plain-list cf">
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">RS3511503816599631</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Model i poziv na broj:</p>
									<p class="value">00  3212546-160</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Šifra plaćanja:</p>
									<p class="value">298-Osiguranje za auti</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Iznos:</p>
									<p class="value">7.200,00</p>
								</div>
							</div>
						</div>
					</div>
					<div class="reciver-info cf">
						<div class="id-img">
							<img src="img/icons/reciver-2.png">
						</div>
						<p class="id-p">Milica Todorović</p>
						<a href="#" class="btn-1 btn-width-card color-1">Odaberi</a>
						<div class="hidden">
							<div class="plain-list cf">
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">RS3511503816599631</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Model i poziv na broj:</p>
									<p class="value">00  3212546-160</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Šifra plaćanja:</p>
									<p class="value">292-igraonica</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Iznos:</p>
									<p class="value">8.200,00</p>
								</div>
							</div>
						</div>
					</div>
					<div class="reciver-info cf">
						<div class="id-tag">E</div>
						<p class="id-p">Elektroprivreda Srbije</p>
						<a href="#" class="btn-1 btn-width-card color-1">Odaberi</a>
						<div class="hidden">
							<div class="plain-list cf">
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">RS3511503816599631</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Model i poziv na broj:</p>
									<p class="value">00  3212546-160</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Šifra plaćanja:</p>
									<p class="value">290-Elektroprivreda Srbije</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Iznos:</p>
									<p class="value">1.200,00</p>
								</div>
							</div>
						</div>
					</div>
					<div class="reciver-info cf">
						<div class="id-tag">V</div>
						<p class="id-p">Vodoprivred Srbije</p>
						<a href="#" class="btn-1 btn-width-card color-1">Odaberi</a>
						<div class="hidden">
							<div class="plain-list cf">
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">RS3511503816599631</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Model i poziv na broj:</p>
									<p class="value">00  3212546-160</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Šifra plaćanja:</p>
									<p class="value">280-Vodoprivreda Srbije</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Iznos:</p>
									<p class="value">1.200,00</p>
								</div>
							</div>
						</div>
					</div>
					<div class="reciver-info cf">
						<div class="id-tag">N</div>
						<p class="id-p">Nešto.d.o.o</p>
						<a href="#" class="btn-1 btn-width-card color-1">Odaberi</a>
						<div class="hidden">
							<div class="plain-list cf">
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">RS3511503816599631</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Model i poziv na broj:</p>
									<p class="value">00  3212546-160</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Šifra plaćanja:</p>
									<p class="value">230-Nešto d.o.o</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Iznos:</p>
									<p class="value">13.200,00</p>
								</div>
							</div>
						</div>
					</div>
					<div class="reciver-info cf">
						<div class="id-tag">M</div>
						<p class="id-p">Med i Mleko</p>
						<a href="#" class="btn-1 btn-width-card color-1">Odaberi</a>
						<div class="hidden">
							<div class="plain-list cf">
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">RS3511503816599631</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Model i poziv na broj:</p>
									<p class="value">00  3212546-160</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Šifra plaćanja:</p>
									<p class="value">230-Medi i mleko</p>
								</div>
							</div>
							<div class="plain-list cf">
								<div>
									<p class="key">Iznos:</p>
									<p class="value">200,00</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>