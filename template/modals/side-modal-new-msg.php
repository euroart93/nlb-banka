<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-new-msg" class="side-modal-popup">
			<div class="actions">
				<a href="#" class="close-box" data-method="closeSideModal">Close</a>
			</div>
			<form>
				<div class="content-inner" data-method="customScroll">
					<h2>New message</h2>
					<div class="custom-form uk-grid">
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Message type:</label>
								<div class="select-3">
									<select data-method="customSelect3">
		            					<option value="0">General inquiry/question</option>
		            					<option value="1">Option 1</option>
		            					<option value="2">Option 2</option>
		            					<option value="3">Option 3</option>
		            				</select>
		            			</div>
							</div>
						</div>
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Message title:</label>
								<input type="text" class="input-1" value="Lorem ipsum dolor">
							</div>
						</div>
						<div class="group uk-width-1-1">
							<div class="group-inner">
								<label class="label-1">Your message:</label>
								<textarea class="textarea-1" value="Lorem ipsum dolor"></textarea>
							</div>
						</div>
					</div>
					<div class="attachments">
						<p class="attachments-title">Attachments</p>
						<ul>
							<li>
								<i class="icon-attachment-1"></i>
								<p class="name">Pellentesque sagittis vehicula maximus</p>
								<p class="doc">PDF document</p>
								<a href="#" class="att-delete"></a>
							</li>
						</ul>
						<a href="#" class="btn-1 color-2">Browse file</a>
					</div>
				</div>
				<div class="bottom-actions">
					<a href="#" class="btn-1 color-3" data-method="closeInbox">Cancel</a>
					<a href="#" class="btn-1 color-1" data-method="closeInbox">Submit message</a>
				</div>
			</form>
		</div>

	</div>
</div>