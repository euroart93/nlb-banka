<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-bill-subscription-3" class="side-modal-popup">
			<div class="actions">
				<div class="col-left">
					<h3 class="title-4 color-2"><span>Nova pretplata</span></h3>
				</div>
				<div class="col-right">
					<a href="#" class="close-box" data-method="closeSideModal">Close</a>
				</div>
			</div>
			<div class="content-inner" data-method="customScroll">
				<ul class="steps left mb-50">
					<li>1 <span>izdavač računa</span></li>
					<li>2 <span>Detalji pretplate</span></li>
					<li class="active">3 <span>Detalji izdavača računa</span></li>
				</ul>

				<p class="text-top">
					Provjerite detalje pretplate na odabrani račun i potvrdite
					pritiskom na dugme Nastavi.
				</p>

				<div class="modal-transaction-details plain-list">
			
					<div class="content-inner">
						<div class="widget-slat">
							<div class="plain-list">
								<div>
									<p class="key">Izdavač računa:</p>
									<p class="value">Elektrodistribucija Beograd</p>
								</div>
								<div>
									<p class="key">Adresa:</p>
									<p class="value">Generala Štefanika 15</p>
								</div>
								<div>
									<p class="key">Mesto:</p>
									<p class="value">Beograd</p>
								</div>
								<div>
									<p class="key">Broj računa:</p>
									<p class="value">125-000000000106-5</p>
								</div>
								<div>
									<p class="key">Period izdavanja računa:</p>
									<p class="value">Mesečni</p>
								</div>
								<div>
									<p class="key">Provizija:</p>
									<p class="value">150,00 RSD</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="bottom-actions">
				<a href="#" class="btn-1 color-1" data-method="openSideModal" data-filter-node="#side-modal-bill-subscription-sms">nastavi</a>
				<a href="#" class="btn-1 color-3" data-method="closeInbox">odustani</a>
			</div>
		</div>
	</div>
</div>