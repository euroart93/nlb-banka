<div class="side-modal">
	<div class="side-modal-inner">
		
		<div id="side-modal-dashboard-currency-selector" class="side-modal-popup">
			<div class="actions">
				<div class="col-left">
					<h3 class="title-4 color-2"><span>Odaberite valute</span></h3>
				</div>
				<div class="col-right">
					<a href="#" class="close-box" data-method="closeSideModal">Close</a>
				</div>
                <p class="col-left">Odaberite brze valute koje želite prikazati na svojem početnom zaslonu.
                Napomena: potrebno je odabrati <strong>tri</strong> akcije.</p>
			</div>
			<form>
				<div class="content-inner" data-method="customScroll">
					<div class="dashboard-shortcut-list">		
						<div class="description">
                            <img src="img/photos/EU-2.png">
                            <p class="shortcut-name">EUR (Evropska Unija)</p>
                            <div class="float-right notification-trigger">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <img src="img/photos/usa-2.png">
                            <p class="shortcut-name">USA (Američki Dolar)</p>
                            <div class="float-right notification-trigger">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <img src="img/photos/great-britain-2.png">
                            <p class="shortcut-name">GBP (Britanska Funta)</p>
                            <div class="float-right notification-trigger">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <img src="img/photos/swiss-2.png">
                            <p class="shortcut-name">CHF (Švajcarski Franak)</p>
                            <div class="float-right notification-trigger">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <img src="img/photos/canada-2.png">
                            <p class="shortcut-name">CAD (Kanadski Dolar)</p>
                            <div class="float-right notification-trigger">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <img src="img/photos/norway.png">
                            <p class="shortcut-name">NOK (Norvešla Kruna)</p>
                            <div class="float-right notification-trigger">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <img src="img/photos/austria.png">
                            <p class="shortcut-name">DKK (Danska Kruna)</p>
                            <div class="float-right notification-trigger">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <img src="img/photos/sweden.png">
                            <p class="shortcut-name">SEK (Švedska Kruna)</p>
                            <div class="float-right notification-trigger">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <img src="img/photos/australia.png">
                            <p class="shortcut-name">AUD (Australski Dolar)</p>
                            <div class="float-right notification-trigger">
                                <div class="checkbox-wrap-2">
                                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
                                </div>
                            </div>
                        </div>
                        
					</div>
				</div>
					
				<div class="bottom-actions">
					<a href="#" class="btn-1 btn-width color-3" data-method="closeInbox">Potvrdi izbor</a>
					<a href="#" class="btn-1 btn-width color-1" data-method="closeInbox">Odustani</a>
				</div>
			</form>
		</div>
	</div>
</div>