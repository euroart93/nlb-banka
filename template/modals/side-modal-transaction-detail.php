<div class="side-modal">
	<div class="side-modal-inner">
		
		<div id="transaction-detail-2" class="side-modal-popup">
			<div class="actions">
				<a href="#" class="close-box" data-method="closeSideModal">Close</a>
			</div>
			<form action="#" method="#">

			<div class="content-inner" data-method="customScroll">
				<h2>Transaction details</h2>
				<div class="plain-list">
					<div>
						<p class="key">Sender name:</p>
						<p class="value">Jonathan Doe</p>
					</div>
					<div>
						<p class="key">Sender address:</p>
						<p class="value">Neue Strasse 236, Munich, Germany</p>
					</div>
					<div>
						<p class="key">Value date:</p>
						<p class="value">07/02/2016</p>
					</div>
					<div>
						<p class="key">Remittance value:</p>
						<p class="value">250,00 EUR</p>
					</div>
					<div>
						<p class="key">Status:</p>
						<p class="value">in process</p>
					</div>
				</div>
				<h3 class="title-1 color-2"><span>Documents</span></h3>
				<div class="user-content">
					<p>To complete the remittance payment to your account, please provide the necessary documents:</p>
					<ul>
						<li>Document name 1</li>
						<li>Document name 2</li>
						<li>Document name 3</li>
					</ul>
				</div>
				<div class="attachments">
					<a href="#" class="btn-1 color-2">Browse files</a>
					<ul>
						<li>
							<i class="icon-attachment-1"></i>
							<p class="name">Pellentesque sagittis vehicula maximus</p>
							<p class="doc">PDF document</p>
							<a href="#" class="att-delete"></a>
						</li>
					</ul>
					<ul>
						<li>
							<i class="icon-attachment-1"></i>
							<p class="name">Pellentesque sagittis vehicula maximus</p>
							<p class="doc">PDF document</p>
							<a href="#" class="att-delete"></a>
						</li>
					</ul>
				</div>

			</div>

			<div class="bottom-actions">
				<button class="btn-1 color-1 full-width" type="submit">Send Documents</button>
			</div>

			</form>
		</div>

	</div>
</div>