<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-settings-phone-change" class="center-modal-4 side-modal-popup">
			<div class="popup-authorize">
				<div class="horiz-blue-center"></div>
				<h3 class="card-modal-title">Promena telefonskog broja</h3>
			</div>
			<p class="center-p">
				Vaš telefonski broj uspešno je izmenjen.<br>
				Novi telefonski broj je: <span>+381611234567</span>.
			</p>
			<div class="group uk-width-1-1">
				<div class="group-inner input">
					<a href="#" class="btn-1 color-1">Sačuvaj izmene</a>
				</div>
			</div>
		</div>
	</div>
</div>