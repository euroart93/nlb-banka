<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-card-center" class="center-modal side-modal-popup">
			<div class="horiz-blue-center"></div>
			<h3 class="card-modal-title">Rezervisana sredstva</h3>
			<form>
				<div class="content-inner scrool-card-height" data-method="customScroll">
					<div class="custom-table">

					<div class="table-header modal-center">
						<div class="table-head grid">
							<div class="col col-ls-6 col-ms-2 col-ss-6">
								<p class="left">Datum</p>
							</div>
							<div class="col col-ls-14 col-ms-hidden">
								<p class="left">Opis transakcije</p>
							</div>
							<div class="col col-ls-3 col-ms-4 col-ss-6">
								<p class="right">Iznos</p>
							</div>
						</div>
					</div>

					<div class="table-row row-1 grid">
						<div class="col col-ls-6 col-ms-2 col-ss-6">
							<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
						</div>
						<div class="col col-ls-11 col-ms-hidden">
							<p class="col-text text-2 text-cl-1 left">Račun za 08/2016</p>
						</div>
						<div class="col col-ls-6 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">-569,99 RSD</p>
						</div>
					</div>

					<div class="table-row row-1 grid">
						<div class="col col-ls-6 col-ms-2 col-ss-6">
							<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
						</div>
						<div class="col col-ls-11 col-ms-hidden">
							<p class="col-text text-2 text-cl-1 left">Račun za 08/2016</p>
						</div>
						<div class="col col-ls-6 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">-569,99 RSD</p>
						</div>
					</div>

					<div class="table-row row-1 grid">
						<div class="col col-ls-6 col-ms-2 col-ss-6">
							<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
						</div>
						<div class="col col-ls-11 col-ms-hidden">
							<p class="col-text text-2 text-cl-1 left">Račun za 08/2016</p>
						</div>
						<div class="col col-ls-6 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">-1.000.569,99 RSD</p>
						</div>
					</div>

					<div class="table-row row-1 grid">
						<div class="col col-ls-6 col-ms-2 col-ss-6">
							<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
						</div>
						<div class="col col-ls-11 col-ms-hidden">
							<p class="col-text text-2 text-cl-1 left">Račun za 08/2016</p>
						</div>
						<div class="col col-ls-6 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">-100.000,99 RSD</p>
						</div>
					</div>

					<div class="table-row row-1 grid">
						<div class="col col-ls-6 col-ms-2 col-ss-6">
							<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
						</div>
						<div class="col col-ls-11 col-ms-hidden">
							<p class="col-text text-2 text-cl-1 left">Račun za 08/2016</p>
						</div>
						<div class="col col-ls-6 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">-300.000,99 RSD</p>
						</div>
					</div>

					<div class="table-row row-1 grid">
						<div class="col col-ls-6 col-ms-2 col-ss-6">
							<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
						</div>
						<div class="col col-ls-11 col-ms-hidden">
							<p class="col-text text-2 text-cl-1 left">Račun za 08/2016</p>
						</div>
						<div class="col col-ls-6 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">-130.000,99 RSD</p>
						</div>
					</div>

					<div class="table-row row-1 grid">
						<div class="col col-ls-6 col-ms-2 col-ss-6">
							<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
						</div>
						<div class="col col-ls-11 col-ms-hidden">
							<p class="col-text text-2 text-cl-1 left">Račun za 08/2016</p>
						</div>
						<div class="col col-ls-6 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">-569,99 RSD</p>
						</div>
					</div>

					
				</div>
			</form>
		</div>
		<a href="#" class="btn-1 color-1 btn-center-modal"  data-method="closeSideModal">zatvori</a>
	</div>
</div>