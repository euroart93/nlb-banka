<div class="side-modal">
	<div class="side-modal-inner">

		<div id="side-modal-account-transaction-list" class="side-modal-popup">
			<div class="actions">
				<div class="col-left">
					<h3 class="title-4 color-2"><span>Detalji transakcije</span></h3>
				</div>
				<div class="col-right">
					<a href="#" class="close-box" data-method="closeSideModal">Close</a>
				</div>
			</div>
			<div class="modal-transaction-details plain-list">
				
				<div class="content-inner no-action-btns" data-method="customScroll">
					<div>
						<p class="key">Račun primaoca:</p>
						<p class="value">115-0000000000602-65</p>
					</div>
					<div>
						<p class="key">Naziv,adresa i mesto primaoca:</p>
						<p class="value">Infostan Beograd, Takovska 202,Beograd</p>
					</div>
					<div>
						<p class="key">Model i poziv na broj odobrenja:</p>
						<p class="value">00 123456-789</p>
					</div>
					<div>
						<p class="key">Šifra plaćanja:</p>
						<p class="value">289</p>
					</div>
					<div>
						<p class="key">Svrha plaćanja:</p>
						<p class="value">Mesečna rata 09/2016</p>
					</div>
					<div>
						<p class="key">Račun platioca:</p>
						<p class="value">115-0000000000123-00</p>
					</div>
					<div>
						<p class="key">Naziv,adresa i mesto platioca:</p>
						<p class="value">Igor Marković,Boška Petrovića 22, Beograd</p>
					</div>
					<div>
						<p class="key">Datum valute:</p>
						<p class="value">01.06.2016</p>
					</div>
					<div>
						<p class="key">Status:</p>
						<p class="value">Realizovana</p>
					</div>
					<div>
						<p class="key">Iznos transakcije:</p>
						<p class="value">569,99 RSD</p>
					</div>
					
					<ul class="side-modal-account-options pdf-download">
						<h4 class="title-modal-options color-2 "><span>Opcije</span></h4>
							
						<a href="#">
							<li>Ponovi plaćanje
							<i class="side-modal-options-triangle"></i>
							</li>
						</a>
						<a href="#">
							<li>Kreiraj šablon
							<i class="side-modal-options-triangle"></i>
							</li>
						</a>
						<a href="#">
							<li>Preuzmi potvrdu u PDF formatu
							<i class="side-modal-options-triangle"></i>
							</li>
						</a>	
					</ul>	
				</div>
			</div>
		</div>

	</div>
</div>