<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li>Kursna lista</li></a>
				<a href="#"><li class="selected">Kupovina/prodaja deviza</li></a>
				<a href="#"><li>Lista menjačkih transakcija</li></a>
			</ul>
		</div>
	</div>

	<div class="widget widget-select-account widget-content-white row" data-method="accountSelect">
		<div class="account-selected">
			<div class="col-ss-12 col-ls-2 acc-img">
				<img src="img/demo/acc-img-medium.jpg">
			</div>
			<div class="col-ss-12 col-ls-6 col-border">
				<div class="content">
					<p class="small">Ime računa:</p>
					<p class="big acc-name">Moj tekući račun</p>
				</div>
			</div>
			<div class="col-ss-12 col-ls-7 col-border">
				<div class="content">
					<p class="small">Broj računa:</p>
					<p class="big-s acc-number">115-0000000000567898-6</p>
				</div>
			</div>
			<div class="col-ss-12 col-ls-7 col-border">
				<div class="content">
					<p class="small float-right">Raspoloživo stanje:</p>
					<p class="big acc-amount float-right">5.123.543,89 RSD</p>
				</div>
			</div>
			<div class="col-ss-1">
				<a href="#" class="acc-list-btn acc-drop-btn" type="button" data-method="accountSelectTrigger">
					<div class="triangle"></div>
				</a>
			</div>
		</div>
		<div class="account-select">
  			<ul class="options">
    			 <li class="item">
    			 	<div class="widget row">
						<div class="col-ss-2 col-ms-2 col-ls-2">
							<div class="triangle-right-white"></div>
							<img class="desaturate" src="img/demo/acc-img-medium.jpg">
						</div>
						<div class="col-ss-8 col-ms-9 col-ls-6">
							<div class="content">
								<p class="acc-select-small">Ime računa:</p>
								<p class="big acc-name">Moj tekući račun</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small">Broj računa:</p>
								<p class="big-s acc-number">111-0000000000567898-6</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small float-right">Raspoloživo stanje:</p>
								<p class="big acc-amount float-right">5.123.543,89 RSD</p>
							</div>
						</div>
						<div class="col-ss-1 col-ms-1 col-ls-1  ">
							<div class="triangle-right"></div>
					</div>
    			 </li>
    			 <li class="item">
    			 	<div class="widget row">
						<div class="col-ss-2 col-ms-2 col-ls-2">
							<div class="triangle-right-white"></div>
							<img class="desaturate" src="img/demo/acc-img-medium-2.jpg">
						</div>
						<div class="col-ss-8 col-ms-9 col-ls-6">
							<div class="content">
								<p class="acc-select-small">Ime računa:</p>
								<p class="big acc-name">Drugi tekući račun</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small">Broj računa:</p>
								<p class="big-s acc-number">112-0000000000567898-6</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small float-right">Raspoloživo stanje:</p>
								<p class="big acc-amount float-right">1.850,89 RSD</p>
							</div>
						</div>
						<div class="col-ss-1 col-ms-1 col-ls-1  ">
							<div class="triangle-right"></div>
					</div>
    			 </li>
    			 <li class="item">
    			 	<div class="widget row">
						<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium-3.jpg">
						</div>
						<div class="col-ss-8 col-ms-9 col-ls-6">
							<div class="content">
								<p class="acc-select-small">Ime računa:</p>
								<p class="big acc-name">Treći tekući račun</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small">Broj računa:</p>
								<p class="big-s acc-number">113-0000000000567898-6</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small float-right">Raspoloživo stanje:</p>
								<p class="big acc-amount float-right">2.850,89 RSD</p>
							</div>
						</div>
						<div class="col-ss-1 col-ms-1 col-ls-1  ">
							<div class="triangle-right"></div>
					</div>
    			 </li>
    			 <li class="item">
    			 	<div class="widget row">
						<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium-4.jpg">
						</div>
						<div class="col-ss-8 col-ms-9 col-ls-6">
							<div class="content">
								<p class="acc-select-small">Ime računa:</p>
								<p class="big acc-name">Četvrti tekući račun</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small">Broj računa:</p>
								<p class="big-s acc-number">114-0000000000567898-6</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small float-right">Raspoloživo stanje:</p>
								<p class="big acc-amount float-right">3.850,89 RSD</p>
							</div>
						</div>
						<div class="col-ss-1 col-ms-1 col-ls-1  ">
							<div class="triangle-right"></div>
					</div>
    			 </li>
   				 
			</ul>
		</div>
	</div>

	<div class="main-content">

	
		<form action="#" method="get">

			<div class="widget content-white left-padding payment-widget payment-steps currency-exchange-buy">
				<h3 class="title-4 color-2"><span>Kupovina/prodaja deviza</span></h3>
				<ul class="steps left">
					<li class="active">1 Menjački nalog</li>
					<li>2 <span>Payment review</span></li>
					<li>3 <span>Payment confirmation</span></li>
				</ul>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Menjačka transakcija</span></h3>
						<p>Klikom na kupi/prodaj devize odaberite vrstu
						transakcije, a iz pripadajućeg menija račun na koji će
						kupljena/prodana sredstva biti uplaćena.</p>
					</div>
					<div class="col-right">
						<ul class="widget-tabs">
							<li class="active"><a href="#">Kupi devize</a></li>
							<li><a href="#">Prodaj devize</a></li>
						</ul>
						<div class="custom-form uk-grid">
							
							<div class="group uk-width-1-1">
								<div class="group uk-width-1-1">
									<div class="group-inner">
										<label class="label-1">Na račun:</label>
										<div class="select-3">
											<select data-method="customSelect3">
				            					<option value="0">160-0000000000602-16</option>
				            					<option value="1">161-0000000000602-17</option>
				            					<option value="2">162-0000000000602-18</option>
				            					<option value="3">163-0000000000602-19</option>
				            				</select>
				            			</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			
				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Iznos:</span></h3>
						<p>Konverzija se vrši korišćenjem trenutno
						važećeg prodajnog kursa Banke za odabranu
						valutu menjačke transakcije.</p>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<div class="group-inner input-select">
									<label class="label-1">Iznos u stranoj valuti:</label>
									<input type="text" class="input-1" value="5.000,00" data-method="valueInput">
      									<div class="select-3">
      										<select data-method="customSelect3">
      			            					<option value="0">RSD</option>
      			            					<option value="1">EUR</option>
      			            					<option value="2">USD</option>
      			            					<option value="3">GBP</option>
      			            				</select>
      			            			</div>
								</div>
							</div>
							<div class="divider-30"></div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<button type="button" class="btn-1 color-3">Odustani</button>
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<a href="<?php echo Site::url('/') ?>" class="btn-1 color-1">Nastavi</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</form>
	</div>

</div>

<?php Site::getFooter(); ?>