<?php
Site::getHeader('header-login'); ?>

<div class="login-main">

	<!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
	<div class="page-cover inner-video video-cover img-overlay-blur">
		<img class="desktop-img promo-bg-0 slide-active" src="img/bg/login-bg-blur.png" alt="cover" data-object-fit="cover">
	</div>

	<div class="prelogin-full">
		<div class="inner">
			<div class="col-left">
				<h2 class="no-margin">Kursna lista</h2>
				<p>Na dan: 01.02.2017, sreda</p>
				<div class="custom-form uk-grid">
					<div class="group uk-width" style="width: 100%">
						<label class="label-2">Prikaži listu za dan:</label>
						<div class="date-wrap" style="max-width: 100%">
							<input type="text" class="input-1 dark date-input" value="30/01/2017" data-method="dateInput">
						</div>
					</div>
				</div>
				<p>Korisnicima internet bankarstva NLB Banka je obezbedila povoljnije kurseve za EUR.</p>
				<a href="<?php echo Site::url('/prelogin-exchange-single') ?>" class="btn-1 color-1 full-width">Saznajte više</a>
			</div>
			<div class="col-right -no-col-head">
				<div class="scrollable-content" data-method="customScroll">

					<div class="custom-table">
						<div class="table-row row-1 table-head no-padding grid">
							<div class="col no-border col-ls-12 col-ms-6 col-ss-6">
								<p class="col-text head-txt-1 left">Valuta</p>
							</div>
							<div class="col no-border col-ls-4 col-ms-3 col-ss-hidden">
								<p class="col-text head-txt-1 center">Kupovni</p>
							</div>
							<div class="col no-border col-ls-4 col-ms-hidden">
								<p class="col-text head-txt-1 center">Srednji</p>
							</div>
							<div class="col no-border col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text head-txt-1 center">Prodajni</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
								<div class="currency-wrapper">
									<img src="img/photos/eu-2.png" alt="">
									<p class="currency-value">1 EUR</p>
									<p class="currency">Euro</p>
								</div>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
								<div class="currency-wrapper">
									<img src="img/photos/usa-2.png" alt="">
									<p class="currency-value">1 USD</p>
									<p class="currency">Američki dolar</p>
								</div>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
								<div class="currency-wrapper">
									<img src="img/photos/swiss-2.png" alt="">
									<p class="currency-value">1 CHF</p>
									<p class="currency">Švajcarski franak</p>
								</div>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
								<div class="currency-wrapper">
									<img src="img/photos/great-britain-2.png" alt="">
									<p class="currency-value">1 GBP</p>
									<p class="currency">Britanska funta</p>
								</div>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
								<div class="currency-wrapper">
									<img src="img/photos/canada-2.png" alt="">
									<p class="currency-value">1 CAD</p>
									<p class="currency">Kanadski dolar</p>
								</div>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
								<div class="currency-wrapper">
									<img src="img/photos/norway.png" alt="">
									<p class="currency-value">1 NOK</p>
									<p class="currency">Norveška kruna</p>
								</div>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
						</div>
						<div class="table-row row-1 table-head no-padding grid">
							<div class="col no-border col-ls-12 col-ms-6 col-ss-6">
								<p class="col-text head-txt-1 left">Valuta</p>
							</div>
							<div class="col no-border col-ls-4 col-ms-3 col-ss-hidden">
								<p class="col-text head-txt-1 center">Kupovni</p>
							</div>
							<div class="col no-border col-ls-4 col-ms-hidden">
								<p class="col-text head-txt-1 center">Srednji</p>
							</div>
							<div class="col no-border col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text head-txt-1 center">Prodajni</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
								<div class="currency-wrapper">
									<img src="img/photos/eu-2.png" alt="">
									<p class="currency-value">1 EUR</p>
									<p class="currency">Euro</p>
								</div>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
								<div class="currency-wrapper">
									<img src="img/photos/usa-2.png" alt="">
									<p class="currency-value">1 USD</p>
									<p class="currency">Američki dolar</p>
								</div>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
								<div class="currency-wrapper">
									<img src="img/photos/swiss-2.png" alt="">
									<p class="currency-value">1 CHF</p>
									<p class="currency">Švajcarski franak</p>
								</div>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
								<div class="currency-wrapper">
									<img src="img/photos/great-britain-2.png" alt="">
									<p class="currency-value">1 GBP</p>
									<p class="currency">Britanska funta</p>
								</div>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
								<div class="currency-wrapper">
									<img src="img/photos/canada-2.png" alt="">
									<p class="currency-value">1 CAD</p>
									<p class="currency">Kanadski dolar</p>
								</div>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
								<div class="currency-wrapper">
									<img src="img/photos/norway.png" alt="">
									<p class="currency-value">1 NOK</p>
									<p class="currency">Norveška kruna</p>
								</div>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
							<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="login-footer">
		<div class="inner">
			<!--<ul class="lang-nav">
				<li class="active"><a href="#">ENG</a></li>
				<li><a href="#">SRB</a></li>
			</ul>-->
			<ul class="social">
				<li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
				<li class="youtube"><a href="#" class="icon-youtube">Youtube</a></li>
				<li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
				<li class="history"><a href="#" class="icon-history">History</a></li>
			</ul>
		</div>
	</div>


</div>


</div><!-- /Wrapper -->
