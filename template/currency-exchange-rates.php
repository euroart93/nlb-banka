<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="main-content">
		<form action="#" method="get">

			<div class="widget content-white">

				<div class="widget-slat">

					<div class="col-left">
						<h2><i class="icon-exchange-3"></i>Exchange Rates</h2>
						<p>Last update <span>19.5.2016. 08:00</span></p>
						<div class="custom-form uk-grid">
							<div class="group uk-width" style="width: 100%">
								<label class="label-2">Show rates on date:</label>
								<div class="group-inner date-wrap" style="max-width: 100%">
									<input type="text" class="input-1 dark date-input" value="15/02/2016" data-method="dateInput">
								</div>
							</div>
						</div>
						<div class="bottom-actions">
							<a href="#" class="btn-1 color-1 full-width" data-method="closeInbox">Buy Currency</a>
							<a href="#" class="btn-1 color-2 full-width" data-method="closeInbox">Sell Currency</a>
						</div>
					</div>

					<div class="col-right">

						<div class="custom-table">
							<div class="table-row row-1 table-head no-padding grid">
								<div class="col no-border col-ls-12 col-ms-6 col-ss-6">
									<p class="col-text head-txt-1 left">Currency</p>
								</div>
								<div class="col no-border col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text head-txt-1 center">Buy rate</p>
								</div>
								<div class="col no-border col-ls-4 col-ms-hidden">
									<p class="col-text head-txt-1 center">Mid rate</p>
								</div>
								<div class="col no-border col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text head-txt-1 center">Sell rate</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/eu-2.png" alt="">
										<p class="currency-value">1 EUR</p>
										<p class="currency">EURO</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/usa-2.png" alt="">
										<p class="currency-value">1 USD</p>
										<p class="currency">American dollar</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/swiss-2.png" alt="">
										<p class="currency-value">1 CHF</p>
										<p class="currency">Swiss franc</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/canada-2.png" alt="">
										<p class="currency-value">1 CAD</p>
										<p class="currency">Canadian dollar</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/norway.png" alt="">
										<p class="currency-value">1 NOK</p>
										<p class="currency">Norwegian crown</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/eu-2.png" alt="">
										<p class="currency-value">1 EUR</p>
										<p class="currency">EURO</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/usa-2.png" alt="">
										<p class="currency-value">1 USD</p>
										<p class="currency">American dollar</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/swiss-2.png" alt="">
										<p class="currency-value">1 CHF</p>
										<p class="currency">Swiss franc</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/canada-2.png" alt="">
										<p class="currency-value">1 CAD</p>
										<p class="currency">Canadian dollar</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col no-border high-padding col-ls-12 col-ms-6 col-ss-6">
									<div class="currency-wrapper">
										<img src="img/photos/norway.png" alt="">
										<p class="currency-value">1 NOK</p>
										<p class="currency">Norwegian crown</p>
									</div>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-hidden">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
								<div class="col no-border high-padding col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 text-lh-46 center">192,6589</p>
								</div>
							</div>
						</div>

					</div>
				</div>

			</div>


		</form>
	</div>

</div>

<?php Site::getFooter(); ?>