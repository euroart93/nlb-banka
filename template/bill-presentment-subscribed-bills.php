<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="main-content">
		<div class="widget">
			<ul class="widget-actions widget-corner">
				<li>
					<div class="widget-options" data-method="openSideModal" data-filter-node="#transactions-filter-2">
            			<ul class="trigger color-1">
            				<li></li>
            				<li></li>
            				<li></li>
            			</ul>
            		</div>
				</li>
			</ul>

			<div class="container top-padding">
				<h2><i class="icon-money-3"></i>Bill presentment</h2>
				<ul class="widget-tabs style-2">
					<li class="active"><a href="<?php echo Site::url() ?>/bill-presentment-subscribed-bills">Subscribed Bills</a></li>
					<li><a href="<?php echo Site::url() ?>/bill-presentment-recived-bills">Received Bills</a></li>
				</ul>

				<a href="#" class="table-corner-button btn-1 xs-btn color-1" data-method="openSideModal" data-filter-node="#new-subscription">New subscription</a>

				<div class="custom-table border-top m-bottom">
					<div class="table-row row-1 table-head grid">
						<div class="col col-ls-8 col-ms-6 col-ss-6">
							<p class="col-text head-txt-1 center">Beneficiary name</p>
						</div>
						<div class="col col-ls-8 col-ms-hidden">
							<p class="col-text head-txt-1 center">Beneficiary address</p>
						</div>
						<div class="col col-ls-8 col-ms-6 col-ss-6">
							<p class="col-text head-txt-1 center">Actions</p>
						</div>
					</div>

					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Telecom Sacramento</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Fulton Street 465, San Francisco</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Electrodistribution San Francisco</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Maple Street 54a, Sacramento</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Infostan</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">St. Mark's Place, New York</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">SBB</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Fulton Street 465, San Francisco</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Radiotelevision Serbia</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Maple Street 54a, Sacramento</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Lorem ipsum d.d.</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">St. Mark's Place, New York</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Tax department</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Fulton Street 465, San Francisco</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Electrodistribution San Francisco</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Maple Street 54a, Sacramento</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Infostan</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">St. Mark's Place, New York</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">SBB</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Fulton Street 465, San Francisco</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Radiotelevision Serbia</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Maple Street 54a, Sacramento</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Lorem ipsum d.d.</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">St. Mark's Place, New York</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Tax department</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Fulton Street 465, San Francisco</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Electrodistribution San Francisco</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Maple Street 54a, Sacramento</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Infostan</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">St. Mark's Place, New York</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">SBB</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Fulton Street 465, San Francisco</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Radiotelevision Serbia</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Maple Street 54a, Sacramento</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Lorem ipsum d.d.</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">St. Mark's Place, New York</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#subscription-details">
							<div class="col col-ls-8 col-ms-6 col-ss-6">
								<p class="col-text text-5 text-cl-2 left">Tax department</p>
							</div>
							<div class="col col-ls-8 col-ms-hidden">
								<p class="col-text text-5 text-cl-2 left">Fulton Street 465, San Francisco</p>
							</div>
							<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
								<a href="#" class="btn-1 xs-btn color-3">Modify</a>
								<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
								<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
							</div>
						</a>
					</div>
					<div class="hidden-content">
						<div class="table-row row-1 grid">
							<a href="#">
								<div class="col col-ls-8 col-ms-6 col-ss-6">
									<p class="col-text text-5 text-cl-2 left">Electrodistribution San Francisco</p>
								</div>
								<div class="col col-ls-8 col-ms-hidden">
									<p class="col-text text-5 text-cl-2 left">Maple Street 54a, Sacramento</p>
								</div>
								<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
									<a href="#" class="btn-1 xs-btn color-3">Modify</a>
									<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
									<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#">
								<div class="col col-ls-8 col-ms-6 col-ss-6">
									<p class="col-text text-5 text-cl-2 left">Infostan</p>
								</div>
								<div class="col col-ls-8 col-ms-hidden">
									<p class="col-text text-5 text-cl-2 left">St. Mark's Place, New York</p>
								</div>
								<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
									<a href="#" class="btn-1 xs-btn color-3">Modify</a>
									<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
									<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#">
								<div class="col col-ls-8 col-ms-6 col-ss-6">
									<p class="col-text text-5 text-cl-2 left">SBB</p>
								</div>
								<div class="col col-ls-8 col-ms-hidden">
									<p class="col-text text-5 text-cl-2 left">Fulton Street 465, San Francisco</p>
								</div>
								<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
									<a href="#" class="btn-1 xs-btn color-3">Modify</a>
									<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
									<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#">
								<div class="col col-ls-8 col-ms-6 col-ss-6">
									<p class="col-text text-5 text-cl-2 left">Radiotelevision Serbia</p>
								</div>
								<div class="col col-ls-8 col-ms-hidden">
									<p class="col-text text-5 text-cl-2 left">Maple Street 54a, Sacramento</p>
								</div>
								<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
									<a href="#" class="btn-1 xs-btn color-3">Modify</a>
									<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
									<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#">
								<div class="col col-ls-8 col-ms-6 col-ss-6">
									<p class="col-text text-5 text-cl-2 left">Lorem ipsum d.d.</p>
								</div>
								<div class="col col-ls-8 col-ms-hidden">
									<p class="col-text text-5 text-cl-2 left">St. Mark's Place, New York</p>
								</div>
								<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
									<a href="#" class="btn-1 xs-btn color-3">Modify</a>
									<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
									<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#">
								<div class="col col-ls-8 col-ms-6 col-ss-6">
									<p class="col-text text-5 text-cl-2 left">Tax department</p>
								</div>
								<div class="col col-ls-8 col-ms-hidden">
									<p class="col-text text-5 text-cl-2 left">Fulton Street 465, San Francisco</p>
								</div>
								<div class="col col-ls-8 col-ms-6 col-ss-6 action-wrap-1">
									<a href="#" class="btn-1 xs-btn color-3">Modify</a>
									<a href="#" class="btn-1 xs-btn color-3">Suspend</a>
									<a href="#" class="btn-1 xs-btn color-1">Cancel</a>
								</div>
							</a>
						</div>
					</div>
				</div>
				<button type="button" class="btn-1 color-2 big-btn" data-method="showMore">Show more</button>

			</div>

		</div>
	</div>

</div>

<?php Site::getFooter(); ?>