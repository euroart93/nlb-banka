<?php 
Site::getHeader(); ?>

<div id="main">


	<div class="main-content product-wrapper">

		<div class="product-slider">
			<p class="title-1 color-3"><span>Featured products</span></p>
			<div class="product-small-slider" data-method="productSlider">
		        <!-- Slides -->
	            <div class="slide" style="background-image: url(img/bg/product-7.jpg)">
	            	<div class="product-inner">
	            		<h3>Master your finances with <strong>Super Account 1</strong></h3>
	            		<p>Revolutionize your banking with our new current account packages and fly into the 21st century.</p>
	            		<a href="#" class="btn-1 color-1" data-method="openPopup" data-popup="3">View details</a>
	            	</div>
	            </div>
	            <div class="slide" style="background-image: url(img/bg/bg-1.jpg)">
	            	<div class="product-inner">
	            		<h3>Master your finances with <strong>Super Account 2</strong></h3>
	            		<p>Revolutionize your banking with our new current account packages and fly into the 21st century.</p>
	            		<a href="#" class="btn-1 color-1" data-method="openPopup" data-popup="3">View details</a>
	            	</div>
	            </div>
	            <div class="slide" style="background-image: url(img/bg/login-bg.jpg)">
	            	<div class="product-inner">
	            		<h3>Master your finances with <strong>Super Account 2</strong></h3>
	            		<p>Revolutionize your banking with our new current account packages and fly into the 21st century.</p>
	            		<a href="#" class="btn-1 color-1" data-method="openPopup" data-popup="3">View details</a>
	            	</div>
	            </div>
		    </div>
		    <div class="slider-button-prev"></div>
			<div class="slider-button-next"></div>
		</div>

		<div class="widget-grid-options">
			<div class="grid-options">
				<div class="grid-option option-dropdown">
					<a href="#" class="widget-select" data-method="dropdownTrigger"><i class="icon-grid-1"></i>Product types</a>
					<div class="widget-select-menu">
						<ul>
							<li>
								Product 1
								<input type="checkbox" class="checkbox-1 checked" checked data-method="customCheckbox1">
							</li>
							<li>
								Product 2
								<input type="checkbox" class="checkbox-1 checked" checked data-method="customCheckbox1">
							</li>
							<li>
								Product 3
								<input type="checkbox" class="checkbox-1 checked" checked data-method="customCheckbox1">
							</li>
							<li>
								Product 4
								<input type="checkbox" class="checkbox-1 checked" checked data-method="customCheckbox1">
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<div class="product-list product-list-full">
			<div class="product-box" style="background-image: url(img/bg/product-1.jpg);">
				<p class="title-1 color-1"><span>Loans</span></p>
				<div class="product-content">
					<h3>Quick Cash Loan</h3>
					<p class="product-txt"><span class="circle color-1"></span>Comming soon</p>
					<div class="expand">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
						<a href="#" class="btn-1 color-1" data-method="openPopup" data-popup="3">View details</a>
					</div>
				</div>
			</div>
			<div class="product-box" style="background-image: url(img/bg/product-2.jpg);">
				<p class="recommended">Recommended</p>
				<p class="title-1 color-1"><span>Savings</span></p>
				<div class="product-content">
					<h3>Quick Cash Loan</h3>
					<p class="product-txt"><span class="circle color-2"></span>You are not using this product</p>
					<div class="expand">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
						<a href="#" class="btn-1 color-1" data-method="openPopup" data-popup="3">View details</a>
					</div>
				</div>
			</div>
			<div class="product-box" style="background-image: url(img/bg/product-3.jpg);">
				<p class="title-1 color-1"><span>Cards</span></p>
				<div class="product-content">
					<h3>Quick Cash Loan</h3>
					<p class="product-txt"><span class="circle color-2"></span>You are not using this product</p>
					<div class="expand">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
						<a href="#" class="btn-1 color-1" data-method="openPopup" data-popup="3">View details</a>
					</div>
				</div>
			</div>
			<div class="product-box" style="background-image: url(img/bg/product-4.jpg);">
				<p class="title-1 color-1"><span>Current account</span></p>
				<div class="product-content">
					<h3>Quick Cash Loan</h3>
					<p class="product-txt"><span class="circle color-1"></span>You are not using this product</p>
					<div class="expand">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
						<a href="#" class="btn-1 color-1" data-method="openPopup" data-popup="3">View details</a>
					</div>
				</div>
			</div>
			<div class="product-box" style="background-image: url(img/bg/product-3.jpg);">
				<p class="title-1 color-1"><span>Cards</span></p>
				<div class="product-content">
					<h3>Quick Cash Loan</h3>
					<p class="product-txt"><span class="circle color-2"></span>You are not using this product</p>
					<div class="expand">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
						<a href="#" class="btn-1 color-1" data-method="openPopup" data-popup="3">View details</a>
					</div>
				</div>
			</div>
			<div class="product-box" style="background-image: url(img/bg/product-4.jpg);">
				<p class="title-1 color-1"><span>Current account</span></p>
				<div class="product-content">
					<h3>Quick Cash Loan</h3>
					<p class="product-txt"><span class="circle color-1"></span>You are not using this product</p>
					<div class="expand">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
						<a href="#" class="btn-1 color-1" data-method="openPopup" data-popup="3">View details</a>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<?php Site::getFooter(); ?>