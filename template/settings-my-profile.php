<?php Site::getHeader(); ?>

	<div id="main">

		<div class="submenu">
			<div class="container">
				<ul>
					<a href="#"><li class="selected">Standarno plaćanje</li></a>
					<a href="#"><li>Interni transferi</li></a>
					<a href="#"><li>Devizno plaćanje</li></a>
					<a href="#"><li>Moji šabloni</li></a>
					<a href="#"><li>Pregled plaćanja</li></a>
				</ul>
			</div>
		</div>
		
		<div class="main-content">
			<div class="widget content-white left-padding payment-widget payment-steps settings">
				<h3 class="title-4 color-2"><span>Moj profil</span></h3>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Podaci o korisniku</span></h3>
						<p>
							Podatke o korisniku nije moguće promeniti online. Kako biste ih promenili, molimo posetite najbližu ekspozituru NLB Banke.
						</p>
					</div>
					<div class="col-right">
						<div class="plain-list">
							<div>
								<p class="key">Ime i prezime:</p>
								<p class="value">Igor Jovanović</p>
							</div>
							<div>
								<p class="key">Datum rođenja:</p>
								<p class="value">15.04.1986</p>
							</div>
							<div>
								<p class="key">Državljanstvo:</p>
								<p class="value">Srpsko</p>
							</div>
							<div>
								<p class="key">Država rođenja:</p>
								<p class="value">Republika Srbija</p>
							</div>
							<div>
								<p class="key">Adresa:</p>
								<p class="value">Kralja Milutina 44</p>
							</div>
							<div>
								<p class="key">Mesto:</p>
								<p class="value">Beograd</p>
							</div>
							<div>
								<p class="key">Poštanski broj:</p>
								<p class="value">11000</p>
							</div>
						</div>
					</div>
				</div>




				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Kontakt adresa</span></h3>
						<p>
							Kako biste izmenili kontakt adresu, jednostavno izmenite podatke u poljima i kliknite na dugme Sačuvaj izmene.
						</p>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<div class="group-inner input">
									<label class="label-1">Kontakt adresa:</label>
									<input type="text" class="input-1" value="Kralja Milutina 44">
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner input">
									<label class="label-1">Mesto:</label>
									<input type="text" class="input-1" value="Beograd">
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group uk-width-1-1">
									<div class="group-inner">
										<label class="label-1">Na račun:</label>
										<div class="select-3">
											<select data-method="customSelect3">
				            					<option value="0">Novi Beograd</option>
				            					<option value="1">opcija-1</option>
				            					<option value="2">opcija-2</option>
				            					<option value="3">opcija-3</option>
				            				</select>
				            			</div>
									</div>
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner input">
									<label class="label-1">Broj stana:</label>
									<input type="text" class="input-1" value="6">
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner input">
									<a href="#" class="btn-1 color-1">Sačuvaj izmene</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>telefonski brojevi</span></h3>
						<p>
							Broj mobilnog telefona je obavezan. Osim broja mobilnog telefona možete opcionalno uneti jedan dodadni telefonski broj. Nakon unosa kliknite na button Sačuvaj izmene.
						</p>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<div class="group-inner input">
									<label class="label-1">Broj mobilnog telefona:</label>
									<input type="text" class="input-1" value="+381613214567">
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner input">
									<a href="#" class="btn-1 color-1" data-filter-node="#side-modal-settings-phone-change" data-method="openSideModal">Sačuvaj izmene</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>e-mail adresa</span></h3>
						<p>
							Ako želite izmeniti e-mail adresu, unesite izmenu u polje i kliknite na dugme Sačuvaj izmene.
						</p>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<div class="group-inner input">
									<label class="label-1">E-mail adresa:</label>
									<input type="text" class="input-1" value="igor.jovanovic@nlb.rs">
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner input">
									<a href="#" class="btn-1 color-1">Sačuvaj izmene</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



<?php Site::getFooter(); ?>