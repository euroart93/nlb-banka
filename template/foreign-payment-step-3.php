<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li>Standarno plaćanje</li></a>
				<a href="#"><li>Interni transferi</li></a>
				<a href="#"><li class="selected">Devizno plaćanje</li></a>
				<a href="#"><li>Moji šabloni</li></a>
				<a href="#"><li>Pregled plaćanja</li></a>
			</ul>
		</div>
	</div>


	<div class="main-content">
		<form action="#" method="get">

			<div class="widget content-white left-padding payment-widget payment-steps m-top foreign-p">
				<h3 class="title-4 color-2"><span>Devizno plaćanje</span></h3>
				<ul class="steps left">
					<li >1 <span>Payment order</span></li>
					<li >2 <span>Payment review</span></li>
					<li class="active">3 <span>Potvrda naloga</span></li>
				</ul>

				<div class="widget-slat">
					<div class="col-left">
					<div class="successfull">
						<img class="successfull-icon" src="img/icons/icon-check-7.png">
						<p class="successfull-title"><strong>Nalog uspješno primljen</p>
					</div>
					</div>
						<div class="col-right">
							<p class="steps-p">Vaš nalog je uspješno primljen i zaveden pod jedinstvenim brojem <color>132664-654</color> dalje promene<br> statusa Vašeg naloga možete pratiti kroz <color><a href="#">pregled plaćanja</a></color></p>

							<ul class="side-modal-account-options pdf-download">
								<h3 class="title-5 margin-5"><span>Opcije</span></h3>
									
								<li>
									<a href="#">Novo plaćanje
									<i class="side-modal-options-triangle"></i>
									</a>
								</li>
								<li>
									<a href="#">Preuzmi potvrdu u PDF formatu
									<i class="side-modal-options-triangle"></i>
									</a>
								</li>	
							</ul>	
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

</div>

<?php Site::getFooter(); ?>