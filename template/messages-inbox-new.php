<?php Site::getHeader(); ?>

	<div id="main">
		
		<div class="main-content">
			<div class="widget content-white left-padding payment-widget payment-steps messages">
				<h3 class="title-4 color-2"><span>Poruke</span></h3>

				<div class="widget-slat">
					<div class="col-left">
						<div class="sub-col-left">
							<a href="#" class="btn-1 color-1" data-filter-node="#side-modal-new-message" data-method="openSideModal">Nova poruka</a>

							<ul class="message-options">
								<li data-method='showMore2'>
									<a href="#" class="selected">Primljene poruke <span>429</span></a>
									<ul class="inbox-slide">
										<li><a href="#">Nepročitane poruke <span class="active">24</span></a></li>
										<li><a href="#">Prioritetne poruke <span>96</span></a></li>
									</ul>
								</li>
								<li>
									<a href="#">Poslate poruke <span>25</span></a>
								</li>
								<li>
									<a href="#">Arhiva <span>89</span></a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-right">
						<div class="group">
							<div class="group-inner input-search">
								<input type="search" class="input-1" placeholder="Traži...">
								<button type="button" class="search-btn"></button>
							</div>
						</div>
						<div class="custom-table">
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-service-1"></div>
									<p class="col-text text-2 text-cl-1 left"><span>Vaša nova kreditna kartica je spremna,možete je podići</span></p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">16:32</p>
								</div>
							</div>
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-service-1 notice"></div>
									<p class="col-text text-2 text-cl-1 left"><span>Obavest o provedenoj transakciji</span></p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">12.02.2017.</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-7 notice icon-service-2"></div>
									<p class="col-text text-2 text-cl-2 left"><span>Obavest o provedenoj transakciji</span></p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">10.02.2017</p>
								</div>
							</div>
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-service-1"></div>
									<p class="col-text text-2 text-cl-1 left"><span>Obavest o provedenoj transakciji</span></p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">09.02.2017.</p>
								</div>
							</div>
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-service-1 notice"></div>
									<p class="col-text text-2 text-cl-1 left"><span>Obavest o provedenoj transakciji</span></p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">09.02.2017.</p>
								</div>
							</div>
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
									<p class="col-text text-2 text-cl-2 left"><span>Odbijena transakcija</span></p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">01.02.2017.</p>
								</div>
							</div>
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-service-1"></div>
									<p class="col-text text-2 text-cl-1 left"><span>Vaša nova kreditna kartica je spremna,možete je podići</span></p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">16:32</p>
								</div>
							</div>
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-service-1 notice"></div>
									<p class="col-text text-2 text-cl-1 left"><span>Obavest o provedenoj transakciji</span></p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">12.02.2017.</p>
								</div>
							</div>
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-7 notice icon-service-2"></div>
									<p class="col-text text-2 text-cl-2 left"><span>Obavest o provedenoj transakciji</span></p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">10.02.2017</p>
								</div>
							</div>
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-service-1"></div>
									<p class="col-text text-2 text-cl-1 left"><span>Obavest o provedenoj transakciji</span></p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">09.02.2017.</p>
								</div>
							</div>
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-service-1 notice"></div>
									<p class="col-text text-2 text-cl-1 left"><span>Obavest o provedenoj transakciji</span></p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">09.02.2017.</p>
								</div>
							</div>
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
									<p class="col-text text-2 text-cl-2 left"><span>Odbijena transakcija</span></p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">01.02.2017.</p>
								</div>
							</div>
							<div class="hidden-content">
								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-service-1"></div>
										<p class="col-text text-2 text-cl-1 left"><span>Vaša nova kreditna kartica je spremna,možete je podići</span></p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">16:32</p>
									</div>
								</div>
								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-service-1 notice"></div>
										<p class="col-text text-2 text-cl-1 left"><span>Obavest o provedenoj transakciji</span></p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">12.02.2017.</p>
									</div>
								</div>
								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-7 notice icon-service-2"></div>
										<p class="col-text text-2 text-cl-2 left"><span>Obavest o provedenoj transakciji</span></p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">10.02.2017</p>
									</div>
								</div>
								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-service-1"></div>
										<p class="col-text text-2 text-cl-1 left"><span>Obavest o provedenoj transakciji</span></p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">09.02.2017.</p>
									</div>
								</div>
								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-service-1 notice"></div>
										<p class="col-text text-2 text-cl-1 left"><span>Obavest o provedenoj transakciji</span></p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">09.02.2017.</p>
									</div>
								</div>
								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
										<p class="col-text text-2 text-cl-2 left"><span>Odbijena transakcija</span></p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">01.02.2017.</p>
									</div>
								</div>
								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-service-1"></div>
										<p class="col-text text-2 text-cl-1 left"><span>Obavest o provedenoj transakciji</span></p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">25.01.2017.</p>
									</div>
								</div>
								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-service-1 notice"></div>
										<p class="col-text text-2 text-cl-1 left"><span>Od 10.02. ivodi kreditnih kartica biće dostupni samo preko e-bankinga</span></p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">23.01.2017.</p>
									</div>
								</div>
								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
										<p class="col-text text-2 text-cl-2 left"><span>Obavest o provedenoj transakciji</span></p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">22.01.2017.</p>
									</div>
								</div>
								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-message-details">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-service-1"></div>
										<p class="col-text text-2 text-cl-1 left"><span>Obavest o provedenoj transakciji</span></p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">21.01.2017.</p>
									</div>
								</div>
							</div>
							<div class="button-wrap">
								<a href="#" class="btn-2 color-1 alignright" data-method="showMore">Prikaži više vesti</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



<?php Site::getFooter(); ?>