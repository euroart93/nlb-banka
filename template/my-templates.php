<?php Site::getHeader(); ?>

<div id="main">
	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li>Standarno plaćanje</li></a>
				<a href="#"><li>Interni transferi</li></a>
				<a href="#"><li>Devizno plaćanje</li></a>
				<a href="#"><li class="selected">Moji šabloni</li></a>
			</ul>
		</div>
	</div>

	<div class="main-content">
		<div class="widget widget-transaction-list payment-list content-white my-template">
			<div class="widget-header header-dark">
				<div class="cnt-2">
					<div class="col-left">
						<h3 class="title-4 color-2"><span>Moji šabloni</span></h3>
					</div>
					<div class="col-right">
						<ul class="widget-actions bg-widget"  data-method="BGoptionsTrigger">
							<a href="#" data-filter-node="#side-modal-account-transaction-filter" data-method="openSideModal">
								<p class="widget-name">Filter</p>
							</a>
							<li>
								<div class="widget-options">
			            			<ul class="trigger" data-method="optionsTrigger">
			            				<li></li>
			            				<li></li>
			            				<li></li>
			            			</ul>
			            		</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="custom-table">

				<div class="table-header">
					<div class="table-head grid">
						<div class="col col-ls-3 col-ms-2 col-ss-hidden">
							<p class="left">Ime šablona</p>
						</div>
						<div class="col col-ls-7 col-ms-4 col-ss-6">
							<p class="left">Naziv primaoca</p>
						</div>
						<div class="col col-ls-6 col-ms-hidden">
							<p class="left">Adresa primaoca</p>
						</div>
						<div class="col col-ls-3 col-ms-2 col-ss-hidden">
							<p class="left">Broj računa</p>
						</div>
						<div class="col col-ls-5 col-ms-4 col-ss-6">
							<p class="right">&nbsp;</p>
						</div>
					</div>
				</div>
				<div class="plain-list transactions-plain-list">
					<a href="#" data-filter-node="#side-modal-my-templates-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">Struja</p>
							</div>
							<div class="col col-ls-7 col-ms-4 col-ss-6">
								<p class="col-text text-2 text-cl-1 left">Elektrodistribucija Beograd</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">Takovska 142, 11000 Beograd</p>
							</div>
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="col-text text-3 text-cl-3 right btn-temp"><a type="button">plati</a></p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-my-templates-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">Infostan</p>
							</div>
							<div class="col col-ls-7 col-ms-4 col-ss-6">
								<p class="col-text text-2 text-cl-1 left">Infostan</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">Danijelova 2, 11000 Beograd</p>
							</div>
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="col-text text-3 text-cl-3 right btn-temp"><a type="button">plati</a></p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-my-templates-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">Internet</p>
							</div>
							<div class="col col-ls-7 col-ms-4 col-ss-6">
								<p class="col-text text-2 text-cl-1 left">SBB</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">Milovina Glišića 44, 11000 Beograd</p>
							</div>
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">115-00000000004265988-25</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="col-text text-3 text-cl-3 right btn-temp"><a type="button">plati</a></p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-my-templates-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">TV pretplata</p>
							</div>
							<div class="col col-ls-7 col-ms-4 col-ss-6">
								<p class="col-text text-2 text-cl-1 left">Radiotelevizija Srbije</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
									<p class="col-text text-2 text-cl-1 left">Bulevar Zorana inđića 24, 11000 Beograd</p>
							</div>
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">115-00000000004265988-26</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="col-text text-3 text-cl-3 right btn-temp"><a type="button">plati</a></p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-my-templates-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">Lorem</p>
							</div>
							<div class="col col-ls-7 col-ms-4 col-ss-6">
								<p class="col-text text-2 text-cl-1 left">Loremipsum d.d.</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">Bulevar Milutina Milankovića 94, 11000 Beograd</p>
							</div>
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">115-00000000004265988-27</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="col-text text-3 text-cl-3 right btn-temp"><a type="button">plati</a></p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-my-templates-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">Porezna uprava</p>
							</div>
							<div class="col col-ls-7 col-ms-4 col-ss-6">
								<p class="col-text text-2 text-cl-1 left">Loremipsum d.d.</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">DR.Petra Markovića 7, 11000 Beograd</p>
							</div>
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">115-00000000004265988-28</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="col-text text-3 text-cl-3 right btn-temp"><a type="button">plati</a></p>
							</div>
						</div>
					</a>
					

				<div class="dropdown hidden-content">
					<a href="#" data-filter-node="#side-modal-my-templates-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">Struja</p>
							</div>
							<div class="col col-ls-7 col-ms-4 col-ss-6">
								<p class="col-text text-2 text-cl-1 left">Elektrodistribucija Beograd</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">Takovska 142, 11000 Beograd</p>
							</div>
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">115-00000000004265988-23</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="col-text text-3 text-cl-3 right btn-temp"><a type="button">plati</a></p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-my-templates-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">Infostan</p>
							</div>
							<div class="col col-ls-7 col-ms-4 col-ss-6">
								<p class="col-text text-2 text-cl-1 left">Infostan</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">Danijelova 2, 11000 Beograd</p>
							</div>
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">115-00000000004265988-24</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="col-text text-3 text-cl-3 right btn-temp"><a type="button">plati</a></p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-my-templates-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">Internet</p>
							</div>
							<div class="col col-ls-7 col-ms-4 col-ss-6">
								<p class="col-text text-2 text-cl-1 left">SBB</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">Milovina Glišića 44, 11000 Beograd</p>
							</div>
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">115-00000000004265988-25</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="col-text text-3 text-cl-3 right btn-temp"><a type="button">plati</a></p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-my-templates-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">TV pretplata</p>
							</div>
							<div class="col col-ls-7 col-ms-4 col-ss-6">
								<p class="col-text text-2 text-cl-1 left">Radiotelevizija Srbije</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
									<p class="col-text text-2 text-cl-1 left">Bulevar Zorana inđića 24, 11000 Beograd</p>
							</div>
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">115-00000000004265988-26</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="col-text text-3 text-cl-3 right btn-temp"><a type="button">plati</a></p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-my-templates-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">Lorem</p>
							</div>
							<div class="col col-ls-7 col-ms-4 col-ss-6">
								<p class="col-text text-2 text-cl-1 left">Loremipsum d.d.</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">Bulevar Milutina Milankovića 94, 11000 Beograd</p>
							</div>
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">115-00000000004265988-27</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="col-text text-3 text-cl-3 right btn-temp"><a type="button">plati</a></p>
							</div>
						</div>
					</a>
					<a href="#" data-filter-node="#side-modal-my-templates-details" data-method="openSideModal">
						<div class="table-row row-1 grid">
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">Porezna uprava</p>
							</div>
							<div class="col col-ls-7 col-ms-4 col-ss-6">
								<p class="col-text text-2 text-cl-1 left">Loremipsum d.d.</p>
							</div>
							<div class="col col-ls-6 col-ms-hidden">
								<p class="col-text text-2 text-cl-1 left">DR.Petra Markovića 7, 11000 Beograd</p>
							</div>
							<div class="col col-ls-3 col-ms-2 col-ss-hidden">
								<p class="col-text text-1 text-cl-1 left">115-00000000004265988-28</p>
							</div>
							<div class="col col-ls-5 col-ms-4 col-ss-6">
								<p class="col-text text-3 text-cl-3 right btn-temp"><a type="button">plati</a></p>
							</div>
						</div>
					</a>	
				</div>
			</div>
			<div class="button-wrap">
				<button class="btn-2 color-1 alignright" data-method="showMore">Prikaži još šablona</button>
			</div>
		</div>
</div>


<?php Site::getFooter(); ?>