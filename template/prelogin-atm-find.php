<?php
Site::getHeader('header-login'); ?>

<div class="login-main">

	<!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
	<div class="page-cover inner-video video-cover img-overlay-blur">
		<img class="desktop-img promo-bg-0 slide-active" src="img/bg/login-bg-blur.png" alt="cover" data-object-fit="cover">
	</div>

	<div class="prelogin-full">
		<div class="inner">
			<div class="atm-close">
				<a href="#" type="button" class="login-close">X</a>
			</div>
			<div class="col-left padding">
				<h2>Pronađi NLB</h2>
				<div class="custom-form">
					<div class="group">
						<div class="group-inner input-search">
							<input type="search" class="input-1" placeholder="Traži...">
							<button type="button" class="search-btn"></button>
						</div>
					</div>
					<div class="group">
						<div class="group-inner">
							<div class="select-3">
								<select data-method="customSelect3">
		        					<option value="0">Ekspoziture i bankomati</option>
		        					<option value="1">Ekspoziture i bankomati</option>
		        					<option value="2">Ekspoziture i bankomati</option>
		        				</select>
		        			</div>
						</div>
					</div>
					<div class="group">
						<div class="group-inner">
							<div class="select-3">
								<select data-method="finderSelect customSelect3">
		        					<option value="1">Prikaži na mapi</option>
		        					<option value="2">List view</option>
		        				</select>
		        			</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-right -no-col-head">
				<div class="scrollable-content" data-method="customScroll">

									<div class="accordian-wrapper">
                       <h4 class="accordian-title no-padding-top" data-method="accordianShowHidden"><i class="faq-atm-icon"></i>Dr. Subotića starijeg 29, 11000 Beograd<span>Bankomat</span></h4>
                       <div class="hidden">
												 <div class="contact-data-wrapper">
													 <div class="half">
														 <div class="data">
  														 <i class="phone-icon"></i>
  														 <p><span>Kontakt telefon:</span>011 7 15 15 22</p>
  													 </div>
														 <div class="data">
  														 <i class="clock-icon"></i>
  														 <p><span>Radno vreme:</span>Radnim danom od 8:00 - 20:00 h</p>
  													 </div>
														 <div class="data">
  														 <i class="mail-icon"></i>
  														 <p><span>E-mail adresa:</span> <a href="mailto:info@nlb.rs">info@nlb.rs</a></p>
  													 </div>
														 <div class="product-actions">
															 <a href="#" class="btn-1 color-1">Pronađi na mapi</a>
									 						<a href="#" class="btn-1 color-1">Zakaži sastanak</a>
									 					</div>
													 </div>
	                       </div>
												 </div>
          	 			</div>
									<div class="accordian-wrapper">
											 <h4 class="accordian-title no-padding-top" data-method="accordianShowHidden"><i class="faq-atm-bank-icon"></i>Braće Nedić 48, 11000 Beograd<span>Ekspozitura/Bankomat</span></h4>
											 <div class="hidden">
												<div class="contact-data-wrapper">
													<div class="half">
														<div class="data">
															<i class="phone-icon"></i>
															<p><span>Kontakt telefon:</span>011 7 15 15 22</p>
														</div>
														<div class="data">
															<i class="clock-icon"></i>
															<p><span>Radno vreme:</span>Radnim danom od 8:00 - 20:00 h</p>
														</div>
														<div class="data">
															<i class="mail-icon"></i>
															<p><span>E-mail adresa:</span> <a href="mailto:info@nlb.rs">info@nlb.rs</a></p>
														</div>
														<div class="product-actions">
															<a href="#" class="btn-1 color-1">Pronađi na mapi</a>
														 <a href="#" class="btn-1 color-1">Zakaži sastanak</a>
													 </div>
													</div>
												</div>
												</div>
								 </div>
								 <div class="accordian-wrapper">
											<h4 class="accordian-title no-padding-top" data-method="accordianShowHidden"><i class="faq-bank-icon"></i>Dimitrija Tucovića 98, 11000 Beograd<span>Ekspozitura</span></h4>
											<div class="hidden">
											 <div class="contact-data-wrapper">
												 <div class="half">
													 <div class="data">
														 <i class="phone-icon"></i>
														 <p><span>Kontakt telefon:</span>011 7 15 15 22</p>
													 </div>
													 <div class="data">
														 <i class="clock-icon"></i>
														 <p><span>Radno vreme:</span>Radnim danom od 8:00 - 20:00 h</p>
													 </div>
													 <div class="data">
														 <i class="mail-icon"></i>
														 <p><span>E-mail adresa:</span> <a href="mailto:info@nlb.rs">info@nlb.rs</a></p>
													 </div>
													 <div class="product-actions">
														 <a href="#" class="btn-1 color-1">Pronađi na mapi</a>
														<a href="#" class="btn-1 color-1">Zakaži sastanak</a>
													</div>
												 </div>
											 </div>
											 </div>
								</div>
								<div class="accordian-wrapper no-padding-top">
										 <h4 class="accordian-title" data-method="accordianShowHidden"><i class="faq-atm-bank-icon"></i>Kralja Milana 17, 11000 Beograd<span>Bankomat</span></h4>
										 <div class="hidden">
											<div class="contact-data-wrapper">
												<div class="half">
													<div class="data">
														<i class="phone-icon"></i>
														<p><span>Kontakt telefon:</span>011 7 15 15 22</p>
													</div>
													<div class="data">
														<i class="clock-icon"></i>
														<p><span>Radno vreme:</span>Radnim danom od 8:00 - 20:00 h</p>
													</div>
													<div class="data">
														<i class="mail-icon"></i>
														<p><span>E-mail adresa:</span> <a href="mailto:info@nlb.rs">info@nlb.rs</a></p>
													</div>
													<div class="product-actions">
														<a href="#" class="btn-1 color-1">Pronađi na mapi</a>
													 <a href="#" class="btn-1 color-1">Zakaži sastanak</a>
												 </div>
												</div>
											</div>
											</div>
							 </div>
							 <div class="accordian-wrapper">
										<h4 class="accordian-title no-padding-top" data-method="accordianShowHidden"><i class="faq-bank-icon"></i>Kralja Milana 17, 11000 Beograd<span>Bankomat</span></h4>
										<div class="hidden">
										 <div class="contact-data-wrapper">
											 <div class="half">
												 <div class="data">
													 <i class="phone-icon"></i>
													 <p><span>Kontakt telefon:</span>011 7 15 15 22</p>
												 </div>
												 <div class="data">
													 <i class="clock-icon"></i>
													 <p><span>Radno vreme:</span>Radnim danom od 8:00 - 20:00 h</p>
												 </div>
												 <div class="data">
													 <i class="mail-icon"></i>
													 <p><span>E-mail adresa:</span> <a href="mailto:info@nlb.rs">info@nlb.rs</a></p>
												 </div>
												 <div class="product-actions">
													 <a href="#" class="btn-1 color-1">Pronađi na mapi</a>
													<a href="#" class="btn-1 color-1">Zakaži sastanak</a>
												</div>
											 </div>
										 </div>
										 </div>
							</div>
							<div class="accordian-wrapper">
									 <h4 class="accordian-title no-padding-top" data-method="accordianShowHidden"><i class="faq-atm-icon"></i>Kralja Milana 17, 11000 Beograd<span>Bankomat</span></h4>
									 <div class="hidden">
										<div class="contact-data-wrapper">
											<div class="half">
												<div class="data">
													<i class="phone-icon"></i>
													<p><span>Kontakt telefon:</span>011 7 15 15 22</p>
												</div>
												<div class="data">
													<i class="clock-icon"></i>
													<p><span>Radno vreme:</span>Radnim danom od 8:00 - 20:00 h</p>
												</div>
												<div class="data">
													<i class="mail-icon"></i>
													<p><span>E-mail adresa:</span> <a href="mailto:info@nlb.rs">info@nlb.rs</a></p>
												</div>
												<div class="product-actions">
													<a href="#" class="btn-1 color-1">Pronađi na mapi</a>
												 <a href="#" class="btn-1 color-1">Zakaži sastanak</a>
											 </div>
											</div>
										</div>
										</div>
						 </div>
						 <div class="accordian-wrapper">
									<h4 class="accordian-title no-padding-top" data-method="accordianShowHidden"><i class="faq-atm-icon"></i>Kralja Milana 17, 11000 Beograd<span>Bankomat</span></h4>
									<div class="hidden">
									 <div class="contact-data-wrapper">
										 <div class="half">
											 <div class="data">
												 <i class="phone-icon"></i>
												 <p><span>Kontakt telefon:</span>011 7 15 15 22</p>
											 </div>
											 <div class="data">
												 <i class="clock-icon"></i>
												 <p><span>Radno vreme:</span>Radnim danom od 8:00 - 20:00 h</p>
											 </div>
											 <div class="data">
												 <i class="mail-icon"></i>
												 <p><span>E-mail adresa:</span> <a href="mailto:info@nlb.rs">info@nlb.rs</a></p>
											 </div>
											 <div class="product-actions">
												 <a href="#" class="btn-1 color-1">Pronađi na mapi</a>
												<a href="#" class="btn-1 color-1">Zakaži sastanak</a>
											</div>
										 </div>
									 </div>
									 </div>
						</div>
						<div class="accordian-wrapper">
								 <h4 class="accordian-title no-padding-top" data-method="accordianShowHidden"><i class="faq-atm-icon"></i>Kako da otvorim tekući račun u mts Banci?<span>Bankomat</span></h4>
								 <div class="hidden">
									<div class="contact-data-wrapper">
										<div class="half">
											<div class="data">
												<i class="phone-icon"></i>
												<p><span>Kontakt telefon:</span>011 7 15 15 22</p>
											</div>
											<div class="data">
												<i class="clock-icon"></i>
												<p><span>Radno vreme:</span>Radnim danom od 8:00 - 20:00 h</p>
											</div>
											<div class="data">
												<i class="mail-icon"></i>
												<p><span>E-mail adresa:</span> <a href="mailto:info@nlb.rs">info@nlb.rs</a></p>
											</div>
											<div class="product-actions">
												<a href="#" class="btn-1 color-1">Pronađi na mapi</a>
											 <a href="#" class="btn-1 color-1">Zakaži sastanak</a>
										 </div>
										</div>
									</div>
									</div>
					 </div>
					 <div class="accordian-wrapper">
								<h4 class="accordian-title no-padding-top" data-method="accordianShowHidden"><i class="faq-atm-icon"></i>Kako da otvorim tekući račun u mts Banci?<span>Bankomat</span></h4>
								<div class="hidden">
								 <div class="contact-data-wrapper">
									 <div class="half">
										 <div class="data">
											 <i class="phone-icon"></i>
											 <p><span>Kontakt telefon:</span>011 7 15 15 22</p>
										 </div>
										 <div class="data">
											 <i class="clock-icon"></i>
											 <p><span>Radno vreme:</span>Radnim danom od 8:00 - 20:00 h</p>
										 </div>
										 <div class="data">
											 <i class="mail-icon"></i>
											 <p><span>E-mail adresa:</span> <a href="mailto:info@nlb.rs">info@nlb.rs</a></p>
										 </div>
										 <div class="product-actions">
											 <a href="#" class="btn-1 color-1">Pronađi na mapi</a>
											<a href="#" class="btn-1 color-1">Zakaži sastanak</a>
										</div>
									 </div>
								 </div>
								 </div>
					</div>
					<div class="accordian-wrapper">
							 <h4 class="accordian-title no-padding-top" data-method="accordianShowHidden"><i class="faq-atm-icon"></i>Kako da otvorim tekući račun u mts Banci?<span>Bankomat</span></h4>
							 <div class="hidden">
								<div class="contact-data-wrapper">
									<div class="half">
										<div class="data">
											<i class="phone-icon"></i>
											<p><span>Kontakt telefon:</span>011 7 15 15 22</p>
										</div>
										<div class="data">
											<i class="clock-icon"></i>
											<p><span>Radno vreme:</span>Radnim danom od 8:00 - 20:00 h</p>
										</div>
										<div class="data">
											<i class="mail-icon"></i>
											<p><span>E-mail adresa:</span> <a href="mailto:info@nlb.rs">info@nlb.rs</a></p>
										</div>
										<div class="product-actions">
											<a href="#" class="btn-1 color-1">Pronađi na mapi</a>
										 <a href="#" class="btn-1 color-1">Zakaži sastanak</a>
									 </div>
									</div>
								</div>
								</div>
				 </div>
				
				</div>
			</div>
		</div>
	</div>

	<div class="login-footer">
		<div class="inner">
			<!--<ul class="lang-nav">
				<li class="active"><a href="#">ENG</a></li>
				<li><a href="#">SRB</a></li>
			</ul>-->
			<ul class="social">
				<li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
				<li class="youtube"><a href="#" class="icon-youtube">Youtube</a></li>
				<li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
				<li class="history"><a href="#" class="icon-history">History</a></li>
			</ul>
		</div>
	</div>


</div>


</div><!-- /Wrapper -->
