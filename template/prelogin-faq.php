<?php
Site::getHeader('header-login'); ?>

<div class="login-main">

	<!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
	<div class="page-cover inner-video video-cover img-overlay-blur">
		<img class="desktop-img promo-bg-0 slide-active" src="img/bg/login-bg-blur.png" alt="cover" data-object-fit="cover">
	</div>

	<div class="prelogin-full">
		<div class="inner">
			<div class="col-left">
				<h2>Pomoć</h2>
				<div class="custom-form">
					<div class="custom-form">
					<div class="group">
						<div class="group-inner">
							<div class="select-3">
								<select data-method="customSelect3">
		        					<option value="0">Sve kategorije</option>
		        					<option value="1">Category 1</option>
		        					<option value="2">Category 2</option>
		        				</select>
		        			</div>
						</div>
					</div>
					<div class="group">
						<div class="group-inner input-search">
							<input type="search" class="input-1" placeholder="Traži...">
							<button type="button" class="search-btn"></button>
						</div>
					</div>
				</div>
				</div>
			</div>
			<div class="col-right -no-col-head">
				<div class="scrollable-content" data-method="customScroll">

										<div class="accordian-wrapper">
												<h4 class="accordian-title" data-method="accordianShowHidden"><i class="faq-help-icon"></i>Šta da radim ukoliko izgubim karticu ili mi je ukradu?</h4>
												<div class="hidden">
														 <p>Nakon isteka važnosti kartice, ista se više ne može koristiti. Mesec dana pre nego što istekne važnost Vaše Visa kartice, Banka će Vas obavestiti o tome, kao i o eventualnim obavezama koje prate obnavljanje važnosti.</p>
														 <p>Dalje je Vaša obaveza da dođete u banku i popunite zahtev za obnovu kartice. Obnavljanje kartice se može uraditi i do mesec dana pre isteka kartice ukoliko Vam to odgovara zbog eventualnog odsustva, tj. putovanja.</p>
												</div>
										</div>
                   <div class="accordian-wrapper">
                       <h4 class="accordian-title" data-method="accordianShowHidden"><i class="faq-help-icon"></i>Da li će me Banka na neki način informisati o finansijskim promenama na mom Visa računu?</h4>
                       <div class="hidden">
												 <p>Nakon isteka važnosti kartice, ista se više ne može koristiti. Mesec dana pre nego što istekne važnost Vaše Visa kartice, Banka će Vas obavestiti o tome, kao i o eventualnim obavezama koje prate obnavljanje važnosti.</p>
												 <p>Dalje je Vaša obaveza da dođete u banku i popunite zahtev za obnovu kartice. Obnavljanje kartice se može uraditi i do mesec dana pre isteka kartice ukoliko Vam to odgovara zbog eventualnog odsustva, tj. putovanja.</p>
                       </div>
                   </div>
                   <div class="accordian-wrapper">
                       <h4 class="accordian-title" data-method="accordianShowHidden"><i class="faq-help-icon"></i>Šta se dešava kada mi istekne važnost kartice?</h4>
                       <div class="hidden">
												 <p>Nakon isteka važnosti kartice, ista se više ne može koristiti. Mesec dana pre nego što istekne važnost Vaše Visa kartice, Banka će Vas obavestiti o tome, kao i o eventualnim obavezama koje prate obnavljanje važnosti.</p>
												 <p>Dalje je Vaša obaveza da dođete u banku i popunite zahtev za obnovu kartice. Obnavljanje kartice se može uraditi i do mesec dana pre isteka kartice ukoliko Vam to odgovara zbog eventualnog odsustva, tj. putovanja.</p>
                       </div>
                   </div>
                   <div class="accordian-wrapper">
                       <h4 class="accordian-title" data-method="accordianShowHidden"><i class="faq-help-icon"></i>Račun mi je zaključan zbog pogrešnog unosa lozinke. Kako da ga otključam?</h4>
                       <div class="hidden">
												 <p>Nakon isteka važnosti kartice, ista se više ne može koristiti. Mesec dana pre nego što istekne važnost Vaše Visa kartice, Banka će Vas obavestiti o tome, kao i o eventualnim obavezama koje prate obnavljanje važnosti.</p>
												 <p>Dalje je Vaša obaveza da dođete u banku i popunite zahtev za obnovu kartice. Obnavljanje kartice se može uraditi i do mesec dana pre isteka kartice ukoliko Vam to odgovara zbog eventualnog odsustva, tj. putovanja.</p>
                       </div>
                   </div>
                   <div class="accordian-wrapper">
                       <h4 class="accordian-title" data-method="accordianShowHidden"><i class="faq-help-icon"></i>Kako znam da li zadovoljavam uslove za kreditnu karticu?</h4>
                       <div class="hidden">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                       </div>
                   </div>
                   <div class="accordian-wrapper">
                       <h4 class="accordian-title" data-method="accordianShowHidden"><i class="faq-help-icon"></i>Kako da otvorim tekući račun u mts Banci?</h4>
                       <div class="hidden">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                       </div>
                   </div>
                   <div class="accordian-wrapper">
                       <h4 class="accordian-title" data-method="accordianShowHidden"><i class="faq-help-icon"></i>Koji su mi dokumenti potrebni za apliciranje za keš kredit?</h4>
                       <div class="hidden">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                       </div>
                   </div>
                   <div class="accordian-wrapper">
                       <h4 class="accordian-title" data-method="accordianShowHidden"><i class="faq-help-icon"></i>Zaboravio sam svoje podatke za login. Kako da ih vratim?</h4>
                       <div class="hidden">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                       </div>
                   </div>
                   <div class="accordian-wrapper">
                       <h4 class="accordian-title" data-method="accordianShowHidden"><i class="faq-help-icon"></i>Račun mi je zaključan zbog pogrešnog unosa lozinke. Kako da ga otključam?</h4>
                       <div class="hidden">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                       </div>
                   </div>
                   <div class="accordian-wrapper">
                       <h4 class="accordian-title" data-method="accordianShowHidden"><i class="faq-help-icon"></i>Kako znam da li zadovoljavam uslove za kreditnu karticu?</h4>
                       <div class="hidden">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                       </div>
                   </div>

				</div>
			</div>
		</div>
	</div>

	<div class="login-footer">
		<div class="inner">
			<!--<ul class="lang-nav">
				<li class="active"><a href="#">ENG</a></li>
				<li><a href="#">SRB</a></li>
			</ul>-->
			<ul class="social">
				<li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
				<li class="youtube"><a href="#" class="icon-youtube">Youtube</a></li>
				<li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
				<li class="history"><a href="#" class="icon-history">History</a></li>
			</ul>
		</div>
	</div>


</div>


</div><!-- /Wrapper -->
