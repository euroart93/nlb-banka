<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="account-row account-bg-3">
		<div class="bg-overlay overlay-2"></div>
		<h3>From account</h3>
		<div class="account-small-slider" data-method="accountSlider2">
	        <!-- Slides -->
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Current Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-1" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Second Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-2" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Third Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-3" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Fourth Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-4" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
	        </div>
	    </div>
	    <div class="slider-button-prev"></div>
		<div class="slider-button-next"></div>
	</div>

	<div class="main-content">
		<form action="#" method="get">

			<div class="widget content-white left-padding payment-widget">
				<h2><i class="icon-money-3"></i>External payment</h2>
				<ul class="steps left">
					<li class="active">1 <span>Payment order</span></li>
					<li>2 <span>Payment review</span></li>
					<li>3 <span>Payment confirmation</span></li>
				</ul>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-1 color-2 title-full"><span>Beneficiary details</span></h3>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<div class="group-inner input-icon-wrap">
									<label class="label-1">beneficiary name:</label>
									<input type="text" class="input-1" value="Jurica Vukovic">
									<a href="#" class="input-icon icon-book-1"></a>
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner">
									<label class="label-1">Account number:</label>
									<input type="text" class="input-1" value="160-000000000000568258-66">
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner ">
									<label class="label-1">Address:</label>
									<input type="text" class="input-1" value="Adress Street 201">
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner uk-width-1-1">
									<label class="label-1">City:</label>
									<input type="text" class="input-1" value="Belgrade">
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner uk-width-1-1">
									<div class="checkbox-wrap-2">
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
										<label class="checkbox-label-2">Edit ordering pary data</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-1 color-2 title-full"><span>Amount</span></h3>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<div class="group-inner input-select">
									<label class="label-1">Amount:</label>
									<input type="text" class="input-1" value="5.000,00" data-method="valueInput">
									<div class="select-3">
										<select data-method="customSelect3">
			            					<option value="0">RSD</option>
			            					<option value="1">EUR</option>
			            					<option value="2">USD</option>
			            					<option value="3">GBP</option>
			            				</select>
			            			</div>
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner">
									<label class="label-1">Code and description:</label>
									<div class="select-3">
										<select  data-method="customSelect3">
			            					<option value="0">230 - Promet robe i usluga</option>
			            					<option value="1">231 - Promet robe i usluga</option>
			            					<option value="2">232 - Promet robe i usluga</option>
			            					<option value="3">233 - Promet robe i usluga</option>
			            				</select>
			            			</div>
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner">
									<label class="label-1">Model:</label>
									<input type="text" class="input-1" value="11">
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner">
									<label class="label-1">Reference:</label>
									<input type="text" class="input-1" value="058-13285-58">
								</div>
							</div>
						</div>
					</div>
				</div>
		
				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-1 color-2 title-full"><span>Value date:</span></h3>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width" style="width: 220px">
								<label class="label-1">Date:</label>
								<div class="group-inner date-wrap">
									<input type="text" class="input-1 dark date-input" value="15/02/2016" data-method="dateInput">
								</div>
							</div>
							<div class="group uk-width" style="width: 200px">
								<div class="group-inner checkbox-wrap">
									<input type="checkbox" checked class="checkbox-1 checked" data-method="customCheckbox1">
									<label class="checkbox-label">Urgent</label>
								</div>
							</div>
							<div class="divider-30"></div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<button type="button" class="btn-1 color-3">Cancel</button>
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<a href="<?php echo Site::url('/payment-review') ?>" class="btn-1 color-1">Continue</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</form>
	</div>

</div>

<?php Site::getFooter(); ?>