<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li>Kursna lista</li></a>
				<a href="#"><li class="selected">Kupovina/prodaja deviza</li></a>
				<a href="#"><li>Lista menjačkih transakcija</li></a>
			</ul>
		</div>
	</div>

	<div class="widget widget-select-account widget-content-white row" data-method="accountSelect">
		<div class="account-selected">
			<div class="col-ss-12 col-ls-2 acc-img">
				<img src="img/demo/acc-img-medium.jpg">
			</div>
			<div class="col-ss-12 col-ls-6 col-border">
				<div class="content">
					<p class="small">Ime računa:</p>
					<p class="big acc-name">Moj tekući račun</p>
				</div>
			</div>
			<div class="col-ss-12 col-ls-7 col-border">
				<div class="content">
					<p class="small">Broj računa:</p>
					<p class="big-s acc-number">115-0000000000567898-6</p>
				</div>
			</div>
			<div class="col-ss-12 col-ls-7 col-border">
				<div class="content">
					<p class="small float-right">Raspoloživo stanje:</p>
					<p class="big acc-amount float-right">5.123.543,89 RSD</p>
				</div>
			</div>
			<div class="col-ss-1">
				<a href="#" class="acc-list-btn acc-drop-btn" type="button" data-method="accountSelectTrigger">
					<div class="triangle"></div>
				</a>
			</div>
		</div>
		<div class="account-select">
  			<ul class="options">
    			 <li class="item">
    			 	<div class="widget row">
						<div class="col-ss-2 col-ms-2 col-ls-2">
							<div class="triangle-right-white"></div>
							<img class="desaturate" src="img/demo/acc-img-medium.jpg">
						</div>
						<div class="col-ss-8 col-ms-9 col-ls-6">
							<div class="content">
								<p class="acc-select-small">Ime računa:</p>
								<p class="big acc-name">Moj tekući račun</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small">Broj računa:</p>
								<p class="big-s acc-number">111-0000000000567898-6</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small float-right">Raspoloživo stanje:</p>
								<p class="big acc-amount float-right">5.123.543,89 RSD</p>
							</div>
						</div>
						<div class="col-ss-1 col-ms-1 col-ls-1  ">
							<div class="triangle-right"></div>
					</div>
    			 </li>
    			 <li class="item">
    			 	<div class="widget row">
						<div class="col-ss-2 col-ms-2 col-ls-2">
							<div class="triangle-right-white"></div>
							<img class="desaturate" src="img/demo/acc-img-medium-2.jpg">
						</div>
						<div class="col-ss-8 col-ms-9 col-ls-6">
							<div class="content">
								<p class="acc-select-small">Ime računa:</p>
								<p class="big acc-name">Drugi tekući račun</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small">Broj računa:</p>
								<p class="big-s acc-number">112-0000000000567898-6</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small float-right">Raspoloživo stanje:</p>
								<p class="big acc-amount float-right">1.850,89 RSD</p>
							</div>
						</div>
						<div class="col-ss-1 col-ms-1 col-ls-1  ">
							<div class="triangle-right"></div>
					</div>
    			 </li>
    			 <li class="item">
    			 	<div class="widget row">
						<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium-3.jpg">
						</div>
						<div class="col-ss-8 col-ms-9 col-ls-6">
							<div class="content">
								<p class="acc-select-small">Ime računa:</p>
								<p class="big acc-name">Treći tekući račun</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small">Broj računa:</p>
								<p class="big-s acc-number">113-0000000000567898-6</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small float-right">Raspoloživo stanje:</p>
								<p class="big acc-amount float-right">2.850,89 RSD</p>
							</div>
						</div>
						<div class="col-ss-1 col-ms-1 col-ls-1  ">
							<div class="triangle-right"></div>
					</div>
    			 </li>
    			 <li class="item">
    			 	<div class="widget row">
						<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium-4.jpg">
						</div>
						<div class="col-ss-8 col-ms-9 col-ls-6">
							<div class="content">
								<p class="acc-select-small">Ime računa:</p>
								<p class="big acc-name">Četvrti tekući račun</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small">Broj računa:</p>
								<p class="big-s acc-number">114-0000000000567898-6</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small float-right">Raspoloživo stanje:</p>
								<p class="big acc-amount float-right">3.850,89 RSD</p>
							</div>
						</div>
						<div class="col-ss-1 col-ms-1 col-ls-1  ">
							<div class="triangle-right"></div>
					</div>
    			 </li>
   				 
			</ul>
		</div>
	</div>

	<div class="main-content">

	
		<form action="#" method="get">

			<div class="widget content-white left-padding payment-widget payment-steps currency-exchange-buy">
				<h3 class="title-4 color-2"><span>Kupovina/prodaja deviza</span></h3>
				<ul class="steps left">
					<li>1 <span>Menjački nalog</span></li>
					<li class="active">2 Provera unosa</li>
					<li>3 <span>Payment confirmation</span></li>
				</ul>

			
				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Detalji u stranoj valuti</span></h3>
					</div>
					<div class="col-right account-savings cf">

					<div class="plain-list">
						<div>
							<p class="key">Sa računa:</p>
							<p class="value">160-0000000000567898-16</p>
						</div>
						<div>
							<p class="key">Na račun:</p>
							<p class="value">160-000000000000602-16</p>
						</div>
						<div>
							<p class="key">Iznos u domaćoj valuti:</p>
							<p class="value">12.240,00 RSD</p>
						</div>
						<div>
							<p class="key">Iznos u stranoj valuti:</p>
							<p class="value">100,00 EUR</p>
						</div>
						<div>
							<p class="key">Provizija:</p>
							<p class="value">100,00 RSD</p>
						</div>
						<div>
							<p class="key">Kurs:</p>
							<p class="value">122,4000</p>
						</div>
					</div>

					<div class="divider-30"></div>
					
					<div class="custom-form uk-grid">
						<div class="group uk-width-1-2">
							<div class="group-inner">
								<button type="button" class="btn-1 color-3">Ispravi podatke</button>
							</div>
						</div>
						<div class="group uk-width-1-2">
							<div class="group-inner">
								<a href="<?php echo Site::url('/currency-exchange-buy-3-new') ?>" class="btn-1 color-1">Kupi</a>
							</div>
						</div>
					</div>
				</div>
			</div>


		</form>
	</div>

</div>

<?php Site::getFooter(); ?>