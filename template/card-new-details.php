<?php
Site::getHeader(); ?>

<div id="main">

	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li class="selected">Detalji kartice</li></a>
				<a href="#"><li>Lista transakcija</li></a>
				<a href="#"><li>Lista izvoda</li></a>
				<a href="#"><li>Upravljanje karticom</li></a>
			</ul>
		</div>
	</div>

	<div class="widget card-new-details widget-select-account widget-content-white row" data-method="accountSelect">
			<div class="account-selected">
				<div class="col-ss-12 col-ls-2 acc-img">
					<img src="img/icons/mastercard-color-icon.png">
				</div>
				<div class="col-ss-12 col-ls-6 col-border">
					<div class="content">
						<p class="small">Ime kartice:</p>
						<p class="big acc-name">Moj Mastercard</p>
					</div>
				</div>
				<div class="col-ss-12 col-ls-7 col-border">
					<div class="content">
						<p class="small">Broj kartice:</p>
						<p class="big-s acc-number">1234**** ****5678</p>
					</div>
				</div>
				<div class="col-ss-12 col-ls-7 col-border">
					<div class="content">
						<p class="small float-right">Raspoloživo stanje:</p>
						<p class="big acc-amount float-right">5.123.543,89 RSD</p>
					</div>
				</div>
				<div class="col-ss-1">
					<a href="#" class="acc-list-btn acc-drop-btn" type="button" data-method="accountSelectTrigger">
						<div class="triangle"></div>
					</a>
				</div>
			</div>
			<div class="account-select">
      			<ul class="options">
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
									<div class="card-brand-mastercard">
										<div class="img-mask"></div>
										<img class="desaturate" src="img/icons/mastercard-color-icon.png">
									</div>
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime kartice:</p>
									<p class="big acc-name">Drugi Mastercard</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj kartice:</p>
									<p class="big-s acc-number">4321**** ****9876</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">929.543,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
									<div class="card-brand-visa">
										<div class="img-mask"></div>
										<img class="desaturate" src="img/icons/visa-color-icon2.png">
									</div>
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime kartice:</p>
									<p class="big acc-name">Visa debitna</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj kartice:</p>
									<p class="big-s acc-number">4578**** ****2678</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">1.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
									<div class="card-brand-maestro">
										<div class="img-mask"></div>
										<img class="desaturate" src="img/icons/maestro-color-icon.png">
									</div>
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime kartice:</p>
									<p class="big acc-name">Maestro debitna</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj kartice:</p>
									<p class="big-s acc-number">9643**** ****2345</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">2.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
						<li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
									<div class="card-brand-dina">
										<div class="img-mask"></div>
										<img class="desaturate" src="img/icons/dina-color-icon.png">
									</div>
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime kartice:</p>
									<p class="big acc-name">Dina debitna</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj kartice:</p>
									<p class="big-s acc-number">9643**** ****2345</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">2.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 </li>
    			</ul>
			</div>
		</div>

	<div class="main-content card-new-details">
		
		<div class="widget content-white payment-widget">
			<div class="widget-header header-dark">
				<div class="cnt-2">
					<div class="col-left">
						<h3 class="title-4 color-2"><span>Detalji računa</span></h3>
					</div>
					<div class="col-right">
						<ul class="widget-actions bg-widget"  data-method="BGoptionsTrigger">
							<p class="widget-name">Opcije</p>
							<li>
								<div class="widget-options">
			            			<ul class="trigger" data-method="optionsTrigger">
			            				<li></li>
			            				<li></li>
			            				<li></li>
			            			</ul>
			            			<ul class="options-list">
			            				<li><a href="#">Plati dospeli iznos</a></li>
			            				<li><a href="#">Promeni naziv kartice</a></li>
			            				<li><a href="#">blokiraj karticu</a></li>
			            				<li><a href="#">Limit kartice</a></li>
			            				<li><a href="#">Lista transakcija</a></li>
			            				<li><a href="#">Lista izvoda</a></li>
			            			</ul>
			            		</div>
							</li>
						</ul>
					</div>
				</div>

			</div>

			<div class="widget-slat ">
				<div class="col-left">
				
						<img class="account-thumbnail-pic" src="img/photos/card-preview.jpg" alt="">
					
				</div>
				<div class="col-right">
					<div class="plain-list">
						<div>
							<p class="key">Vlasnik kartice:</p>
							<p class="value bigger">Marko Marković</p>
						</div>
						<div>
							<p class="key">Tip kartice:</p>
							<p class="value">Kreditna kartica</p>
						</div>
						<div>
							<p class="key">Raspoloživo stanje:</p>
							<p class="value">5.325.893,00 RSD</p>
						</div>
						<div>
							<p class="key">Iznos dozvoljenog prekoračenja:</p>
							<p class="value">800.680,00 RSD</p>
						</div>
						<div  data-filter-node="#side-modal-card-center" data-method="openSideModal">
							<p class="key">Rezervisana sredstva:</p>
							<p class="value indent">-800.000,00 RSD</p>
							<p class="triangle-right triangle-position"></p>
							
						</div>
						<div>
							<p class="key">Dospele obaveze:</p>
							<p class="value">128.000,00 RSD <a href="#" class="btn-pay">plati</a></p>
						</div>
						<div>
							<p class="key">Kredit limit:</p>
							<p class="value">6.000.000,00</p>
						</div>
						<div class="dropdown">
							<div>
								<p class="key">Ukupne obaveze:</p>
								<p class="value">455.400,00 RSD</p>
							</div>
							<div>
								<p class="key">Poslednja promena:</p>
								<p class="value">-25.000,00</p>
							</div>
							<div>
								<p class="key">Datum poslednje promene:</p>
								<p class="value">14.03.2017</p>
							</div>
							<div>
								<p class="key">Broj kartice:</p>
								<p class="value">1234**** ****5678</p>
							</div>
							<div>
								<p class="key">Status kartice:</p>
								<p class="value">Aktivna</p>
							</div>
							<div>
								<p class="key">Broj računa:</p>
								<p class="value">258-0000000000123-45</p>
							</div>
							<div>
								<p class="key">Ime kartice:</p>
								<p class="value">Moj Mastercard</p>
							</div>
						</div>

					</div>
					<a href="#" class="btn-2 color-1 alignright" data-method="panelDropTrigger">Prikaži više detalja</a>
				</div>
			</div>

		</div>
		
		<div class="widget content-white payment-widget">
			<div class="widget-header header-dark">
				<div class="cnt-2">
					<div class="col-left">
						<h3 class="title-4 color-2"><span>Limit kartice</span></h3>
					</div>
				</div>

			</div>

			<div class="widget-slat ">
				<div class="col-left">
					<p>Za svaki od načina potrošnje(bankomati
					,prodajna mesta,internet) možete da podesite
					dnevnei mesečne limite broja transakcija i ukupnog iznosa transakcija</p>
				</div>
				<div class="col-right">
					<form action="#" method="#">
						<div class="user-notification-list card-new-details">

							<div class="notification-wraper limit-background">
								<div class="limit-title ">
									<p><strong>Limit na bankomatima</strong></p>
								</div>
								<div class="notification-trigger">
									<div class="checkbox-wrap-2">
										<input type="checkbox"  checked class="checkbox-2  checked" data-method="customCheckbox1 checkboxTriggerHidden">
									</div>
								</div>
								<div class="hidden">
									<div class="value-input">
										<p class="float-l">Broj i iznos transakcija dnevno:</p>
										<div class="notification-condition float-r">
											<input type="text" value="50.000,00" data-method="valueInput">
										</div>

										<div class="notification-condition float-r">
											<input type="text" value="20" data-method="valueInput">
										</div>
										<div class="notification-divider"></div>
									</div>
									

									<div class="value-input value-bottom">
										<p class="float-l">Broj i iznos transakcija mesecno:</p>
										<div class="notification-condition float-r">
											<input type="text" value="450.000,00" data-method="valueInput">
										</div>

										<div class="notification-condition float-r">
											<input type="text" value="120" data-method="valueInput">
										</div>

										<div class="notification-divider"></div>
									</div>
								</div>
							</div>

								<div class="notification-divider"></div>

							<div class="notification-wraper limit-background">
								<div class="limit-title">
									<p><strong>Limit na prodajnim mestima</strong></p>
								</div>
								<div class="notification-trigger">
									<div class="checkbox-wrap-2">
										<input type="checkbox"  checked class="checkbox-2 checked " data-method="customCheckbox1 checkboxTriggerHidden">
									</div>
								</div>
								<div class="hidden">
									<div class="value-input">
										<p class="float-l">Broj i iznos transakcija dnevno:</p>
										<div class="notification-condition float-r">
											<input type="text" value="50.000,00" data-method="valueInput">
										</div>

										<div class="notification-condition float-r">
											<input type="text" value="20" data-method="valueInput">
										</div>
										<div class="notification-divider"></div>
									</div>
									

									<div class="value-input value-bottom">
										<p class="float-l">Broj i iznos transakcija mesecno:</p>
										<div class="notification-condition float-r">
											<input type="text" value="450.000,00" data-method="valueInput">
										</div>

										<div class="notification-condition float-r">
											<input type="text" value="120" data-method="valueInput">
										</div>

										<div class="notification-divider"></div>
									</div>
								</div>
							</div>

								<div class="notification-divider"></div>

							<div class="notification-wraper limit-background">
								<div class="limit-title">
									<p><strong>Limit na internetu</strong></p>
								</div>
								<div class="notification-trigger">
									<div class="checkbox-wrap-2">
										<input type="checkbox" checked class="checkbox-2 checked " data-method="customCheckbox1 checkboxTriggerHidden">
									</div>
								</div>
								<div class="hidden">
									<div class="value-input">
										<p class="float-l">Broj i iznos transakcija dnevno:</p>
										<div class="notification-condition float-r">
											<input type="text" value="50.000,00" data-method="valueInput">
										</div>

										<div class="notification-condition float-r">
											<input type="text" value="20" data-method="valueInput">
										</div>
										<div class="notification-divider"></div>
									</div>
									

									<div class="value-input value-bottom">
										<p class="float-l">Broj i iznos transakcija mesecno:</p>
										<div class="notification-condition float-r">
											<input type="text" value="450.000,00" data-method="valueInput">
										</div>

										<div class="notification-condition float-r">
											<input type="text" value="120" data-method="valueInput">
										</div>
										<div class="notification-divider"></div>
									</div>
									

								</div>
							</div>
						</div>
                    </form>
				<div class="bottom-actions">
					<a href="#" class="btn-1 btn-width-card color-3" data-method="closeInbox">Vrati na podrazumevane vrijednosti</a>
					<a href="#" class="btn-1 btn-width-card color-1" data-method="closeInbox">Sačuvaj izmjene</a>
				</div>
				</div>
			</div>
			
			
		</div>


		<div class="widget widget-transaction-list payment-list content-white">
			<div class="widget-header header-dark">
				<div class="cnt-2">
					<div class="col-left">
						<h3 class="title-4 color-2"><span>Poslednje transakcije</span></h3>
					</div>
					<div class="col-right">
						<ul class="widget-actions bg-widget"  data-method="BGoptionsTrigger">
							<p class="widget-name">Filter</p>
							<li>
								<div class="widget-options">
			            			<ul class="trigger" data-method="optionsTrigger">
			            				<li></li>
			            				<li></li>
			            				<li></li>
			            			</ul>
			            			<ul class="options-list options-list-2">
			            				<li><a href="#">Filter transactions</a></li>
			            				<li><a href="#">Settings</a></li>
			            				<li class="border"><a href="#">Export...</a></li>
			            				<li><a href="#" class="active">List view</a></li>
			            				<li><a href="#">Bubble chart view</a></li>
			            				<li><a href="#">Bar chart view</a></li>
			            				<li><a href="#">Flow chart view</a></li>
			            				<li><a href="#">Calendar view</a></li>
			            				<li><a href="#">Map view</a></li>
			            			</ul>
			            		</div>
							</li>
						</ul>
					</div>
				</div>
				<ul class="widget-tabs">
					<li class="active"><a href="#">Sve</a></li>
					<li><a href="#">Isplate</a></li>
					<li><a href="#">Uplate</a></li>
				</ul>
			</div>
			<div class="custom-table">

				<div class="table-header">
					<div class="table-head grid">
						<div class="col col-ls-3 col-ms-2 col-ss-6">
							<p class="left">Datum</p>
						</div>
						<div class="col col-ls-7 col-ms-4 col-ss-hidden">
							<p class="left">Primalac</p>
						</div>
						<div class="col col-ls-6 col-ms-hidden">
							<p class="left">Opis transakcije</p>
						</div>
						<div class="col col-ls-3 col-ms-2 col-ss-hidden">
							<p class="left">Status</p>
						</div>
						<div class="col col-ls-5 col-ms-4 col-ss-6">
							<p class="right">Iznos</p>
						</div>
					</div>
				</div>

				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
					</div>
					<div class="col col-ls-7 col-ms-4 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">Elektroprivreda Srbije<br>115-00000000004265988-23</p>
					</div>
					<div class="col col-ls-6 col-ms-hidden">
						<p class="col-text text-2 text-cl-1 left">Račun za 08/2016</p>
					</div>
					<div class="col col-ls-3 col-ms-2 col-ss-hidden">
						<p class="col-text text-1 text-cl-1 left">Na čekanju</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-drop col-text text-3 text-cl-3 right">-569,99 RSD</p>
					</div>
				</div>

				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">7 Feb 2016</p>
					</div>
					<div class="col col-ls-7 col-ms-4 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">To: Ivana Ivanović<br>115-00000000004265988-23</p>
					</div>
					<div class="col col-ls-6 col-ms-hidden">
						<p class="col-text text-2 text-cl-1 left">Lorem ipsum dolor sit amet consectetur</p>
					</div>
					<div class="col col-ls-3 col-ms-2 col-ss-hidden">
						<p class="col-text text-1 text-cl-1 left">Realizovano</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-rise col-text text-3 text-cl-3 right">-323.450,00 RSD</p>
					</div>
				</div>

				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
					</div>
					<div class="col col-ls-7 col-ms-4 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">Elektroprivreda Srbije<br>115-00000000004265988-23</p>
					</div>
					<div class="col col-ls-6 col-ms-hidden">
						<p class="col-text text-2 text-cl-1 left">Račun za 08/2016</p>
					</div>
					<div class="col col-ls-3 col-ms-2 col-ss-hidden">
						<p class="col-text text-1 text-cl-1 left">Na čekanju</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-drop col-text text-3 text-cl-3 right">-569,99 RSD</p>
					</div>
				</div>

				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
					</div>
					<div class="col col-ls-7 col-ms-4 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">Elektroprivreda Srbije<br>115-00000000004265988-23</p>
					</div>
					<div class="col col-ls-6 col-ms-hidden">
						<p class="col-text text-2 text-cl-1 left">Račun za 08/2016</p>
					</div>
					<div class="col col-ls-3 col-ms-2 col-ss-hidden">
						<p class="col-text text-1 text-cl-1 left">Odbijeno</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-drop col-text text-3 text-cl-3 right">-569,99 RSD</p>
					</div>
				</div>

				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
					</div>
					<div class="col col-ls-7 col-ms-4 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">Elektroprivreda Srbije<br>115-00000000004265988-23</p>
					</div>
					<div class="col col-ls-6 col-ms-hidden">
						<p class="col-text text-2 text-cl-1 left">Račun za 08/2016</p>
					</div>
					<div class="col col-ls-3 col-ms-2 col-ss-hidden">
						<p class="col-text text-1 text-cl-1 left">Na čekanju</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-drop col-text text-3 text-cl-3 right">-569,99 RSD</p>
					</div>
				</div>

			</div>
			<div class="button-wrap">
				<a href="#" class="btn-1 color-1">Pogledaj sve transakcije</a>
			</div>
		</div>
		

	</div>

</div>

<?php Site::getFooter(); ?>
