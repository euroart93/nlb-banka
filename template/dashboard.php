<?php
Site::getHeader(); ?>

<div id="main">

	<div class="accounts-widget">
		<div class="bg-overlay overlay-1"></div>
		<div id="account-slider" class="swiper-container account-tab tab-1 active" data-method="accountSlider">
	        <!-- Slides -->
            <div class="slide">
            	<div class="account-box account-bg-1">
            		<div class="widget-options">
            			<ul class="trigger" data-method="optionsTrigger">
            				<li></li>
            				<li></li>
            				<li></li>
            			</ul>
            			<ul class="options-list">
							<li><a href="#">Option select 1</a></li>
							<li><a href="#">Option select 2</a></li>
							<li><a href="#">Option select 3</a></li>
							<li><a href="#">Option select 4</a></li>
            			</ul>
            		</div>
            		<div class="account-main">
            			<div class="acc-img">
            				<img src="img/demo/icon-acc1.png" alt="acc-image">
            			</div>
						<p class="acc-name">Moj tekući račun</p>
	            		<p class="acc-nr">115-0000000000567898-65</p>
	            	</div>
	            	<div class="balance-slat">
	            		<p class="acc-balance-txt">Raspoloživo stanje:</p>
	            		<div class="main-balance">
	            			<p>5.125.893,49 RSD</p>
	            		</div>
						<a href="#" class="btn-1 color-1 full-width">Kreiraj nalog za plaćanje</a>
	            	</div>
            	</div>
            </div>
			<div class="slide">
            	<div class="account-box account-bg-1">
            		<div class="widget-options">
            			<ul class="trigger" data-method="optionsTrigger">
            				<li></li>
            				<li></li>
            				<li></li>
            			</ul>
            			<ul class="options-list">
							<li><a href="#">Option select 1</a></li>
							<li><a href="#">Option select 2</a></li>
							<li><a href="#">Option select 3</a></li>
							<li><a href="#">Option select 4</a></li>
            			</ul>
            		</div>
            		<div class="account-main">
            			<div class="acc-img">
            				<img src="img/demo/icon-acc2.png" alt="acc-image">
            			</div>
						<p class="acc-name">Moj tekući račun</p>
	            		<p class="acc-nr">115-0000000000567898-65</p>
	            	</div>
	            	<div class="balance-slat">
	            		<p class="acc-balance-txt">Raspoloživo stanje:</p>
	            		<div class="main-balance">
	            			<p>5.125.893,49 RSD</p>
	            		</div>
						<a href="#" class="btn-1 color-1 full-width">Kreiraj nalog za plaćanje</a>
	            	</div>
            	</div>
            </div>
			<div class="slide">
            	<div class="account-box account-bg-1">
            		<div class="widget-options">
            			<ul class="trigger" data-method="optionsTrigger">
            				<li></li>
            				<li></li>
            				<li></li>
            			</ul>
            			<ul class="options-list">
							<li><a href="#">Option select 1</a></li>
							<li><a href="#">Option select 2</a></li>
							<li><a href="#">Option select 3</a></li>
							<li><a href="#">Option select 4</a></li>
            			</ul>
            		</div>
            		<div class="account-main">
            			<div class="acc-img">
            				<img src="img/demo/icon-acc3.png" alt="acc-image">
            			</div>
						<p class="acc-name">Moj tekući račun</p>
	            		<p class="acc-nr">115-0000000000567898-65</p>
	            	</div>
	            	<div class="balance-slat">
	            		<p class="acc-balance-txt">Raspoloživo stanje:</p>
	            		<div class="main-balance">
	            			<p>5.125.893,49 RSD</p>
	            		</div>
						<a href="#" class="btn-1 color-1 full-width">Kreiraj nalog za plaćanje</a>
	            	</div>
            	</div>
            </div>


		</div>
        <div class="slider-button-prev"></div>
		<div class="slider-button-next"></div>
	</div>

	<!-- Grid widgets -->
	<div class="widget-grid grid">

		<div class="container product-wrapper">

			<div class="widget-grid-options">
				<div class="grid-options">
					<div class="grid-option option-dropdown">
						<a href="#" class="widget-select" data-method="dropdownTrigger"><i class="icon-grid-1"></i>Promeni izgled početne stranice</a>
						<div class="widget-select-menu">
							<ul>
								<li>
									Menjačnica
									<input type="checkbox" class="checkbox-1 checked" checked data-method="customCheckbox1">
								</li>
								<li>
									Moja NLB Banka
									<input type="checkbox" class="checkbox-1 checked" checked data-method="customCheckbox1">
								</li>
								<li>
									Poslovnice/Bankomati
									<input type="checkbox" class="checkbox-1 checked" checked data-method="customCheckbox1">
								</li>
								<li>
									Poruke
									<input type="checkbox" class="checkbox-1 checked" checked data-method="customCheckbox1">
								</li>
							</ul>
						</div>
					</div>
					<div class="grid-option">
						<a href="#" class="reset"><i class="icon-reset-1"></i>Reset layout</a>
					</div>
				</div>
			</div>

			<ul class="sortable-widgets-container" data-method="sortableWidgets">

				<!-- Widget small currency -->
				<li class="widget widget-small-currency col-ls-8 col-ms-12 col-ss-12 widget-h-1">
					<div class="widget-inner">
						<div class="widget-header">
							<div class="cnt">
								<div class="col-left">
									<h2 class="title-1 color-2"><span>Menjačnica</span></h2>
								</div>
								<div class="col-right">
									<ul class="widget-actions">
										<li class="drag-handle"><i class="icon-move-2"></i></li>

									</ul>
								</div>
							</div>
						</div>

						<div class="custom-exchange-widget">
							<div class="currency-wrapper relative">
								<img src="img/photos/eu.png" alt="">
								<p class="currency-value">EUR</p>
							</div>
							<p>Kurs na dan: 30.01.2017.</p>
							<div class="custom-currency-details">
								<div class="half">
									<p class="currency-detail">120,9915</p>
								</div>
								<div class="half border-left">
									<p class="currency-detail">124,0015</p>
								</div>
							</div>
						</div>
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<button type="button" class="btn-1 color-2">Prodaj</button>
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<button type="button" class="btn-1 color-1">Kupi</button>
								</div>
							</div>
						</div>
					</div>
				</li>


				<!-- Widget MyBank -->
				<li class="widget widget-myBank col-ls-16 col-ms-12 widget-h-1">
					<div class="widget-inner">
						<div class="widget-header">
							<div class="cnt">
								<div class="col-left">
									<h2 class="title-1 color-2"><span>Moja NLB Banka</span></h2>
								</div>
								<div class="col-right">
									<ul class="widget-actions">
										<li class="drag-handle"><i class="icon-move-2"></i></li>

									</ul>
								</div>
							</div>

						</div>
						<div class="custom-table">

							<div class="table-header">
								<div class="table-head grid">
									<div class="col col-ls-8 col-ms-4 col-ss-6">
										<p class="left">Datum</p>
									</div>
									<div class="col col-ls-8 col-ms-4 col-ss-hidden">
										<p class="left">Sa računa/na račun</p>
									</div>
									<div class="col col-ls-8 col-ms-4 col-ss-6">
										<p class="right">Iznos</p>
									</div>
								</div>
							</div>

							<div class="table-row row-1 grid">
								<div class="col col-ls-8 col-ms-4 col-ss-6 tag-col">
									<div class="tag"><img src="img/icons/icon-acc1.png" alt=""></div>
									<p class="col-text text-1 text-cl-3 left">Moj tekući račun</p>
								</div>
								<div class="col col-ls-8 col-ms-4 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 left">115-00000000004265988-23</p>
								</div>
								<div class="col col-ls-8 col-ms-4 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">323.450,00 RSD</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-8 col-ms-4 col-ss-6 tag-col">
									<div class="tag"><img src="img/icons/icon-acc2.png" alt=""></div>
									<p class="col-text text-1 text-cl-3 left">Drugi tekući račun</p>
								</div>
								<div class="col col-ls-8 col-ms-4 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 left">115-00000000004265988-23</p>
								</div>
								<div class="col col-ls-8 col-ms-4 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">323.450,00 RSD</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-8 col-ms-4 col-ss-6 tag-col">
									<div class="tag"><img src="img/icons/icon-acc3.png" alt=""></div>
									<p class="col-text text-1 text-cl-3 left">Devizni račun</p>
								</div>
								<div class="col col-ls-8 col-ms-4 col-ss-hidden">
									<p class="col-text text-1 text-cl-1 left">115-00000000004265988-23</p>
								</div>
								<div class="col col-ls-8 col-ms-4 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">323.450,00 RSD</p>
								</div>
							</div>

						</div>
						<div class="widget-buttons">
							<a href="#" class="btn-1 color-1">Pogledaj sve račune</a>
						</div>
					</div>
				</li>

				<!-- Widget Exchange -->
				<li class="widget widget-exchange col-ls-8 col-ms-12 col-ss-12 widget-h-1">
					<div class="widget-inner">
						<div class="widget-header">
							<div class="cnt">
								<div class="col-left">
									<h2 class="title-1 color-2"><span>Kursna lista</span></h2>
								</div>
								<div class="col-right">
									<ul class="widget-actions">
										<li class="drag-handle"><i class="icon-move-2"></i></li>
										<li>
											<div class="widget-options">
						            			<ul class="trigger color-1" data-method="optionsTrigger">
						            				<li></li>
						            				<li></li>
						            				<li></li>
						            			</ul>
						            			<ul class="options-list">
													<li><a href="#" data-filter-node="#side-modal-dashboard-currency-selector" data-method="openSideModal">Odaberite valute</a></li>
						            			</ul>
						            		</div>
										</li>
									</ul>
								</div>
							</div>



						</div>
						<div class="custom-table">
							<div class="table-header">
								<div class="table-head grid">
									<div class="col col-ls-9 col-ms-6 col-ss-6 tag-col">
										<p class="left">Valuta</p>
									</div>
									<div class="col col-ls-5 col-ms-3 col-ss-hidden">
										<p class="center">Kupovni</p>
									</div>
									<div class="col col-ls-5 col-ms-hidden">
										<p class="center">Srednji</p>
									</div>
									<div class="col col-ls-5 col-ms-3 col-ss-6">
										<p class="right">Prodajni</p>
									</div>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-9 col-ms-6 col-ss-6 flag-col no-border">
									<div class="flag-box"><img src="img/photos/eu.png" alt="flag"></div>
									<p class="col-text text-4 text-cl-1 left">1 EUR</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-hidden no-border">
									<p class="col-text text-5 text-cl-1 center">0.2345</p>
								</div>
								<div class="col col-ls-5 col-ms-hidden no-border">
									<p class="col-text text-5 text-cl-1 center">0.2435</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-6 no-border">
									<p class="col-text text-5 text-cl-1 right">0.2522</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-9 col-ms-6 col-ss-6 flag-col no-border">
									<div class="flag-box"><img src="img/photos/usa.png" alt="flag"></div>
									<p class="col-text text-4 text-cl-1 left">1 USD</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-hidden no-border">
									<p class="col-text text-5 text-cl-1 center">0.2345</p>
								</div>
								<div class="col col-ls-5 col-ms-hidden no-border">
									<p class="col-text text-5 text-cl-1 center">0.2435</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-6 no-border">
									<p class="col-text text-5 text-cl-1 right">0.2522</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-9 col-ms-6 col-ss-6 flag-col no-border">
									<div class="flag-box"><img src="img/photos/swiss.png" alt="flag"></div>
									<p class="col-text text-4 text-cl-1 left">1 CHF</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-hidden no-border">
									<p class="col-text text-5 text-cl-1 center">0.2345</p>
								</div>
								<div class="col col-ls-5 col-ms-hidden no-border">
									<p class="col-text text-5 text-cl-1 center">0.2435</p>
								</div>
								<div class="col col-ls-5 col-ms-3 col-ss-6 no-border">
									<p class="col-text text-5 text-cl-1 right">0.2522</p>
								</div>
							</div>


						</div>
						<div class="widget-buttons">
							<a href="#" class="btn-1 color-1 full-width">Pogledaj celu listu</a>
						</div>
					</div>
				</li>

				<!-- Widget Map -->
				<li class="widget widget-map col-ls-8 col-ms-12 widget-h-1">
					<div class="widget-inner">
						<div class="cnt">
							<div class="widget-header">
								<div class="col-left">
									<p class="title-1 color-2"><span>Poslovnice/Bankomati</span></p>
								</div>
								<div class="col-right">
									<ul class="widget-actions">
										<li class="drag-handle"><i class="icon-move-2"></i></li>
										<li>
											<div class="widget-options">
						            			<ul class="trigger color-1" data-method="optionsTrigger">
						            				<li></li>
						            				<li></li>
						            				<li></li>
						            			</ul>
						            			<ul class="options-list">
													<li><a href="#">Option select 1</a></li>
													<li><a href="#">Option select 2</a></li>
													<li><a href="#">Option select 3</a></li>
													<li><a href="#">Option select 4</a></li>
						            			</ul>
						            		</div>
										</li>
									</ul>
								</div>
							</div>
							<input type="search" class="map-search place-1" placeholder="Search...">
						</div>
						<div class="map-wrapper">
							<div class="infobox-wrapper">
								<div id="infobox1" class="infobox bg-atm">
							        <p class="loc-title">Empire State Building</p>
							        <p class="loc-info">380 Madison Avenue<br>
							        	New York, NY, 12345
							        </p>
							    </div>
							    <div id="infobox2" class="infobox bg-atm">
							        <p class="loc-title"><span>ATM</span></p>
							         <p class="loc-info">380 Madison Avenue<br>
							        	New York, NY, 12345
							        </p>
							    </div>
							     <div id="infobox3" class="infobox bg-branch">
							        <p class="loc-title"><span>Branch</span></p>
							         <p class="loc-info">380 Madison Avenue<br>
							        	New York, NY, 12345
							        </p>
							    </div>
							    <div id="infobox4" class="infobox bg-atm">
							        <p class="loc-title"><span>ATM</span></p>
							        <p class="loc-info">380 Madison Avenue<br>
							        	New York, NY, 12345
							        </p>
							    </div>
							</div>
							<div id="map-canvas" data-method="googleMapATM"></div>
						</div>
					</div>
				</li>

				<!-- Widget Advertising -->
				<li class="widget widget-advertising col-ls-8 col-ms-12 col-ss-12 widget-h-1">
					<div class="widget-inner">
						<img class="promo" src="img/demo/promo-2.jpg" alt="promo" data-object-fit="cover">
						<h2>NLB keš krediti</h2>
					</div>
				</li>

				<!-- Widget Transactions -->
				<li class="widget widget-transactions col-ls-16 col-ms-12 widget-h-1">
					<div class="widget-inner">
						<div class="widget-header header-dark">
							<div class="cnt">
								<div class="col-left">
									<h2 class="title-1 color-2"><span>Poslednje transakcije</span></h2>
								</div>
								<div class="col-right">
									<ul class="widget-actions">
										<li class="drag-handle"><i class="icon-move-2"></i></li>

									</ul>
								</div>
							</div>

						</div>
						<div class="custom-table">

							<div class="table-header">
								<div class="table-head grid">
									<div class="col col-ls-4 col-ms-3 col-ss-6">
										<p class="left">Datum</p>
									</div>
									<div class="col col-ls-8 col-ms-5 col-ss-hidden">
										<p class="left">Sa računa/na račun</p>
									</div>
									<div class="col col-ls-6 col-ms-hidden">
										<p class="left">Opis</p>
									</div>
									<div class="col col-ls-6 col-ms-4 col-ss-6">
										<p class="right">Iznos</p>
									</div>
								</div>
							</div>

							<div class="table-row row-1 grid">
								<div class="col col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 left">7 Feb 2016</p>
								</div>
								<div class="col col-ls-8 col-ms-5 col-ss-hidden">
									<p class="col-text text-2 text-cl-1 left">To: Ivana Ivanović<br>115-00000000004265988-23</p>
								</div>
								<div class="col col-ls-6 col-ms-hidden">
									<p class="col-text text-2 text-cl-1 left">Lorem ipsum dolor sit amet consectetur</p>
								</div>
								<div class="col col-ls-6 col-ms-4 col-ss-6">
									<p class="change-drop col-text text-3 text-cl-3 right">-323.450,00 RSD</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 left">6 Feb 2016</p>
								</div>
								<div class="col col-ls-8 col-ms-5 col-ss-hidden">
									<p class="col-text text-2 text-cl-1 left">From: Darko Pejnović<br>115-00000000004265988-23</p>
								</div>
								<div class="col col-ls-6 col-ms-hidden">
									<p class="col-text text-2 text-cl-1 left">Lorem ipsum dolor sit amet consectetur</p>
								</div>
								<div class="col col-ls-6 col-ms-4 col-ss-6">
									<p class="change-drop col-text text-3 text-cl-3 right">-1.550,00 RSD</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-1 text-cl-1 left">4 Feb 2016</p>
								</div>
								<div class="col col-ls-8 col-ms-5 col-ss-hidden">
									<p class="col-text text-2 text-cl-1 left">To: Aleksandra Stanković<br>115-00000000004265988-23</p>
								</div>
								<div class="col col-ls-6 col-ms-hidden">
									<p class="col-text text-2 text-cl-1 left">Lorem ipsum dolor sit amet consectetur</p>
								</div>
								<div class="col col-ls-6 col-ms-4 col-ss-6">
									<p class="change-rise col-text text-3 text-cl-3 right">5.250.129,99 RSD</p>
								</div>
							</div>

						</div>
						<div class="widget-buttons">
							<a href="#" class="btn-1 color-1">Pogledaj sve transakcije</a>
						</div>
					</div>
				</li>

				<!-- Widget small inbox -->
				<li class="widget widget-small-inbox col-ls-8 col-ms-12 col-ss-12 widget-h-1">
					<div class="widget-inner">
						<div class="widget-header">
							<div class="cnt">
								<div class="col-left">
									<h2 class="title-1 color-2"><span>Poruke</span></h2>
								</div>
								<div class="col-right">
									<ul class="widget-actions">
										<li class="drag-handle"><i class="icon-move-2"></i></li>

									</ul>
								</div>
							</div>
						</div>

						<div class="custom-table">

							<div class="inbox-counter">
								<div class="row-1 grid">
									<div class="col col-ls-12 col-ms-6 col-ss-6">
										<p class="new-messages">2<span>Nove<br>poruke</span></p>
									</div>
									<div class="col col-ls-12 col-ms-6 col-ss-6">
										<p class="total-messages">26<span>Ukupno<br>poruka</span></p>
									</div>
								</div>
							</div>

							<div class="table-row row-1 grid">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-service-1"></div>
									<p class="col-text text-2 text-cl-1 left">To: Ivana Ivanović<br><span>Vaša nova kreditna kartica je kartica</span></p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">Danas</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-service-1 notice"></div>
									<p class="col-text text-2 text-cl-1 left">To: Ivana Ivanović<br><span>Vaša nova kreditna kartica je kartica</span></p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">09/02</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-7 icon-service-2"></div>
									<p class="col-text text-2 text-cl-2 left">To: Ivana Ivanović<br><span>Vaša nova kreditna kartica je kartica</span></p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">30/09</p>
								</div>
							</div>
						</div>
						<div class="widget-buttons">
							<a href="#" class="btn-1 color-1 full-width">Pogledaj sve poruke</a>
						</div>
					</div>
				</li>


				<!-- Widget quick actions -->
				<li class="widget widget-quick-actions col-ls-8 col-ms-12 col-ss-12 widget-h-1">
					<div class="widget-inner">
						<div class="widget-header">
							<div class="cnt">
								<div class="col-left">
									<h2 class="title-1 color-2"><span>Brze akcije</span></h2>
								</div>
								<div class="col-right">
									<ul class="widget-actions">
										<li class="drag-handle ui-sortable-handle"><i class="icon-move-2"></i></li>
										<li>
											<div class="widget-options">
						            			<ul class="trigger color-1" data-method="optionsTrigger">
						            				<li></li>
						            				<li></li>
						            				<li></li>
						            			</ul>
						            			<ul class="options-list" style="display: none;">
						            				<li><a href="#" data-method="openSideModal" data-filter-node="#side-modal-dashboard-shortcut-selector">Odaberite svoje brze akcije</a></li>
						            			</ul>
						            		</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="actions-wrap">
							<div class="action-box action-1">
								<a href="#"><i class="icon-money-2"></i><span>Brzo plaćanje</span></a>
							</div>
							<div class="action-box action-2">
								<a href="#"><i class="icon-exchange-2"></i><span>Menjačnica</span></a>
							</div>
							<div class="action-box action-3">
								<a href="#"><i class="icon-card-2"></i><span>Moje kartice</span></a>
							</div>
							<div class="action-box action-4">
								<a href="#"><i class="icon-safe-3"></i><span>Moji računi</span></a>
							</div>
						</div>
					</div>
				</li>

				<!-- Widget small news -->
				<li class="widget widget-small-news col-ls-8 col-ms-12 col-ss-12 widget-h-1">
					<div class="widget-inner">
						<div class="widget-header">
							<div class="cnt">
								<div class="col-left">
									<h2 class="title-1 color-2"><span>Vesti</span></h2>
								</div>
								<div class="col-right">
									<ul class="widget-actions">
										<li class="drag-handle"><i class="icon-move-2"></i></li>
									</ul>
								</div>
							</div>
						</div>

						<div class="custom-table">

							<div class="thumbnail">
								<img src="img/demo/b-92.png" alt="">
							</div>

							<div class="table-row row-1 grid">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
									<p class="col-text text-2 text-cl-1 left">Putin spreman da se sa Trampom sastane u...</p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">15:30</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
									<p class="col-text text-2 text-cl-1 left">Dojče vele: EU - spasavaj šta se spasti može</p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">14:59</p>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
									<p class="col-text text-2 text-cl-2 left">Vojna baza u Kelebiji - čista politika ili nešto drugo? </p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">14:45</p>
								</div>
							</div>
						</div>
						<div class="widget-buttons">
							<a href="#" class="btn-1 color-1 full-width">Pogledaj više vesti</a>
						</div>
					</div>
				</li>


			</ul>


		</div>
	</div>

</div>

<?php Site::getFooter(); ?>
