<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="notification red">
		<p><i class="icon-alert-1"></i><strong>ERROR!</strong> Your must choose different accounts.</p>
		<a href="#" class="close-btn" data-method="closeNotification"></a>
	</div>

	<div class="account-row account-bg-1">
		<div class="bg-overlay overlay-1"></div>
		<div class="account-single">
        	<div class="account-box">
        		<h2 class="title-1 color-1"><span>From account</span></h2>
        		<div class="account-main">
        			<div class="acc-img">
        				<img src="img/demo/acc-img.jpg" alt="acc-image">
        			</div>
            		<p class="acc-nr">115-0000000000567898-65</p>
            		<p class="acc-name">My Current Account</p>
            	</div>
            	<div class="balance-slat">
            		<div class="main-balance">
            			<p class="acc-balance-txt">Available balance:</p>
            			<p class="balance">5.126.893,49 <span>RSD</span></p>
            		</div>
            	</div>
            </div>
	    </div>
	</div>

	<div class="main-content">
		<form action="#" method="get">

			<div class="widget content-white payment-widget">
				<h2><i class="icon-money-3"></i>External payment</h2>
				<ul class="steps left">
					<li>1 <span>Payment order</span></li>
					<li class="active">2 <span>Payment review</span></li>
				</ul>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-1 color-2 title-full"><span>Summary</span></h3>
					</div>
					<div class="col-right">
						<div class="plain-list">
							<div>
								<p class="key">Amount:</p>
								<p class="value">5.000,00 RSD</p>
							</div>
							<div>
								<p class="key">Fee:</p>
								<p class="value">150,00 RSD</p>
							</div>
							<div>
								<p class="key">Beneficiary name:</p>
								<p class="value">Jurica Vuković</p>
							</div>
							<div>
								<p class="key">Beneficiary account:</p>
								<p class="value">115-00000000000562828-66</p>
							</div>
							<div>
								<p class="key">Reference:</p>
								<p class="value">058-13285-58</p>
							</div>
							<div>
								<p class="key">Code and description:</p>
								<p class="value">230 - Promet robe i usluga</p>
							</div>
						</div>
					</div>
				</div>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-1 color-2 title-full"><span>Other details</span></h3>
					</div>
					<div class="col-right">
						<div class="plain-list">
							<div>
								<p class="key">Model:</p>
								<p class="value">11</p>
							</div>
							<div>
								<p class="key">Description:</p>
								<p class="value">Payment description</p>
							</div>
							<div>
								<p class="key">Value date:</p>
								<p class="value">155/02/2016</p>
							</div>
							<div>
								<p class="key">Code and description:</p>
								<p class="value">230 - Promet robe i usluga</p>
							</div>
						</div>
						<div class="custom-form uk-grid">
							<div class="divider-30"></div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<a href="<?php echo Site::url('/payment') ?>" class="btn-1 color-3">Modify</a>
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<button type="button" class="btn-1 color-1" data-method="openPopup" data-popup="1">Confirm</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</form>
	</div>

</div>

<?php Site::getFooter(); ?>