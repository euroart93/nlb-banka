<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="account-row account-bg-3">
		<div class="bg-overlay overlay-2"></div>
		<div class="account-small-slider" data-method="accountSlider2">
	        <!-- Slides -->
            <div class="slide">
            	<div class="account-box">
					<div class="card-logo">
						<img src="<?php echo Site::url() ?>/img/photos/mastercard.png" alt="Card logo">
					</div>
            		<p class="acc-name">My Current Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-1" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<div class="card-logo">
						<img src="<?php echo Site::url() ?>/img/photos/mastercard.png" alt="Card logo">
					</div>
            		<p class="acc-name">My Second Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-2" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<div class="card-logo">
						<img src="<?php echo Site::url() ?>/img/photos/visa.png" alt="Card logo">
					</div>
            		<p class="acc-name">My Third Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-3" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<div class="card-logo">
						<img src="<?php echo Site::url() ?>/img/photos/visa.png" alt="Card logo">
					</div>
            		<p class="acc-name">My Fourth Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-4" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
	        </div>
	    </div>
	    <div class="slider-button-prev"></div>
		<div class="slider-button-next"></div>
	</div>

	<div class="main-content">
		<form action="#" method="#">

			<div class="widget content-white payment-widget">
				<div class="widget-slat no-margin">
					<div class="col-left">
						<h2 class="full-width"><i class="icon-card-4"></i>Card Limits</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
						Dicta pariatur, deserunt! Illum qui cum et quae consequatur? 
						Natus doloremque maiores ullam, laboriosam omnis nihil minima 
						olore rerum nam at hic.</p>
					</div>

					<div class="col-right">
						<ul class="widget-tabs style-2">
							<li class="active"><a href="#">Domestic</a></li>
							<li><a href="#">Foreign</a></li>
						</ul>

						<div class="custom-table border-top m-bottom card-limit">
							<div class="table-row row-1 grid">
								<div class="col col-ls-8 col-ms-4 col-ss-12 no-border">
									<p class="col-text text-3 text-cl-2">Limit Group 1</p>
								</div>
								<div class="col col-ls-8 col-ms-4 col-ss-6 no-border">
									<div class="checkbox-wrap-2 fl-r">
										<label class="checkbox-label-4">Selected</label>
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
									</div>
								</div>
								<div class="col col-ls-8 col-ms-4 col-ss-6 no-border">
									<div class="checkbox-wrap-2 fl-r">
										<label class="checkbox-label-4">Details</label>
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 revealHidden">
									</div>
								</div>
								<div class="col col-ls-24 col-ms-12 col-ss-12 no-border hidden-col">
									<div class="plain-list no-margin">
										<div class="no-border">
											<p class="key">Period:</p>
											<p class="value">7 Dana</p>
										</div>
										<div class="no-border">
											<p class="key">Daily shopping limit:</p>
											<p class="value">10</p>
										</div>
										<div class="no-border">
											<p class="key">Daily payments limit:</p>
											<p class="value">10</p>
										</div>
										<div class="no-border">
											<p class="key">Daily spending limit:</p>
											<p class="value">100.00,00 RDS</p>
										</div>
										<div class="no-border">
											<p class="key">Shopping limit per period:</p>
											<p class="value">50 RDS</p>
										</div>
										<div class="no-border">
											<p class="key">Payments limit per period:</p>
											<p class="value">50 RDS</p>
										</div>
										<div class="no-border">
											<p class="key">Spending limit per period:</p>
											<p class="value">500.00,00 RDS</p>
										</div>
									</div>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-8 col-ms-4 col-ss-12 no-border">
									<p class="col-text text-3 text-cl-2">Limit Group 1</p>
								</div>
								<div class="col col-ls-8 col-ms-4 col-ss-6 no-border">
									<div class="checkbox-wrap-2 fl-r">
										<label class="checkbox-label-4">Selected</label>
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
									</div>
								</div>
								<div class="col col-ls-8 col-ms-4 col-ss-6 no-border">
									<div class="checkbox-wrap-2 fl-r">
										<label class="checkbox-label-4">Details</label>
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 revealHidden">
									</div>
								</div>
								<div class="col col-ls-24 col-ms-12 col-ss-12 no-border hidden-col">
									<div class="plain-list no-margin">
										<div class="no-border">
											<p class="key">Period:</p>
											<p class="value">7 Dana</p>
										</div>
										<div class="no-border">
											<p class="key">Daily shopping limit:</p>
											<p class="value">10</p>
										</div>
										<div class="no-border">
											<p class="key">Daily payments limit:</p>
											<p class="value">10</p>
										</div>
										<div class="no-border">
											<p class="key">Daily spending limit:</p>
											<p class="value">100.00,00 RDS</p>
										</div>
										<div class="no-border">
											<p class="key">Shopping limit per period:</p>
											<p class="value">50 RDS</p>
										</div>
										<div class="no-border">
											<p class="key">Payments limit per period:</p>
											<p class="value">50 RDS</p>
										</div>
										<div class="no-border">
											<p class="key">Spending limit per period:</p>
											<p class="value">500.00,00 RDS</p>
										</div>
									</div>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-8 col-ms-4 col-ss-12 no-border">
									<p class="col-text text-3 text-cl-2">Limit Group 1</p>
								</div>
								<div class="col col-ls-8 col-ms-4 col-ss-6 no-border">
									<div class="checkbox-wrap-2 fl-r">
										<label class="checkbox-label-4">Selected</label>
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
									</div>
								</div>
								<div class="col col-ls-8 col-ms-4 col-ss-6 no-border">
									<div class="checkbox-wrap-2 fl-r">
										<label class="checkbox-label-4">Details</label>
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 revealHidden">
									</div>
								</div>
								<div class="col col-ls-24 col-ms-12 col-ss-12 no-border hidden-col">
									<div class="plain-list no-margin">
										<div class="no-border">
											<p class="key">Period:</p>
											<p class="value">7 Dana</p>
										</div>
										<div class="no-border">
											<p class="key">Daily shopping limit:</p>
											<p class="value">10</p>
										</div>
										<div class="no-border">
											<p class="key">Daily payments limit:</p>
											<p class="value">10</p>
										</div>
										<div class="no-border">
											<p class="key">Daily spending limit:</p>
											<p class="value">100.00,00 RDS</p>
										</div>
										<div class="no-border">
											<p class="key">Shopping limit per period:</p>
											<p class="value">50 RDS</p>
										</div>
										<div class="no-border">
											<p class="key">Payments limit per period:</p>
											<p class="value">50 RDS</p>
										</div>
										<div class="no-border">
											<p class="key">Spending limit per period:</p>
											<p class="value">500.00,00 RDS</p>
										</div>
									</div>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-8 col-ms-4 col-ss-12 no-border">
									<p class="col-text text-3 text-cl-2">Limit Group 1</p>
								</div>
								<div class="col col-ls-8 col-ms-4 col-ss-6 no-border">
									<div class="checkbox-wrap-2 fl-r">
										<label class="checkbox-label-4">Selected</label>
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
									</div>
								</div>
								<div class="col col-ls-8 col-ms-4 col-ss-6 no-border">
									<div class="checkbox-wrap-2 fl-r">
										<label class="checkbox-label-4">Details</label>
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 revealHidden">
									</div>
								</div>
								<div class="col col-ls-24 col-ms-12 col-ss-12 no-border hidden-col">
									<div class="plain-list no-margin">
										<div class="no-border">
											<p class="key">Period:</p>
											<p class="value">7 Dana</p>
										</div>
										<div class="no-border">
											<p class="key">Daily shopping limit:</p>
											<p class="value">10</p>
										</div>
										<div class="no-border">
											<p class="key">Daily payments limit:</p>
											<p class="value">10</p>
										</div>
										<div class="no-border">
											<p class="key">Daily spending limit:</p>
											<p class="value">100.00,00 RDS</p>
										</div>
										<div class="no-border">
											<p class="key">Shopping limit per period:</p>
											<p class="value">50 RDS</p>
										</div>
										<div class="no-border">
											<p class="key">Payments limit per period:</p>
											<p class="value">50 RDS</p>
										</div>
										<div class="no-border">
											<p class="key">Spending limit per period:</p>
											<p class="value">500.00,00 RDS</p>
										</div>
									</div>
								</div>
							</div>
							<div class="table-row row-1 grid">
								<div class="col col-ls-8 col-ms-4 col-ss-12 no-border">
									<p class="col-text text-3 text-cl-2">Limit Group 1</p>
								</div>
								<div class="col col-ls-8 col-ms-4 col-ss-6 no-border">
									<div class="checkbox-wrap-2 fl-r">
										<label class="checkbox-label-4">Selected</label>
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
									</div>
								</div>
								<div class="col col-ls-8 col-ms-4 col-ss-6 no-border">
									<div class="checkbox-wrap-2 fl-r">
										<label class="checkbox-label-4">Details</label>
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 revealHidden">
									</div>
								</div>
								<div class="col col-ls-24 col-ms-12 col-ss-12 no-border hidden-col">
									<div class="plain-list no-margin">
										<div class="no-border">
											<p class="key">Period:</p>
											<p class="value">7 Dana</p>
										</div>
										<div class="no-border">
											<p class="key">Daily shopping limit:</p>
											<p class="value">10</p>
										</div>
										<div class="no-border">
											<p class="key">Daily payments limit:</p>
											<p class="value">10</p>
										</div>
										<div class="no-border">
											<p class="key">Daily spending limit:</p>
											<p class="value">100.00,00 RDS</p>
										</div>
										<div class="no-border">
											<p class="key">Shopping limit per period:</p>
											<p class="value">50 RDS</p>
										</div>
										<div class="no-border">
											<p class="key">Payments limit per period:</p>
											<p class="value">50 RDS</p>
										</div>
										<div class="no-border">
											<p class="key">Spending limit per period:</p>
											<p class="value">500.00,00 RDS</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<button type="submit" class="btn-1 color-2 big-btn">Save changes</button>
					</div>

				</div>
			</div>

		</form>
	</div>

</div>

<?php Site::getFooter(); ?>