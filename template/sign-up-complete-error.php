<?php 
Site::getHeader('header-login'); ?>

<div class="login-main">

  <!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
  <div class="page-cover inner-video video-cover img-overlay">
    <img class="desktop-img" src="img/bg/login-bg.jpg" alt="cover" data-object-fit="cover">
    <img class="mobile-img" src="img/bg/login-mobile.jpg" alt="cover" data-object-fit="cover">
  </div>

  <div class="prelogin-full sign-up-steps">
    <div class="inner">
      <div class="col-head">
        <a href="#" class="close">Close</a>
      </div>
      <div class="col-left">
        <h2>There seems to<br> be a problem...</h2>
      </div>
      <div class="col-right">
        <h3 class="title-error"><i class="error-icon"></i>Error</h3>
        <form action="#" method="#">
          <div class="scrollable-content" data-method="customScroll">
            
            <div class="error-log-wrapper">
              <p>Etiam maximus scelerisque nunc, non vehicula nisi pretium nec. Praesent dolor 
              metus, semper a arcu sed, lobortis vestibulum augue. In luctus efficitur volutpat.</p>
              
              <div class="error-log">
                <p class="error-log-title" data-method="errorTitleLog"><span>Show more details</span></p>
                <div class="hidden">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur officiis corporis eos 
                  eaque ex ad porro provident vel iusto dolor accusamus, quo dolore animi odit dolorum vero, 
                  magnam rerum hic.</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur officiis corporis eos 
                  eaque ex ad porro provident vel iusto dolor accusamus, quo dolore animi odit dolorum vero, 
                  magnam rerum hic.</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur officiis corporis eos 
                  eaque ex ad porro provident vel iusto dolor accusamus, quo dolore animi odit dolorum vero, 
                  magnam rerum hic.</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur officiis corporis eos 
                  eaque ex ad porro provident vel iusto dolor accusamus, quo dolore animi odit dolorum vero, 
                  magnam rerum hic.</p>
                </div>
              </div>

            </div>

          </div>

          <div class="button-actions">
            <a href="#" class="btn-1 color-1 fl-r">Continue</a>
          </div>

        </form>
      </div>
    </div>
  </div>

  <div class="login-footer">
    <div class="inner">
      <ul class="lang-nav">
        <li class="active"><a href="#">ENG</a></li>
        <li><a href="#">SRB</a></li>
      </ul>
      <ul class="social">
        <li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
        <li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
        <li class="twitter"><a href="#" class="icon-twitter">Twitter</a></li>
      </ul>
    </div>
  </div>


</div>


</div><!-- /Wrapper -->


