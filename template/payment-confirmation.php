<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="account-row account-bg-3">
		<div class="bg-overlay overlay-2"></div>
		<h3>From account</h3>
		<div class="account-small-slider" data-method="accountSlider2">
	        <!-- Slides -->
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Current Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-1" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Second Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-2" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Third Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-3" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Fourth Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-4" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
	        </div>
	    </div>
	    <div class="slider-button-prev"></div>
		<div class="slider-button-next"></div>
	</div>

	<div class="main-content">
		<div class="widget content-white payment-widget">
			<h2><i class="icon-money-3"></i>External payment</h2>
			<ul class="steps left">
				<li>1 <span>Payment order</span></li>
				<li>2 <span>Payment review</span></li>
				<li class="active">3 <span>Payment confirmation</span></li>
			</ul>

			<div class="widget-slat">
				<div class="col-left">
					<div class="succes-box">
						<i class="checkmark"></i>
						<p>Payment successful</p>
					</div>
				</div>
				<div class="col-right">
					<div class="content">
						<p>Your payment was successful and its under its unique number <span>WI-0098745</span>.<br>
						You can track any further changes to the status of your payment in the Bill 
						presentment screen</p>
					</div>

					<div class="links-list-2">
						<p>Options</p>
						<a href="#">New payment</a>
						<a href="#">Save beneficiary</a>
						<a href="#">Save as template</a>
						<a href="#" class="map-init" data-method="openPopup mapInit" data-popup="22">Add geolocation</a>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<?php Site::getFooter(); ?>