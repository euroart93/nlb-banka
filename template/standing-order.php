<?php
Site::getHeader(); ?>

<div id="main">

	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li>Standarno plaćanje</li></a>
				<a href="#"><li>Interni transferi</li></a>
				<a href="#"><li>Devizno plaćanje</li></a>
				<a href="#"><li>Plaćanje računa</li></a>
				<a href="#"><li class="selected">Trajni nalozi</li></a>
				<a href="#"><li>Moji šabloni</li></a>
				<a href="#"><li>Pregled plaćanja</li></a>
			</ul>
		</div>
	</div>

	<div class="main-content mc-top-space ">
		<div class="widget content-white payment-widget">
			<div class="widget-header header-dark">
				<div class="cnt-2">
					<div class="col-left">
						<h3 class="title-4 color-2"><span>Trajni nalozi</span></h3>
					</div>
					<div class="col-right">
						<ul class="widget-actions bg-widget"  data-method="BGoptionsTrigger">
							<p class="widget-name">Opcije</p>
							<li>
								<div class="widget-options">
			            			<ul class="trigger" data-method="optionsTrigger">
			            				<li></li>
			            				<li></li>
			            				<li></li>
			            			</ul>
			            			<ul class="options-list">
			            				<li><a href="#">Filter</a></li>
			            				<li><a href="#">Novi trajni nalog interni prenos</a></li>
			            				<li><a href="#">Novi trajni nalog eksterni prenos</a></li>
			            				<li><a href="#">Novi menjački trajni nalog</a></li>
			            			</ul>
			            		</div>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<ul class="widget-tabs">
				<li class="active"><a href="#">Platni nalozi</a></li>
				<li><a href="#">Menjački nalozi</a></li>
			</ul>

			<div class="custom-table standing-order">
				<div class="table-header head-width">
					<div class="table-head grid">
						<div class="col col-ls-4 col-ms-2 col-ss-4">
							<p class="left order">Naziv trajnog nalog</p>
						</div>
						<div class="col col-ls-4 col-ms-hidden">
							<p class="left order">Ime računa platioca</p>
						</div>
						<div class="col col-ls-5 col-ms-6 col-ss-hidden">
							<p class="left order">Naziv primaoca</p>
						</div>
						<div class="col col-ls-5 col-ms-hidden">
							<p class="left order">Broj računa primaoca</p>
						</div>
						<div class="col col-ls-6 col-ms-4 col-ss-8">
							<p class="center order">Iznos</p>
						</div>
					</div>
				</div>
				<div class="plain-list transactions-plain-list">
					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-4">
							<p class="col-text text-1 text-cl-1 left">Interni prenos</p>
						</div>
						<div class="col col-ls-4 col-ms-hidden">
							<p class="col-text text-1 text-cl-1 left">Moj tekući račun</p>
						</div>
						<div class="col col-ls-5 col-ms-6 col-ss-hidden">
							<p class="col-text text-2 text-cl-1 left">Lorem ipsum dolor sit amet consectetur</p>
						</div>
						<div class="col col-ls-5 col-ms-hidden">
							<p class="col-text text-1 text-cl-1 left">310-1500000023456-15</p>
						</div>
						<div class="col col-ls-6 col-ms-4 col-ss-8">
							<p class="col-text text-3 text-cl-3">2,000.00 RSD</p>
							<a href="#" class="btn-1 xs-btn color-1" data-method="openSideModal" data-filter-node="#standing-order-details">Detalji</a>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-4">
							<p class="col-text text-1 text-cl-1 left">Interni prenos</p>
						</div>
						<div class="col col-ls-4 col-ms-hidden">
							<p class="col-text text-1 text-cl-1 left">Moj tekući račun</p>
						</div>
						<div class="col col-ls-5 col-ms-6 col-ss-hidden">
							<p class="col-text text-2 text-cl-1 left">Lorem ipsum dolor sit amet consectetur</p>
						</div>
						<div class="col col-ls-5 col-ms-hidden">
							<p class="col-text text-1 text-cl-1 left">310-1500000023456-15</p>
						</div>
						<div class="col col-ls-6 col-ms-4 col-ss-8">
							<p class="col-text text-3 text-cl-3">2,000.00 RSD</p>
							<a href="#" class="btn-1 xs-btn color-1" data-method="openSideModal" data-filter-node="#standing-order-details">Detalji</a>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-4">
							<p class="col-text text-1 text-cl-1 left">Interni prenos</p>
						</div>
						<div class="col col-ls-4 col-ms-hidden">
							<p class="col-text text-1 text-cl-1 left">Moj tekući račun</p>
						</div>
						<div class="col col-ls-5 col-ms-6 col-ss-hidden">
							<p class="col-text text-2 text-cl-1 left">Lorem ipsum dolor sit amet consectetur</p>
						</div>
						<div class="col col-ls-5 col-ms-hidden">
							<p class="col-text text-1 text-cl-1 left">310-1500000023456-15</p>
						</div>
						<div class="col col-ls-6 col-ms-4 col-ss-8">
							<p class="col-text text-3 text-cl-3">2,000.00 RSD</p>
							<a href="#" class="btn-1 xs-btn color-1" data-method="openSideModal" data-filter-node="#standing-order-details">Detalji</a>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-4">
							<p class="col-text text-1 text-cl-1 left">Interni prenos</p>
						</div>
						<div class="col col-ls-4 col-ms-hidden">
							<p class="col-text text-1 text-cl-1 left">Moj tekući račun</p>
						</div>
						<div class="col col-ls-5 col-ms-6 col-ss-hidden">
							<p class="col-text text-2 text-cl-1 left">Lorem ipsum dolor sit amet consectetur</p>
						</div>
						<div class="col col-ls-5 col-ms-hidden">
							<p class="col-text text-1 text-cl-1 left">310-1500000023456-15</p>
						</div>
						<div class="col col-ls-6 col-ms-4 col-ss-8">
							<p class="col-text text-3 text-cl-3">2,000.00 RSD</p>
							<a href="#" class="btn-1 xs-btn color-1" data-method="openSideModal" data-filter-node="#standing-order-details">Detalji</a>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-4">
							<p class="col-text text-1 text-cl-1 left">Interni prenos</p>
						</div>
						<div class="col col-ls-4 col-ms-hidden">
							<p class="col-text text-1 text-cl-1 left">Moj tekući račun</p>
						</div>
						<div class="col col-ls-5 col-ms-6 col-ss-hidden">
							<p class="col-text text-2 text-cl-1 left">Lorem ipsum dolor sit amet consectetur</p>
						</div>
						<div class="col col-ls-5 col-ms-hidden">
							<p class="col-text text-1 text-cl-1 left">310-1500000023456-15</p>
						</div>
						<div class="col col-ls-6 col-ms-4 col-ss-8">
							<p class="col-text text-3 text-cl-3">2,000.00 RSD</p>
							<a href="#" class="btn-1 xs-btn color-1" data-method="openSideModal" data-filter-node="#standing-order-details">Detalji</a>
						</div>
					</div>

					<div class=" hidden-content">
						<div class="table-row row-1 grid">
							<div class="col col-ls-4 col-ms-2 col-ss-4">
								<p class="col-text text-1 text-cl-1 left">Interni prenos</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 left">Moj tekući račun</p>
							</div>
							<div class="col col-ls-5 col-ms-6 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">Lorem ipsum dolor sit amet consectetur</p>
							</div>
							<div class="col col-ls-5 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 left">310-1500000023456-15</p>
							</div>
							<div class="col col-ls-6 col-ms-4 col-ss-8">
								<p class="col-text text-3 text-cl-3">2,000.00 RSD</p>
								<a href="#" class="btn-1 xs-btn color-1" data-method="openSideModal" data-filter-node="#standing-order-details">Detalji</a>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-4 col-ms-2 col-ss-4">
								<p class="col-text text-1 text-cl-1 left">Interni prenos</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 left">Moj tekući račun</p>
							</div>
							<div class="col col-ls-5 col-ms-6 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">Lorem ipsum dolor sit amet consectetur</p>
							</div>
							<div class="col col-ls-5 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 left">310-1500000023456-15</p>
							</div>
							<div class="col col-ls-6 col-ms-4 col-ss-8">
								<p class="col-text text-3 text-cl-3">2,000.00 RSD</p>
								<a href="#" class="btn-1 xs-btn color-1" data-method="openSideModal" data-filter-node="#standing-order-details">Detalji</a>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-4 col-ms-2 col-ss-4">
								<p class="col-text text-1 text-cl-1 left">Interni prenos</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 left">Moj tekući račun</p>
							</div>
							<div class="col col-ls-5 col-ms-6 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">Lorem ipsum dolor sit amet consectetur</p>
							</div>
							<div class="col col-ls-5 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 left">310-1500000023456-15</p>
							</div>
							<div class="col col-ls-6 col-ms-4 col-ss-8">
								<p class="col-text text-3 text-cl-3">2,000.00 RSD</p>
								<a href="#" class="btn-1 xs-btn color-1" data-method="openSideModal" data-filter-node="#standing-order-details">Detalji</a>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-4 col-ms-2 col-ss-4">
								<p class="col-text text-1 text-cl-1 left">Interni prenos</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 left">Moj tekući račun</p>
							</div>
							<div class="col col-ls-5 col-ms-6 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">Lorem ipsum dolor sit amet consectetur</p>
							</div>
							<div class="col col-ls-5 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 left">310-1500000023456-15</p>
							</div>
							<div class="col col-ls-6 col-ms-4 col-ss-8">
								<p class="col-text text-3 text-cl-3">2,000.00 RSD</p>
								<a href="#" class="btn-1 xs-btn color-1" data-method="openSideModal" data-filter-node="#standing-order-details">Detalji</a>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-4 col-ms-2 col-ss-4">
								<p class="col-text text-1 text-cl-1 left">Interni prenos</p>
							</div>
							<div class="col col-ls-4 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 left">Moj tekući račun</p>
							</div>
							<div class="col col-ls-5 col-ms-6 col-ss-hidden">
								<p class="col-text text-2 text-cl-1 left">Lorem ipsum dolor sit amet consectetur</p>
							</div>
							<div class="col col-ls-5 col-ms-hidden">
								<p class="col-text text-1 text-cl-1 left">310-1500000023456-15</p>
							</div>
							<div class="col col-ls-6 col-ms-4 col-ss-8">
								<p class="col-text text-3 text-cl-3">2,000.00 RSD</p>
								<a href="#" class="btn-1 xs-btn color-1" data-method="openSideModal" data-filter-node="#standing-order-details">Detalji</a>
							</div>
						</div>
					</div>
				</div>
			<div class="button-wrap">
				<button class="btn-2 color-1 alignright" data-method="showMore">Prikaži još</button>
			</div>
		</div>
	</div>

</div>

<?php Site::getFooter(); ?>
