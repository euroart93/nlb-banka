<?php Site::getHeader(); ?>

	<div id="main">

		<div class="submenu">
			<div class="container">
				<ul>
					<a href="#"><li>Standarno plaćanje</li></a>
					<a href="#"><li>Administracija</li></a>
					<a href="#"><li>Lista zahteva</li></a>
					<a href="#"><li>Notifikacije</li></a>
					<a href="#"><li>Mobilno bankarstvo</li></a>
				</ul>
			</div>
		</div>
		
		<div class="main-content">
			<div class="widget content-white left-padding payment-widget payment-steps settings">
				<h3 class="title-4 color-2"><span>Mobilno bankarstvo</span></h3>

				<div class="widget-slat">
					<div class="col-left">
						<p>
							Podaci o uređaju na kojem ste personalizovali aplikaciju za mobilno bankarstvo. Uređaju možete promeniti ime ili ga blokirati/izbrisati.
						</p>
					</div>
					<div class="col-right">
						<div class="plain-list">
							<div>
								<p class="key">Naziv uređaja:</p>
								<p class="value">Samsung Galaxy S7</p>
							</div>
						</div>
						<div class="group uk-width-1-2">
							<div class="group-inner input">
								<a href="#" class="btn-1 color-1">Izmjeni naziv</a>
							</div>
						</div>
						<div class="divider-30"></div>
						<div class="plain-list">
							<div>
								<p class="key">Datum aktivacije:</p>
								<p class="value">12.08.2016.</p>
							</div>
							<div>
								<p class="key">Status uređaja:</p>
								<p class="value">Aktivan</p>
							</div>
						</div>
						<div class="uk-grid">
							<div class="group uk-width-1-2">
								<div class="group-inner input">
									<a href="#" class="btn-1 color-1">blokiraj</a>
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner input">
									<a href="#" class="btn-1 color-1">izbriši</a>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>



<?php Site::getFooter(); ?>