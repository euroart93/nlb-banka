<?php 
Site::getHeader('header-login'); ?>

<div class="login-main">

  <!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
  <div class="page-cover inner-video video-cover img-overlay">
    <img class="desktop-img" src="img/bg/login-bg.jpg" alt="cover" data-object-fit="cover">
    <img class="mobile-img" src="img/bg/login-mobile.jpg" alt="cover" data-object-fit="cover">
  </div>

  <div class="prelogin-full sign-up-steps">
    <div class="inner">
      <div class="col-head">
        <a href="#" class="close">Close</a>
      </div>
      <div class="col-left">
        <h2>Your statements</h2>
      </div>
      <div class="col-right">
        <ul class="login-steps">
          <li>1</li>
          <li>2</li>
          <li>3</li>
          <li>4</li>
          <li>5</li>
          <li>6</li>
          <li class="active">7 Your statements</li>
        </ul>
        <form action="#" method="#">
          <div class="scrollable-content" data-method="customScroll">

              <div class="statements-wrapper">

                <div class="statement-half">
                  <div class="statement-content">
                    <div class="content-wrapper">
                      <p>I am the real owner of the account</p>
                      <a href="#">Details</a>
                    </div>
                    <div class="group-inner">
                      <div class="checkbox-wrap-2">
                        <input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
                      </div>
                    </div>
                  </div>
                  <div class="statement-content">
                    <div class="content-wrapper">
                      <p>I am part of a group of related customers</p>
                      <a href="#">Details</a>
                    </div>
                    <div class="group-inner">
                      <div class="checkbox-wrap-2">
                        <input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
                      </div>
                    </div>
                  </div>
                  <div class="statement-content">
                    <div class="content-wrapper">
                      <p>I am politically exposed person</p>
                      <a href="#">Details</a>
                    </div>
                    <div class="group-inner">
                      <div class="checkbox-wrap-2">
                        <input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
                      </div>
                    </div>
                  </div>
                </div>

                <div class="statement-half">
                  <div class="statement-content">
                    <div class="content-wrapper">
                      <p>I am part of a group of people with significant influence</p>
                      <a href="#">Details</a>
                    </div>
                    <div class="group-inner">
                      <div class="checkbox-wrap-2">
                        <input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
                      </div>
                    </div>
                  </div>
                  <div class="statement-content">
                    <div class="content-wrapper">
                      <p>I am affiliated with the bank</p>
                      <a href="#">Details</a>
                    </div>
                    <div class="group-inner">
                      <div class="checkbox-wrap-2">
                        <input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
                      </div>
                    </div>
                  </div>
                  <div class="statement-content">
                    <div class="content-wrapper">
                      <p>I am an USE citizen according with FATCA regulation</p>
                      <a href="#">Details</a>
                    </div>
                    <div class="group-inner">
                      <div class="checkbox-wrap-2">
                        <input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
                      </div>
                    </div>
                  </div>
                </div>

              </div>

          </div>

          <div class="button-actions">
            <a href="#" class="btn-1 color-3">Back</a>
            <a href="#" class="btn-1 color-1">Become a client</a>
          </div>

        </form>
      </div>
    </div>
  </div>

  <div class="login-footer">
    <div class="inner">
      <ul class="lang-nav">
        <li class="active"><a href="#">ENG</a></li>
        <li><a href="#">SRB</a></li>
      </ul>
      <ul class="social">
        <li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
        <li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
        <li class="twitter"><a href="#" class="icon-twitter">Twitter</a></li>
      </ul>
    </div>
  </div>


</div>


</div><!-- /Wrapper -->


