<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="main-content content-full">
		<div class="widget widget-transaction-list">
			<ul class="widget-actions widget-corner">
				<li>
					<div class="widget-options" data-method="openSideModal" data-filter-node="#transactions-filter">
            			<ul class="trigger color-1">
            				<li></li>
            				<li></li>
            				<li></li>
            			</ul>
            		</div>
				</li>
			</ul>
			<div class="container top-padding">
				<h2><i class="icon-exchange-3"></i>Exchange transactions</h2>
				<ul class="widget-tabs style-2">
					<li class="active"><a href="#">All</a></li>
					<li><a href="#">Pending</a></li>
				</ul>
				<div class="custom-table table-2 border-top m-bottom">
					<div class="table-row row-1 table-head grid">
						<div class="col col-ls-4 col-ms-3 col-ss-6">
							<p class="col-text head-txt-1 center">Date</p>
						</div>
						<div class="col col-ls-16 col-ms-hidden">
							<p class="col-text head-txt-1 left">Detination Account name</p>
						</div>
						<div class="col col-ls-4 col-ms-3 col-ss-6">
							<p class="col-text head-txt-1 right">Amount</p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#transaction-detail">
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<div class="arr-up-tag"></div>
								<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
							</div>
							<div class="col col-ls-16 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">My foreign currency account</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#transaction-detail">
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<div class="arr-up-tag"></div>
								<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
							</div>
							<div class="col col-ls-16 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">My foreign currency account</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#transaction-detail">
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<div class="arr-down-tag"></div>
								<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
							</div>
							<div class="col col-ls-16 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">My foreign currency account</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#transaction-detail">
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<div class="arr-down-tag"></div>
								<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
							</div>
							<div class="col col-ls-16 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">My foreign currency account</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#transaction-detail">
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<div class="arr-up-tag"></div>
								<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
							</div>
							<div class="col col-ls-16 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">My foreign currency account</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#transaction-detail">
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<div class="arr-down-tag"></div>
								<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
							</div>
							<div class="col col-ls-16 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">My foreign currency account</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#transaction-detail">
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<div class="arr-up-tag"></div>
								<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
							</div>
							<div class="col col-ls-16 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">My foreign currency account</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#transaction-detail">
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<div class="arr-up-tag"></div>
								<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
							</div>
							<div class="col col-ls-16 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">My foreign currency account</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#transaction-detail">
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<div class="arr-down-tag"></div>
								<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
							</div>
							<div class="col col-ls-16 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">My foreign currency account</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					<div class="table-row row-1 grid">
						<a href="#" data-method="openSideModal" data-filter-node="#transaction-detail">
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<div class="arr-down-tag"></div>
								<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
							</div>
							<div class="col col-ls-16 col-ms-6 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">My foreign currency account</p>
							</div>
							<div class="col col-ls-4 col-ms-3 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</a>
					</div>
					
					<div class="hidden-content">
						<div class="table-row row-1 grid">
							<a href="#" data-method="openSideModal" data-filter-node="#transaction-detail">
								<div class="col col-ls-4 col-ms-3 col-ss-6">
									<div class="arr-up-tag"></div>
									<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
								</div>
								<div class="col col-ls-16 col-ms-6 col-ss-hidden">
									<p class="col-text text-6 text-cl-2 left">My foreign currency account</p>
								</div>
								<div class="col col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#" data-method="openSideModal" data-filter-node="#transaction-detail">
								<div class="col col-ls-4 col-ms-3 col-ss-6">
									<div class="arr-down-tag"></div>
									<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
								</div>
								<div class="col col-ls-16 col-ms-6 col-ss-hidden">
									<p class="col-text text-6 text-cl-2 left">My foreign currency account</p>
								</div>
								<div class="col col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#" data-method="openSideModal" data-filter-node="#transaction-detail">
								<div class="col col-ls-4 col-ms-3 col-ss-6">
									<div class="arr-up-tag"></div>
									<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
								</div>
								<div class="col col-ls-16 col-ms-6 col-ss-hidden">
									<p class="col-text text-6 text-cl-2 left">My foreign currency account</p>
								</div>
								<div class="col col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
								</div>
							</a>
						</div>
						<div class="table-row row-1 grid">
							<a href="#" data-method="openSideModal" data-filter-node="#transaction-detail">
								<div class="col col-ls-4 col-ms-3 col-ss-6">
									<div class="arr-down-tag"></div>
									<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
								</div>
								<div class="col col-ls-16 col-ms-6 col-ss-hidden">
									<p class="col-text text-6 text-cl-2 left">My foreign currency account</p>
								</div>
								<div class="col col-ls-4 col-ms-3 col-ss-6">
									<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
								</div>
							</a>
						</div>
					</div>
				</div>
				<button type="button" class="btn-1 color-2 big-btn" data-method="showMore">Show more</button>
			</div>
		</div>


	</div>

</div>

<?php Site::getFooter(); ?>