<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="account-row account-bg-3">
		<div class="bg-overlay overlay-2"></div>
		<div class="account-small-slider" data-method="accountSlider2">
	        <!-- Slides -->
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Current Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-1" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Second Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-2" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Third Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-3" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Fourth Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-4" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
	        </div>
	    </div>
	    <div class="slider-button-prev"></div>
		<div class="slider-button-next"></div>
	</div>
	
	<div class="main-content">
		<div class="widget calendar-view">
			<ul class="widget-actions widget-corner">
				<li>
					<div class="widget-options">
            			<ul class="trigger color-1" data-method="optionsTrigger"> 
            				<li></li>
            				<li></li>
            				<li></li>
            			</ul>
            			<ul class="options-list options-list-2">
            				<li><a href="#">Filter transactions</a></li>
            				<li><a href="#">Settings</a></li>
            				<li class="border"><a href="#">Export...</a></li>
            				<li><a href="#" class="active">List view</a></li>
            				<li><a href="#">Bar chart view</a></li>
            				<li><a href="#">Flow chart view</a></li>
            				<li><a href="#">Calendar view</a></li>
            				<li><a href="#">Map view</a></li>
            			</ul>
            		</div>
				</li>
			</ul>
			<div class="container top-padding">
				<h2>Transactions - Balance Evolution</h2>

				<form action="#" method="#">

					<div class="time-lapse-trigger">
						<div class="select-3">
							<select data-method="customSelect3">
		    					<option value="0">Last 30 days</option>
		    					<option value="1">Last 15 days</option>
		    					<option value="2">Last 10 days</option>
		    					<option value="3">Last 5 days</option>
		    				</select>
		    			</div>
		    		</div>

		    		<div id="balance-evolution-chart" data-method="balanceEvolution"></div>

				</form>

			</div>
		</div>
	</div>

</div>

<?php Site::getFooter(); ?>