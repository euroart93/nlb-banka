<?php 
Site::getHeader('header-login'); ?>

<div class="login-main">

  <!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
  <div class="page-cover inner-video video-cover img-overlay">
    <img class="desktop-img" src="img/bg/login-bg.jpg" alt="cover" data-object-fit="cover">
    <img class="mobile-img" src="img/bg/login-mobile.jpg" alt="cover" data-object-fit="cover">
  </div>

  <div class="prelogin-full sign-up-steps">
    <div class="inner">
      <div class="col-head">
        <a href="#" class="close">Close</a>
      </div>
      <div class="col-left">
        <h2>To sign up, please<br>follow there 7<br> simlpe steps...</h2>
      </div>
      <div class="col-right">
        <ul class="login-steps">
          <li class="active">1 Enter your personal data</li>
          <li>2</li>
          <li>3</li>
          <li>4</li>
          <li>5</li>
          <li>6</li>
          <li>7</li>
        </ul>
        <form action="#" method="#">
          <div class="scrollable-content" style="overflow: visible;">

            <div class="custom-form uk-grid">

              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">First name:</label>
                  <input type="text" class="input-1" value="Jurica">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Middle name:</label>
                  <input type="text" class="input-1" value="Jurko">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Last name:</label>
                  <input type="text" class="input-1" value="Vukovic">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Personal registration number:</label>
                  <input type="text" class="input-1" value="123456">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner mobile-entry">
                  <label class="label-1">Mobile phone:</label>
                  <div class="select-3">
                    <select data-method="customSelect3">
                      <option value="0">+ 381</option>
                      <option value="1">+ 382</option>
                      <option value="2">+ 383</option>
                      <option value="3">+ 384</option>
                    </select>
                  </div>
                  <input type="text" class="input-1" value="92123445456">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">E-mail:</label>
                  <input type="text" class="input-1" value="sample@email.com">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner checkbox-wrap">
                  <input type="checkbox" checked class="checkbox-1 checked" data-method="customCheckbox1">
                  <label class="checkbox-label">Recive marketing materials from a bank</label>
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner checkbox-wrap download-link">
                  <input type="checkbox" checked class="checkbox-1 checked" data-method="customCheckbox1">
                  <label class="checkbox-label"><a href="#">I agree with <span>terms and conditions</span></a></label>
                </div>
              </div>

            </div>

          </div>

          <div class="button-actions">
            <a href="#" class="btn-1 color-3">Cancel</a>
            <a href="#" class="btn-1 color-1">Next step</a>
          </div>

        </form>
      </div>
    </div>
  </div>

  <div class="login-footer">
    <div class="inner">
      <ul class="lang-nav">
        <li class="active"><a href="#">ENG</a></li>
        <li><a href="#">SRB</a></li>
      </ul>
      <ul class="social">
        <li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
        <li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
        <li class="twitter"><a href="#" class="icon-twitter">Twitter</a></li>
      </ul>
    </div>
  </div>


</div>


</div><!-- /Wrapper -->


