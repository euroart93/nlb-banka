<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="account-row profile-row account-bg-1">
		<div class="bg-overlay overlay-2"></div>
		<div class="">
			<div class="img-box">
				<img src="img/demo/profile-1.jpg" alt="profile-img">
			</div>
			<p>Anna Kendrick</p>
		</div>
	</div>

	<div class="main-content">
		
		<div class="widget content-white">
			<div class="widget-slat">
				<div class="col-left">
					<h2><i class="icon-settings-3"></i>Self Care</h2>
				</div>
				<div class="col-right">
					<div class="group-inner">
						<div class="select-3">
							<select  data-method="customSelect3">
            					<option value="0">My profile</option>
        						<option value="1">Authentication and Authorization</option>
            					<option value="2">Applications</option>
            					<option value="3">My documents</option>
            					<option value="4">Notifications</option>
            				</select>
            			</div>
					</div>
				</div>
			</div>
			
			<div class="widget-slat">
				<div class="col-left">
					<h3 class="title-1 color-2 title-full"><span>Notifications</span></h3>
					<p>Choose which notifications you want to receive, and through which channels (SMS, e-mail, virtual inbox)</p>
				</div>
				<div class="col-right">
					<form action="#" method="#">
						<div class="user-notification-list">

							<div class="notification-wraper">
								<div class="description">
									<p><strong>Bank offers</strong></p>
								</div>
								<div class="notification-trigger">
									<div class="checkbox-wrap-2">
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
									</div>
								</div>
								<div class="hidden">
									<div class="notification-group-title">
										<p><strong>Products in use</strong></p>
									</div>
									<div class="sub-notification">
										<div class="description">
											<p>SMS notifications (30,00 USD monthly)</p>
										</div>
										<div class="notification-trigger">
											<div class="checkbox-wrap-2">
												<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
											</div>
										</div>
									</div>
									<div class="sub-notification">
										<div class="description">
											<p>E-mail notifications</p>
										</div>
										<div class="notification-trigger">
											<div class="checkbox-wrap-2">
												<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
											</div>
										</div>
									</div>
									<div class="sub-notification">
										<div class="description">
											<p>Virtual inbox notifications</p>
										</div>
										<div class="notification-trigger">
											<div class="checkbox-wrap-2">
												<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
											</div>
										</div>
									</div>

									<div class="notification-divider"></div>

									<div class="notification-group-title">
										<p><strong>Incoming transactions</strong></p>
									</div>

									<div class="sub-notification">
										<div class="description">
											<p>Send notifications only for amounts over</p>
										</div>
										<div class="notification-condition">
											<input type="text" value="5.000,00" data-method="valueInput">
										</div>
									</div>

									<div class="sub-notification">
										<div class="description">
											<p>SMS notifications (30,00 USD monthly)</p>
										</div>
										<div class="notification-trigger">
											<div class="checkbox-wrap-2">
												<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
											</div>
										</div>
									</div>
									<div class="sub-notification">
										<div class="description">
											<p>E-mail notifications</p>
										</div>
										<div class="notification-trigger">
											<div class="checkbox-wrap-2">
												<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
											</div>
										</div>
									</div>
									<div class="sub-notification">
										<div class="description">
											<p>Virtual inbox notifications</p>
										</div>
										<div class="notification-trigger">
											<div class="checkbox-wrap-2">
												<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
											</div>
										</div>
									</div>

								</div>
							</div>

							<div class="notification-wraper">
								<div class="description">
									<p><strong>Outgoing transactions</strong></p>
								</div>
								<div class="notification-trigger">
									<div class="checkbox-wrap-2">
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
									</div>
								</div>
							</div>

							<div class="notification-wraper">
								<div class="description">
									<p><strong>Declined transactions</strong></p>
								</div>
								<div class="notification-trigger">
									<div class="checkbox-wrap-2">
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
									</div>
								</div>
							</div>

							<div class="notification-wraper">
								<div class="description">
									<p><strong>Mobile banking</strong></p>
								</div>
								<div class="notification-trigger">
									<div class="checkbox-wrap-2">
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
									</div>
								</div>

								<div class="hidden">
									<div class="notification-group-title">
										<p><strong>Products in use</strong></p>
									</div>
									<div class="sub-notification">
										<div class="description">
											<p>SMS notifications (30,00 USD monthly)</p>
										</div>
										<div class="notification-trigger">
											<div class="checkbox-wrap-2">
												<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
											</div>
										</div>
									</div>
									<div class="sub-notification">
										<div class="description">
											<p>E-mail notifications</p>
										</div>
										<div class="notification-trigger">
											<div class="checkbox-wrap-2">
												<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
											</div>
										</div>
									</div>
									<div class="sub-notification">
										<div class="description">
											<p>Virtual inbox notifications</p>
										</div>
										<div class="notification-trigger">
											<div class="checkbox-wrap-2">
												<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
											</div>
										</div>
									</div>

									<div class="notification-divider"></div>

									<div class="notification-group-title">
										<p><strong>Incoming transactions</strong></p>
									</div>

									<div class="sub-notification">
										<div class="description">
											<p>Send notifications only for amounts over</p>
										</div>
										<div class="notification-condition">
											<input type="text" value="5.000,00" data-method="valueInput">
										</div>
									</div>

									<div class="sub-notification">
										<div class="description">
											<p>SMS notifications (30,00 USD monthly)</p>
										</div>
										<div class="notification-trigger">
											<div class="checkbox-wrap-2">
												<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
											</div>
										</div>
									</div>
									<div class="sub-notification">
										<div class="description">
											<p>E-mail notifications</p>
										</div>
										<div class="notification-trigger">
											<div class="checkbox-wrap-2">
												<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
											</div>
										</div>
									</div>
									<div class="sub-notification">
										<div class="description">
											<p>Virtual inbox notifications</p>
										</div>
										<div class="notification-trigger">
											<div class="checkbox-wrap-2">
												<input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
											</div>
										</div>
									</div>

								</div>
							</div>

						</div>
                    </form>
				</div>
			</div>
		</div>

	</div>

</div>

<?php Site::getFooter(); ?>