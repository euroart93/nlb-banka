<?php
Site::getHeader('header-login'); ?>

<div class="login-main">

	<!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
	<div class="page-cover inner-video video-cover img-overlay-blur">
		<img class="desktop-img promo-bg-0 slide-active" src="img/bg/login-bg-blur.png" alt="cover" data-object-fit="cover">
	</div>

	<div class="prelogin-full full-break">
		<div class="inner">
			<div class="col-left">
				<h2>NLB Keš kredit</h2>
				<p>NLB Dinarski gotovinski i krediti za refinansiranje uz dodatni keš - dobar razlog za nove želje!</p>
				<img src="img/bg/product-1.jpg" alt="img">
			</div>
			<div class="col-right">
				<div class="product-single widget">
					<div class="info-inner" data-method="customScroll">
						<h4 class="title-1 color-2 no-margin"><span>Osnovne informacije</span></h4>
						<div class="product-info">
							<ul>
								<li>Vrsta proizvoda:<span>Keš kredit</span></li>
								<li>Nominalna kamatna stopa:<span>9,95% - 12,95%</span></li>
								<li>Iznos kredita:<span>50.000 - 1.500.000 RSD</span></li>
								<li>EKS:<span>10,74% - 13,79%</span></li>
								<li>Period otplate:<span>Od 6 do 100 meseci</span></li>
								<li>Naknada:<span>0%</span></li>
							</ul>
						</div>
						<h4 class="title-1 color-2"><span>Više detalja</span></h4>
						<p>NLB Dinarski gotovinski i krediti za refinansiranje uz dodatni keš:</p>
						<ul class="content-list">
							<li>Fiksna rata tokom celokupnog perioda otplate kredita</li>
							<li>Iznos kredita: 50.000 - 1.500.000 RSD</li>
							<li>Period otplate kredita: 6 - 100 meseci</li>
							<li>Fiksna nominalna kamatna stopa: <br>
								- 9,95% - gotovinski krediti i krediti za refinansiranje uz dodatni keš - uz prijem zarade preko NLB <br>
								banke - period otplate 6-24 meseca
							</li>
						</ul>
						<p class="download">Download all documents<a href="#" class="icon-download-1"></a></p>
						<h4 class="title-1 color-2"><span>More details</span></h4>
						<p>Please check the documentation necessary for application and download it by clicking ‘Download all documents’ option.</p>
					</div>
					<div class="product-actions">
						<a href="#" class="btn-1 color-1">Zakaži sastanak</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="login-footer">
		<div class="inner">
			<!--<ul class="lang-nav">
				<li class="active"><a href="#">ENG</a></li>
				<li><a href="#">SRB</a></li>
			</ul>-->
			<ul class="social">
				<li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
				<li class="youtube"><a href="#" class="icon-youtube">Youtube</a></li>
				<li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
				<li class="history"><a href="#" class="icon-history">History</a></li>
			</ul>
		</div>
	</div>


</div>


</div><!-- /Wrapper -->
