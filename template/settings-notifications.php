<?php Site::getHeader(); ?>

	<div id="main">

		<div class="submenu">
			<div class="container">
				<ul>
					<a href="#"><li>Standarno plaćanje</li></a>
					<a href="#"><li>Administracija</li></a>
					<a href="#"><li>Lista zahteva</li></a>
					<a href="#"><li  class="selected">Notifikacije</li></a>
					<a href="#"><li>Mobilno bankarstvo</li></a>
				</ul>
			</div>
		</div>
		
		<div class="main-content">
			<div class="widget content-white left-padding payment-widget payment-steps settings">
				<h3 class="title-4 color-2"><span>Notifikacije</span></h3>

				<div class="widget-slat">
					<div class="col-left">
						<p>
							Na ovom mestu možete proveriti vreme i trajanje poslednje prijave.
						</p>
					</div>
					<div class="col-right">
					<form action="#" method="#">
						<div class="user-notification-list card-new-details">

							<div class="notification-wraper limit-background">
								<div class="limit-title ">
									<p><strong>Limit na bankomatima</strong></p>
								</div>
								<div class="notification-trigger">
									<div class="checkbox-wrap-2">
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
									</div>
								</div>
								<div class="hidden" style="display: none;">
									<div class="value-input">
										<p class="float-l">Broj i iznos transakcija dnevno:</p>
									</div>
									<div class="notification-trigger">
										<div class="checkbox-wrap-2">
											<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
										</div>
									</div>

									<div class="value-input">
										<div class="notification-divider"></div>
									</div>

									<div class="value-input">
										<p class="float-l">Broj i iznos transakcija dnevno:</p>
									</div>
									<div class="notification-trigger">
										<div class="checkbox-wrap-2">
											<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
										</div>
									</div>

									<div class="value-input">
										<div class="notification-divider"></div>
									</div>

									<div class="value-input">
										<p class="float-l">Broj i iznos transakcija dnevno:</p>
									</div>
									<div class="notification-trigger">
										<div class="checkbox-wrap-2">
											<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
										</div>
									</div>

									<div class="value-input">
										<div class="notification-divider"></div>
									</div>
									

									<div class="value-input value-bottom">
										<p class="float-l">Broj i iznos transakcija mesecno:</p>
										<div class="notification-condition float-r">
											<input type="text" value="450.000,00" data-method="valueInput">
										</div>

										<div class="notification-condition float-r">
											<input type="text" value="120" data-method="valueInput">
										</div>

										<div class="notification-divider"></div>
									</div>
								</div>
							</div>

							<div class="notification-divider"></div>

							
							<div class="notification-wraper limit-background">
								<div class="limit-title ">
									<p><strong>Limit na bankomatima</strong></p>
								</div>
								<div class="notification-trigger">
									<div class="checkbox-wrap-2">
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
									</div>
								</div>
								<div class="hidden" style="display: none;">
									<div class="value-input">
										<p class="float-l">Broj i iznos transakcija dnevno:</p>
									</div>
									<div class="notification-trigger">
										<div class="checkbox-wrap-2">
											<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
										</div>
									</div>

									<div class="value-input">
										<div class="notification-divider"></div>
									</div>

									<div class="value-input">
										<p class="float-l">Broj i iznos transakcija dnevno:</p>
									</div>
									<div class="notification-trigger">
										<div class="checkbox-wrap-2">
											<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
										</div>
									</div>

									<div class="value-input">
										<div class="notification-divider"></div>
									</div>

									<div class="value-input">
										<p class="float-l">Broj i iznos transakcija dnevno:</p>
									</div>
									<div class="notification-trigger">
										<div class="checkbox-wrap-2">
											<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
										</div>
									</div>

									<div class="value-input">
										<div class="notification-divider"></div>
									</div>
									

									<div class="value-input value-bottom">
										<p class="float-l">Broj i iznos transakcija mesecno:</p>
										<div class="notification-condition float-r">
											<input type="text" value="450.000,00" data-method="valueInput">
										</div>

										<div class="notification-condition float-r">
											<input type="text" value="120" data-method="valueInput">
										</div>

										<div class="notification-divider"></div>
									</div>
								</div>
							</div>

							<div class="notification-divider"></div>

							
							<div class="notification-wraper limit-background">
								<div class="limit-title ">
									<p><strong>Limit na bankomatima</strong></p>
								</div>
								<div class="notification-trigger">
									<div class="checkbox-wrap-2">
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
									</div>
								</div>
								<div class="hidden" style="display: none;">
									<div class="value-input">
										<p class="float-l">Broj i iznos transakcija dnevno:</p>
									</div>
									<div class="notification-trigger">
										<div class="checkbox-wrap-2">
											<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
										</div>
									</div>

									<div class="value-input">
										<div class="notification-divider"></div>
									</div>

									<div class="value-input">
										<p class="float-l">Broj i iznos transakcija dnevno:</p>
									</div>
									<div class="notification-trigger">
										<div class="checkbox-wrap-2">
											<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
										</div>
									</div>

									<div class="value-input">
										<div class="notification-divider"></div>
									</div>

									<div class="value-input">
										<p class="float-l">Broj i iznos transakcija dnevno:</p>
									</div>
									<div class="notification-trigger">
										<div class="checkbox-wrap-2">
											<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
										</div>
									</div>

									<div class="value-input">
										<div class="notification-divider"></div>
									</div>
									

									<div class="value-input value-bottom">
										<p class="float-l">Broj i iznos transakcija mesecno:</p>
										<div class="notification-condition float-r">
											<input type="text" value="450.000,00" data-method="valueInput">
										</div>

										<div class="notification-condition float-r">
											<input type="text" value="120" data-method="valueInput">
										</div>

										<div class="notification-divider"></div>
									</div>
								</div>
							</div>

							<div class="notification-divider"></div>


							<div class="notification-wraper limit-background">
								<div class="limit-title ">
									<p><strong>Limit na bankomatima</strong></p>
								</div>
								<div class="notification-trigger">
									<div class="checkbox-wrap-2">
										<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
									</div>
								</div>
								<div class="hidden" style="display: none;">
									<div class="value-input">
										<p class="float-l">Broj i iznos transakcija dnevno:</p>
									</div>
									<div class="notification-trigger">
										<div class="checkbox-wrap-2">
											<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
										</div>
									</div>

									<div class="value-input">
										<div class="notification-divider"></div>
									</div>

									<div class="value-input">
										<p class="float-l">Broj i iznos transakcija dnevno:</p>
									</div>
									<div class="notification-trigger">
										<div class="checkbox-wrap-2">
											<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
										</div>
									</div>

									<div class="value-input">
										<div class="notification-divider"></div>
									</div>

									<div class="value-input">
										<p class="float-l">Broj i iznos transakcija dnevno:</p>
									</div>
									<div class="notification-trigger">
										<div class="checkbox-wrap-2">
											<input type="checkbox" class="checkbox-2" data-method="customCheckbox1 checkboxTriggerHidden">
										</div>
									</div>

									<div class="value-input">
										<div class="notification-divider"></div>
									</div>
									

									<div class="value-input value-bottom">
										<p class="float-l">Broj i iznos transakcija mesecno:</p>
										<div class="notification-condition float-r">
											<input type="text" value="450.000,00" data-method="valueInput">
										</div>

										<div class="notification-condition float-r">
											<input type="text" value="120" data-method="valueInput">
										</div>

										<div class="notification-divider"></div>
									</div>
								</div>
							</div>

							<div class="notification-divider"></div>

							<div class="divider"></div>

							<div class="group uk-width-1-2">
								<div class="group-inner input">
									<a href="#" class="btn-1 color-1">Sačuvaj izmene</a>
								</div>
							</div>
						</div>
                    </form>
				</div>
			</div>
		</div>
	</div>



<?php Site::getFooter(); ?>