<?php
Site::getHeader('header-login'); ?>

<div class="login-main">

	<!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
	<div class="page-cover inner-video video-cover img-overlay">
		<img class="desktop-img promo-bg-0 slide-active" src="img/bg/login-bg.png" alt="cover" data-object-fit="cover">
		<img class="desktop-img promo-bg-1" src="img/bg/login-bg1.png" alt="cover" data-object-fit="cover">
		<img class="desktop-img promo-bg-2" src="img/bg/login-bg2.png" alt="cover" data-object-fit="cover">
	</div>

	<div class="login-content">
		<div class="col-right">
			<div class="prelogin-slider" data-method="preloginSlider">
				  <div class="slide">
						<div class="inner">
							<!--<p class="title-1 color-3"><span>Sign up today</span></p>-->
							<h2>E-banking nikada <br> nije bio lakši</h2>
							<!--<p>New generation of internet banking is here! seamlessly manage your money whenever and wherever you want, from whichever device you choose...</p>-->
							<a href="#" class="btn-1 color-1">Saznajte više</a>
						</div>
				  </div>
					<div class="slide">
						<div class="inner">
							<!--<p class="title-1 color-3"><span>Sign up today</span></p>-->
							<h2>2 E-banking nikada <br> nije bio lakši</h2>
							<!--<p>New generation of internet banking is here! seamlessly manage your money whenever and wherever you want, from whichever device you choose...</p>-->
							<a href="#" class="btn-1 color-1">Saznajte više</a>
						</div>
				  </div>
					<div class="slide">
						<div class="inner">
							<!--<p class="title-1 color-3"><span>Sign up today</span></p>-->
							<h2>3 E-banking nikada <br> nije bio lakši</h2>
							<!--<p>New generation of internet banking is here! seamlessly manage your money whenever and wherever you want, from whichever device you choose...</p>-->
							<a href="#" class="btn-1 color-1">Saznajte više</a>
						</div>
				  </div>
			</div>
		</div>
		<div class="col-left">
			<div class="login-box">
				<h3>Izaberite novu lozinku</h3>
				<form action="#" method="get">
					<p class="message">Molimo, izaberite svoju novu lozinku i zatim je ponovo unesite kako biste potvrdili svoj izbor.</p>
					<div class="group">
						<div class="group-inner">
							<label class="label-1">Nova lozinka:</label>
							<input type="password" class="input-1">
						</div>
					</div>
					<div class="group">
						<div class="group-inner">
							<label class="label-1">Potvrdite novu lozinku:</label>
							<input type="password" class="input-1">
						</div>
					</div>
					<!--<p class="hint"><img src="img/photos/norway.png" alt="hint norway">This is your hint image</p>-->
					<div class="group group-submit">
						<div class="group-inner">
							<a href="#" class="btn-1 color-1">Nastavi</a>
							<!--<button type="submit" class="btn-1 color-1">Prijava</button>-->
							<a href="#" class="btn-1 color-2">Odustani</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="login-footer">
		<div class="inner">
			<!--<ul class="lang-nav">
				<li class="active"><a href="#">ENG</a></li>
				<li><a href="#">SRB</a></li>
			</ul>-->
			<ul class="social">
				<li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
				<li class="youtube"><a href="#" class="icon-youtube">Youtube</a></li>
				<li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
				<li class="history"><a href="#" class="icon-history">History</a></li>
			</ul>
		</div>
	</div>


</div>


</div><!-- /Wrapper -->
