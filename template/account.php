<?php
Site::getHeader(); ?>

<div id="main">

	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li class="selected">Detalji računa</li></a>
				<a href="#"><li>Lista transakcija</li></a>
				<a href="#"><li>Lista izvoda</li></a>
			</ul>
		</div>
	</div>

	<div class="widget widget-select-account widget-content-white row" data-method="accountSelect">
			<div class="account-selected">
				<div class="col-ss-12 col-ls-2 acc-img">
					<img src="img/demo/acc-img-medium.jpg">
				</div>
				<div class="col-ss-12 col-ls-6 col-border">
					<div class="content">
						<p class="small">Ime računa:</p>
						<p class="big acc-name">Moj tekući račun</p>
					</div>
				</div>
				<div class="col-ss-12 col-ls-7 col-border">
					<div class="content">
						<p class="small">Broj računa:</p>
						<p class="big-s acc-number">115-0000000000567898-6</p>
					</div>
				</div>
				<div class="col-ss-12 col-ls-7 col-border">
					<div class="content">
						<p class="small float-right">Raspoloživo stanje:</p>
						<p class="big acc-amount float-right">5.123.543,89 RSD</p>
					</div>
				</div>
				<div class="col-ss-1">
					<a href="#" class="acc-list-btn acc-drop-btn" type="button" data-method="accountSelectTrigger">
						<div class="triangle"></div>
					</a>
				</div>
			</div>
			<div class="account-select">
      			<ul class="options">
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime računa:</p>
									<p class="big acc-name">Moj tekući račun</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj računa:</p>
									<p class="big-s acc-number">111-0000000000567898-6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">5.123.543,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium-2.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime računa:</p>
									<p class="big acc-name">Drugi tekući račun</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj računa:</p>
									<p class="big-s acc-number">112-0000000000567898-6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">1.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
									<div class="triangle-right-white"></div>
									<img class="desaturate" src="img/demo/acc-img-medium-3.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime računa:</p>
									<p class="big acc-name">Treći tekući račun</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj računa:</p>
									<p class="big-s acc-number">113-0000000000567898-6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">2.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
									<div class="triangle-right-white"></div>
									<img class="desaturate" src="img/demo/acc-img-medium-4.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime računa:</p>
									<p class="big acc-name">Četvrti tekući račun</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj računa:</p>
									<p class="big-s acc-number">114-0000000000567898-6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">3.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
       				 
    			</ul>
			</div>
		</div>

	<div class="main-content">
		
		<div class="widget content-white payment-widget">
			<div class="widget-header header-dark">
				<div class="cnt-2">
					<div class="col-left">
						<h3 class="title-4 color-2"><span>Detalji računa</span></h3>
					</div>
					<div class="col-right">
						<ul class="widget-actions bg-widget"  data-method="BGoptionsTrigger">
							<p class="widget-name">Opcije</p>
							<li>
								<div class="widget-options">
			            			<ul class="trigger" data-method="optionsTrigger">
			            				<li></li>
			            				<li></li>
			            				<li></li>
			            			</ul>
			            			<ul class="options-list">
			            				<li><a href="#">Kreiraj novi interni nalog za plaćanje</a></li>
			            				<li><a href="#">Kreiraj novi nalog za plaćanje</a></li>
			            				<li><a href="#">Promeni naziv računa</a></li>
			            				<li><a href="#">Promeni sliku računa</a></li>
			            				<li><a href="#">Lista transakcija po računu</a></li>
			            				<li><a href="#">Lista izvoda</a></li>
			            			</ul>
			            		</div>
							</li>
						</ul>
					</div>
				</div>

			</div>

			<div class="widget-slat">
				<div class="col-left">
					<div class="account-thumbnail">
						<div class="account-thumbnail-bg thumbnail-pic-1 grayscale"></div>
						<img class="account-thumbnail-pic" src="img/demo/acc-img-bg.jpg" alt="">
						<p class="account-thumbnail-name">Moj tekući račun</p>
						<p class="account-thumbnail-number">115-0000000000567898-65</p>
					</div>
				</div>
				<div class="col-right">
					<div class="plain-list">
						<div>
							<p class="key">Vlasnik računa:</p>
							<p class="value bigger">Ana Marković</p>
						</div>
						<div>
							<p class="key">Raspoloživo stanje:</p>
							<p class="value">5.125.893,49 RSD</p>
						</div>
						<div>
							<p class="key">Trenutno stanje:</p>
							<p class="value">4.325.893,00 RSD</p>
						</div>
						<div>
							<p class="key">Rezervisana sredstva:</p>
							<p class="value">-520.680,00 RSD</p>
						</div>
						<div>
							<p class="key">Iznos dozvoljenog prekoračenja:</p>
							<p class="value">800.000,00 RSD</p>
						</div>

						<div class="dropdown">
							<div>
								<p class="key">Broj računa:</p>
								<p class="value">115-0000000000567898-65</p>
							</div>
							<div>
								<p class="key">Opis:</p>
								<p class="value">NLB Dobar račun - za zaposlene</p>
							</div>
							<div>
								<p class="key">Status račna:</p>
								<p class="value">Aktivan</p>
							</div>
							<div>
								<p class="key">Kamantna stopa na pozitivno stanje:</p>
								<p class="value">0,50%</p>
							</div>
							<div>
								<p class="key">Datum otvaranja računa:</p>
								<p class="value">17.03.2016</p>
							</div>
							<div>
								<p class="key">Datum isteka dozvoljenog prekoračenja:</p>
								<p class="value">17.05.2017</p>
							</div>
							<div>
								<p class="key">Kamatna stopa na dozvoljeno prekoračenje:</p>
								<p class="value">3,75%</p>
							</div>
							<div>
								<p class="key">Kamatna stopa na nedozvoljeno prekoračenje:</p>
								<p class="value">12,50%</p>
							</div>
							<div>
								<p class="key">Broj transakcija na čekanju:</p>
								<p class="value">6</p>
							</div>
							<div>
								<p class="key">Iznos transakcija na čekanju:</p>
								<p class="value">15.950,00 RSD</p>
							</div>
							<div>
								<p class="key">Poslednja promena na računu:</p>
								<p class="value">12.03.2017</p>
							</div>
							<div>
								<p class="key">Iznos poslednje promene:</p>
								<p class="value">-6.500,00 RSD</p>
							</div>
							<div>
								<p class="key">Broj nerealizovanih čekova:</p>
								<p class="value">4</p>
							</div>
							<div>
								<p class="key">Kartice vezane na račun:</p>
								<p class="value">MasterCard Standard</p>
							</div>
							<div>
								<p class="key">Ovlašćena osoba:</p>
								<p class="value">Igor Marković</p>
							</div>
						</div>

					</div>
					<a href="#" class="btn-2 color-1 alignright" data-method="panelDropTrigger">Prikaži više detalja</a>
				</div>
			</div>

		</div>

		<div class="widget widget-transaction-list payment-list content-white">
			<div class="widget-header header-dark">
				<div class="cnt-2">
					<div class="col-left">
						<h3 class="title-4 color-2"><span>List of transactions</span></h3>
					</div>
					<div class="col-right">
						<ul class="widget-actions bg-widget"  data-method="BGoptionsTrigger">
							<p class="widget-name">Filter</p>
							<li>
								<div class="widget-options">
			            			<ul class="trigger" data-method="optionsTrigger">
			            				<li></li>
			            				<li></li>
			            				<li></li>
			            			</ul>
			            			<ul class="options-list options-list-2">
			            				<li><a href="#">Filter transactions</a></li>
			            				<li><a href="#">Settings</a></li>
			            				<li class="border"><a href="#">Export...</a></li>
			            				<li><a href="#" class="active">List view</a></li>
			            				<li><a href="#">Bubble chart view</a></li>
			            				<li><a href="#">Bar chart view</a></li>
			            				<li><a href="#">Flow chart view</a></li>
			            				<li><a href="#">Calendar view</a></li>
			            				<li><a href="#">Map view</a></li>
			            			</ul>
			            		</div>
							</li>
						</ul>
					</div>
				</div>
				<ul class="widget-tabs">
					<li class="active"><a href="#">Sve</a></li>
					<li><a href="#">Isplate</a></li>
					<li><a href="#">Uplate</a></li>
				</ul>
			</div>
			<div class="custom-table">

				<div class="table-header">
					<div class="table-head grid">
						<div class="col col-ls-3 col-ms-2 col-ss-6">
							<p class="left">Datum</p>
						</div>
						<div class="col col-ls-7 col-ms-4 col-ss-hidden">
							<p class="left">Primalac</p>
						</div>
						<div class="col col-ls-6 col-ms-hidden">
							<p class="left">Opis transakcije</p>
						</div>
						<div class="col col-ls-3 col-ms-2 col-ss-hidden">
							<p class="left">Status</p>
						</div>
						<div class="col col-ls-5 col-ms-4 col-ss-6">
							<p class="right">Iznos</p>
						</div>
					</div>
				</div>

				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
					</div>
					<div class="col col-ls-7 col-ms-4 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">Elektroprivreda Srbije<br>115-00000000004265988-23</p>
					</div>
					<div class="col col-ls-6 col-ms-hidden">
						<p class="col-text text-2 text-cl-1 left">Račun za 08/2016</p>
					</div>
					<div class="col col-ls-3 col-ms-2 col-ss-hidden">
						<p class="col-text text-1 text-cl-1 left">Na čekanju</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-drop col-text text-3 text-cl-3 right">-569,99 RSD</p>
					</div>
				</div>

				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">7 Feb 2016</p>
					</div>
					<div class="col col-ls-7 col-ms-4 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">To: Ivana Ivanović<br>115-00000000004265988-23</p>
					</div>
					<div class="col col-ls-6 col-ms-hidden">
						<p class="col-text text-2 text-cl-1 left">Lorem ipsum dolor sit amet consectetur</p>
					</div>
					<div class="col col-ls-3 col-ms-2 col-ss-hidden">
						<p class="col-text text-1 text-cl-1 left">Na čekanju</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-rise col-text text-3 text-cl-3 right">-323.450,00 RSD</p>
					</div>
				</div>

				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
					</div>
					<div class="col col-ls-7 col-ms-4 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">Elektroprivreda Srbije<br>115-00000000004265988-23</p>
					</div>
					<div class="col col-ls-6 col-ms-hidden">
						<p class="col-text text-2 text-cl-1 left">Račun za 08/2016</p>
					</div>
					<div class="col col-ls-3 col-ms-2 col-ss-hidden">
						<p class="col-text text-1 text-cl-1 left">Na čekanju</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-drop col-text text-3 text-cl-3 right">-569,99 RSD</p>
					</div>
				</div>

				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
					</div>
					<div class="col col-ls-7 col-ms-4 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">Elektroprivreda Srbije<br>115-00000000004265988-23</p>
					</div>
					<div class="col col-ls-6 col-ms-hidden">
						<p class="col-text text-2 text-cl-1 left">Račun za 08/2016</p>
					</div>
					<div class="col col-ls-3 col-ms-2 col-ss-hidden">
						<p class="col-text text-1 text-cl-1 left">Na čekanju</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-drop col-text text-3 text-cl-3 right">-569,99 RSD</p>
					</div>
				</div>

				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
					</div>
					<div class="col col-ls-7 col-ms-4 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">Elektroprivreda Srbije<br>115-00000000004265988-23</p>
					</div>
					<div class="col col-ls-6 col-ms-hidden">
						<p class="col-text text-2 text-cl-1 left">Račun za 08/2016</p>
					</div>
					<div class="col col-ls-3 col-ms-2 col-ss-hidden">
						<p class="col-text text-1 text-cl-1 left">Na čekanju</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-drop col-text text-3 text-cl-3 right">-569,99 RSD</p>
					</div>
				</div>

			</div>
			<div class="button-wrap">
				<a href="#" class="btn-1 color-1">View all transactions</a>
			</div>
		</div>


	</div>

</div>

<?php Site::getFooter(); ?>
