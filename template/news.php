<?php Site::getHeader(); ?>
	<div id="main">
		

		<div class="main-content">
		
			<div class="widget content-white news">
				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-4 color-2"><span>Vesti</span></h3>
						<img src="img/demo/b-92.png">
						<p>Pronađite najnovije vesti poređane 
						hronološkim redosledom. Vesti možete i da pretražujete korištenjem polja za pretraživanje na vrhu stranice.</p>
					</div>
					<div class="col-right">
						<div class="group">
							<div class="group-inner input-search">
								<input type="search" class="input-1" placeholder="Traži...">
								<button type="button" class="search-btn"></button>
							</div>
						</div>
						<div class="custom-table">

							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
									<p class="col-text text-2 text-cl-1 left">Dojče vele: EU - spasavaj šta se spasti može</p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">16:32</p>
								</div>
							</div>

							
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
									<p class="col-text text-2 text-cl-1 left">Vojna baza u Kelebiji - čista politika ili nešto drugo?</p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">16:30</p>
								</div>
							</div>

							
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
									<p class="col-text text-2 text-cl-1 left">Na Kelebiji šestoro migranata, u bazi 150 vojnika</p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">16:20</p>
								</div>
							</div>

							
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
									<p class="col-text text-2 text-cl-1 left">U odronima i poplavama u Avganistanu poginula 191 osoba</p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">16:05</p>
								</div>
							</div>

							
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
									<p class="col-text text-2 text-cl-1 left">Merkelova i Pens sledeće nedelje u Minhenu</p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">15:59</p>
								</div>
							</div>

							
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
									<p class="col-text text-2 text-cl-1 left">Kertes oslobođen i za šverc cigareta</p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">15:49</p>
								</div>
							</div>

							
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
									<p class="col-text text-2 text-cl-1 left">Izbori moraju biti raspisani do 3. marta, održani do 1. maja</p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">15:30</p>
								</div>
							</div>

							
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
									<p class="col-text text-2 text-cl-1 left">Šojble: Martin Šulc - nemački Donald Tramp</p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">15:22</p>
								</div>
							</div>

							
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
									<p class="col-text text-2 text-cl-1 left">Čanak potpisao deklaraciju o militarizaciji Balkana</p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">15:20</p>
								</div>
							</div>

							
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
									<p class="col-text text-2 text-cl-1 left">Vučić i Palmer: Odnosi Srbije i SAD na snažnim temeljima</p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">14:48</p>
								</div>
							</div>

							
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
									<p class="col-text text-2 text-cl-1 left">Delovi Beograda bez vode na dan naredne nedelje</p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">14:44</p>
								</div>
							</div>

							
							<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
								<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
									<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
									<p class="col-text text-2 text-cl-1 left">Ljubiša Beara, šesti Srbin koji je preminuo u Hagu</p>
								</div>
								<div class="col col-ls-6 col-ms-3 col-ss-3">
									<p class="col-text text-1 text-cl-1 right">14:41</p>
								</div>
							</div>

							<div class="hidden-content">
								
								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
										<p class="col-text text-2 text-cl-1 left">Dojče vele: EU - spasavaj šta se spasti može</p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">16:32</p>
									</div>
								</div>

								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
										<p class="col-text text-2 text-cl-1 left">Vojna baza u Kelebiji - čista politika ili nešto drugo?</p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">16:30</p>
									</div>
								</div>

								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
										<p class="col-text text-2 text-cl-1 left">Na Kelebiji šestoro migranata, u bazi 150 vojnika</p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">16:20</p>
									</div>
								</div>

								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
										<p class="col-text text-2 text-cl-1 left">U odronima i poplavama u Avganistanu poginula 191 osoba</p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">16:05</p>
									</div>
								</div>

								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
										<p class="col-text text-2 text-cl-1 left">Merkelova i Pens sledeće nedelje u Minhenu</p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">15:59</p>
									</div>
								</div>

								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
										<p class="col-text text-2 text-cl-1 left">Kertes oslobođen i za šverc cigareta</p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">15:49</p>
									</div>
								</div>

								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
										<p class="col-text text-2 text-cl-1 left">Izbori moraju biti raspisani do 3. marta, održani do 1. maja</p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">15:30</p>
									</div>
								</div>

								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
										<p class="col-text text-2 text-cl-1 left">Šojble: Martin Šulc - nemački Donald Tramp</p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">15:22</p>
									</div>
								</div>

								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
										<p class="col-text text-2 text-cl-1 left">Čanak potpisao deklaraciju o militarizaciji Balkana</p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">15:20</p>
									</div>
								</div>

								<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-news">
									<div class="col col-ls-18 col-ms-9 col-ss-9 tag-col">
										<div class="tag tag-2 tagcolor-6 icon-rss-1"></div>
										<p class="col-text text-2 text-cl-1 left">Vučić i Palmer: Odnosi Srbije i SAD na snažnim temeljima</p>
									</div>
									<div class="col col-ls-6 col-ms-3 col-ss-3">
										<p class="col-text text-1 text-cl-1 right">14:48</p>
									</div>
								</div>
							</div>
						</div>
						

						<div class="button-wrap">
							<a href="#" class="btn-2 color-1 alignright" data-method="showMore">Prikaži više vesti</a>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
<?php Site::getFooter(); ?>
