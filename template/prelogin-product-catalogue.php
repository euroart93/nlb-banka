<?php
Site::getHeader('header-login'); ?>

<div class="login-main">

	<!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
	<div class="page-cover inner-video video-cover img-overlay-blur">
		<img class="desktop-img promo-bg-0 slide-active" src="img/bg/login-bg-blur.png" alt="cover" data-object-fit="cover">
	</div>

	<div class="prelogin-full -prelogin-autoheight">
		<div class="inner">
			<a href="#" type="button" class="catalogue-close">X</a>
			<div class="col-left">
				<h2>Proizvodi</h2>
				<p>Informacije o našim proizvodima i uslugama su sada samo na klik od Vas!</p>
			</div>

			<div class="col-right product-wrapper">

				<div class="widget-grid-options">
					<div class="grid-options">
						<div class="grid-option option-dropdown">
							<a href="#" class="widget-select" data-method="dropdownTrigger"><i class="icon-grid-1"></i>Svi proizvodi</a>
							<div class="widget-select-menu">
								<ul>
									<li>
										Product 1
										<input type="checkbox" class="checkbox-1 checked" checked data-method="customCheckbox1">
									</li>
									<li>
										Product 2
										<input type="checkbox" class="checkbox-1 checked" checked data-method="customCheckbox1">
									</li>
									<li>
										Product 3
										<input type="checkbox" class="checkbox-1 checked" checked data-method="customCheckbox1">
									</li>
									<li>
										Product 4
										<input type="checkbox" class="checkbox-1 checked" checked data-method="customCheckbox1">
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="product-cat-slat prelogin-product">
					<div class="product-list" data-method="customScroll">
						<div class="product-box" style="background-image: url(img/bg/product-1.jpg);">
							<p class="title-1 color-1"><span>Krediti</span></p>
							<div class="product-content">
								<h3>Brzi keš kredit</h3>
								<p class="product-txt"><span class="circle color-1"></span>Dolazi uskoro!</p>
								<div class="expand">
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
									<a href="#" class="btn-1 color-1">Saznajte više</a>
								</div>
							</div>
						</div>
						<div class="product-box" style="background-image: url(img/bg/product-6.jpg);">
							<p class="recommended">Preporučeno</p>
							<p class="title-1 color-1"><span>Štednje</span></p>
							<div class="product-content">
								<h3>Štedite u NLB banci</h3>
								<p class="product-txt"><span class="circle color-2"></span>Nekoristite ovaj proizvod!</p>
								<div class="expand">
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
									<a href="#" class="btn-1 color-1">Saznajte više</a>
								</div>
							</div>
						</div>
						<div class="product-box" style="background-image: url(img/bg/product-5.jpg);">
							<p class="title-1 color-1"><span>Kartice</span></p>
							<div class="product-content">
								<h3>Pregled kartica</h3>
								<p class="product-txt"><span class="circle color-2"></span>Nekoristite ovaj proizvod!</p>
								<div class="expand">
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
									<a href="#" class="btn-1 color-1">Saznajte više</a>
								</div>
							</div>
						</div>
						<div class="product-box" style="background-image: url(img/bg/product-2.jpg);">
							<p class="title-1 color-1"><span>Tekući računi</span></p>
							<div class="product-content">
								<h3>Pregled računa</h3>
								<p class="product-txt"><span class="circle color-1"></span>Nekoristite ovaj proizvod!</p>
								<div class="expand">
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
									<a href="#" class="btn-1 color-1">Saznajte više</a>
								</div>
							</div>
						</div>
						<div class="product-box" style="background-image: url(img/bg/product-3.jpg);">
							<p class="title-1 color-1"><span>Cards</span></p>
							<div class="product-content">
								<h3>Quick Cash Loan</h3>
								<p class="product-txt"><span class="circle color-2"></span>You are not using this product</p>
								<div class="expand">
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
									<a href="#" class="btn-1 color-1">View details</a>
								</div>
							</div>
						</div>
						<div class="product-box" style="background-image: url(img/bg/product-4.jpg);">
							<p class="title-1 color-1"><span>Current account</span></p>
							<div class="product-content">
								<h3>Quick Cash Loan</h3>
								<p class="product-txt"><span class="circle color-1"></span>You are not using this product</p>
								<div class="expand">
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
									<a href="#" class="btn-1 color-1">View details</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="login-footer">
		<div class="inner">
			<!--<ul class="lang-nav">
				<li class="active"><a href="#">ENG</a></li>
				<li><a href="#">SRB</a></li>
			</ul>-->
			<ul class="social">
				<li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
				<li class="youtube"><a href="#" class="icon-youtube">Youtube</a></li>
				<li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
				<li class="history"><a href="#" class="icon-history">History</a></li>
			</ul>
		</div>
	</div>


</div>


</div><!-- /Wrapper -->
