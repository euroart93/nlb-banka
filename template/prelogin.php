<?php
Site::getHeader('header-login'); ?>

<div class="login-main">

	<!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
	<div class="page-cover inner-video video-cover img-overlay">
		<img class="desktop-img promo-bg-0 slide-active" src="img/bg/login-bg.png" alt="cover" data-object-fit="cover">
		<img class="desktop-img promo-bg-1" src="img/bg/login-bg1.png" alt="cover" data-object-fit="cover">
		<img class="desktop-img promo-bg-2" src="img/bg/login-bg2.png" alt="cover" data-object-fit="cover">
	</div>

	<div class="login-content">
		<div class="col-right">
			<div class="prelogin-slider" data-method="preloginSlider">
				  <div class="slide">
						<div class="inner">
							<!--<p class="title-1 color-3"><span>Sign up today</span></p>-->
							<h2>E-banking nikada <br> nije bio lakši</h2>
							<!--<p>New generation of internet banking is here! seamlessly manage your money whenever and wherever you want, from whichever device you choose...</p>-->
							<a href="#" class="btn-1 color-1">Saznajte više</a>
						</div>
				  </div>
					<div class="slide">
						<div class="inner">
							<!--<p class="title-1 color-3"><span>Sign up today</span></p>-->
							<h2>2 E-banking nikada <br> nije bio lakši</h2>
							<!--<p>New generation of internet banking is here! seamlessly manage your money whenever and wherever you want, from whichever device you choose...</p>-->
							<a href="#" class="btn-1 color-1">Saznajte više</a>
						</div>
				  </div>
					<div class="slide">
						<div class="inner">
							<!--<p class="title-1 color-3"><span>Sign up today</span></p>-->
							<h2>3 E-banking nikada <br> nije bio lakši</h2>
							<!--<p>New generation of internet banking is here! seamlessly manage your money whenever and wherever you want, from whichever device you choose...</p>-->
							<a href="#" class="btn-1 color-1">Saznajte više</a>
						</div>
				  </div>
			</div>
		</div>
		<div class="col-left">
		<a href="#" type="button" class="close-box">Zatvori</a>
			<div class="login-box">
				<h3>Prijava na e-banking</h3>
				<form action="#" method="get">
					<div class="group">
						<div class="group-inner">
							<label class="label-1">Korisničko ime:</label>
							<input type="text" class="input-1">
						</div>
					</div>
					<div class="group">
						<div class="group-inner">
							<label class="label-1">Lozinka:</label>
							<input type="password" class="input-1">
						</div>
					</div>
					<p class="forgot-pass">Zaboravili ste <a href="#">korisničko ime</a> ili <a href="#">lozinku</a>?</p>
					<!--<p class="hint"><img src="img/photos/norway.png" alt="hint norway">This is your hint image</p>-->
					<div class="group group-submit">
						<div class="group-inner">
							<button type="submit" class="btn-1 color-1 full-width">Prijava</button>
							<!--<a href="#" class="btn-1 color-2">Sign up for an account</a>-->
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="login-footer">
		<div class="inner">
			<!--<ul class="lang-nav">
				<li class="active"><a href="#">ENG</a></li>
				<li><a href="#">SRB</a></li>
			</ul>-->
			<ul class="social">
				<li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
				<li class="youtube"><a href="#" class="icon-youtube">Youtube</a></li>
				<li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
				<li class="history"><a href="#" class="icon-history">History</a></li>
			</ul>
		</div>
	</div>


</div>


</div><!-- /Wrapper -->
