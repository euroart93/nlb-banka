<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="main-content">
		<form action="#" method="get">

			<div class="widget content-white">

				<h3 class="title-2 color-1 title-full"><span>Textbox</span></h3>
				<div class="custom-form uk-grid">
					<div class="group uk-width-1-1">
						<div class="group-inner">
							<label class="label-1">Textbox field:</label>
							<input type="text" class="input-1">
						</div>
					</div>
					<div class="group uk-width-1-1">
						<div class="group-inner error">
							<label class="label-1">Textbox field error:</label>
							<input type="text" class="input-1">
							<p class="error-msg">Invalid entry</p>
						</div>
					</div>
					<div class="group uk-width-1-1">
						<div class="group-inner">
							<label class="label-1">Textbox field read only:</label>
							<input type="text" class="input-1" readonly value="Input text">
						</div>
					</div>
				</div>

				<h3 class="title-2 color-1 title-full"><span>Text area</span></h3>
				<div class="custom-form uk-grid">
					<div class="group uk-width-1-1">
						<div class="group-inner">
							<label class="label-1">Text area field:</label>
							<textarea class="textarea-1"></textarea>
						</div>
					</div>
					<div class="group uk-width-1-1">
						<div class="group-inner error">
							<label class="label-1">Text area field error:</label>
							<textarea class="textarea-1"></textarea>
							<p class="error-msg">Invalid entry</p>
						</div>
					</div>
					<div class="group uk-width-1-1">
						<div class="group-inner">
							<label class="label-1">Text area field ead only:</label>
							<textarea type="text" class="textarea-1" readonly>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi vero, tenetur cumque magni in repellendus quam voluptates ea culpa quod praesentium.</textarea>
						</div>
					</div>
				</div>

				<h3 class="title-2 color-1 title-full"><span>Select field</span></h3>
				<div class="custom-form uk-grid">
					<div class="group uk-width-1-1">
						<div class="group-inner">
							<label class="label-1">Select field:</label>
							<div class="select-3">
								<select data-method="customSelect3">
	            					<option value="0">230 - Promet robe i usluga</option>
	            					<option value="1">231 - Promet robe i usluga</option>
	            					<option value="2">232 - Promet robe i usluga</option>
	            					<option value="3">233 - Promet robe i usluga</option>
	            				</select>
	            			</div>
						</div>
					</div>
					<div class="group uk-width-1-1">
						<div class="group-inner error">
							<label class="label-1">Select field error:</label>
							<div class="select-3">
								<select data-method="customSelect3">
	            					<option value="0">230 - Promet robe i usluga</option>
	            					<option value="1">231 - Promet robe i usluga</option>
	            					<option value="2">232 - Promet robe i usluga</option>
	            					<option value="3">233 - Promet robe i usluga</option>
	            				</select>
	            			</div>
	            			<p class="error-msg">Invalid entry</p>
						</div>
					</div>
					<div class="group uk-width-1-1">
						<div class="group-inner">
							<label class="label-1">Select field disabled:</label>
							<div class="select-3">
								<select data-method="customSelect3" disabled>
	            					<option value="0">230 - Promet robe i usluga</option>
	            					<option value="1">231 - Promet robe i usluga</option>
	            					<option value="2">232 - Promet robe i usluga</option>
	            					<option value="3">233 - Promet robe i usluga</option>
	            				</select>
	            			</div>
						</div>
					</div>
				</div>

				<h3 class="title-2 color-1 title-full"><span>Date picker</span></h3>
				<div class="custom-form uk-grid">
					<div class="group uk-width-1-1">
						<label class="label-1">Date picker field:</label>
						<div class="group-inner date-wrap">
							<input type="text" class="input-1 dark date-input" value="15/02/2016" data-method="dateInput">
						</div>
					</div>
					<div class="group uk-width-1-1">
						<label class="label-1">Date picker field error:</label>
						<div class="group-inner date-wrap error">
							<input type="text" class="input-1 dark date-input" value="15/02/2016" data-method="dateInput">
							<p class="error-msg">Invalid entry</p>
						</div>
					</div>
					<div class="group uk-width-1-1">
						<label class="label-1">Date picker field disabled:</label>
						<div class="group-inner date-wrap">
							<input type="text" class="input-1 dark date-input" disabled value="15/02/2016" data-method="dateInput">
						</div>
					</div>
				</div>
				
				<h3 class="title-2 color-1 title-full"><span>Checkbox</span></h3>
				<div class="custom-form uk-grid">
					<div class="group uk-width-1-1">
						<div class="group-inner checkbox-wrap">
							<input type="checkbox" checked class="checkbox-1 checked" data-method="customCheckbox1">
							<label class="checkbox-label">Checkbox field active</label>
						</div>
					</div>
					<div class="group uk-width-1-1">
						<div class="group-inner checkbox-wrap">
							<input type="checkbox" class="checkbox-1" data-method="customCheckbox1">
							<label class="checkbox-label">Checkbox field inactive</label>
						</div>
					</div>
					<div class="group uk-width-1-1">
						<div class="group-inner checkbox-wrap">
							<input type="checkbox" checked class="checkbox-1 disabled" disabled data-method="customCheckbox1">
							<label class="checkbox-label">Checkbox field disabled</label>
						</div>
					</div>
				</div>

				<h3 class="title-2 color-1 title-full"><span>Radio buttons</span></h3>
				<div class="custom-form uk-grid">
					<div class="group uk-width-1-1">
						<div class="group-inner radio-wrap">
							<input type="radio" checked class="radio-1 checked" name="radio" data-method="customRadio1">
							<label class="radio-label">Radio active</label>
						</div>
						<div class="group-inner radio-wrap">
							<input type="radio" class="radio-1" name="radio" data-method="customRadio1">
							<label class="radio-label">Radio inactive</label>
						</div>
						<div class="group-inner radio-wrap">
							<input type="radio" class="radio-1 disabled" name="radio" disabled data-method="customRadio1">
							<label class="radio-label">Radio disabled</label>
						</div>
					</div>
				</div>
					
				<h3 class="title-2 color-1 title-full"><span>File upload</span></h3>
				<div class="custom-form uk-grid">
					<div class="group uk-width-1-1">
						<div class="group-inner">
							<label class="label-1">File upload field:</label>
							<div class="file-wrapper">
								<input type="file" class="input-1 upload-file" data-method="uploadFile">
								<div class="name-wrapper">
									<span class="file-name"></span>
								</div>
								<button class="upload-btn"></button>
							</div>
						</div>
					</div>
					<div class="group uk-width-1-1">
						<div class="group-inner error">
							<label class="label-1">File upload field error:</label>
							<div class="file-wrapper">
								<input type="file" class="input-1 upload-file" data-method="uploadFile">
								<div class="name-wrapper">
									<span class="file-name"></span>
								</div>
								<button class="upload-btn"></button>
							</div>
							<p class="error-msg">File type not recognized</p>
						</div>
					</div>
					<div class="group uk-width-1-1">
						<div class="group-inner">
							<label class="label-1">File upload field disabled:</label>
							<div class="file-wrapper disabled">
								<input type="file" class="input-1 upload-file" disabled data-method="uploadFile">
								<div class="name-wrapper">
									<span class="file-name"></span>
								</div>
								<button class="upload-btn"></button>
							</div>
						</div>
					</div>
				</div>

				<h3 class="title-2 color-1 title-full"><span>Form grid</span></h3>
				<div class="custom-form uk-grid">
					<div class="group uk-width-1-2">
						<div class="group-inner">
							<label class="label-1">Textbox field:</label>
							<input type="text" class="input-1">
						</div>
					</div>
					<div class="group uk-width-1-2">
						<div class="group-inner">
							<label class="label-1">Textbox field:</label>
							<input type="text" class="input-1">
						</div>
					</div>
					<div class="group uk-width-1-3">
						<div class="group-inner">
							<label class="label-1">Textbox field:</label>
							<input type="text" class="input-1">
						</div>
					</div>
					<div class="group uk-width-1-3">
						<div class="group-inner">
							<label class="label-1">Textbox field:</label>
							<input type="text" class="input-1">
						</div>
					</div>
					<div class="group uk-width-1-3">
						<div class="group-inner">
							<label class="label-1">Textbox field:</label>
							<input type="text" class="input-1">
						</div>
					</div>
					<div class="group uk-width-1-3">
						<div class="group-inner">
							<label class="label-1">Textbox field:</label>
							<input type="text" class="input-1">
						</div>
					</div>
					<div class="group uk-width-2-3">
						<div class="group-inner">
							<label class="label-1">Textbox field:</label>
							<input type="text" class="input-1">
						</div>
					</div>
					<div class="group uk-width-1-4">
						<div class="group-inner">
							<label class="label-1">Textbox field:</label>
							<input type="text" class="input-1">
						</div>
					</div>
					<div class="group uk-width-1-4">
						<div class="group-inner">
							<label class="label-1">Textbox field:</label>
							<input type="text" class="input-1">
						</div>
					</div>
					<div class="group uk-width-1-2">
						<div class="group-inner">
							<label class="label-1">Textbox field:</label>
							<input type="text" class="input-1">
						</div>
					</div>
				</div>

			</div>


		</form>
	</div>

</div>

<?php Site::getFooter(); ?>