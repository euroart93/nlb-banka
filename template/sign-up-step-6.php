<?php 
Site::getHeader('header-login'); ?>

<div class="login-main">

  <!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
  <div class="page-cover inner-video video-cover img-overlay">
    <img class="desktop-img" src="img/bg/login-bg.jpg" alt="cover" data-object-fit="cover">
    <img class="mobile-img" src="img/bg/login-mobile.jpg" alt="cover" data-object-fit="cover">
  </div>

  <div class="prelogin-full sign-up-steps">
    <div class="inner">
      <div class="col-head">
        <a href="#" class="close">Close</a>
      </div>
      <div class="col-left">
        <h2>Enter your residental data</h2>
        <p>Please enter your residental address data in the following fields.</p>
      </div>
      <div class="col-right">
        <ul class="login-steps">
          <li>1</li>
          <li>2</li>
          <li>3</li>
          <li>4</li>
          <li>5</li>
          <li class="active">6 enter your residental data</li>
          <li>7</li>
        </ul>
        <form action="#" method="#">
          <div class="scrollable-content" data-method="customScroll">

            <div class="custom-form uk-grid">

              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Enter your country:</label>
                  <input type="text" class="input-1" value="Serbia">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Enter your city:</label>
                  <input type="text" class="input-1" value="Belgrade">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Enter county:</label>
                  <input type="text" class="input-1" value="Palilula">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Postal code:</label>
                  <input type="text" class="input-1" value="11000">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Enter street name:</label>
                  <input type="text" class="input-1" value="Bojana Grdovića">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner error">
                  <label class="label-1">Street number:</label>
                  <input type="text" class="input-1" value="kr">
                  <p class="error-msg">Invalid entry</p>
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Building number:</label>
                  <input type="text" class="input-1" value="3">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Enterance:</label>
                  <input type="text" class="input-1" value="6">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Floor number:</label>
                  <input type="text" class="input-1" value="1">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Apartment number:</label>
                  <input type="text" class="input-1" value="102">
                </div>
              </div>

            </div>

            <div class="grey-divider" style="margin: 5px 0 20px 0"></div>

            <div class="custom-form uk-grid">

              <div class="group uk-width-1-1">
                <div class="group-inner uk-width-1-1">
                  <div class="checkbox-wrap-2">
                    <input type="checkbox" class="checkbox-2" data-method="customCheckbox1">
                    <label class="checkbox-label-3">My correspondence address is the same as residential address.</label>
                  </div>
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Enter your country:</label>
                  <input type="text" class="input-1" value="Serbia">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Enter your city:</label>
                  <input type="text" class="input-1" value="Belgrade">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Enter county:</label>
                  <input type="text" class="input-1" value="Palilula">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Postal code:</label>
                  <input type="text" class="input-1" value="11000">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Enter street name:</label>
                  <input type="text" class="input-1" value="Bojana Grdovića">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner error">
                  <label class="label-1">Street number:</label>
                  <input type="text" class="input-1" value="kr">
                  <p class="error-msg">Invalid entry</p>
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Building number:</label>
                  <input type="text" class="input-1" value="3">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Enterance:</label>
                  <input type="text" class="input-1" value="6">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Floor number:</label>
                  <input type="text" class="input-1" value="1">
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Apartment number:</label>
                  <input type="text" class="input-1" value="102">
                </div>
              </div>

            </div>

          </div>

          <div class="button-actions">
            <a href="#" class="btn-1 color-3">Back</a>
            <a href="#" class="btn-1 color-1">Next step</a>
          </div>

        </form>
      </div>
    </div>
  </div>

  <div class="login-footer">
    <div class="inner">
      <ul class="lang-nav">
        <li class="active"><a href="#">ENG</a></li>
        <li><a href="#">SRB</a></li>
      </ul>
      <ul class="social">
        <li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
        <li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
        <li class="twitter"><a href="#" class="icon-twitter">Twitter</a></li>
      </ul>
    </div>
  </div>


</div>


</div><!-- /Wrapper -->


