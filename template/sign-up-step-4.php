<?php 
Site::getHeader('header-login'); ?>

<div class="login-main">

  <!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
  <div class="page-cover inner-video video-cover img-overlay">
    <img class="desktop-img" src="img/bg/login-bg.jpg" alt="cover" data-object-fit="cover">
    <img class="mobile-img" src="img/bg/login-mobile.jpg" alt="cover" data-object-fit="cover">
  </div>

  <div class="prelogin-full sign-up-steps">
    <div class="inner">
      <div class="col-head">
        <a href="#" class="close">Close</a>
      </div>
      <div class="col-left">
        <h2>Choose one of our products</h2>
        <img src="img/bg/product-promo.jpg" alt="img">
      </div>
      <div class="col-right">
        <ul class="login-steps">
          <li>1</li>
          <li>2</li>
          <li>3</li>
          <li class="active">4 Products and services</li>
          <li>5</li>
          <li>6</li>
          <li>7</li>
        </ul>
        <form action="#" method="#">
          <div class="scrollable-content" data-method="customScroll">
             
            <div class="custom-form uk-grid">

              <div class="group uk-width-1-1">
                <div class="group-inner">
                  <label class="label-1">Please choose one of our products:</label>
                  <div class="select-3">
                    <select data-method="customSelect3">
                      <option value="0">Product name</option>
                      <option value="1">Product name 2</option>
                      <option value="2">Product name 3</option>
                      <option value="3">Product name 4</option>
                    </select>
                  </div>
                </div>
              </div>

            </div>

            <div class="info-content">
              <p>Etiam maximus scelerisque nunc, non vehicula nisi pretium nec. Praesent dolor metus, semper a arcu sed, lobortis
                vestibulum augue. In luctus efficitur volutpat. Nunc interdum, tortor vitae fermentum facilisis, nisi enim vestibulum
              </p>
              <p>
                tellus, a congue lorem purus at velit. Maecenas lacinia lacinia magna ac ullamcorper. 
                Vestibulum sollicitudin felis ut ipsum pharetra suscipit. Duis elementum vel velit tristique rutrum. Aenean sollicitudin sapien eget ornare rhoncus. Etiam maximus scelerisque nunc, non vehicula nisi pretium nec. Praesent dolor metus, semper a arcu sed, lobortis vestibulum augue. Etiam maximus scelerisque nunc, non vehicula nisi pretium nec. Praesent dolor metus, semper a arcu sed, lobortis vestibulum augue.
              </p>
            </div>

            <div class="custom-form uk-grid">

              <div class="group uk-width-1-2">

                <div class="group-inner">
                  <label class="label-1">Choose your account number:</label>
                  <div class="select-3">
                    <select data-method="customSelect3">
                      <option value="0">From my phone number</option>
                      <option value="1">From my mail account</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="group uk-width-1-2">
                <div class="group-inner">
                  <label class="label-1">Phone number:</label>
                  <input type="text" class="input-1" value="+381 11 123445566">
                </div>
              </div>
              <div class="group uk-width-1-1">
                <div class="group-inner">
                  <label class="label-1">Name onn your payment card:</label>
                  <input type="text" class="input-1" value="Jurica Vukovic">
                </div>
              </div>

            </div>

          </div>

          <div class="button-actions">
            <a href="#" class="btn-1 color-1 fl-r">Next step</a>
          </div>

        </form>
      </div>
    </div>
  </div>

  <div class="login-footer">
    <div class="inner">
      <ul class="lang-nav">
        <li class="active"><a href="#">ENG</a></li>
        <li><a href="#">SRB</a></li>
      </ul>
      <ul class="social">
        <li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
        <li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
        <li class="twitter"><a href="#" class="icon-twitter">Twitter</a></li>
      </ul>
    </div>
  </div>


</div>


</div><!-- /Wrapper -->


