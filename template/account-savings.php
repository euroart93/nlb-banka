<?php
Site::getHeader(); ?>

<div id="main">

	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li class="selected">Detalji računa</li></a>
				<a href="#"><li>Lista transakcija</li></a>
				<a href="#"><li>Lista izvoda</li></a>
			</ul>
		</div>
	</div>

	<div class="widget widget-select-account widget-content-white row" data-method="accountSelect">
			<div class="account-selected">
				<div class="col-ss-12 col-ls-2 acc-img">
					<img src="img/demo/acc-img-10.jpg">
				</div>
				<div class="col-ss-12 col-ls-6 col-border">
					<div class="content">
						<p class="small">Ime računa:</p>
						<p class="big acc-name">Moj štedni račun</p>
					</div>
				</div>
				<div class="col-ss-12 col-ls-7 col-border">
					<div class="content">
						<p class="small">Broj računa:</p>
						<p class="big-s acc-number">115-0000000000567898-6</p>
					</div>
				</div>
				<div class="col-ss-12 col-ls-7 col-border">
					<div class="content">
						<p class="small float-right">Raspoloživo stanje:</p>
						<p class="big acc-amount float-right">2.653.543,89 RSD</p>
					</div>
				</div>
				<div class="col-ss-1">
					<a href="#" class="acc-list-btn acc-drop-btn" type="button" data-method="accountSelectTrigger">
						<div class="triangle"></div>
					</a>
				</div>
			</div>
			<div class="account-select">
      			<ul class="options">
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime računa:</p>
									<p class="big acc-name">Moj prvi štedni račun</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj računa:</p>
									<p class="big-s acc-number">111-0000000000567898-6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">5.123.543,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium-2.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime računa:</p>
									<p class="big acc-name">Drugi štedni račun</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj računa:</p>
									<p class="big-s acc-number">112-0000000000567898-6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">1.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
									<div class="triangle-right-white"></div>
									<img class="desaturate" src="img/demo/acc-img-medium-3.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime računa:</p>
									<p class="big acc-name">Treći štedni račun</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj računa:</p>
									<p class="big-s acc-number">113-0000000000567898-6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">2.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
									<div class="triangle-right-white"></div>
									<img class="desaturate" src="img/demo/acc-img-medium-4.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime računa:</p>
									<p class="big acc-name">Četvrti štedni račun</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj računa:</p>
									<p class="big-s acc-number">114-0000000000567898-6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">3.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
       				 
    			</ul>
			</div>
	</div>

	<div class="main-content">
			
		<div class="widget content-white payment-widget">
			<div class="widget-header header-dark">
				<div class="cnt-2">
					<div class="col-left">
						<h3 class="title-4 color-2"><span>Detalji računa</span></h3>
					</div>
					<div class="col-right">
						<ul class="widget-actions bg-widget"  data-method="BGoptionsTrigger">
							<p class="widget-name">Opcije</p>
							<li>
								<div class="widget-options">
			            			<ul class="trigger" data-method="optionsTrigger">
			            				<li></li>
			            				<li></li>
			            				<li></li>
			            			</ul>
			            			<ul class="options-list">
			            				<li><a href="#">Uplati na štedni račun</a></li>
			            				<li><a href="#">Prebaci sredstva</a></li>
			            				<li><a href="#">Promeni naziv računa</a></li>
			            				<li><a href="#">Promeni sliku računa</a></li>
			            				<li><a href="#">Lista transakcija po računu</a></li>
			            				<li><a href="#">Lista izvoda</a></li>
			            			</ul>
			            		</div>
							</li>
						</ul>
					</div>
				</div>

			</div>

			<div class="widget-slat">
				<div class="col-left">
					<div class="account-thumbnail">
						<div class="account-thumbnail-bg thumbnail-pic-2 grayscale"></div>
						<img class="account-thumbnail-pic" src="img/demo/acc-img-10.jpg" alt="">
						<p class="account-thumbnail-name">Moj štedni račun</p>
						<p class="account-thumbnail-number">115-0000000000567898-65</p>
					</div>
				</div>
				<div class="col-right account-savings cf">
					<div class="plain-list">
						<div>
							<p class="key">Vlasnik računa:</p>
							<p class="value bigger">Ana Marković</p>
						</div>
						<div>
							<p class="key">Stanje računa:</p>
							<p class="value">2.560.893,49 RSD</p>
						</div>
						<div>
							<p class="key">Nominalna kamatna stopa:</p>
							<p class="value">0.10%</p>
						</div>
						<div class="dropdown">
							<div>
								<p class="key">Tip štednje:</p>
								<p class="value">Dinarska a vista štednja</p>
							</div>
							<div>
								<p class="key">Valuta:</p>
								<p class="value">RSD</p>
							</div>
							<div>
								<p class="key">Rok štednje:</p>
								<p class="value">120 meseci</p>
							</div>
							<div>
								<p class="key">Datum otvaranja štednje:</p>
								<p class="value">30.05.2010.</p>
							</div>
							<div>
								<p class="key">Broj štednog računa:</p>
								<p class="value">80512000000234</p>
							</div>
							<div>
								<p class="key">Posljednja promena:</p>
								<p class="value rise change-rise" >1.200,00 RSD</p>
							</div>
							<div>
								<p class="key">Datum posljednje promene:</p>
								<p class="value">16.02.2017</p>
							</div>
							<div>
								<p class="key">Posljednja kamata:</p>
								<p class="value">1.200,00 RSD</p>
							</div>
							<div>
								<p class="key">Datum posljednje kamate:</p>
								<p class="value">16.02.2017.</p>
							</div>
						</div>

					</div>
					<a href="#" class="btn-2 color-1 alignright" data-method="panelDropTrigger">Prikaži više detalja</a>
				</div>
			</div>

		</div>

		<div class="widget widget-transaction-list payment-list content-white">
			<div class="widget-header header-dark">
				<div class="cnt-2">
					<div class="col-left">
						<h3 class="title-4 color-2"><span>Lista transakcija</span></h3>
					</div>
					<div class="col-right">
						<ul class="widget-actions bg-widget"   data-filter-node="#side-modal-account-transaction-filter" data-method="openSideModal">
							<p class="widget-name">Filter</p>
							<li>
								<div class="widget-options">
			            			<ul class="trigger" data-method="optionsTrigger">
			            				<li></li>
			            				<li></li>
			            				<li></li>
			            			</ul>
			            		</div>
							</li>
						</ul>
					</div>
				</div>
				<ul class="widget-tabs">
					<li class="active"><a href="#">Sve</a></li>
					<li><a href="#">Isplate</a></li>
					<li><a href="#">Uplate</a></li>
				</ul>
			</div>
			<div class="custom-table account-savings">

				<div class="table-header">
					<div class="table-head grid">
						<div class="col col-ls-3 col-ms-2 col-ss-6 ">
							<p class="left">Datum</p>
						</div>
						<div class="col col-ls-15 col-ms-6 col-ss-hidden">
							<p class="left">Opis transakcije</p>
						</div>
						<div class="col col-ls-5 ">
							<p class="right">Iznos</p>
						</div>
					</div>
				</div>

				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
					</div>
					<div class="col col-ls-15 col-ms-6 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">Uplata na štedni račun 8051000000234</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-rise col-text text-3 text-cl-3 right"><strong>25.250,00 RSD</strong></p>
					</div>
				</div>
				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
					</div>
					<div class="col col-ls-15 col-ms-6 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">Ugovorena kamata</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-rise col-text text-3 text-cl-3 right"><strong>1.200,00 RSD</strong></p>
					</div>
				</div>
				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
					</div>
					<div class="col col-ls-15 col-ms-6 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">Uplata na štedni račun 8051000000234</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-rise col-text text-3 text-cl-3 right"><strong>25.250,00 RSD</strong></p>
					</div>
				</div>
				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
					</div>
					<div class="col col-ls-15 col-ms-6 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">Ugovorena kamata</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-rise col-text text-3 text-cl-3 right"><strong>1.200,00 RSD</strong></p>
					</div>
				</div>
				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
					</div>
					<div class="col col-ls-15 col-ms-6 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">Uplata na štedni račun 8051000000234</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-rise col-text text-3 text-cl-3 right"><strong>25.250,00 RSD</strong></p>
					</div>
				</div>
				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
					</div>
					<div class="col col-ls-15 col-ms-6 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">Ugovorena kamata</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-rise col-text text-3 text-cl-3 right"><strong>1.200,00 RSD</strong></p>
					</div>
				</div>
				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
					</div>
					<div class="col col-ls-15 col-ms-6 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">Uplata na štedni račun 8051000000234</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-rise col-text text-3 text-cl-3 right"><strong>25.250,00 RSD</strong></p>
					</div>
				</div>
				<div class="table-row row-1 grid">
					<div class="col col-ls-3 col-ms-2 col-ss-6">
						<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
					</div>
					<div class="col col-ls-15 col-ms-6 col-ss-hidden">
						<p class="col-text text-2 text-cl-1 left">Ugovorena kamata</p>
					</div>
					<div class="col col-ls-5 col-ms-4 col-ss-6">
						<p class="change-rise col-text text-3 text-cl-3 right"><strong>1.200,00 RSD</strong></p>
					</div>
				</div>

				<div class="dropdown hidden-content">
					<div class="table-row row-1 grid">
						<div class="col col-ls-3 col-ms-2 col-ss-6">
							<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
						</div>
						<div class="col col-ls-15 col-ms-6 col-ss-hidden">
							<p class="col-text text-2 text-cl-1 left">Uplata na štedni račun 8051000000234</p>
						</div>
						<div class="col col-ls-5 col-ms-4 col-ss-6">
							<p class="change-rise col-text text-3 text-cl-3 right"><strong>25.250,00 RSD</strong></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-3 col-ms-2 col-ss-6">
							<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
						</div>
						<div class="col col-ls-15 col-ms-6 col-ss-hidden">
							<p class="col-text text-2 text-cl-1 left">Ugovorena kamata</p>
						</div>
						<div class="col col-ls-5 col-ms-4 col-ss-6">
							<p class="change-rise col-text text-3 text-cl-3 right"><strong>1.200,00 RSD</strong></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-3 col-ms-2 col-ss-6">
							<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
						</div>
						<div class="col col-ls-15 col-ms-6 col-ss-hidden">
							<p class="col-text text-2 text-cl-1 left">Uplata na štedni račun 8051000000234</p>
						</div>
						<div class="col col-ls-5 col-ms-4 col-ss-6">
							<p class="change-rise col-text text-3 text-cl-3 right"><strong>25.250,00 RSD</strong></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-3 col-ms-2 col-ss-6">
							<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
						</div>
						<div class="col col-ls-15 col-ms-6 col-ss-hidden">
							<p class="col-text text-2 text-cl-1 left">Ugovorena kamata</p>
						</div>
						<div class="col col-ls-5 col-ms-4 col-ss-6">
							<p class="change-rise col-text text-3 text-cl-3 right"><strong>1.200,00 RSD</strong></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-3 col-ms-2 col-ss-6">
							<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
						</div>
						<div class="col col-ls-15 col-ms-6 col-ss-hidden">
							<p class="col-text text-2 text-cl-1 left">Uplata na štedni račun 8051000000234</p>
						</div>
						<div class="col col-ls-5 col-ms-4 col-ss-6">
							<p class="change-rise col-text text-3 text-cl-3 right"><strong>25.250,00 RSD</strong></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-3 col-ms-2 col-ss-6">
							<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
						</div>
						<div class="col col-ls-15 col-ms-6 col-ss-hidden">
							<p class="col-text text-2 text-cl-1 left">Ugovorena kamata</p>
						</div>
						<div class="col col-ls-5 col-ms-4 col-ss-6">
							<p class="change-rise col-text text-3 text-cl-3 right"><strong>1.200,00 RSD</strong></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-3 col-ms-2 col-ss-6">
							<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
						</div>
						<div class="col col-ls-15 col-ms-6 col-ss-hidden">
							<p class="col-text text-2 text-cl-1 left">Uplata na štedni račun 8051000000234</p>
						</div>
						<div class="col col-ls-5 col-ms-4 col-ss-6">
							<p class="change-rise col-text text-3 text-cl-3 right"><strong>25.250,00 RSD</strong></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-3 col-ms-2 col-ss-6">
							<p class="col-text text-1 text-cl-1 left">16.09.2016.</p>
						</div>
						<div class="col col-ls-15 col-ms-6 col-ss-hidden">
							<p class="col-text text-2 text-cl-1 left">Ugovorena kamata</p>
						</div>
						<div class="col col-ls-5 col-ms-4 col-ss-6">
							<p class="change-rise col-text text-3 text-cl-3 right"><strong>1.200,00 RSD</strong></p>
						</div>
					</div>
				</div>
				

			</div>
			<div class="button-wrap">
				<a href="#" class="btn-1 color-1" data-method="showMore">Pogledaj sve transakcije</a>
			</div>
		</div>


	</div>

</div>

<?php Site::getFooter(); ?>
