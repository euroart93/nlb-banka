<?php 
Site::getHeader('header-login'); ?>

<div class="login-main">

	<!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
	<div class="page-cover inner-video video-cover img-overlay">
		<img class="desktop-img" src="img/bg/login-bg.jpg" alt="cover" data-object-fit="cover">
		<img class="mobile-img" src="img/bg/login-mobile.jpg" alt="cover" data-object-fit="cover">
	</div>

	<div class="login-content">
		<div class="col-right">
			<p class="title-1 color-1"><span>Sign up today</span></p>
			<h2>E-banking has never been easier</h2>
			<p>New generation of internet banking is here! seamlessly manage your money whenever and wherever you want, from whichever device you choose...</p>
			<a href="#" class="btn-1 color-3">Find out how</a>
		</div>
		<div class="col-left">
			<div class="login-box">
				<p class="login-error">Invalid username or password!</p>
				<h3>Ready to log in?</h3>
				<form action="#" method="get">
					<div class="group">
						<div class="group-inner">
							<label class="label-1">User name:</label>
							<input type="text" class="input-1">
						</div>
					</div>
					<div class="group">
						<div class="group-inner">
							<label class="label-1">Your password:</label>
							<input type="password" class="input-1">
						</div>
					</div>
					<p class="forgot-pass">Did you forget <a href="#">username</a> or <a href="#">password</a>?</p>
					<p class="hint"><img src="img/photos/norway.png" alt="hint norway">This is your hint image</p>
					<div class="group group-submit">
						<div class="group-inner">
							<button type="submit" class="btn-1 color-1">Log in</button>
							<a href="#" class="btn-1 color-2">Sign up for an account</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="login-footer">
		<div class="inner">
			<ul class="lang-nav">
				<li class="active"><a href="#">ENG</a></li>
				<li><a href="#">SRB</a></li>
			</ul>
			<ul class="social">
				<li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
				<li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
				<li class="twitter"><a href="#" class="icon-twitter">Twitter</a></li>
			</ul>
		</div>
	</div>


</div>


</div><!-- /Wrapper -->


