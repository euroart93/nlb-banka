<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="account-row profile-row account-bg-1">
		<div class="bg-overlay overlay-2"></div>
		<div class="">
			<div class="img-box">
				<img src="img/demo/profile-1.jpg" alt="profile-img">
			</div>
			<p>Anna Kendrick</p>
		</div>
	</div>

	<div class="main-content">
		<form action="#" method="get">

			<div class="widget content-white">
				<div class="widget-slat">
					<div class="col-left">
						<h2><i class="icon-settings-3"></i>Self Care</h2>
					</div>
					<div class="col-right">
						<div class="group-inner">
							<div class="select-3">
								<select  data-method="customSelect3">
	            					<option value="0">My profile</option>
	        						<option value="1">Authentication and Authorization</option>
	            					<option value="2">Applications</option>
	            					<option value="3">My documents</option>
	            					<option value="4">Notifications</option>
	            				</select>
	            			</div>
						</div>
					</div>
				</div>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-1 color-2 title-full"><span>Login information</span></h3>
						<p>Changes made to user account information have to be authorized with OTP.</p>
					</div>
					<div class="col-right">
						<div class="plain-list">
							<div>
								<p class="key">Last login:</p>
								<p class="value">15.05.2016. 22:34</p>
							</div>
							<div>
								<p class="key">Duration of login:</p>
								<p class="value">15 minutes</p>
							</div>
						</div>
					</div>
				</div>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-1 color-2 title-full"><span>User account information</span></h3>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<label class="label-1">User name:</label>
								<div class="group-inner">
									<input type="text" class="input-1" value="Akendrick99">
								</div>
							</div>
							<div class="group uk-width" style="width: 220px">
								<div class="group-inner">
									<button type="button" class="btn-1 color-1" data-method="openPopup" data-popup="19">Save username</button>
								</div>
							</div>
							<div class="divider-20"></div>
							<div class="group uk-width-1-1">
								<label class="label-1">Password:</label>
								<div class="group-inner">
									<input type="password" class="input-1" value="**********">
								</div>
							</div>
							<div class="group uk-width" style="width: 220px">
								<div class="group-inner">
									<button type="button" class="btn-1 color-2" data-method="openPopup" data-popup="5">Change password</button>
								</div>
							</div>
							<div class="divider-20"></div>
							<div class="group uk-width-1-1">
								<label class="label-1">hint image:</label>
								<div class="group-inner">
									<div class="img-flag">
										<img src="img/photos/norway.png" alt="flag">
									</div>
								</div>
							</div>
							<div class="group uk-width" style="width: 220px">
								<div class="group-inner">
									<button type="button" class="btn-1 color-2" data-method="openPopup" data-popup="4">Change hint image</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</form>
	</div>

</div>

<?php Site::getFooter(); ?>