<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li>Standarno plaćanje</li></a>
				<a href="#"><li>Interni transferi</li></a>
				<a href="#"><li>Devizno plaćanje</li></a>
				<a href="#"><li>Plaćanje računa</li></a>
				<a href="#"><li class="selected">Trajni nalozi</li></a>
				<a href="#"><li>Moji šabloni</li></a>
				<a href="#"><li>Pregled plaćanja</li></a>
			</ul>
		</div>
	</div>

	<div class="widget widget-select-account widget-content-white row" data-method="accountSelect">
		<div class="account-selected">
			<div class="col-ss-12 col-ls-2 acc-img">
				<img src="img/demo/acc-img-medium.jpg">
			</div>
			<div class="col-ss-12 col-ls-6 col-border">
				<div class="content">
					<p class="small">Ime računa:</p>
					<p class="big acc-name">Moj tekući račun</p>
				</div>
			</div>
			<div class="col-ss-12 col-ls-7 col-border">
				<div class="content">
					<p class="small">Broj računa:</p>
					<p class="big-s acc-number">115-0000000000567898-6</p>
				</div>
			</div>
			<div class="col-ss-12 col-ls-7 col-border">
				<div class="content">
					<p class="small float-right">Raspoloživo stanje:</p>
					<p class="big acc-amount float-right">5.123.543,89 RSD</p>
				</div>
			</div>
			<div class="col-ss-1">
				<a href="#" class="acc-list-btn acc-drop-btn" type="button" data-method="accountSelectTrigger">
					<div class="triangle"></div>
				</a>
			</div>
		</div>
		<div class="account-select">
  			<ul class="options">
    			 <li class="item">
    			 	<div class="widget row">
						<div class="col-ss-2 col-ms-2 col-ls-2">
							<div class="triangle-right-white"></div>
							<img class="desaturate" src="img/demo/acc-img-medium.jpg">
						</div>
						<div class="col-ss-8 col-ms-9 col-ls-6">
							<div class="content">
								<p class="acc-select-small">Ime računa:</p>
								<p class="big acc-name">Moj tekući račun</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small">Broj računa:</p>
								<p class="big-s acc-number">111-0000000000567898-6</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small float-right">Raspoloživo stanje:</p>
								<p class="big acc-amount float-right">5.123.543,89 RSD</p>
							</div>
						</div>
						<div class="col-ss-1 col-ms-1 col-ls-1  ">
							<div class="triangle-right"></div>
					</div>
    			 </li>
    			 <li class="item">
    			 	<div class="widget row">
						<div class="col-ss-2 col-ms-2 col-ls-2">
							<div class="triangle-right-white"></div>
							<img class="desaturate" src="img/demo/acc-img-medium-2.jpg">
						</div>
						<div class="col-ss-8 col-ms-9 col-ls-6">
							<div class="content">
								<p class="acc-select-small">Ime računa:</p>
								<p class="big acc-name">Drugi tekući račun</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small">Broj računa:</p>
								<p class="big-s acc-number">112-0000000000567898-6</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small float-right">Raspoloživo stanje:</p>
								<p class="big acc-amount float-right">1.850,89 RSD</p>
							</div>
						</div>
						<div class="col-ss-1 col-ms-1 col-ls-1  ">
							<div class="triangle-right"></div>
					</div>
    			 </li>
    			 <li class="item">
    			 	<div class="widget row">
						<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium-3.jpg">
						</div>
						<div class="col-ss-8 col-ms-9 col-ls-6">
							<div class="content">
								<p class="acc-select-small">Ime računa:</p>
								<p class="big acc-name">Treći tekući račun</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small">Broj računa:</p>
								<p class="big-s acc-number">113-0000000000567898-6</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small float-right">Raspoloživo stanje:</p>
								<p class="big acc-amount float-right">2.850,89 RSD</p>
							</div>
						</div>
						<div class="col-ss-1 col-ms-1 col-ls-1  ">
							<div class="triangle-right"></div>
					</div>
    			 </li>
    			 <li class="item">
    			 	<div class="widget row">
						<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium-4.jpg">
						</div>
						<div class="col-ss-8 col-ms-9 col-ls-6">
							<div class="content">
								<p class="acc-select-small">Ime računa:</p>
								<p class="big acc-name">Četvrti tekući račun</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small">Broj računa:</p>
								<p class="big-s acc-number">114-0000000000567898-6</p>
							</div>
						</div>
						<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
							<div class="content">
								<p class="acc-select-small float-right">Raspoloživo stanje:</p>
								<p class="big acc-amount float-right">3.850,89 RSD</p>
							</div>
						</div>
						<div class="col-ss-1 col-ms-1 col-ls-1  ">
							<div class="triangle-right"></div>
					</div>
    			 </li>
   				 
			</ul>
		</div>
	</div>

	<div class="main-content">

	
		<form action="#" method="get">

			<div class="widget content-white left-padding payment-widget payment-steps standing-order-inner-transfer-1">
				<h3 class="title-4 color-2"><span>Novi trajni nalog interni transfer</span></h3>
				<ul class="steps left">
					<li class="active">1 Detalji trajnog naloga</li>
					<li>2 <span>Payment review</span></li>
				</ul>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Naziv trajnog naloga</span></h3>
						<p>Definišite naziv za svoj novi trajni nalog.</p>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<div class="group-inner">
									<label class="label-1">Naziv:</label>
									<input type="text" class="input-1">
								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Podaci o trajnom nalogu</span></h3>
						<p>Unesite osnovne podatke o trajnom nalogu.</p>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<div class="group uk-width-1-1">
									<div class="group-inner">
										<label class="label-1">Račun primaoca:</label>
										<div class="select-3">
											<select data-method="customSelect3">
				            					<option value="0">160-0000000000602-16</option>
				            					<option value="1">161-0000000000602-17</option>
				            					<option value="2">162-0000000000602-18</option>
				            					<option value="3">163-0000000000602-19</option>
				            				</select>
				            			</div>
									</div>
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner input-select">
									<label class="label-1">Iznos:</label>
									<input type="text" class="input-1" value="200,00" data-method="valueInput">
      									<div class="select-3">
      										<select data-method="customSelect3">
      			            					<option value="0">RSD</option>
      			            					<option value="1">EUR</option>
      			            					<option value="2">USD</option>
      			            					<option value="3">GBP</option>
      			            				</select>
      			            			</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			
				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Učestalost trajnog naloga</span></h3>
						<p>Odredite početni i završni datum trajnog naloga i učestalost naplate.</p>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="custom-form uk-grid">
								<div class="group uk-width-1-2">
									<label class="label-1">Početni datum:</label>
									<div class="group-inner date-wrap">
										<input type="text" class="input-1 dark date-input" value="01/05/2017" data-method="dateInput">
									</div>
								</div>
								<div class="group uk-width-1-2">
									<label class="label-1">Datum završetka:</label>
									<div class="group-inner date-wrap">
										<input type="text" class="input-1 dark date-input" value="01/09/2017" data-method="dateInput">
									</div>
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner">
									<label class="label-1">Period:</label>
									<div class="select-3">
										<select data-method="customSelect3">
			            					<option value="0">Mesečni</option>
			            					<option value="1">Sedmični</option>
			            					<option value="2">Dnevni</option>
			            				</select>
			            			</div>
								</div>
							</div>
							<div class="divider-30"></div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<button type="button" class="btn-1 color-3">Odustani</button>
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<a href="<?php echo Site::url('/') ?>" class="btn-1 color-1">Nastavi</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</form>
	</div>

</div>

<?php Site::getFooter(); ?>