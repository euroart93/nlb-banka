<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="account-row account-bg-3">
		<div class="bg-overlay overlay-2"></div>
		<div class="account-small-slider" data-method="accountSlider2">
	        <!-- Slides -->
            <div class="slide">
            	<div class="account-box">
					<div class="card-logo">
						<img src="<?php echo Site::url() ?>/img/photos/mastercard.png" alt="Card logo">
					</div>
            		<p class="acc-name">My Current Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-1" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<div class="card-logo">
						<img src="<?php echo Site::url() ?>/img/photos/mastercard.png" alt="Card logo">
					</div>
            		<p class="acc-name">My Second Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-2" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<div class="card-logo">
						<img src="<?php echo Site::url() ?>/img/photos/visa.png" alt="Card logo">
					</div>
            		<p class="acc-name">My Third Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-3" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<div class="card-logo">
						<img src="<?php echo Site::url() ?>/img/photos/visa.png" alt="Card logo">
					</div>
            		<p class="acc-name">My Fourth Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-4" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
	        </div>
	    </div>
	    <div class="slider-button-prev"></div>
		<div class="slider-button-next"></div>
	</div>

	<div class="main-content">
		<div class="widget content-white payment-widget">

		<div class="widget-slat">
			<div class="col-left">
				<h2 class="full-width"><i class="icon-card-4"></i>Card details</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque 
				labore laborum at alias quisquam, unde praesentium autem nam! Enim 
				dolores, perspiciatis assumenda a vero molestias fuga mollitia dolore 
				nesciunt cumque.</p>
			</div>
			<div class="col-right">
				<div class="links-list">
					<a href="#" data-method="openPopup" data-popup="7">Activate Card</a>
					<a href="#" data-method="openPopup" data-popup="10">Block Card</a>
					<a href="#" data-method="openPopup" data-popup="13">Unblock Card</a>
					<a href="#" data-method="openPopup" data-popup="15">Change PIN</a>
				</div>
			</div>
		</div>

	</div>

</div>

<?php Site::getFooter(); ?>