<?php 
Site::getHeader('header-login'); ?>

<div class="login-main">

  <!-- <div class="page-cover inner-video video-cover img-overlay" style="background-image: url(img/bg/login-bg.jpg)"> -->
  <div class="page-cover inner-video video-cover img-overlay">
    <img class="desktop-img" src="img/bg/login-bg.jpg" alt="cover" data-object-fit="cover">
    <img class="mobile-img" src="img/bg/login-mobile.jpg" alt="cover" data-object-fit="cover">
  </div>

  <div class="prelogin-full sign-up-steps">
    <div class="inner">
      <div class="col-head">
        <a href="#" class="close">Close</a>
      </div>
      <div class="col-left">
        <h2>And yes! We are...</h2>
      </div>
      <div class="col-right">
        <h3 class="title-done"><i class="checkmark-icon"></i>Done</h3>
        <form action="#" method="#">
          <div class="scrollable-content" data-method="customScroll">

              <div class="sign-up-data-check">

                <div class="half">
                  <div class="data-control">
                    <p class="key">Account number:</p>
                    <p class="value">115-0000000000562828-66</p>
                  </div>
                  <div class="data-control">
                    <p class="key">Username:</p>
                    <p class="value">jurica.vukovic</p>
                  </div>
                  <div class="data-control">
                    <p class="key">Mobile phone:</p>
                    <p class="value">+38164123456789</p>
                  </div>
                  <div class="data-control">
                    <p class="key">E-mail:</p>
                    <p class="value">jurica.vukovic@domain.com</p>
                  </div>
                  <div class="download-documents">
                    <a href="#" class="download-link-1">Download contratct option <i class="download-icon"></i></a>
                    <a href="#" class="download-link-1">Download other documents option <i class="download-icon"></i></a>
                    <a href="#" class="download-link-1">Download fees and commissions document option option <i class="download-icon"></i></a>
                  </div>
                </div>
                
                <div class="half">
                  <p>To comlpete the registration process, please go to the nearest shop with your ID</p>
                  <div class="map-wrapper-signup">
                    <div class="infobox-wrapper">
                      <div id="infobox1" class="infobox bg-atm">
                            <p class="loc-title">Bank</p>
                            <p class="loc-info">Jurija Gagarina 12<br>
                              Beograd
                            </p>
                        </div>
                    </div>
                    <div id="map-canvas-signup" data-method="googleMapSignUp"></div>
                  </div>
                </div>

              </div>

          </div>

          <div class="button-actions">
            <a href="#" class="btn-1 color-1 fl-r">Continue</a>
          </div>

        </form>
      </div>
    </div>
  </div>

  <div class="login-footer">
    <div class="inner">
      <ul class="lang-nav">
        <li class="active"><a href="#">ENG</a></li>
        <li><a href="#">SRB</a></li>
      </ul>
      <ul class="social">
        <li class="linkedin"><a href="#" class="icon-linkedin">Linkedin</a></li>
        <li class="facebook"><a href="#" class="icon-facebook">Facebook</a></li>
        <li class="twitter"><a href="#" class="icon-twitter">Twitter</a></li>
      </ul>
    </div>
  </div>


</div>


</div><!-- /Wrapper -->


