<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li>Standarno plaćanje</li></a>
				<a href="#"><li>Interni transferi</li></a>
				<a href="#"><li class="selected">Devizno plaćanje</li></a>
				<a href="#"><li>Moji šabloni</li></a>
				<a href="#"><li>Pregled plaćanja</li></a>
			</ul>
		</div>
	</div>

	<div class="widget widget-select-account widget-content-white row" data-method="accountSelect">
			<div class="account-selected">
				<div class="col-ss-12 col-ls-2 acc-img">
					<img src="img/demo/acc-img-medium.jpg">
				</div>
				<div class="col-ss-12 col-ls-6 col-border">
					<div class="content">
						<p class="small">Ime računa:</p>
						<p class="big acc-name">Moj tekući račun</p>
					</div>
				</div>
				<div class="col-ss-12 col-ls-7 col-border">
					<div class="content">
						<p class="small">Broj računa:</p>
						<p class="big-s acc-number">115-0000000000567898-6</p>
					</div>
				</div>
				<div class="col-ss-12 col-ls-7 col-border">
					<div class="content">
						<p class="small float-right">Raspoloživo stanje:</p>
						<p class="big acc-amount float-right">5.123.543,89 RSD</p>
					</div>
				</div>
				<div class="col-ss-1">
					<a href="#" class="acc-list-btn acc-drop-btn" type="button" data-method="accountSelectTrigger">
						<div class="triangle"></div>
					</a>
				</div>
			</div>
			<div class="account-select">
      			<ul class="options">
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime računa:</p>
									<p class="big acc-name">Moj tekući račun</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj računa:</p>
									<p class="big-s acc-number">111-0000000000567898-6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">5.123.543,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium-2.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime računa:</p>
									<p class="big acc-name">Drugi tekući račun</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj računa:</p>
									<p class="big-s acc-number">112-0000000000567898-6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">1.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
									<div class="triangle-right-white"></div>
									<img class="desaturate" src="img/demo/acc-img-medium-3.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime računa:</p>
									<p class="big acc-name">Treći tekući račun</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj računa:</p>
									<p class="big-s acc-number">113-0000000000567898-6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">2.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
									<div class="triangle-right-white"></div>
									<img class="desaturate" src="img/demo/acc-img-medium-4.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime računa:</p>
									<p class="big acc-name">Četvrti tekući račun</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj računa:</p>
									<p class="big-s acc-number">114-0000000000567898-6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">3.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
       				 
    			</ul>
			</div>
	</div>

	<div class="main-content">

		<form action="#" method="get">

			<div class="widget content-white left-padding payment-widget payment-steps foreign-p">
				<h3 class="title-4 color-2"><span>Devizno plaćanje</span></h3>
				<ul class="steps left">
					<li class="active">1 Nalog za plaćanje</li>
					<li>2 <span>Payment review</span></li>
					<li>3 <span>Payment confirmation</span></li>
				</ul>
				<div class="widget-slat tab">
					<div class="col-right">
						<ul class="widget-tabs">
							<li class="active"><a href="#">Plaćanje u Srbiji</a></li>
							<li><a href="#">Plaćanje ka inostranstvu</a></li>
						</ul>
					</div>
				</div>
				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Podaci o platiocu</span></h3>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<div class="group-inner input">
									<label class="label-1">Naziv platioca:</label>
									<input type="text" class="input-1" value="Petar Petrović">
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner ">
									<label class="label-1">Adresa:</label>
									<input type="text" class="input-1" value="Julija Gagarina 98">
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner uk-width-1-1">
									<label class="label-1">Mesto:</label>
									<input type="text" class="input-1" value="Beograd">
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Podaci o primaocu</span></h3>
						<p>Unesite podatke o primaocu i odaberite banku primaoca.</p>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<div class="group-inner input-icon-wrap">
									<label class="label-1">Naziv primaoca:</label>
									<input type="text" class="input-1" value="Telekom Srbija">
									<a href="#" class="input-icon icon-payment"  data-method="openSideModal" data-filter-node="#side-modal-payment-recivers-profiles"></a>
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner ">
									<label class="label-1">Država:</label>
									<div class="select-3">
										<select data-method="customSelect3" data-dkcacheid="0">
			            					<option value="0">Velika Britanija</option>
			            					<option value="1">Njemačka</option>
			            					<option value="2">Hrvatska</option>
			            					<option value="3">Mađarska</option>
			            				</select>
	            					</div>
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner ">
									<label class="label-1">Adresa:</label>
									<input type="text" class="input-1" value="1 sir Matt Busby Way">
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner uk-width-1-1">
									<label class="label-1">Mesto:</label>
									<input type="text" class="input-1" value="Manchester">
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner">
									<label class="label-1">Račun primaoca:</label>
									<input type="text" class="input-1" value="160-0000000000602-16">
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner ">
									<label class="label-1">SWIFT:</label>
									<input type="text" class="input-1" value="MIDLGB822">
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner uk-width-1-1">
									<label class="label-1">Banka primaoca:</label>
									<input type="text" class="input-1" disabled value="Barclays">
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner uk-width-1-1">
									<label class="label-1">Adresa:</label>
									<input type="text" class="input-1" disabled value="8 Canada Square, 13 E04 01,London">
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner uk-width-1-1">
									<label class="label-1">Država</label>
									<input type="text" class="input-1" disabled value="Velika Britanija">
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Detalji plaćanja</span></h3>
						<p>Unesite detalje plaćanja: iznos,šifru i svrhu plaćanja.</p>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1 input-value">
								<div class="group-inner">
									<label class="label-1">Iznos:</label>
									<input type="text" class="input-1" value="850.00">
								</div>
							</div>
							<div class="group uk-width-1-3 input-currency">
								<div class="group-inner ">
									<label class="label-1">Valuta:</label>
									<div class="select-3">
										<select data-method="customSelect3" data-dkcacheid="0">
			            					<option value="0">EUR</option>
			            					<option value="1">RSD</option>
			            					<option value="2">KN</option>
			            					<option value="3">USD</option>
			            				</select>
	            					</div>
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner ">
									<label class="label-1">Šifra plaćanja:</label>
									<div class="select-3">
										<select data-method="customSelect3" data-dkcacheid="0">
			            					<option value="0">289 - Plaćanje telekomunikacijskih usluga</option>
			            					<option value="1">290 - Plaćanje telekomunikacijskih usluga</option>
			            					<option value="2">291 - Plaćanje telekomunikacijskih usluga</option>
			            					<option value="3">292 - Plaćanje telekomunikacijskih usluga</option>
			            				</select>
	            					</div>
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner uk-width-1-1">
									<label class="label-1">Svrha plaćanja:</label>
									<input type="text" class="input-1" value="Payment for services">
								</div>
							</div>
							<label class="label-1 radio-l">Troškovi plaćanja:</label>
							<div class="custom-form uk-grid left">
								<div class="group uk-width-1-1">
									<div class="group-inner radio-wrap">
										<input type="radio" checked class="radio-2 checked" name="radio" data-method="customRadio1">
										<label class="radio-label"><h3>OUR</h3> <p>nalogodavac plaća sve troškove transakcije</p></label>
									</div>
									<div class="group-inner radio-wrap">
										<input type="radio" class="radio-2" name="radio" data-method="customRadio1">
										<label class="radio-label"><h3>SHA</h3> <p>nalogodavac plaća troškove samo sa strane svoje banke</p></label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		
				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Dokumenti:</span></h3>
						<p>Priložite navedenu neophodnu dokumentaciju. Ovaj je korak obavezan. Dokumentaciju možete priložiti u pdf, doc(x) i jpeg formatu.</p>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="plain-list width-m">
								<div class="border">
									<p class="key">Ime dokumenta 1 (primjer bez uploadanog dokumenta)</p>
									<div class="file-upload">
									    <span>Upload</span>
									    <input type="file" class="upload">
									</div>
								</div>
								<div>
									<p class="key">Ime dokumenta 2 (primjer s uploadanim dokumentom)</p>
									<div class="file-upload">
									    <span>Upload</span>
									    <input type="file" class="upload">
									</div>
									<div class="uploaded-docs file-completed">
										<div class="doc-wrapper">
											<p class="doc">C:\Documents...\e-mail.docx</p>
											<a href="#" class="delete">Obriši</a>
										</div>
									</div>
								</div>
							</div>
						
							<div class="divider-30"></div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<button type="button" class="btn-1 color-3">Snimi kao šablon</button>
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<a href="<?php echo Site::url('/foreign-payment-step-2') ?>" class="btn-1 color-1">Nastavi</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</form>
	</div>

</div>
<script type="text/javascript">
	$(document).ready(
    function(){
        $(".notification-trigger").click(function () {
            $("#warrning").toggle();
        });

    });
</script>
<?php Site::getFooter(); ?>