<?php
Site::getHeader(); ?>

<div id="main">

	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li class="selected">Detalji računa</li></a>
				<a href="#"><li>Lista transakcija</li></a>
				<a href="#"><li>Lista izvoda</li></a>
			</ul>
		</div>
	</div>


	<div class="main-content">

		<div class="widget bill-p-new content-white">
			<div class="widget-header header-dark">
				<div class="cnt-2">
					<div class="col-left">
						<h3 class="title-4 color-2"><span>Plaćanje računa</span></h3>
					</div>
					<div class="col-right">
						<a href="#" class="btn-1 color-1" data-method="openSideModal" data-filter-node="#side-modal-bill-subscription">Nova pretplata</a>
					</div>
				</div>
			</div>
			<div class="custom-table">

				<div class="table-header">
					<div class="table-head grid">
						<div class="col col-ls-6 col-ms-4 col-ss-12">
							<p class="left">Naziv primaoca</p>
						</div>
						<div class="col col-ls-18 col-ms-8 col-ss-hidden">
							<p class="left">Adresa primaoca</p>
						</div>
					</div>
				</div>

				<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-bill-subscription-confirmation">
					<div class="col col-ls-6 col-ms-4 col-ss-12">
						<p class="col-text text-1 text-cl-1 left">Elektrodistribucija Beograd</p>
					</div>
					<div class="col col-ls-18 col-ms-8 col-ss-12">
						<p class="col-text text-2 text-cl-1 left">Takovska 142, 11000 Beograd</p>
						<a href="#" class="btn-1 color-1">otkaži</a>
					</div>
				</div>

				<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-bill-details">
					<div class="col col-ls-6 col-ms-4 col-ss-12">
						<p class="col-text text-1 text-cl-1 left">Elektrodistribucija Beograd</p>
					</div>
					<div class="col col-ls-18 col-ms-8 col-ss-12">
						<p class="col-text text-2 text-cl-1 left">Takovska 142, 11000 Beograd</p>
						<a href="#" class="btn-1 color-1">otkaži</a>
					</div>
				</div>

				<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-bill-details">
					<div class="col col-ls-6 col-ms-4 col-ss-12">
						<p class="col-text text-1 text-cl-1 left">Elektrodistribucija Beograd</p>
					</div>
					<div class="col col-ls-18 col-ms-8 col-ss-12">
						<p class="col-text text-2 text-cl-1 left">Takovska 142, 11000 Beograd</p>
						<a href="#" class="btn-1 color-1">otkaži</a>
					</div>
				</div>

				<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-bill-details">
					<div class="col col-ls-6 col-ms-4 col-ss-12">
						<p class="col-text text-1 text-cl-1 left">Elektrodistribucija Beograd</p>
					</div>
					<div class="col col-ls-18 col-ms-8 col-ss-12">
						<p class="col-text text-2 text-cl-1 left">Takovska 142, 11000 Beograd</p>
						<a href="#" class="btn-1 color-1">otkaži</a>
					</div>
				</div>

				<div class="hidden-content">
					<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-bill-details">
						<div class="col col-ls-6 col-ms-4 col-ss-12">
							<p class="col-text text-1 text-cl-1 left">Elektrodistribucija Beograd</p>
						</div>
						<div class="col col-ls-18 col-ms-8 col-ss-12">
							<p class="col-text text-2 text-cl-1 left">Takovska 142, 11000 Beograd</p>
							<a href="#" class="btn-1 color-1">otkaži</a>
						</div>
					</div>

					<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-bill-details">
						<div class="col col-ls-6 col-ms-4 col-ss-12">
							<p class="col-text text-1 text-cl-1 left">Elektrodistribucija Beograd</p>
						</div>
						<div class="col col-ls-18 col-ms-8 col-ss-12">
							<p class="col-text text-2 text-cl-1 left">Takovska 142, 11000 Beograd</p>
							<a href="#" class="btn-1 color-1">otkaži</a>
						</div>
					</div>

					<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-bill-details">
						<div class="col col-ls-6 col-ms-4 col-ss-12">
							<p class="col-text text-1 text-cl-1 left">Elektrodistribucija Beograd</p>
						</div>
						<div class="col col-ls-18 col-ms-8 col-ss-12">
							<p class="col-text text-2 text-cl-1 left">Takovska 142, 11000 Beograd</p>
							<a href="#" class="btn-1 color-1">otkaži</a>
						</div>
					</div>

					<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-bill-details">
						<div class="col col-ls-6 col-ms-4 col-ss-12">
							<p class="col-text text-1 text-cl-1 left">Elektrodistribucija Beograd</p>
						</div>
						<div class="col col-ls-18 col-ms-8 col-ss-12">
							<p class="col-text text-2 text-cl-1 left">Takovska 142, 11000 Beograd</p>
							<a href="#" class="btn-1 color-1">otkaži</a>
						</div>
					</div>

					<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-bill-details">
						<div class="col col-ls-6 col-ms-4 col-ss-12">
							<p class="col-text text-1 text-cl-1 left">Elektrodistribucija Beograd</p>
						</div>
						<div class="col col-ls-18 col-ms-8 col-ss-12">
							<p class="col-text text-2 text-cl-1 left">Takovska 142, 11000 Beograd</p>
							<a href="#" class="btn-1 color-1">otkaži</a>
						</div>
					</div>
					<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-bill-details">
						<div class="col col-ls-6 col-ms-4 col-ss-12">
							<p class="col-text text-1 text-cl-1 left">Elektrodistribucija Beograd</p>
						</div>
						<div class="col col-ls-18 col-ms-8 col-ss-12">
							<p class="col-text text-2 text-cl-1 left">Takovska 142, 11000 Beograd</p>
							<a href="#" class="btn-1 color-1">otkaži</a>
						</div>
					</div>

					<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-bill-details">
						<div class="col col-ls-6 col-ms-4 col-ss-12">
							<p class="col-text text-1 text-cl-1 left">Elektrodistribucija Beograd</p>
						</div>
						<div class="col col-ls-18 col-ms-8 col-ss-12">
							<p class="col-text text-2 text-cl-1 left">Takovska 142, 11000 Beograd</p>
							<a href="#" class="btn-1 color-1">otkaži</a>
						</div>
					</div>

					<div class="table-row row-1 grid" data-method="openSideModal" data-filter-node="#side-modal-bill-details">
						<div class="col col-ls-6 col-ms-4 col-ss-12">
							<p class="col-text text-1 text-cl-1 left">Elektrodistribucija Beograd</p>
						</div>
						<div class="col col-ls-18 col-ms-8 col-ss-12">
							<p class="col-text text-2 text-cl-1 left">Takovska 142, 11000 Beograd</p>
							<a href="#" class="btn-1 color-1">otkaži</a>
						</div>
					</div>
				</div>

			</div>
			<a href="#" class="btn-2 color-1 alignright" data-method="showMore">Prikaži još</a>
		</div>


	</div>

</div>

<?php Site::getFooter(); ?>
