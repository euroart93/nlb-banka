<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="account-row profile-row account-bg-1">
		<div class="bg-overlay overlay-2"></div>
		<div class="">
			<div class="img-box">
				<img src="img/demo/profile-1.jpg" alt="profile-img">
			</div>
			<p>Anna Kendrick</p>
		</div>
	</div>

	<div class="main-content">
		
		<div class="widget content-white">
			<div class="widget-slat">
				<div class="col-left">
					<h2><i class="icon-settings-3"></i>Self Care</h2>
				</div>
				<div class="col-right">
					<div class="group-inner">
						<div class="select-3">
							<select  data-method="customSelect3">
            					<option value="0">My profile</option>
        						<option value="1">Authentication and Authorization</option>
            					<option value="2">Applications</option>
            					<option value="3">My documents</option>
            					<option value="4">Notifications</option>
            				</select>
            			</div>
					</div>
				</div>
			</div>
			
			<div class="widget-slat">
				<div class="col-left">
					<h3 class="title-1 color-2 title-full"><span>Your applications</span></h3>
					<p>Changes made to user account inforation have to be authorized with OTP.</p>
				</div>
				<div class="col-right">
					<div class="application-wrapper">
						<div class="application-head -border-top">
							<h3>Mastercard credit card application</h3>
							<p>Status: Pending approval</p>
							<div class="widget-options">
		            			<ul class="trigger color-1" data-method="optionsTrigger">
		            				<li></li>
		            				<li></li>
		            				<li></li>
		            			</ul>
		            			<ul class="options-list color-1 options-list">
		            				<li><a href="#">Option 1</a></li>
		            				<li><a href="#">Option 2</a></li>
		            				<li><a href="#">Option 3</a></li>
		            				<li><a href="#">Option 4</a></li>
		            			</ul>
		            		</div>
						</div>
						<div class="links-list">
							<a href="#">Document overview</a>
							<a href="#">Cancel the application</a>
						</div>
					</div>

					<div class="application-wrapper">
						<div class="application-head">
							<h3>Standard cash credit application WI-34983475</h3>
							<p>Status: Pending approval</p>
							<div class="widget-options">
		            			<ul class="trigger color-1" data-method="optionsTrigger">
		            				<li></li>
		            				<li></li>
		            				<li></li>
		            			</ul>
		            			<ul class="options-list color-1 options-list">
		            				<li><a href="#">Option 1</a></li>
		            				<li><a href="#">Option 2</a></li>
		            				<li><a href="#">Option 3</a></li>
		            				<li><a href="#">Option 4</a></li>
		            			</ul>
		            		</div>
						</div>
						<div class="links-list">
							<a href="#">Accept the chosen terms</a>
							<a href="#">Document overview</a>
							<a href="#">Cancel the application</a>
						</div>
					</div>

					<div class="application-wrapper">
						<div class="application-head">
							<h3>Saving account application</h3>
							<p>Status: Document editing requierd</p>
							<div class="widget-options">
		            			<ul class="trigger color-1" data-method="optionsTrigger">
		            				<li></li>
		            				<li></li>
		            				<li></li>
		            			</ul>
		            			<ul class="options-list color-1 options-list">
		            				<li><a href="#">Option 1</a></li>
		            				<li><a href="#">Option 2</a></li>
		            				<li><a href="#">Option 3</a></li>
		            				<li><a href="#">Option 4</a></li>
		            			</ul>
		            		</div>
						</div>
						<div class="links-list">
							<a href="#" data-method="openPopup" data-popup="20">Edit documents</a>
							<a href="#">Cancel the application</a>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>

<?php Site::getFooter(); ?>