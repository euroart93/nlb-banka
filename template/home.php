<?php 
Site::addBodyClass('nestodrugo');
Site::getHeader(); ?>

<!-- Promo -->
<div id="promo">
	<div class="container">
		
		<!-- Promo content -->
		
	</div>
</div>
<!-- /Promo -->

<!-- Main -->
<div id="main">
	<div class="container">

		<!-- Main content -->
		<div class="main-content">

			<article>
				<h1>Main Content</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
			</article>

		</div>
		<!-- /Main content -->
		
	</div>
</div>
<!-- /Main -->

<?php Site::getFooter(); ?>