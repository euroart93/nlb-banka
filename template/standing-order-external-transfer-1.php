<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="submenu">
		<div class="container">
			<ul>
				<a href="#"><li>Standarno plaćanje</li></a>
				<a href="#"><li>Interni transferi</li></a>
				<a href="#"><li>Devizno plaćanje</li></a>
				<a href="#"><li>Plaćanje računa</li></a>
				<a href="#"><li class="selected">Trajni nalozi</li></a>
				<a href="#"><li>Moji šabloni</li></a>
				<a href="#"><li>Pregled plaćanja</li></a>
			</ul>
		</div>
	</div>

	<div class="widget widget-select-account widget-content-white row" data-method="accountSelect">
			<div class="account-selected">
				<div class="col-ss-12 col-ls-2 acc-img">
					<img src="img/demo/acc-img-medium.jpg">
				</div>
				<div class="col-ss-12 col-ls-6 col-border">
					<div class="content">
						<p class="small">Ime računa:</p>
						<p class="big acc-name">Moj tekući račun</p>
					</div>
				</div>
				<div class="col-ss-12 col-ls-7 col-border">
					<div class="content">
						<p class="small">Broj računa:</p>
						<p class="big-s acc-number">115-0000000000567898-6</p>
					</div>
				</div>
				<div class="col-ss-12 col-ls-7 col-border">
					<div class="content">
						<p class="small float-right">Raspoloživo stanje:</p>
						<p class="big acc-amount float-right">5.123.543,89 RSD</p>
					</div>
				</div>
				<div class="col-ss-1">
					<a href="#" class="acc-list-btn acc-drop-btn" type="button" data-method="accountSelectTrigger">
						<div class="triangle"></div>
					</a>
				</div>
			</div>
			<div class="account-select">
      			<ul class="options">
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime računa:</p>
									<p class="big acc-name">Moj tekući račun</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj računa:</p>
									<p class="big-s acc-number">111-0000000000567898-6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">5.123.543,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
								<div class="triangle-right-white"></div>
								<img class="desaturate" src="img/demo/acc-img-medium-2.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime računa:</p>
									<p class="big acc-name">Drugi tekući račun</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj računa:</p>
									<p class="big-s acc-number">112-0000000000567898-6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">1.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
									<div class="triangle-right-white"></div>
									<img class="desaturate" src="img/demo/acc-img-medium-3.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime računa:</p>
									<p class="big acc-name">Treći tekući račun</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj računa:</p>
									<p class="big-s acc-number">113-0000000000567898-6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">2.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
        			 <li class="item">
        			 	<div class="widget row">
							<div class="col-ss-2 col-ms-2 col-ls-2">
									<div class="triangle-right-white"></div>
									<img class="desaturate" src="img/demo/acc-img-medium-4.jpg">
							</div>
							<div class="col-ss-8 col-ms-9 col-ls-6">
								<div class="content">
									<p class="acc-select-small">Ime računa:</p>
									<p class="big acc-name">Četvrti tekući račun</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small">Broj računa:</p>
									<p class="big-s acc-number">114-0000000000567898-6</p>
								</div>
							</div>
							<div class="col-ss-hidden col-ms-hidden col-ls-7 acc-select-border">
								<div class="content">
									<p class="acc-select-small float-right">Raspoloživo stanje:</p>
									<p class="big acc-amount float-right">3.850,89 RSD</p>
								</div>
							</div>
							<div class="col-ss-1 col-ms-1 col-ls-1  ">
								<div class="triangle-right"></div>
						</div>
        			 </li>
       				 
    			</ul>
			</div>
	</div>

	<div class="main-content">

		<form action="#" method="get">

			<div class="widget content-white left-padding payment-widget payment-steps foreign-p">
				<h3 class="title-4 color-2"><span>Novi eksterni trajni nalog</span></h3>
				<ul class="steps left">
					<li class="active">1 Detalji trajnog naloga</li>
					<li>2 <span>Payment review</span></li>
				</ul>
				
				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Naziv trajnog naloga</span></h3>
						<p>Definišite naziv za svoj novi trajni nalog</p>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<div class="group-inner input">
									<label class="label-1">Naziv:</label>
									<input type="text" class="input-1" value="Telefonski račun">
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Podaci o platiocu</span></h3>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<div class="group-inner">
									<label class="label-1">Naziv platioca:</label>
									<input type="text" class="input-1" value="Petar Perković">
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<label class="label-1">Adresa:</label>
									<input type="text" class="input-1" value="Jurija Gagarina 98">
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<label class="label-1">Mesto:</label>
									<input type="text" class="input-1" value="Beograd">
									
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Podaci o primaocu</span></h3>
						<p>Unesite podatke o primaocu</p>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<div class="group-inner input-icon-wrap">
									<label class="label-1">Naziv primaoca:</label>
									<input type="text" class="input-1" value="Telekom Srbija">
									<a href="#" class="input-icon icon-payment"  data-method="openSideModal" data-filter-node="#side-modal-payment-recivers-profiles"></a>
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<label class="label-1">Adresa:</label>
									<input type="text" class="input-1" value="Takovska 132">
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<label class="label-1">Mesto:</label>
									<input type="text" class="input-1" value="Beograd">
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner ">
									<label class="label-1">Račun primaoca:</label>
									<input type="text" class="input-1" value="160-000000000602-16">
								</div>
							</div>
							<div class="group uk-width-1-3">
								<div class="group-inner uk-width-1-1">
									<label class="label-1">Model odobrenja:</label>
									<input type="text" class="input-1" value="00">
								</div>
							</div>
							<div class="group uk-width-2-3">
								<div class="group-inner uk-width-1-1">
									<label class="label-1">Poziv na broj odobrenja</label>
									<input type="text" class="input-1" value="3212546-16062016-747">
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Detalji plaćanja</span></h3>
						<p>Unesite detalje plaćanja: iznos,šifru i svrhu plaćanja.</p>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<label class="label-1">Iznos:</label>
									<input type="text" class="input-1" value="5.000,00 RSD">
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner ">
									<label class="label-1">Šifra plaćanja:</label>
									<div class="select-3">
										<select data-method="customSelect3" data-dkcacheid="0">
			            					<option value="0">289 - Plaćanje telekomunikacijskih usluga</option>
			            					<option value="1">290 - Plaćanje telekomunikacijskih usluga</option>
			            					<option value="2">291 - Plaćanje telekomunikacijskih usluga</option>
			            					<option value="3">292 - Plaćanje telekomunikacijskih usluga</option>
			            				</select>
	            					</div>
								</div>
							</div>
							<div class="group uk-width-1-1">
								<div class="group-inner uk-width-1-1">
									<label class="label-1">Svrha plaćanja:</label>
									<input type="text" class="input-1" value="Račun za telekom usluge za 01//2017">
								</div>
							</div>
						</div>
					</div>
				</div>
		
				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-5"><span>Učestalost trajnog naloga</span></h3>
						<p>Odredite početni i završni datum trajnog naloga i učestalost naplate.</p>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="custom-form uk-grid">
								<div class="group uk-width-1-2">
									<label class="label-1">Početni datum:</label>
									<div class="group-inner date-wrap">
										<input type="text" class="input-1 dark date-input" value="01/05/2017" data-method="dateInput">
									</div>
								</div>
								<div class="group uk-width-1-2">
									<label class="label-1">Datum završetka:</label>
									<div class="group-inner date-wrap">
										<input type="text" class="input-1 dark date-input" value="01/09/2017" data-method="dateInput">
									</div>
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<label class="label-1">Period:</label>
									<div class="select-3">
										<select data-method="customSelect3">
			            					<option value="0">Mesečni</option>
			            					<option value="1">Sedmični</option>
			            					<option value="2">Dnevni</option>
			            				</select>
			            			</div>
								</div>
							</div>
							<div class="divider-30"></div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<button type="button" class="btn-1 color-3">Odustani</button>
								</div>
							</div>
							<div class="group uk-width-1-2">
								<div class="group-inner">
									<a href="<?php echo Site::url('/') ?>" class="btn-1 color-1">Nastavi</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</form>
	</div>

</div>
<script type="text/javascript">
	$(document).ready(
    function(){
        $(".notification-trigger").click(function () {
            $("#warrning").toggle();
        });

    });
</script>
<?php Site::getFooter(); ?>