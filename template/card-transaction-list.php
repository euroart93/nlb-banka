<?php 
Site::getHeader(); ?>

<div id="main">
	
	<div class="account-row account-bg-3">
		<div class="bg-overlay overlay-2"></div>
		<div class="account-small-slider" data-method="accountSlider2">
	        <!-- Slides -->
            <div class="slide">
            	<div class="account-box">
					<div class="card-logo">
						<img src="<?php echo Site::url() ?>/img/photos/mastercard.png" alt="Card logo">
					</div>
            		<p class="acc-name">My Current Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-1" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<div class="card-logo">
						<img src="<?php echo Site::url() ?>/img/photos/mastercard.png" alt="Card logo">
					</div>
            		<p class="acc-name">My Second Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-2" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<div class="card-logo">
						<img src="<?php echo Site::url() ?>/img/photos/visa.png" alt="Card logo">
					</div>
            		<p class="acc-name">My Third Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-3" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<div class="card-logo">
						<img src="<?php echo Site::url() ?>/img/photos/visa.png" alt="Card logo">
					</div>
            		<p class="acc-name">My Fourth Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-4" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
	        </div>
	    </div>
	    <div class="slider-button-prev"></div>
		<div class="slider-button-next"></div>
	</div>

	<div class="main-content">
		<div class="widget">
			<ul class="widget-actions widget-corner">
				<li>
					<div class="widget-options" data-method="openSideModal" data-filter-node="#transactions-filter-2">
            			<ul class="trigger color-1">
            				<li></li>
            				<li></li>
            				<li></li>
            			</ul>
            		</div>
				</li>
			</ul>

			<div class="container top-padding">
				<h2><i class="icon-card-4"></i>Transaction list</h2>
				<ul class="widget-tabs style-2">
					<li class="active"><a href="#">Completed</a></li>
					<li><a href="#">Pending</a></li>
				</ul>

				<div class="custom-table border-top m-bottom">
					<div class="table-row row-1 table-head grid">
						<div class="col col-ls-4 col-ms-2 col-ss-6">
							<p class="col-text head-txt-1 left">Date</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text head-txt-1 left">Name of recipient</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text head-txt-1 center">Recipient account number</p>
						</div>
						<div class="col col-ls-4 col-ms-4 col-ss-6">
							<p class="col-text head-txt-1 right">Amount</p>
						</div>
					</div>

					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-6">
							<div class="arr-up-tag"></div>
							<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 left">Asseco Bank</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 center">123-0000000000321-65</p>
						</div>
						<div class="col col-ls-4 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-6">
							<div class="arr-down-tag"></div>
							<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 left">Asseco Bank</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 center">123-0000000000321-65</p>
						</div>
						<div class="col col-ls-4 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">500.000,<span>00 EUR</span></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-6">
							<div class="arr-down-tag"></div>
							<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 left">Asseco Bank</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 center">123-0000000000321-65</p>
						</div>
						<div class="col col-ls-4 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">500.000.000,<span>00 EUR</span></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-6">
							<div class="arr-up-tag"></div>
							<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 left">Asseco Bank</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 center">123-0000000000321-65</p>
						</div>
						<div class="col col-ls-4 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-6">
							<div class="arr-down-tag"></div>
							<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 left">Asseco Bank</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 center">123-0000000000321-65</p>
						</div>
						<div class="col col-ls-4 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">500.000,<span>00 EUR</span></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-6">
							<div class="arr-down-tag"></div>
							<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 left">Asseco Bank</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 center">123-0000000000321-65</p>
						</div>
						<div class="col col-ls-4 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">500.000.000,<span>00 EUR</span></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-6">
							<div class="arr-up-tag"></div>
							<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 left">Asseco Bank</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 center">123-0000000000321-65</p>
						</div>
						<div class="col col-ls-4 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-6">
							<div class="arr-down-tag"></div>
							<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 left">Asseco Bank</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 center">123-0000000000321-65</p>
						</div>
						<div class="col col-ls-4 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">500.000,<span>00 EUR</span></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-6">
							<div class="arr-down-tag"></div>
							<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 left">Asseco Bank</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 center">123-0000000000321-65</p>
						</div>
						<div class="col col-ls-4 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">500.000.000,<span>00 EUR</span></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-6">
							<div class="arr-up-tag"></div>
							<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 left">Asseco Bank</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 center">123-0000000000321-65</p>
						</div>
						<div class="col col-ls-4 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-6">
							<div class="arr-down-tag"></div>
							<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 left">Asseco Bank</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 center">123-0000000000321-65</p>
						</div>
						<div class="col col-ls-4 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">500.000,<span>00 EUR</span></p>
						</div>
					</div>
					<div class="table-row row-1 grid">
						<div class="col col-ls-4 col-ms-2 col-ss-6">
							<div class="arr-down-tag"></div>
							<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 left">Asseco Bank</p>
						</div>
						<div class="col col-ls-8 col-ms-3 col-ss-hidden">
							<p class="col-text text-6 text-cl-2 center">123-0000000000321-65</p>
						</div>
						<div class="col col-ls-4 col-ms-4 col-ss-6">
							<p class="col-text text-3 text-cl-3 right">500.000.000,<span>00 EUR</span></p>
						</div>
					</div>

					<div class="hidden-content">
						<div class="table-row row-1 grid">
							<div class="col col-ls-4 col-ms-2 col-ss-6">
								<div class="arr-up-tag"></div>
								<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
							</div>
							<div class="col col-ls-8 col-ms-3 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">Asseco Bank</p>
							</div>
							<div class="col col-ls-8 col-ms-3 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 center">123-0000000000321-65</p>
							</div>
							<div class="col col-ls-4 col-ms-4 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500,<span>00 EUR</span></p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-4 col-ms-2 col-ss-6">
								<div class="arr-down-tag"></div>
								<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
							</div>
							<div class="col col-ls-8 col-ms-3 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">Asseco Bank</p>
							</div>
							<div class="col col-ls-8 col-ms-3 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 center">123-0000000000321-65</p>
							</div>
							<div class="col col-ls-4 col-ms-4 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500.000,<span>00 EUR</span></p>
							</div>
						</div>
						<div class="table-row row-1 grid">
							<div class="col col-ls-4 col-ms-2 col-ss-6">
								<div class="arr-down-tag"></div>
								<p class="col-text text-6 text-cl-2 right">7 FEB 2016</p>
							</div>
							<div class="col col-ls-8 col-ms-3 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 left">Asseco Bank</p>
							</div>
							<div class="col col-ls-8 col-ms-3 col-ss-hidden">
								<p class="col-text text-6 text-cl-2 center">123-0000000000321-65</p>
							</div>
							<div class="col col-ls-4 col-ms-4 col-ss-6">
								<p class="col-text text-3 text-cl-3 right">500.000.000,<span>00 EUR</span></p>
							</div>
						</div>
					</div>
				</div>
				<button type="button" class="btn-1 color-2 big-btn mb-60" data-method="showMore">Show more</button>

				<h3 class="title-1 color-2 title-full link-list-align"><span>Options</span></h3>
				<div class="links-list">
					<a href="#" class="pl-10">Download list in PDF format</a>
					<a href="#" class="pl-10">Download list in XLS format</a>
				</div>
			</div>
		</div>
	</div>

</div>

<?php Site::getFooter(); ?>