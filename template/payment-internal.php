<?php 
Site::getHeader(); ?>

<div id="main">

	<div class="account-row account-bg-3">
		<div class="bg-overlay overlay-2"></div>
		<h3>From account</h3>
		<div class="account-small-slider upper" data-method="accountSlider2MultipleSliders">
	        <!-- Slides -->
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Current Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-1" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Second Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-2" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Third Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-3" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Fourth Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-4" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
	        </div>
	    </div>
	    <div class="slider-button-prev first-slider"></div>
		<div class="slider-button-next first-slider"></div>
	</div>
	<div class="account-row account-bg-3">
		<div class="bg-overlay overlay-2"></div>
		<h3>To account</h3>
		<div class="account-small-slider slider-down" data-method="accountSlider2MultipleSliders">
	        <!-- Slides -->
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Current Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-1" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Second Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-2" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Third Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-3" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
            </div>
            <div class="slide">
            	<div class="account-box">
            		<p class="acc-name">My Fourth Account</p>
	            	<p class="acc-nr">115-0000000000567898-65</p>
            		<div class="main-balance">
            			<div class="select-1">
            				<select id="select-currency-4" data-method="customSelect">
            					<option value="0">RSD</option>
            					<option value="1">EUR</option>
            					<option value="2">USD</option>
            					<option value="3">GBP</option>
            				</select>
            			</div>
            			<p class="balance">5.126.893,49</p>
            		</div>
            	</div>
	        </div>
	    </div>
	    <div class="slider-button-prev second-slider"></div>
		<div class="slider-button-next second-slider"></div>
	</div>

	<div class="main-content">
		<form action="#" method="get">

			<div class="widget content-white left-padding payment-widget">
				<h2><i class="icon-money-3"></i>Internal payment</h2>
				<ul class="steps left">
					<li class="active">1 <span>Payment order</span></li>
					<li>2 <span>Payment review</span></li>
				</ul>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-1 color-2 title-full"><span>Amount</span></h3>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width-1-1">
								<div class="group-inner input-select">
									<label class="label-1">Amount:</label>
									<input type="text" class="input-1" value="5.000,00" data-method="valueInput">
      									<div class="select-3">
      										<select data-method="customSelect3">
      			            					<option value="0">RSD</option>
      			            					<option value="1">EUR</option>
      			            					<option value="2">USD</option>
      			            					<option value="3">GBP</option>
      			            				</select>
      			            			</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="widget-slat">
					<div class="col-left">
						<h3 class="title-1 color-2 title-full"><span>Value date:</span></h3>
					</div>
					<div class="col-right">
						<div class="custom-form uk-grid">
							<div class="group uk-width" style="width: 220px">
								<label class="label-1">Date:</label>
								<div class="group-inner date-wrap">
									<input type="text" class="input-1 dark date-input" value="15/02/2016" data-method="dateInput">
								</div>
							</div>
							<div class="group uk-width" style="width: 200px">
								<div class="group-inner checkbox-wrap">
									<input type="checkbox" checked class="checkbox-1 checked" data-method="customCheckbox1">
									<label class="checkbox-label">Urgent</label>
								</div>
							</div>
						</div>
					</div>
				</div>

                        <div class="widget-slat">
                              <div class="col-left">
                                    <h3 class="title-1 color-2 title-full"><span>Details:</span></h3>
                              </div>
                              <div class="col-right">
                                    <div class="custom-form uk-grid">
                                          <div class="group uk-width-1-1">
                                                <label class="label-1">Description:</label>
                                                <div class="group-inner">
                                                      <input type="text" class="input-1">
                                                </div>
                                          </div>
                                          <div class="divider-30"></div>
                                          <div class="group uk-width-1-2">
                                                <div class="group-inner">
                                                      <button type="button" class="btn-1 color-3">Cancel</button>
                                                </div>
                                          </div>
                                          <div class="group uk-width-1-2">
                                                <div class="group-inner">
                                                      <a href="<?php echo Site::url('/payment-review') ?>" class="btn-1 color-1">Continue</a>
                                                </div>
                                          </div>
                                    </div>
                              </div>
                        </div>	
		
			</div>


		</form>
	</div>

</div>

<?php Site::getFooter(); ?>