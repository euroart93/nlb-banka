var gulp = require('gulp'),
	sass = require('gulp-sass'),
	uglify = require('gulp-uglify'),
	cleanCSS = require('gulp-clean-css');
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	svgstore = require('gulp-svgstore'),
	cheerio = require('gulp-cheerio');


// Sass
gulp.task('sass', function() {
  	return gulp.src('sass/**/*.scss')
    	.pipe(sass().on('error', sass.logError))
    	.pipe(gulp.dest('css'))
});

// Merge CSS
gulp.task('cssmin', function () {
 	return gulp.src('css/style.css, css/**/*.css')
    .pipe(concat('style.min.css'))
  	.pipe(cleanCSS({compatibility: 'ie9'}))
    .pipe(gulp.dest('css'));
});

// Javascript
gulp.task('js', function() {
  	gulp.src(['js-dev/ea-init.js', 'js-dev/plugins/**/*.js', 'js-dev/global.js', 'js-dev/methods/**/*.js'])
	.pipe(concat('script.js'))
	.pipe(gulp.dest('js'))
});

// Javascript minimize
gulp.task('uglify', function() {
	return gulp.src('js/script.js')
	.pipe(uglify().on('error', gulpUtil.log))
	.pipe(rename({ suffix: '.min' }))
	.pipe(gulp.dest('js'))
});

// SVG Sprites
gulp.task('svgstore', function () {
    return gulp.src('img/svg/*.svg', {base: 'sprite'})
        .pipe(cheerio({
            run: function($) {
                $('[fill]').removeAttr('fill');
            },
            parserOptions: { xmlMode: true }
        }))
        .pipe(svgstore({ inlineSvg: true }))
        .pipe(gulp.dest('svg/'));
});


// Gulp watch
gulp.task('watch', function(){
	gulp.watch('sass/**/*.scss', ['sass']); 
	// gulp.watch('sass/**/*.scss', ['cssmin']);
	gulp.watch('js-dev/**/*.js', ['js']);
})