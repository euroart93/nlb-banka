EA.Methods.toggleOptions = function($this) {
    $this.click(function(e){
        e.preventDefault();
        $this.toggleClass('active');
        $this.closest('.widget-slat').find('.expanable').slideToggle(300);
    });
}
