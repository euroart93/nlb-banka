EA.Methods.productSlider = function($this){

    var slider = $this;

	slider.slick({
		// centerMode: true,
		slidesToShow: 1,
		arrows: false,
		dots: true,
		infinite: false,
		centerPadding: '0px',
		fade: true,
  		cssEase: 'linear'
	});  

	$('.slider-button-prev').click(function(){
		slider.slick('slickPrev');
	});
	$('.slider-button-next').click(function(){
		slider.slick('slickNext');
	});

};

