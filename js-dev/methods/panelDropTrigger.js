EA.Methods.panelDropTrigger = function($this){

    $this.click(function(e){
        var button = $(this);
        var btnText1 = "Prikaži manje detalja";
        var btnText2 = "Prikaži više detalja";
    	var dropdown = button.parent().find(".dropdown");

        button.toggleClass("active");
        dropdown.slideToggle(300);

        if (button.hasClass("active")) {
            button.text(btnText1);
        }else {
            button.text(btnText2);
        }
    })

};
