EA.Methods.videoSize = function($this){

	var video = $this;

	var videoResize = function(){
		var videoWidth = $this.data('width');
		var videoHeight = $this.data('height');
		var videoRatio = videoWidth / videoHeight;
		var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		var windowRatio = windowWidth / windowHeight;
		// console.log(videoRatio);
		// console.log(windowRatio);

		if(videoRatio > windowRatio){
			var videoWidth2 = windowHeight * videoRatio;
			video.css({'width': videoWidth2});
			video.css({'height': windowHeight});
		}
		 else {
			var videoHeight2 = videoHeight * videoRatio;
			video.css({'width': windowWidth});
			video.css({'height': 'auto'});
		}
	}

	if($(window).width() > 720){
		videoResize();
	}

	$(window).resize(function(){
		if($(window).width() > 720){
			videoResize();
		} else {
			video.css({'width': '', 'height': ''});
		}
	});
};
