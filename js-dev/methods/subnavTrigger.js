EA.Methods.subnavTrigger = function($this){

	$this.click(function(e){
		e.preventDefault();
		$(this).parent().toggleClass('open').siblings('open').removeClass('open');
	});

	$('html, body').click(function(){
		$('.main-nav .open').removeClass('open');
	});

	$('.main-nav').click(function(e){
		e.stopPropagation();
	});

};
