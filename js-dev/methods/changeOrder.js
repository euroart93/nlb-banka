EA.Methods.changeOrder = function($this){
	
	var table = $this.closest('.custom-table');

	$this.click(function(){
		if($(this).hasClass('down')){
			$(this).removeClass('down').addClass('up');
		} else if($(this).hasClass('up')){
			$(this).removeClass('up').addClass('down');
		} else {
			table.find('.order').removeClass('down up');
			$(this).addClass('down');
		}
	});

};