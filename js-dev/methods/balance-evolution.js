EA.Methods.balanceEvolution = function($this){

   $(function () {
    Highcharts.chart('balance-evolution-chart', {
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        credits: {
                enabled: false
            },
        legend: {
            enabled: false
        },
        xAxis: {
           type: 'datetime'
        },
        yAxis: {
            title: {
              text: ''
            },
            labels: {
              formatter: function () {
                return Highcharts.numberFormat(this.value,0,',','.');
              },
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' RSD',
            style: {
                fontFamily: 'geomanist',
                textAlign: 'center',
                fontSize: '16px',
                color: '#239bc8',
                fontWeight: '500',
                lineHeight: '20px',
            },
            formatter: function() {
               return '<span style="color: #000; font-size: 12px;">' + Highcharts.dateFormat('%e.%m.%Y', new Date(this.x)) + '</span><br>' + Highcharts.numberFormat(this.y,0,',','.') + ' RSD';
            }
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.15,
                pointInterval: 36000000, // one hour
                  pointStart: Date.UTC(2016, 7, 12, 0, 0, 0),
                    marker: {
                        enabled: false,
                    }
            }
        },
        series: [{  
            name: 'Balance',
            data: [1000, 4000, 4000, 4000, 5000, 6500, 7500, 14500, 9500, 13000, 8000, 15000, 20000, 18000, 22500, 21000, 22500, 22500, 20000, 15500, 13500, 20000, 13500, 11500, 11500, 20500, 38500, 35500, 28000, 30500, 29000, 43000, 36000, 45000, 54500, 57500, 58000, 55500, 60000, 61500, 53500, 47000, 49000, 48000, 49000, 47500, 42500, 37000, 38000],
            color: '#229bc7',
              animation:{
                  duration: 2000  
                },
        }]
    });
});

};
