EA.Methods.accountSlider2MultipleSliders = function($this){

	var slider = $this,
    	wrap = slider.parent();
    	slides = slider.find('.slide');

    if(slides.length < 2){
    	wrap.addClass('single-slide');
    } else {
	    if(slides.length == 2 || slides.length == 3){
	    	console.log(slides.length);
	    	slides.clone().appendTo(slider);
	    }

		slider.slick({
			centerMode: true,
			slidesToShow: 3,
			arrows: false,
			initialSlide: 1,
			infinite: true,
			centerPadding: '0px',
			responsive: [
		    	{
			      	breakpoint: 980,
			      	settings: {
				        slidesToShow: 1,
				        dots: true
				    }
		    	},
		    ]

		}); 

		slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
		    var items = slider.find('.slide');
		  	EA.LoadMethod(items);
		}); 

		$('.slider-button-prev.first-slider').click(function(){
			$('.account-small-slider.upper').slick('slickPrev');
		});
		$('.slider-button-next.first-slider').click(function(){
			$('.account-small-slider.upper').slick('slickNext');
		});

		$('.slider-button-prev.second-slider').click(function(){
			$('.account-small-slider.slider-down').slick('slickPrev');
		});
		$('.slider-button-next.second-slider').click(function(){
			$('.account-small-slider.slider-down').slick('slickNext');
		});
	}
	
	var items = slider.find('.slide');
	EA.LoadMethod(items);

};
