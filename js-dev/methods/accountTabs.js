EA.Methods.accountTabs = function($this){

	var listItem = $('.list-item .inner');

	$this.click(function(){
		var tabId = $(this).data('tab');
		// console.log(tabId);
		if(!$(this).hasClass('active')){
			$(this).addClass('active').siblings().removeClass('active');
			$('.account-tab.active').removeClass('active');
			setTimeout(function(){
				$('.account-tab.tab-'+tabId).addClass('active');
			}, 500);
		}
	}); 

};
