EA.Methods.openSideModal = function($this) {
    $this.click(function(e){
        e.preventDefault();

        var nodeSelector = $(this).attr('data-filter-node');
        $(nodeSelector).addClass('active');

        $(nodeSelector).parents('.side-modal').addClass('active').fadeIn(300);
        // $('.side-modal').addClass('active').fadeIn(300);

        if($(window).width() < 600){
            $('html, body').animate({
                scrollTop: 0,
            }, 500);
        }

    });

}
