EA.Methods.flagSlider = function($this){

    var slider = $this;
    var sl = slider.find('.slide');
    var selected = $('#selected-flag');

	slider.slick({
		slidesToShow: 8,
		dots: false,
		infinite: true,
		centerPadding: '0px',
		centerMode: false,
		responsive: [
		    {
		        breakpoint: 600,
		        settings: {
		            slidesToShow: 6,
		        },
		    },
		    {
		    	breakpoint: 400,
		        settings: {
		            slidesToShow: 4,
		        },
		    },
		]
	});  

	slider.click(function(event){
		if(event.target.nodeName == 'IMG'){
			var cloned = event.target.clone();
			selected.empty().append(cloned);
		}
	});

};

