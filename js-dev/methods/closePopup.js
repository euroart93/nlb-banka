EA.Methods.closePopup = function($this){

	function close(){
		$('body').removeClass('popup-open');
		$('.modal-container').fadeOut(500);
		$('.popup-box.active').fadeOut(500).removeClass('active');
	}

	$this.click(function(e){
		e.preventDefault(); 
		close();	
	});

	$('.modal-inner').click(function(){
		close();
	})

	$('.popup-box').click(function(e){
		e.stopPropagation();
	})

	$(document).keyup(function(e) {
        if (e.keyCode == 27) {
        	if($('body').hasClass('popup-open')){
        		close();
        	}
    	}
    });
};