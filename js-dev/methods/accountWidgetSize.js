EA.Methods.accountWidgetSize = function($this){

	var accountWidget = $('.accounts-widget');
	var accountSlider = $('#account-slider');
	var accountList = $('.account-gridster');
	var tabBtn = $('.list-options li');

	var sliderHeight;
	var listHeight;

	function heightDetect(){
		sliderHeight = accountSlider.height() + 100;
		listHeight = accountList.height() + 200;
	};

	function heightSet(){
		if($('.tab-1').hasClass('active')){
			accountWidget.css({'height': sliderHeight});
		} else if($('.tab-2').hasClass('active')){
			accountWidget.css({'height': listHeight});
		}
	};



	tabBtn.click(function(){
		if($('.tab-1').hasClass('active')){
			accountWidget.css({'height': listHeight});
		} else if($('.tab-2').hasClass('active')){
			accountWidget.css({'height': sliderHeight});
		}
	});


	// Show and hide more widgets
	var accountList = $('.account-list');
	var listItem = accountList.find('.list-item');
	var listLength = listItem.length;

	for(i=5; i < listLength; i++){
		listItem.eq(i).hide();
	};

	$('.more-list').click(function(){
		listItem = accountList.find('.list-item');
		if(!$(this).hasClass('active')){
			for(i=5; i < listLength; i++){
				listItem.eq(i).fadeIn();
			};
	    	heightDetect();
			heightSet();
			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
			for(i=5; i < listLength; i++){
				listItem.eq(i).slideUp(300);
			};
			setTimeout(function(){
				heightDetect();
				heightSet();
			}, 400);
		}

    });


	// Height trigger
	heightDetect();
	// console.log(sliderHeight);
	// console.log(listHeight);

	heightSet();

	$(window).resize(function(){
		heightDetect();
		heightSet();
	});
	
};
