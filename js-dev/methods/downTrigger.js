EA.Methods.dropdownTrigger = function($this){

	$this.click(function(e){
		e.preventDefault();
		
		if(!$(this).parent().hasClass('active')){
			$(this).parent().addClass('active');
		} else {
			$(this).parent().removeClass('active');
		}
	})

	$('html, body').click(function(){
		$('.grid-option.active').removeClass('active');

	});

	$('.option-dropdown').click(function(event){
		event.stopPropagation();
	});
};