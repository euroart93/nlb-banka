EA.Methods.uploadFile = function($this){

	var btn = $this.siblings('.upload-btn');

	$this.change(function(){
    	$(this).next().find('span.file-name').text(this.value.replace(/^.*[\\\/]/, ''));
    });
    btn.click(function(e){
        e.preventDefault();
        $(this).siblings('.upload-file').click();
    });

};
