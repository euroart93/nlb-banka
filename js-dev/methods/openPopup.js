EA.Methods.openPopup = function($this){

	$this.click(function(e){
		e.preventDefault();

		var popupId = $(this).data('popup');

		if(!$('body').hasClass('popup-open')){
			$('body').addClass('popup-open');
			$('.modal-container').fadeIn(500);
			$('.popup-'+popupId).fadeIn(500).addClass('active');
		} else {
			$('.popup-box.active').fadeOut(500).removeClass('active');
			$('.popup-'+popupId).fadeIn(500).addClass('active');
		}

		if($(window).height() < 600){
			$('html, body').animate({
				scrollTop: 0,
			}, 500);
		}

		// Flag slider reinit on popup open
		if($('.popup-4').hasClass('active')){
			$('.flag-slider').slick('reinit');
		}

		// Go up after opening popup

		 $('html, body').animate({ scrollTop: 0 }, 'slow');

	});

};