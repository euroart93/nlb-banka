EA.Methods.mapList = function($this){

	$this.click(function(){
		if(!$(this).parent().hasClass('active')){
			$(this).parent().siblings('.active').removeClass('active').find('.hidden-content').slideUp(300);
			$(this).parent().addClass('active').find('.hidden-content').slideDown(300);
		} else {
			$(this).parent().removeClass('active').find('.hidden-content').slideUp(300);
		}
	});

};
