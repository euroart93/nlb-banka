EA.Methods.accountList = function($this){

    $( ".tab-2.gridster ul" ).sortable({
    	cursor: "move",
    	forcePlaceholderSize: true,

    	// animation speed
		sortAnimateDuration: 200,
	
		// enable sort animation
		sortAnimate: true
    });
    $( ".tab-2.gridster ul" ).disableSelection();

    // Disable on mobile
    function sortableToggle() {
    	if($(window).width() > 980){
    		$( ".tab-2.gridster ul" ).sortable( "option", "handle", false );
    	} else {
    		$( ".tab-2.gridster ul" ).sortable( "option", "handle", ".drag" );
    	}
    }

    sortableToggle();

    $(window).resize(function(){
    	sortableToggle();
    })

    // Item selection
    $('.list-item').click(function(){
    	$(this).addClass('selected').siblings().removeClass('selected');
    })

};
