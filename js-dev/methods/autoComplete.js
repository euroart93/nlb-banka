EA.Methods.autoComplete = function($this){

    var contacts = [
      {
        value: "Jurica Vuković",
        label: "Jurica Vuković",
        icon: "profile-1.jpg"
      },
      {
        value: "Darko Prezime",
        label: "Darko Prezime",
        icon: "promo-1.jpg"
      },
      {
        value: "Ivan Ivanović",
        label: "Ivan Ivanović",
        icon: "profile-1.jpg"
      }
    ];

    $('.autocomplete').autocomplete({
      minLength: 0,
      source: contacts
    });

    $('.autocomplete').data( "ui-autocomplete" )._renderItem = function( ul, item ) {
  
      var $li = $('<li>'),
          $img = $('<img>');


      $img.attr({
        src: 'img/demo/' + item.icon ,
        alt: item.label
      });

      $li.attr('data-value', item.label);
      $li.append('<div class="autoCompleteImgWrapper">');
      $li.append('<div class="autoCompleteImgLabel">');
      $li.find('.autoCompleteImgWrapper').append($img); 
      $li.find('.autoCompleteImgLabel').append(item.label);    

      return $li.appendTo(ul);
    };

};
