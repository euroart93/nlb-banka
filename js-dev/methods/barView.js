EA.Methods.barView = function($this){

$(function () {
    Highcharts.chart('bar-view', {
        chart: {
            type: 'column'
        },
        colors: ['#eb0255' , '#239bc8'],
        title: {
            text: ''
        },
        credits: {
                enabled: false
            },
        legend: {
            enabled: false
        },
        xAxis: {
           type: 'datetime',
           tickInterval: 24 * 3600 * 1000,
           dateTimeLabelFormats: { // don't display the dummy year
                month: '%b',
                day: '%e',
            },
        },
        yAxis: {
            min: 10000,
            max: 55000,
            tickInterval: 5000,
            title: {
                text: ''
            },
            labels: {
              formatter: function () {
                return Highcharts.numberFormat(this.value,0,',','.');
              },
            },
            stackLabels: {
                enabled: false,
            }
        },
        tooltip: {
	            formatter: function() {
               return '<span style="color: #000; font-family: geomanist; text-transform: uppercase; font-size: 12px;">' + this.series.name + '</span><br>' + '<span style="color: #000; font-family: geomanist; font-size: 12px;">' + Highcharts.dateFormat('%e.%m.%Y', new Date(this.x)) + '</span><br>' + '<span style="color: #000; font-family: geomanist; font-size: 16px;">' + Highcharts.numberFormat(this.y,0,',','.') + ' RSD' + '</span>';
            }
	        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                }
            }
        },
        series: [{
	            name: 'Incoming Funds',
              pointStart: Date.UTC(2017, 1, 1),
            	pointInterval: 24 * 3600 * 1000,
	            data: [25000, 30000, 0, 0, 12000, 25000, 30000, 0, 0, 13000, 27000, 0, 0, 0, 13000, 0, 0, 13000, 27000, 50000, 0, 0, 13000, 28000, 0, 0, 0, 13000, 27000, 50000, 25000]
	        }, {
	            name: 'Outgoing Funds',
              pointStart: Date.UTC(2017, 1, 1),
            	pointInterval: 24 * 3600 * 1000,
	            data: [15000, 17000, 22000, 27000, 17000, 30000, 36000, 47000, 0, 0, 27000, 34000, 0, 23000, 26000, 27000, 32000, 0, 0, 0, 27000, 36000, 0, 0, 0, 32000, 38000, 0, 35000]
	        }]
    });
});

};
