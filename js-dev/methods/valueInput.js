EA.Methods.valueInput = function($this){

	var input1 = $this;
	var val1;

	input1.keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });


    input1.focus(function (event) {
    	input1.val(input1.val().replace(/\./g, ''));
    	input1.val(input1.val().replace(/\,00/g, ''));
    });

	function format(comma, period) {
	  comma = comma || ',';
	  period = period || '.';
	  var split = this.toString().split('.');
	  var numeric = split[0];
	  var decimal = split.length > 1 ? period + split[1] : '';
	  var reg = /(\d+)(\d{3})/;
	  while (reg.test(numeric)) {
	    numeric = numeric.replace(reg, '$1' + comma + '$2');
	  }
	  return numeric + decimal;
	}

	input1.blur(function(){
		if($this.val() != ''){
	    	$(this).val(format.call($(this).val().split(' ').join(''),'.','.')+',00');
	    }
	});



};