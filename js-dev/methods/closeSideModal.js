EA.Methods.closeSideModal = function($this){

	function close(){
		$('.side-modal').fadeOut(500);
		$('.side-modal-popup.active').removeClass('active');
	}

	$this.click(function(e){
		e.preventDefault(); 
		close();	
	});

	$('.side-modal-inner').click(function(){
		close();
	})

	$('.side-modal-popup').click(function(e){
		e.stopPropagation();
	})

	$('.side-modal').click(function(e){
    	close();
    }); 

};