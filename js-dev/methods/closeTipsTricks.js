EA.Methods.closeTipsTricks = function($this){

	function close(){
		$('body').removeClass('popup-open');
		$('.modal-container').fadeOut(500);
		$('.popup-box.active').fadeOut(500).removeClass('active');
	}

	$this.click(function(e){
		e.preventDefault(); 
		$('.tips-and-tricks-modal-wrapper').fadeOut();
	});

};