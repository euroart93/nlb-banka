EA.Methods.googleMap = function($this){

		var location1 = new google.maps.LatLng(40.7473268, -73.9874371);
		var location2 = new google.maps.LatLng(40.7463268, -73.9864371);
		var location3 = new google.maps.LatLng(40.7453268, -73.9894371);
		var location4 = new google.maps.LatLng(40.7443268, -73.9844371);
		var location5 = new google.maps.LatLng(40.7463268, -73.9854371);
		var map;

		function initialize() {
	    	var map_canvas = document.getElementById('map-canvas');

			var styles = [
				{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#d3d3d3"}]},{"featureType":"transit","stylers":[{"color":"#808080"},{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#b3b3b3"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":1.8}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#d7d7d7"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ebebeb"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#a7a7a7"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#efefef"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#696969"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#737373"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#d6d6d6"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#dadada"}]}
			];

			var infoboxes = [];

	    	var map_options = {
	    		center: new google.maps.LatLng(40.7463268, -73.9874371),
	    		zoom: 16,
	    		mapTypeId: google.maps.MapTypeId.ROADMAP,
	    		scrollwheel: false,
				draggable: !("ontouchend" in document),
				styles: styles,
				panControl: false,
				zoomControl: false,
				mapTypeControl: false,
				streetViewControl: false,
				zoomControlOptions: {
					style: google.maps.ZoomControlStyle.SMALL,
					position: google.maps.ControlPosition.RIGHT_BOTTOM
				},
				mapTypeControlOptions: {
				    position:google.maps.ControlPosition.BOTTOM_LEFT
				}
	    	}

	    	map = new google.maps.Map(map_canvas, map_options)

	        var locImg1 = new google.maps.MarkerImage(
	        	'img/icons/pin-5.png',
	        	null,
	        	null,
	        	new google.maps.Point(16, 42),
	        	new google.maps.Size(32, 42)
		    );

		    var locImg2 = new google.maps.MarkerImage(
	        	'img/icons/pin-4.png',
	        	null,
	        	null,
	        	new google.maps.Point(16, 42),
	        	new google.maps.Size(32, 42)
		    );

			/* location1 */
	        var markerLoc1 = new google.maps.Marker({
			    position: location1,
			    map: map,
			    icon: locImg1,
			});

	        //infobox
			infoboxes['infobox0'] = new InfoBox({
			        content: document.getElementById("infobox1"),
			        disableAutoPan: false,
			        maxWidth: 200,
			        pixelOffset: new google.maps.Size(20, -100),
			        zIndex: null,
			        boxStyle: {
			            background: "",
			            opacity: 1,
			        },
			        closeBoxMargin: "10px 10px 0 0",
			        closeBoxURL: "img/icons/close.png",
			        infoBoxClearance: new google.maps.Size(1, 1)
			});

		    var n = $('.infobox').length;


		    google.maps.event.addListener(markerLoc1, 'click', function() {
		    	for(i = 0; i < n; i++){
		    		infoboxes['infobox' + i].close();
		    	};
		        infoboxes['infobox0'].open(map, this);
		    });

			/* location2 */
	        var markerLoc2 = new google.maps.Marker({
			    position: location2,
			    map: map,
			    icon: locImg1,
			});

			//infobox
			infoboxes['infobox1'] = new InfoBox({
			        content: document.getElementById("infobox2"),
			        disableAutoPan: false,
			        maxWidth: 200,
			        pixelOffset: new google.maps.Size(20, -100),
			        zIndex: null,
			        boxStyle: {
			            background: "",
			            opacity: 1,
			        },
			        closeBoxMargin: "10px 10px 0 0",
			        closeBoxURL: "img/icons/close.png",
			        infoBoxClearance: new google.maps.Size(1, 1)
			});

		    google.maps.event.addListener(markerLoc2, 'click', function() {
		    	for(i = 0; i < n; i++){
		    		infoboxes['infobox' + i].close();
		    	};
		       infoboxes['infobox1'].open(map, this);
		    });


			/* location3 */
	        var markerLoc3 = new google.maps.Marker({
			    position: location3,
			    map: map,
			    icon: locImg2,
			});

			//infobox
			infoboxes['infobox2'] = new InfoBox({
			        content: document.getElementById("infobox3"),
			        disableAutoPan: false,
			        maxWidth: 200,
			        pixelOffset: new google.maps.Size(20, -100),
			        zIndex: null,
			        boxStyle: {
			            background: "",
			            opacity: 1,
			        },
			        closeBoxMargin: "10px 10px 0 0",
			        closeBoxURL: "img/icons/close.png",
			        infoBoxClearance: new google.maps.Size(1, 1)
			});

		    google.maps.event.addListener(markerLoc3, 'click', function() {
		    	for(i = 0; i < n; i++){
		    		infoboxes['infobox' + i].close();
		    	};
		        infoboxes['infobox2'].open(map, this);
		    });


			/* location4 */
	        var markerLoc4 = new google.maps.Marker({
			    position: location4,
			    map: map,
			    icon: locImg1,
			});

			//infobox
			infoboxes['infobox3'] = new InfoBox({
			        content: document.getElementById("infobox4"),
			        disableAutoPan: false,
			        maxWidth: 200,
			        pixelOffset: new google.maps.Size(20, -100),
			        zIndex: null,
			        boxStyle: {
			            background: "",
			            opacity: 1,
			        },
			        closeBoxMargin: "10px 10px 0 0",
			        closeBoxURL: "img/icons/close.png",
			        infoBoxClearance: new google.maps.Size(1, 1)
			});

			/* location5 */
	        var markerLoc5 = new google.maps.Marker({
			    position: location5,
			    map: map,
			    icon: locImg1,
			});

		    google.maps.event.addListener(markerLoc4, 'click', function() {
		    	for(i = 0; i < n; i++){
		    		infoboxes['infobox' + i].close();
		    	};
		        infoboxes['infobox3'].open(map, this);
		    });

	  	}

	  	google.maps.event.addDomListener(window, 'load', initialize);


};
