EA.Methods.geolocationMap = function($this){

		var location1 = new google.maps.LatLng(40.7473268, -73.9874371);
		var location2 = new google.maps.LatLng(40.7463268, -73.9864371);
		var map;
			
		function initialize() {
	    	var map_canvas = document.getElementById('geolocation-map');

			var styles = [
				{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#d3d3d3"}]},{"featureType":"transit","stylers":[{"color":"#808080"},{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#b3b3b3"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":1.8}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#d7d7d7"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ebebeb"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#a7a7a7"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#efefef"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#696969"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#737373"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#d6d6d6"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#dadada"}]}
			];

			var infoboxes = [];

	    	var map_options = {
	    		center: new google.maps.LatLng(40.748655, -73.989980),
	    		zoom: 16,
	    		mapTypeId: google.maps.MapTypeId.ROADMAP,
	    		scrollwheel: false,
				draggable: !("ontouchend" in document),
				styles: styles,
				panControl: false,
				zoomControl: false,
				mapTypeControl: false,
				streetViewControl: false,
				zoomControlOptions: {
					style: google.maps.ZoomControlStyle.SMALL,
					position: google.maps.ControlPosition.RIGHT_BOTTOM
				},
				mapTypeControlOptions: {
				    position:google.maps.ControlPosition.BOTTOM_LEFT
				}
	    	}

	    	map = new google.maps.Map(map_canvas, map_options)

	        var locImg1 = new google.maps.MarkerImage(
	        	'img/icons/pin-3.png',
	        	null,
	        	null,
	        	new google.maps.Point(16, 42),
	        	new google.maps.Size(32, 42)
		    );

			/* location1 */
	        var markerLoc1 = new google.maps.Marker({
			    position: location1,
			    map: map,
			    icon: locImg1,
			});

			/* location2 */
	        var markerLoc2 = new google.maps.Marker({
			    position: location2,
			    map: map,
			    icon: locImg1,
			});

	  	}

	  	google.maps.event.addDomListener(window, 'load', initialize);

	  	$('.map-init').click(function(){
			google.maps.event.trigger(map, 'resize');
		});

};


