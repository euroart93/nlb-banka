EA.Methods.accountSelectTrigger = function($this){

    var trigger = $this,
        options = $('.account-select');

    $('html, body').click(function(){
        trigger.removeClass('active');
        options.slideUp(300);
    })

    trigger.click(function(e){
        e.preventDefault();
        e.stopPropagation();

        if (!trigger.hasClass('animating')) {
            if (!trigger.hasClass('active')) {
                trigger.addClass('active');
                options.slideDown(300);
            } else {
                trigger.removeClass('active');
                options.slideUp(300);
            }
        }

        trigger.addClass('animating');
        setTimeout(function(){
            trigger.removeClass('animating');
        }, 320);
    });

};
