EA.Methods.toPopup = function($this){
	$this.click(function(e){
		e.preventDefault();

		var popupId = $(this).data('popup');

		$('.popup-box.active').fadeOut(500).removeClass('active');
		setTimeout(function(){
			$('.popup-'+popupId).fadeIn(500).addClass('active');
		}, 300);

	});
};