EA.Methods.accountSelect = function($this){

    var item = $this.find('.item'),
        mainItem = $this.find('.account-selected'),
        mainImg = mainItem.find('.acc-img'),
        mainName = mainItem.find('.acc-name'),
        mainNumber = mainItem.find('.acc-number'),
        mainAmount = mainItem.find('.acc-amount'),
        selectedName,
        selectedNumber,
        selectedAmount;

    console.log(item.length);

    item.click(function(){
        mainImg.empty();
        $(this).find('img').clone().appendTo(mainImg);
        selectedName = $(this).find('.acc-name').html();
        selectedNumber = $(this).find('.acc-number').html();
        selectedAmount = $(this).find('.acc-amount').html();
        mainName.text(selectedName);
        mainNumber.text(selectedNumber);
        mainAmount.text(selectedAmount);
    })

};
