EA.Methods.openInbox = function($this){
	$this.click(function(e){
		e.preventDefault();

		var inboxId = $(this).data('inbox');

		if(!$('body').hasClass('inbox-open')){
			$('.side-modal').fadeIn(500);
			$('body').addClass('inbox-open');
			$('.inbox-'+inboxId).addClass('active');
		} else {
			$('.side-modal-popup.active').removeClass('active');
			$('.inbox-'+inboxId).addClass('active');
		}

		if($(window).width() < 600){
			$('html, body').animate({
				scrollTop: 0,
			}, 500);
		}

	});
};