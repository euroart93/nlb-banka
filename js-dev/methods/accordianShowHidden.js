EA.Methods.accordianShowHidden = function($this){

	$this.click(function(){
		if ($(this).next('.hidden').hasClass('active')) {
			$('.accordian-wrapper .hidden').removeClass('active').slideUp();
		} else {
			$('.accordian-wrapper .hidden').removeClass('active').slideUp();
			$(this).next('.hidden').addClass('active').slideDown();
		}
	});
	
	$this.click(function(){
		if ($(this).hasClass('active')) {
			$('.accordian-title').removeClass('active');
		} else {
			$('.accordian-title').removeClass('active');
			$(this).addClass('active');
		}
	});

}
