EA.Methods.sortableWidgets = function($this){

	var sort = $this;

    sort.sortable({
    	cursor: "move",
    	forcePlaceholderSize: true,
    	handle: '.drag-handle',

    	// animation speed
		sortAnimateDuration: 200,
	
		// enable sort animation
		sortAnimate: true
    });


};
