EA.Methods.googleMapSideModal = function($this){

		var location1 = new google.maps.LatLng(44.8037939, 20.4187798);

		var map;
			
		function initialize() {
	    	var map_canvas = document.getElementById('map-canvas-side-modal');

			var styles = [
				{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","elementType":"all","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]}
			];

			var infoboxes = [];

	    	var map_options = {
	    		center: new google.maps.LatLng(44.806214, 20.415798),
	    		zoom: 16,
	    		mapTypeId: google.maps.MapTypeId.ROADMAP,
	    		scrollwheel: false,
				draggable: !("ontouchend" in document),
				styles: styles,
				panControl: false,
				zoomControl: true,
				mapTypeControl: false,
				streetViewControl: false,
				zoomControlOptions: {
					style: google.maps.ZoomControlStyle.SMALL,
					position: google.maps.ControlPosition.RIGHT_BOTTOM
				},
				mapTypeControlOptions: {
				    position:google.maps.ControlPosition.BOTTOM_LEFT
				}
	    	}

	    	map = new google.maps.Map(map_canvas, map_options)

	        var locImg1 = new google.maps.MarkerImage(
	        	'img/icons/pin-3.png',
	        	null,
	        	null,
	        	new google.maps.Point(16, 42),
	        	new google.maps.Size(32, 42)
		    );

			/* location1 */
	        var markerLoc1 = new google.maps.Marker({
			    position: location1,
			    map: map,
			    icon: locImg1,
			});

	        //infobox
			infoboxes['infobox0'] = new InfoBox({
			        content: document.getElementById("infobox1"),
			        disableAutoPan: false,
			        maxWidth: 200,
			        pixelOffset: new google.maps.Size(20, -100),
			        zIndex: null,
			        boxStyle: {
			            background: "",
			            opacity: 1,
			        },
			        closeBoxMargin: "10px 10px 0 0",
			        closeBoxURL: "img/icons/close.png",
			        infoBoxClearance: new google.maps.Size(1, 1)
			});

		    var n = $('.infobox').length;


		    google.maps.event.addListener(markerLoc1, 'click', function() {
		    	for(i = 0; i < n; i++){
		    		infoboxes['infobox' + i].close();
		    	};
		        infoboxes['infobox0'].open(map, this);
		    });

	  	}

	  	google.maps.event.addDomListener(window, 'load', initialize);

	  	$('.custom-table .table-row').click(function(){
  			google.maps.event.trigger(map, 'resize');
  		});

};


