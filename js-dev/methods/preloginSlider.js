EA.Methods.preloginSlider = function($this){

    var slider = $this;

    slider.slick({
      slidesToShow: 1,
      arrows: false,
      dots: true,
      infinite: false,
      fade: true,
    	cssEase: 'linear',
      autoplay: true,
      autoplaySpeed: 7500
    });

    slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
  		// // Images change
  		$('.page-cover .desktop-img.slide-active').removeClass('slide-active');
  		$('.page-cover .promo-bg-'+nextSlide).addClass('slide-active');

  	});


};
