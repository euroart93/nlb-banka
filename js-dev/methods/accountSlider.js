EA.Methods.accountSlider = function($this){

    var slider = $this,
    	wrap = slider.parent();
    	slides = slider.find('.slide');

    if(slides.length < 2){
    	wrap.addClass('single-slide');
    } else {
    	if(slides.length == 2 || slides.length == 3){
	    	console.log(slides.length);
	    	slides.clone().appendTo(slider);
	    	var items = slider.find('.slide');
	    	EA.LoadMethod(items);
	    }
		slider.slick({
			centerMode: true,
			slidesToShow: 1,
			initialSlide: 1,
			speed: 500,
			arrows: false,
			variableWidth: true,
			dots: true,
			infinite: false,
			centerPadding: '0px',
			responsive: [
			    {
			        breakpoint: 480,
			        settings: {
			            centerPadding: '50px',
			        }
			    }
			]
		});

		$('.slider-button-prev').click(function(){
			slider.slick('slickPrev');
		});
		$('.slider-button-next').click(function(){
			slider.slick('slickNext');
		});
	}

};
