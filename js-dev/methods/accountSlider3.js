EA.Methods.accountSlider3 = function($this){

	var slider = $this;

	var mySwiper = new Swiper (slider, {
		// loop: true,
		speed: 500,
		pagination: '.swiper-pagination-2',
    	paginationClickable: true,
        prevButton: '.swiper-button-prev-2',
        nextButton: '.swiper-button-next-2',
        // effect: 'coverflow',
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 1,
        onSlideChangeEnd : function(swiper) {
	       slideId2 = swiper.activeIndex;
	       sameAccountDetect();
	    }
    }) ;

	slideId2 = 0;

};
